#! /usr/bin/env python3

# Copyright 2019 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function, division

# NOTE: This GUI assumes that `convert` is in the path

import collections
import numpy as np
import os
import rospy
import signal
import six
import subprocess  # for check_call, for converting image
import sys

import PyQt5.QtCore as QtCore  # Used for Timer
import PyQt5.QtGui as QtGui  # Used for QFont and QValidator
import PyQt5.QtWidgets as QtWidgets

import ds_acomms_msgs.msg # will be needed for whatever "acks expected"/"start of TDMA transmit slot" message we wind up with
import progressive_imagery.msg # Where ProgressiveImagery status messages live, at least for now.


class ReceivedImageStatus(object):
    def __init__(self, fraction=0, filepath=None):
        self.fraction = fraction
        self.filepath = filepath


class ReceiverStatusWidget(QtWidgets.QWidget):
    '''
    Widget that displays current status of all images received by topside
    progressive imagery receiver node.
    '''
    def __init__(self):
        super(ReceiverStatusWidget, self).__init__()
        self.needs_update = False
        self.msg_timestamp = None
        self.image_statuses = {}  # type: Dict[int, ReceivedImageStatus]

        self.setup_ui()


        self.status_sub = rospy.Subscriber('~receiver_status',
                                           progressive_imagery.msg.ReceivedStatus,
                                           self.status_callback)
        self.image_sub = rospy.Subscriber('~image_event',
                                          progressive_imagery.msg.UpdatedImageEvent,
                                          self.image_callback)

        # NB: This needs to be a Qt timer if the callback is modifying the GUI;
        #      a ROS timer will cause threading errors.
        self.update_timer = QtCore.QTimer()
        self.update_timer.timeout.connect(self.maybe_update)
        self.update_timer.start(100)  # 10Hz -- arg is in ms.
        self.age_timer = QtCore.QTimer()
        self.age_timer.timeout.connect(self.age_timer_callback)
        self.age_timer.start(1000)

    def age_timer_callback(self):
        if self.msg_timestamp is not None:
            dt = rospy.get_time() - self.msg_timestamp
            self.time_elapsed_label.setText("(age: {} s)".format(int(np.round(dt))))

    def status_callback(self, msg):
        # type: (progressive_imagery.msg.ReceivedStatus) -> None
        self.msg_timestamp = rospy.get_time()
        for status in msg.image_status:
            image_id, fraction = status.image_id, status.fraction_received
            if image_id in self.image_statuses:
                if fraction < self.image_statuses[image_id].fraction:
                    rospy.logwarn("Received image status update with lower fraction completion")
                self.image_statuses[image_id].fraction = fraction
            else:
                self.image_statuses[image_id] = ReceivedImageStatus(fraction, None)

        self.needs_update = True

    def image_callback(self, msg):
        # type: (progressive_imagery.msg.UpdatedImageEvent) -> None

        # The image event always indicates the image_rx_####.tif filename, which
        # continually updates. Instead, grab the most recent file.
        image_dir = os.path.dirname(msg.image_path)
        filebase = os.path.basename(msg.image_path).split('.')[0]

        files = os.listdir(image_dir)
        sorted_files = sorted([ff for ff in files if ff.startswith(filebase)])

        png_filepath = None
        # Find the most recent file that's not empty
        for ff in sorted_files[-1:]:
            tif_filepath = '/'.join([image_dir, ff])
            if os.path.getsize(tif_filepath) > 0:
                png_filepath = tif_filepath.replace('.tif', '.png')
                break
        if png_filepath is None:
            # at startup, don't always have a valid image
            return
        try:
            subprocess.check_call(['convert', tif_filepath, png_filepath])
            self.image_statuses[msg.image_id].filepath = png_filepath
            self.needs_update = True
        except subprocess.CalledProcessError as ex:
            rospy.logerr("Could not convert image to PNG! Exception: \n {}".format(ex))

    def show_image(self, img):
        ff = self.image_statuses[img].filepath
        subprocess.Popen(['eog', ff])

    def maybe_update(self):
        if self.needs_update:
            # Clear the grid
            while self.scroll_grid.count() > 0:
                item = self.scroll_grid.takeAt(0)
                widget = item.widget()
                widget.deleteLater()
            # Repopulate the grid, in order of image ID
            id_col = 0
            percent_col = 2
            image_col = 4
            row = 1
            for image_id in sorted(self.image_statuses.keys()):
                id_label = QtWidgets.QLabel("{}".format(image_id))
                status = self.image_statuses[image_id]
                percent_label = QtWidgets.QLabel("{:.1f}".format(100*status.fraction))
                pixmap = QtGui.QPixmap(QtCore.QSize(150, 100))
                if status.filepath is not None:
                    # TODO: I haven't figured out WHY pixmap is sometimes null.
                    # status.filepath is only ever set if the corresponding tif
                    # exists, is not empty, and the conversion was successful...
                    # Meanwhile, this check prevents the grid from changing row
                    # heights when an invalid pixmap shows up.
                    pixmap = QtGui.QPixmap(status.filepath)
                    if pixmap.isNull():
                        pixmap = QtGui.QPixmap(QtCore.QSize(150, 100))
                icon = QtGui.QIcon(pixmap.scaled(QtCore.QSize(150, 100),
                                                 QtCore.Qt.KeepAspectRatio))
                image_button = QtWidgets.QPushButton()
                image_button.setIcon(icon)
                image_button.setIconSize(QtCore.QSize(150, 100))
                # Throw away the value that's automatically passed by the click
                image_button.clicked.connect(lambda checked, img=image_id: self.show_image(img))
                self.scroll_grid.addWidget(id_label, row, id_col)
                self.scroll_grid.addWidget(percent_label, row, percent_col)
                self.scroll_grid.addWidget(image_button, row, image_col)
                row += 1
            self.needs_update = False

    def setup_ui(self):
        title_label = QtWidgets.QLabel("Receiver Status")
        bold_font = title_label.font()
        bold_font.setWeight(QtGui.QFont.Bold)
        title_label.setFont(bold_font)
        self.time_elapsed_label = QtWidgets.QLabel("(age: N/A)")
        title_hbox = QtWidgets.QHBoxLayout()
        title_hbox.addWidget(title_label)
        title_hbox.addWidget(self.time_elapsed_label)
        title_hbox.addStretch(1)

        header_hbox = QtWidgets.QHBoxLayout()
        id_label = QtWidgets.QLabel("ID")
        progress_label = QtWidgets.QLabel("progress")
        thumbnail_label = QtWidgets.QLabel("Thumbnail")
        header_hbox.addWidget(id_label)
        header_hbox.addWidget(progress_label)
        header_hbox.addWidget(thumbnail_label)

        scroll_area = QtWidgets.QScrollArea()
        scroll_area.setWidgetResizable(True)
        scroll_area.setFocusPolicy(QtCore.Qt.NoFocus)
        scroll_frame = QtWidgets.QFrame()
        scroll_vbox = QtWidgets.QVBoxLayout()
        self.scroll_grid = QtWidgets.QGridLayout()
        scroll_vbox.addLayout(self.scroll_grid)
        scroll_vbox.addStretch(1)
        scroll_frame.setLayout(scroll_vbox)
        scroll_area.setWidget(scroll_frame)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addLayout(title_hbox)
        vbox.addLayout(header_hbox)
        vbox.addWidget(scroll_area)
        self.setLayout(vbox)


class SenderStatusWidget(QtWidgets.QWidget):
    '''
    Display current status of the subsea progressive imagery sender node.
    (This feels kinda goofy, since it's just a pretty way to rostopic echo the
    information in this message.)
    '''
    def __init__(self):
        super(SenderStatusWidget, self).__init__()
        self.sender_status = None
        self.msg_timestamp = None
        self.setup_ui()
        self.needs_update = False
        self.sub = rospy.Subscriber('~sender_progress',
                                    progressive_imagery.msg.TransferProgress,
                                    self.progress_callback)
        self.update_timer = QtCore.QTimer()
        self.update_timer.timeout.connect(self.maybe_update)
        self.update_timer.start(100)  # 10Hz -- arg is in ms.

        self.age_timer = QtCore.QTimer()
        self.age_timer.timeout.connect(self.age_timer_callback)
        self.age_timer.start(1000)  # 1Hz

    def progress_callback(self, msg):
        # type: (progressive_imagery.msg.TransferProgress) -> None
        self.sender_status = msg
        self.msg_timestamp = rospy.get_time()
        self.needs_update = True

    def age_timer_callback(self):
        if self.msg_timestamp is not None:
            dt = rospy.get_time() - self.msg_timestamp
            self.time_elapsed_label.setText("(age: {} s)".format(int(np.round(dt))))

    def maybe_update(self):
        if self.needs_update:
            # Clear the grid
            while self.scroll_grid.count() > 0:
                item = self.scroll_grid.takeAt(0)
                widget = item.widget()
                widget.deleteLater()
            # Repopulate the grid, in order of image ID
            id_col = 0
            percent_col = 2
            thumb_col = 4
            row = 1
            for action in self.sender_status.next_actions:
                id_label = QtWidgets.QLabel("{}".format(action.image_id))
                self.scroll_grid.addWidget(id_label, row, id_col)
                percent_label = QtWidgets.QLabel("{:.1f}".format(100*action.fraction_goal))
                self.scroll_grid.addWidget(percent_label, row, percent_col)
                row += 1
            txt = "{} unsent images \n {} unacked fragments".format(
                self.sender_status.entirely_unsent_images,
                self.sender_status.sent_but_unacked_fragments)
            self.unsent_label.setText(txt)
            self.needs_update = False

    def setup_ui(self):
        title_label = QtWidgets.QLabel("Sender Status")
        bold_font = title_label.font()
        bold_font.setWeight(QtGui.QFont.Bold)
        title_label.setFont(bold_font)
        self.time_elapsed_label = QtWidgets.QLabel("(age: N/A)")
        title_hbox = QtWidgets.QHBoxLayout()
        title_hbox.addWidget(title_label)
        title_hbox.addWidget(self.time_elapsed_label)
        title_hbox.addStretch(1)

        header_hbox = QtWidgets.QHBoxLayout()
        id_label = QtWidgets.QLabel("ID")
        percent_label = QtWidgets.QLabel("goal (%)")
        header_hbox.addWidget(id_label)
        header_hbox.addWidget(percent_label)

        scroll_area = QtWidgets.QScrollArea()
        scroll_area.setWidgetResizable(True)
        scroll_area.setFocusPolicy(QtCore.Qt.NoFocus)
        scroll_frame = QtWidgets.QFrame()
        scroll_vbox = QtWidgets.QVBoxLayout()
        self.scroll_grid = QtWidgets.QGridLayout()
        # NB: Must add gridlayout to parent layout when created, before using it
        scroll_vbox.addLayout(self.scroll_grid)
        scroll_vbox.addStretch(1)
        scroll_frame.setLayout(scroll_vbox)
        scroll_area.setWidget(scroll_frame)

        self.unsent_label = QtWidgets.QLabel(" ___ unsent images \n ___ unacked fragments")

        vbox = QtWidgets.QVBoxLayout()
        vbox.addLayout(title_hbox)
        vbox.addLayout(header_hbox)
        vbox.addWidget(scroll_area)
        vbox.addWidget(self.unsent_label)
        self.setLayout(vbox)


class GoalUpdateWidget(QtWidgets.QWidget):
    '''
    User Interface for updating the sender's goal queue.

    Goals are pre-pended to the list, with higher priority than the
    default goals.
    The PI sender only tracks one user-specified goal for each image,
    and new goals replace old ones.

    NB: There is no checking for whether a given goal ID is "valid";
    if we want that, this widget should be combined with the
    SenderStatus one. However, the status lists have a fixed length,
    so it is actually possible for an image to be available that isn't
    currently receiving status updates, and since the sender just ignores
    any invalid IDs, tracking isn't necessary. (However, they ARE stored,
    and will be prepended to the list if they become relevant.)
    '''
    def __init__(self):
        super(GoalUpdateWidget, self).__init__()
        self.needs_update = False
        self.goals = collections.OrderedDict()  # maps image_id to percent
        self.pub = rospy.Publisher("~image_goals",
                                   progressive_imagery.msg.QueueAction,
                                   queue_size=1)
        self.setup_ui()

        self.update_timer = QtCore.QTimer()
        self.update_timer.timeout.connect(self.maybe_update)
        self.update_timer.start(100)  # 10Hz -- arg is in ms.

    def maybe_update(self):
        if self.needs_update:
            # Clear the grid
            while self.scroll_grid.count() > 0:
                item = self.scroll_grid.takeAt(0)
                widget = item.widget()
                widget.deleteLater()
            id_col = 0
            percent_col = 2
            remove_col = 4
            row = 1
            for image_id, percent in six.iteritems(self.goals):
                id_label = QtWidgets.QLabel("{}".format(image_id))
                percent_label = QtWidgets.QLabel("{}".format(percent))
                remove_button = QtWidgets.QPushButton("Remove")
                remove_button.setAutoDefault(True)
                remove_button.setFocusPolicy(QtCore.Qt.StrongFocus)
                fxn = lambda ee, img=image_id: self.remove_button_clicked(ee, img)
                remove_button.clicked.connect(fxn)
                self.scroll_grid.addWidget(id_label, row, id_col)
                self.scroll_grid.addWidget(percent_label, row, percent_col)
                self.scroll_grid.addWidget(remove_button, row, remove_col)
                row += 1
            self.needs_update = False

    def remove_button_clicked(self, _, image_id):
        self.goals.pop(image_id)
        self.needs_update = True

    def add_button_clicked(self):
        try:
            image_id = int(self.id_edit.text())
            percent = float(self.percent_edit.text())
            self.goals[image_id] = percent
            self.needs_update = True
        except Exception as ex:
            txt = ["Invalid value in GoalUpdateWidget's input. Test validators!",
                   "ID: {}, percent:{}".format(self.id_edit.text(),
                                                  self.percent_edit.text()),
                   "{}".format(ex)]
            rospy.logerr('\n'.join(txt))

    def publish_button_clicked(self):
        '''
        Publishes message corresponding to current list of goals.
        Does NOT clear the list of goals because it assumes that
        message transmission is flaky. The operator will clear last
        sent when the sender status shows that it was received /
        is being acted on.
        (This is different from how the QM GUI works; make them
        consistent based on user preference.)

        The Progressive Imagery sender uses the most recent goal
        message, with no attempt to combine goals from multiple messages.

        for each image, so this message is idempotent. (It also means that
        we can't get too clever with sequencing.) The goals are pre-pended

        '''
        msg = progressive_imagery.msg.QueueAction()
        for image_id, percent in six.iteritems(self.goals):
            fraction = percent / 100.
            tx = progressive_imagery.msg.ImageTxAction(image_id, fraction)
            msg.queue_action.append(tx)
        self.pub.publish(msg)

    def setup_ui(self):
        title_label = QtWidgets.QLabel("New Goals")
        font = title_label.font()
        font.setWeight(QtGui.QFont.Bold)
        title_label.setFont(font)

        add_hbox = QtWidgets.QHBoxLayout()
        id_label = QtWidgets.QLabel("ID:")
        self.id_edit = QtWidgets.QLineEdit()
        self.id_edit.setValidator(QtGui.QIntValidator(0, 999999))
        self.id_edit.setMaximumWidth(70) # Trying for 6 characters...
        percent_label = QtWidgets.QLabel("Goal (%):")
        self.percent_edit = QtWidgets.QLineEdit()
        self.percent_edit.setValidator(QtGui.QIntValidator(0, 100))
        self.percent_edit.setMaximumWidth(40)
        add_button = QtWidgets.QPushButton("Add")
        add_button.setAutoDefault(True)
        add_button.clicked.connect(self.add_button_clicked)
        add_hbox.addWidget(id_label)
        add_hbox.addWidget(self.id_edit)
        add_hbox.addWidget(percent_label)
        add_hbox.addWidget(self.percent_edit)
        add_hbox.addStretch(1)
        add_hbox.addWidget(add_button)
        add_button.setFocusPolicy(QtCore.Qt.StrongFocus)

        scroll_area = QtWidgets.QScrollArea()
        scroll_area.setWidgetResizable(True)
        scroll_area.setFocusPolicy(QtCore.Qt.NoFocus)
        scroll_frame = QtWidgets.QFrame()
        scroll_vbox = QtWidgets.QVBoxLayout()
        self.scroll_grid = QtWidgets.QGridLayout()
        scroll_vbox.addLayout(self.scroll_grid)
        scroll_vbox.addStretch(1)
        scroll_frame.setLayout(scroll_vbox)
        scroll_area.setWidget(scroll_frame)

        publish_hbox = QtWidgets.QHBoxLayout()
        publish_button = QtWidgets.QPushButton("Publish")
        publish_button.clicked.connect(self.publish_button_clicked)
        publish_hbox.addStretch(1)
        publish_hbox.addWidget(publish_button)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(title_label)
        vbox.addLayout(add_hbox)
        vbox.addWidget(scroll_area)
        vbox.addLayout(publish_hbox)
        self.setLayout(vbox)


class ProgressiveImageryGUI(QtWidgets.QWidget):
    def __init__(self):
        super(ProgressiveImageryGUI, self).__init__()
        self.setup_ui()

    def setup_ui(self):
        self.setGeometry(300, 300, 450, 600)
        self.setWindowTitle("Progressive Imagery Status")

        # TODO: I want the receiver_status widget to get the most vertical
        # space, but haven't figured out how to do so yet.
        receiver_status = ReceiverStatusWidget()

        second_row_hbox = QtWidgets.QHBoxLayout()
        sender_status = SenderStatusWidget()
        goal_update = GoalUpdateWidget()
        goal_update.setSizePolicy(QtWidgets.QSizePolicy.Minimum,
                                  QtWidgets.QSizePolicy.Expanding)
        second_row_hbox.addWidget(sender_status)
        second_row_hbox.addWidget(goal_update)

        main_vbox = QtWidgets.QVBoxLayout()
        main_vbox.addWidget(receiver_status)
        main_vbox.setStretchFactor(receiver_status, 3)
        main_vbox.addLayout(second_row_hbox)
        main_vbox.setStretchFactor(second_row_hbox, 2)

        self.setLayout(main_vbox)
        self.show()


if __name__ == "__main__":
    rospy.init_node("pi_gui")
    app = QtWidgets.QApplication(sys.argv)
    pi = ProgressiveImageryGUI()
    # Enable the GUI to exit gracefully on Ctrl-C, rather than requiring SIGKILL
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())
