#! /usr/bin/env python3

# Copyright 2019 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


'''
The big issue with managing TDMA slots this way is that we need all nodes
on the network to update at the same time. If topside switches and starts
transmitting on the new schedule while subsea is still on the old, there
are likely to be problems, unless the operator performs the update in
multiple steps (new topside defs are compatible with both old and new
subsea defs; when subsea observed to transmit on the new message (or when
the _service_ call has been acked) topside switches to the final desired
definitions.

TODOs:
1) When checking a TDMA definition, make sure the topside slots are compatible
   with both the previously-sent and newly-sent subsea slots. (This isn't
   perfect because we want to know that it was received. Once I have service
   calls working, it should be set by service call and only updated then.)
2) For help debugging TDMA slot issues, should listen to received and published
   micromodem data and plot that in the GUI. (Maybe a 10-minute buffer of
   all CACYC messages?)
'''

from __future__ import print_function, division

import rospy
import signal
import sys

import PyQt5.QtGui as QtGui
import PyQt5.QtWidgets as QtWidgets

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.patches import Rectangle

import ds_acomms_msgs.msg

import comms_manager


class TDMASlotsConfiguration(QtWidgets.QWidget):
    def __init__(self):
        super(TDMASlotsConfiguration, self).__init__()

        # TODO: Enable initialization of these from parameter, since we'll
        #       usually want to operate with a default.
        msg = ds_acomms_msgs.msg.TDMASlots()
        self.prev_topside = ds_acomms_msgs.msg.TDMASlots(
            [0, 30], [10, 10], [msg.DEFAULT, msg.DEFAULT])
        self.prev_subsea = ds_acomms_msgs.msg.TDMASlots(
            [45], [10], [msg.DEFAULT])

        self.next_topside = self.prev_topside
        self.next_subsea = self.prev_subsea

        self.topside_pub = rospy.Publisher('~topside_tdma_slots',
                                           ds_acomms_msgs.msg.TDMASlots,
                                           queue_size=1)
        self.subsea_pub = rospy.Publisher('~subsea_tdma_slots',
                                          ds_acomms_msgs.msg.TDMASlots,
                                          queue_size=1)

        # Used to include time-of-flight in the timing plots.
        self.slant_range = 4000.0  # meters
        self.speed_of_sound = 1500.0  # meters/second; approximate is fine.

        self.setup_ui()
        self.reset()

    def plot_slots(self):
        # type: () -> None
        '''
        update the two bar charts based on the time increments included in the
        next message.
        '''
        dt = 2 * self.slant_range / self.speed_of_sound
        for ax, msg, color in zip((self.topside_ax, self.subsea_ax),
                                  (self.next_topside, self.next_subsea),
                                  ('red', 'blue')):
            # Can't just use ax.clear() because it also removes axis labels
            while len(ax.patches) > 0:
                artist = ax.patches[0]
                artist.remove()

            for start, duration in zip(msg.starts, msg.durations):
                height = 1
                rect1 = Rectangle((start, 0), duration, height, color=color,
                                  alpha=0.5)
                ax.add_patch(rect1)
                # TODO: Handle case where time delay wraps around ...
                if start+duration+dt > 60:
                    width2 = 60 - start - duration
                    rect2 = Rectangle((start + duration, 0), width2, height,
                                      color=color, alpha=0.25)
                    ax.add_patch(rect2)
                    width3 = (start + duration + dt) % 60
                    rect3 = Rectangle((0, 0), width3, height,
                                      color=color, alpha=0.25)
                    ax.add_patch(rect3)
                else:
                    rect2 = Rectangle((start + duration, 0), dt, 1,
                                      color=color, alpha=0.25)
                    ax.add_patch(rect2)
        self.slot_figure.canvas.draw()

    def publish_tdma_slots(self):
        # type: () -> None
        self.topside_pub.publish(self.next_topside)
        self.subsea_pub.publish(self.next_subsea)
        self.prev_topside = self.next_topside
        self.prev_subsea = self.next_subsea

    def reset(self):
        # type: () -> None
        '''
        Resets the next message to the previously sent one.
        Also update the displayed text and the bar charts.
        '''
        topside_rows = zip(self.prev_topside.starts,
                           self.prev_topside.durations,
                           self.prev_topside.rates)
        topside_text = "\n".join(['\t'.join(map(str, row)) for row in topside_rows])
        subsea_rows = zip(self.prev_subsea.starts,
                          self.prev_subsea.durations,
                          self.prev_subsea.rates)
        subsea_text = "\n".join(['\t'.join(map(str, row)) for row in subsea_rows])

        self.topside_edit.setPlainText(topside_text)
        self.subsea_edit.setPlainText(subsea_text)

        self.next_topside = self.prev_topside
        self.next_subsea = self.prev_subsea
        self.plot_slots()
        self.publish_button.setEnabled(True)

    def update_range(self):
        # type: () -> None
        self.slant_range = float(self.range_edit.text())

    def apply_presets(self, period, _):
        subsea_presets = {1: "30\t15\t5",
                          2: "15\t10\t5 \n 45\t10\t5",
                          3: "20\t5\t5 \n 40\t5\t5",
                          6: "5\t2\t5 \n 15\t2\t5 \n 25\t2\t5 \n 35\t2\t5 \n 45\t2\t5 \n 55\t2\t5",
                          12: "3\t1\t5 \n 8\t1\t5 \n 13\t1\t5 \n 18\t1\t5 \n 23\t1\t5 \n 28\t1\t5 \n 33\t1\t5 \n 38\t1\t5 \n 43\t1\t5 \n 48\t1\t5 \n 53\t1\t5 \n 58\t1\t5"}

        topside_presets = {1: "0\t15\t4",
                           2: "0\t10\t4 \n 30\t10\t4",
                           3: "10\t5\t4 \n 30\t5\t4 \n 50\t5\t4",
                           6: "0\t2\t4 \n 10\t2\t4 \n 20\t2\t4 \n 30\t2\t4 \n 40\t2\t4 \n 50\t2\t4",
                           12: "0\t2\t4 \n 5\t2\t4 \n 10\t2\t4 \n 15\t2\t4 \n 20\t2\t4 \n 25\t2\t4 \n 30\t2\t4 \n  35\t2\t4 \n 40\t2\t4 \n 45\t2\t4 \n 50\t2\t4 \n 55\t2\t4"}

        self.topside_edit.setPlainText(topside_presets[period])
        self.subsea_edit.setPlainText(subsea_presets[period])

    def text_changed(self):
        # type: () -> None
        '''
        When the text changes, we want to disable publication until
        it has been validated.
        '''
        self.publish_button.setEnabled(False)

    def update_slots(self):
        # type: () -> None
        '''
        Update the graphical representation of the TDMA slots
        based on the data currently contained in the text boxes.

        First perform validation on the input, then copy to our model
        and enable publish button, then update plot.
        '''
        topside_text = self.topside_edit.toPlainText()
        subsea_text = self.subsea_edit.toPlainText()

        new_topside_msg = ds_acomms_msgs.msg.TDMASlots()
        new_subsea_msg = ds_acomms_msgs.msg.TDMASlots()

        for msg, text in zip((new_topside_msg, new_subsea_msg),
                             (topside_text, subsea_text)):
            # iswhitespace() didn't work since text is unicode
            if len(text.strip()) == 0:
                msg.starts, msg.durations, msg.rates = [], [], []
            else:
                try:
                    msg.starts, msg.durations, msg.rates = zip(
                        *[[float(row.split()[0]),
                           float(row.split()[1]),
                           int(row.split()[2])]
                          for row in text.strip().split('\n')])

                except Exception as ex:
                    msgbox = QtWidgets.QMessageBox()
                    msgbox.setText("{}".format(ex))
                    msgbox.exec_()
                    return

                for rate in msg.rates:
                    # TODO: Way to automate this check? I don't like having
                    #       to manually enumerate the options here.
                    if rate not in [msg.DEFAULT, msg.MM1, msg.MM4, msg.MM5]:
                        msgbox = QtWidgets.QMessageBox()
                        msgbox.setText("Invalid rate: {}".format(rate))
                        msgbox.exec_()
                        return

        topside_valid = comms_manager.tdma_msg_is_valid(new_topside_msg)
        subsea_valid = comms_manager.tdma_msg_is_valid(new_subsea_msg)
        if topside_valid and subsea_valid:
            self.next_topside = new_topside_msg
            self.next_subsea = new_subsea_msg
            self.plot_slots()
            self.publish_button.setEnabled(True)
        else:
            # NB: Right now, the is_valid checker eats errors and reports them
            #    via roslog. It might be good to have them show up here.
            msgbox = QtWidgets.QMessageBox()
            msgbox.setText("Could not construct valid TDMA messages")
            msgbox.exec_()

    def setup_ui(self):
        # type: () -> None
        self.setGeometry(450, 300, 600, 450)
        self.setWindowTitle('TDMA Slots Configuration')

        input_row = QtWidgets.QHBoxLayout()

        topside_col = QtWidgets.QVBoxLayout()
        topside_label = QtWidgets.QLabel("Topside \nStart\tDuration\t\tRate")
        # NB: Not attaching callbacks to this b/c we want the user to indicate
        #     that they've finished editing both fields before we start any
        #     sanity checks.
        self.topside_edit = QtWidgets.QTextEdit()
        self.topside_edit.setAcceptRichText(False)
        self.topside_edit.setMinimumHeight(200)
        self.topside_edit.textChanged.connect(self.text_changed)
        topside_col.addWidget(topside_label)
        topside_col.addWidget(self.topside_edit)

        subsea_col = QtWidgets.QVBoxLayout()
        subsea_label = QtWidgets.QLabel("Subsea \nStart\tDuration\t\tRate")
        self.subsea_edit = QtWidgets.QTextEdit()
        self.subsea_edit.setAcceptRichText(False)
        self.subsea_edit.setMinimumHeight(200)
        self.subsea_edit.textChanged.connect(self.text_changed)
        subsea_col.addWidget(subsea_label)
        subsea_col.addWidget(self.subsea_edit)

        preset_col = QtWidgets.QVBoxLayout()
        preset_label = QtWidgets.QLabel("Presets")
        preset_buttons = {}
        for period in [1, 2, 3, 6, 12]:
            label = "{}/minute".format(period)
            preset_buttons[period] = QtWidgets.QPushButton(label)
            preset_buttons[period].setAutoDefault(True)
            cb = lambda arg, period=period: self.apply_presets(period, arg)
            preset_buttons[period].clicked.connect(cb)

        preset_col.addWidget(preset_label)
        for period in sorted(preset_buttons.keys()):
            preset_col.addWidget(preset_buttons[period])
        preset_col.addStretch(1)

        input_row.addLayout(topside_col)
        input_row.addLayout(subsea_col)
        input_row.addLayout(preset_col)

        key_row = QtWidgets.QHBoxLayout()
        msg = ds_acomms_msgs.msg.TDMASlots()
        rates = {msg.MM1: "MM1", msg.MM4: "MM4", msg.MM5: "MM5",
                 msg.DEFAULT: "DEFAULT"}
        key_label = QtWidgets.QLabel("Rates: {}".format(rates))
        key_row.addWidget(key_label)
        key_row.addStretch(1)

        range_row = QtWidgets.QHBoxLayout()
        range_label = QtWidgets.QLabel("Max slant range (m):")
        self.range_edit = QtWidgets.QLineEdit(str(self.slant_range))
        # TODO: This validator works to avoid text input and decimals, but
        #       doesn't seem to work on the upper limit.
        self.range_edit.setValidator(QtGui.QDoubleValidator(0, 12000, 0))
        self.range_edit.editingFinished.connect(self.update_range)
        update_button = QtWidgets.QPushButton("Update")
        update_button.setAutoDefault(True)
        update_button.clicked.connect(self.update_slots)
        range_row.addWidget(range_label)
        range_row.addWidget(self.range_edit)
        range_row.addStretch(1)
        range_row.addWidget(update_button)

        # TODO: Will want this to NOT be resizable...
        self.slot_figure = Figure()
        self.topside_ax = self.slot_figure.add_axes([0.2, 0.5, 0.75, 0.3])
        self.subsea_ax = self.slot_figure.add_axes([0.2, 0.2, 0.75, 0.3])
        self.topside_ax.set_ylabel('topside', rotation=0,
                                   verticalalignment='center')
        self.subsea_ax.set_ylabel('subsea', rotation=0,
                                  verticalalignment='center')
        for ax in [self.subsea_ax, self.topside_ax]:
            ax.yaxis.set_label_coords(-.12, 0.5)
            ax.set_xlim([0, 60])
            ax.set_xticks(range(0, 70, 10))
            ax.set_ylim([0, 1.0])

            for side in ['top', 'bottom', 'left', 'right']:
                ax.tick_params(axis='both', which='both', direction='out',
                               left='False', right='False', top='False', bottom='False',
                               labelleft='False', labelright='False', pad=1.5,
                               labeltop='False', labelbottom='False')
        self.topside_ax.tick_params(top='True', labeltop='True')
        self.subsea_ax.tick_params(bottom='True', labelbottom='True')
        slot_canvas = FigureCanvas(self.slot_figure)

        button_row = QtWidgets.QHBoxLayout()
        reset_button = QtWidgets.QPushButton("Reset")
        reset_button.setAutoDefault(True)
        reset_button.setToolTip('Reset to previously-published configuration')
        reset_button.clicked.connect(self.reset)
        self.publish_button = QtWidgets.QPushButton("Publish")
        self.publish_button.setAutoDefault(True)
        self.publish_button.setToolTip('Publish this configuration')
        self.publish_button.setEnabled(False)
        self.publish_button.clicked.connect(self.publish_tdma_slots)
        button_row.addStretch(1)
        button_row.addWidget(reset_button)
        button_row.addWidget(self.publish_button)

        main_vbox = QtWidgets.QVBoxLayout()
        main_vbox.addLayout(input_row)
        main_vbox.addLayout(key_row)
        main_vbox.addLayout(range_row)
        main_vbox.addWidget(slot_canvas)
        main_vbox.addLayout(button_row)
        self.setLayout(main_vbox)
        self.show()


if __name__ == "__main__":
    rospy.init_node('tdma_slots_gui')

    app = QtWidgets.QApplication(sys.argv)
    ts = TDMASlotsConfiguration()
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())
