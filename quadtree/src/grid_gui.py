#! /usr/bin/env python3

# Copyright 2019 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function, division

import collections
import copy
import numpy as np
import os
import rospy
import signal
import six
import struct
import sys

import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui
import PyQt5.QtWidgets as QtWidgets

import matplotlib
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.patches import Rectangle

import coexploration_msgs.msg
import nav_msgs.msg
import std_msgs.msg

GridParameters = collections.namedtuple('GridParameters',
                                        'll_x ll_y rows cols')

# Divider lines. Example code from:
# https://stackoverflow.com/questions/5671354/how-to-programmatically-make-a-horizontal-line-in-qt
class QHLine(QtWidgets.QFrame):
    def __init__(self):
        super(QHLine, self).__init__()
        self.setFrameShape(QtWidgets.QFrame.HLine)
        self.setFrameShadow(QtWidgets.QFrame.Sunken)

class QVLine(QtWidgets.QFrame):
    def __init__(self):
        super(QVLine, self).__init__()
        self.setFrameShape(QtWidgets.QFrame.VLine)
        self.setFrameShadow(QtWidgets.QFrame.Sunken)

class GridPrioritization(QtWidgets.QWidget):
    def __init__(self, grid_dir = None):
        super(GridPrioritization, self).__init__()
        self.grid_dir = grid_dir

        # Updated on first grid to set figure bounds, but never again.
        self.vmin = None
        self.vmax = None
        self.xlim = None
        self.ylim = None
        # Updated on first metadata
        self.send_levels = None

        # Maximum level to display. All grids at lower resolution are ignored.
        self.max_display_level = 10

        self.grids = {}  # type: Dict[int, np.array]; actual grid data
        self.grid_params = {}  # type: Dict[int, GridParameters]
        self.grid_images = {}  # artist objects, stored so they can be removed
        self.hres = None
        self.vres = None

        # Need to setup the UI first, because some ROS callbacks modify it.
        self.setup_ui()
        self.setup_ros()

        # There may be existing grids that we want to load immediately
        self.update_grids()

    def setup_ros(self):
        self.region_pub = rospy.Publisher("~requested_region",
                                          coexploration_msgs.msg.QuadtreeRegion,
                                          queue_size=1)

        self.update_sub = rospy.Subscriber("~update_grids", std_msgs.msg.Empty,
                                           self.handle_update_grids)

        self.metadata_sub = rospy.Subscriber("~metadata",
                                             coexploration_msgs.msg.QuadtreeMetadata,
                                             self.handle_metadata)

        self.path_sub = rospy.Subscriber("path",
                                         nav_msgs.msg.Path,
                                         self.handle_path)

        if self.grid_dir is None:
            self.grid_dir = rospy.get_param("~grid_directory")
        if not os.path.isdir(self.grid_dir):
            os.makedirs(self.grid_dir)

    def setup_ui(self):
        self.setGeometry(1000, 100, 900, 600)
        self.setWindowTitle('Quadtree Grid GUI')

        #########################
        # Figure that will be used to display the grids that have been received
        self.map_figure = Figure()
        self.legend_ax = self.map_figure.add_axes([0.05, 0.25, 0.03, 0.5])
        self.map_ax = self.map_figure.add_axes([0.2, 0.05, 0.75, 0.9])

        self.map_canvas = FigureCanvas(self.map_figure)
        self.toolbar = NavigationToolbar(self.map_canvas, self)

        map_column = QtWidgets.QVBoxLayout()
        map_column.addWidget(self.map_canvas)
        map_column.addWidget(self.toolbar)

        #########################
        # Radio buttons for showing different resolutions, indicating
        # how much of the map area would be requested per subgrid.
        self.subgrid_bounds_layout = QtWidgets.QVBoxLayout()
        self.bounds_buttons = {}
        self.hlines = None  # Will be LineCollection of currently-displayed bounds
        self.vlines = None
        self.bold_hlines = None
        self.bold_vlines = None
        bounds_label = QtWidgets.QLabel("Request level:")
        self.subgrid_bounds_layout.addWidget(bounds_label)
        button = QtWidgets.QRadioButton("None")
        button.setChecked(True)
        button.toggled.connect(self.subgrid_bounds_clicked)
        self.bounds_buttons[-1] = button
        self.subgrid_bounds_layout.addWidget(self.bounds_buttons[-1])
        self.add_bounds_buttons()

        self.subgrid_visibility_layout = QtWidgets.QVBoxLayout()
        visibility_label = QtWidgets.QLabel("Visible Layers:")
        self.subgrid_visibility_layout.addWidget(visibility_label)
        self.visibility_checkboxes = {}
        self.all_checkbox = QtWidgets.QCheckBox("All")
        self.all_checkbox.setChecked(True)
        self.all_checkbox.clicked.connect(self.subgrid_visibility_clicked)
        self.subgrid_visibility_layout.addWidget(self.all_checkbox)
        self.visibility_checkboxes[-1] = self.all_checkbox
        self.add_layer_checkboxes()

        selection_layout = QtWidgets.QHBoxLayout()
        selection_layout.addLayout(self.subgrid_bounds_layout)
        selection_layout.addLayout(self.subgrid_visibility_layout)

        #########################
        # Display information received from metadata/stats
        stats_layout = QtWidgets.QVBoxLayout()
        self.num_points_label   = QtWidgets.QLabel("Num Points:        n/a")
        self.hres_label         = QtWidgets.QLabel("Horiz. Resolution: n/a")
        self.vres_label         = QtWidgets.QLabel("Vert. Resolution:  n/a")
        self.subgrid_size_label = QtWidgets.QLabel("Subgrid size:      n/a")
        stats_layout.addWidget(self.num_points_label)
        stats_layout.addWidget(self.hres_label)
        stats_layout.addWidget(self.vres_label)
        stats_layout.addWidget(self.subgrid_size_label)

        #########################
        # * button for activating user input to drag rectangles
        #   - on mouse button down, select coords
        #   - on mouse button up, draw grey rectangle
        # * send selection
        # * clear selection
        self.corner1 = None
        self.corner2 = None
        self.selection_rect = None
        self.region_selection_active = False
        self.area_checkbox = QtWidgets.QCheckBox("Select Region Active")
        self.area_checkbox.clicked.connect(self.area_checkbox_clicked)
        self.send_button = QtWidgets.QPushButton("Send Selection")
        self.send_button.clicked.connect(self.send_button_pushed)
        self.clear_button = QtWidgets.QPushButton("Clear Selection")
        self.clear_button.clicked.connect(self.clear_button_pushed)

        self.map_canvas.mpl_connect('button_press_event', self.on_mouse_press)
        self.map_canvas.mpl_connect('button_release_event', self.on_mouse_release)

        select_row = QtWidgets.QHBoxLayout()
        select_row.addWidget(self.send_button)
        select_row.addWidget(self.clear_button)
        select_layout = QtWidgets.QVBoxLayout()
        select_layout.addWidget(self.area_checkbox)
        select_layout.addLayout(select_row)

        #########################
        # Handle for displaying the robot's path.
        self.path_points = None
        self.path_track = None

        #########################
        # Controls for displayed color limits of imshow grids.
        # NB: Shading and contour lines would be nice; but that may be
        #     a feature that comes when this is ported to navg3
        self.data_limits_label = QtWidgets.QLabel("Grid min/max: None/None")

        vlim_row = QtWidgets.QHBoxLayout()
        vmin_label = QtWidgets.QLabel("Vmin:")
        self.vmin_edit = QtWidgets.QLineEdit(str(self.vmin))
        self.vmin_edit.setValidator(QtGui.QDoubleValidator(-100000, 100000, 0))
        self.vmin_edit.editingFinished.connect(self.update_vmin)
        vmax_label = QtWidgets.QLabel("Vmax:")
        self.vmax_edit = QtWidgets.QLineEdit(str(self.vmax))
        self.vmax_edit.setValidator(QtGui.QDoubleValidator(-100000, 100000, 0))
        self.vmax_edit.editingFinished.connect(self.update_vmax)

        vlim_row.addWidget(vmin_label)
        vlim_row.addWidget(self.vmin_edit)
        vlim_row.addWidget(vmax_label)
        vlim_row.addWidget(self.vmax_edit)

        limits_layout = QtWidgets.QVBoxLayout()
        limits_layout.addWidget(self.data_limits_label)
        limits_layout.addLayout(vlim_row)

        # All the controls!
        control_layout = QtWidgets.QVBoxLayout()
        control_layout.addLayout(selection_layout)
        control_layout.addWidget(QHLine())
        control_layout.addLayout(stats_layout)
        control_layout.addWidget(QHLine())
        control_layout.addLayout(select_layout)
        control_layout.addWidget(QHLine())
        control_layout.addStretch(1)
        control_layout.addWidget(QHLine())
        control_layout.addLayout(limits_layout)
        control_layout.addStretch(1)

        # And finally, the overall layout.
        main_hbox = QtWidgets.QHBoxLayout()
        main_hbox.addLayout(map_column, stretch=1)
        main_hbox.addLayout(control_layout)
        self.setLayout(main_hbox)
        self.show()

    def send_button_pushed(self):
        if self.corner1 is None or self.corner2 is None:
            print("Send button pushed but can't send selection because at least one corner is None")
            return

        msg = coexploration_msgs.msg.QuadtreeRegion()

        level = None
        for idx, button in six.iteritems(self.bounds_buttons):
            if button.isChecked():
                if idx < 0:
                    print("Can't publish region; invalid level {}".format(idx))
                    return
                level = idx
                break
        if level is None:
            print("WARNING: No level radio button was selected?")
            return
        msg.level = level

        try:
            msg.x0, msg.y0, msg.dx, msg.dy = self._get_rect_bounds()
        except Exception as ex:
            print("Tried to get rectangle bounds, failed with exception:")
            print(ex)
            return

        self.region_pub.publish(msg)

    def clear_button_pushed(self):
        self.corner1 = None
        self.corner2 = None
        self.maybe_remove_rect()

    def maybe_remove_rect(self):
        if self.selection_rect is not None:
            try:
                self.selection_rect.remove()
                self.selection_rect = None
                self.map_canvas.draw_idle()
            except Exception as ex:
                print("Exception while removing rectangle: {}".format(ex))

    def area_checkbox_clicked(self):
        checkbox = self.sender()
        self.region_selection_active = checkbox.isChecked()

    def on_mouse_press(self, event):
        if not self.region_selection_active:
            return
        self.corner1 = (event.xdata, event.ydata)

    def on_mouse_release(self, event):
        if not self.region_selection_active:
            return
        self.maybe_remove_rect()

        self.corner2 = (event.xdata, event.ydata)
        try:
            x0, y0, dx, dy = self._get_rect_bounds()
        except Exception as ex:
            print("Tried to get rectangle bounds, failed with exception:")
            print(ex)
            return

        rect = Rectangle((x0, y0), dx, dy, color='grey', alpha=0.5)
        self.selection_rect = self.map_ax.add_patch(rect)
        self.map_canvas.draw_idle()

    def _get_rect_bounds(self):
        x0 = min(self.corner1[0], self.corner2[0])
        dx = np.abs(self.corner1[0] - self.corner2[0])
        y0 = min(self.corner1[1], self.corner2[1])
        dy = np.abs(self.corner1[1] - self.corner2[1])
        return x0, y0, dx, dy

    def subgrid_visibility_clicked(self):
        # If "all" was checked, set all layers to its state
        # If any other is checked, then only change its state.
        checkbox = self.sender()
        checked = checkbox.isChecked()

        level = None
        for idx, box in six.iteritems(self.visibility_checkboxes):
            if box == checkbox:
                level = idx
                break
        if idx == -1:
            for box in self.visibility_checkboxes.values():
                box.setChecked(checked)
            for im in self.grid_images.values():
                im.set_visible(checked)
        else:
            if idx in self.grid_images.keys():
                self.grid_images[idx].set_visible(checked)
            else:
                print("Can't change visibility of level {}; nonexistant grid".format(idx))
        self.map_canvas.draw_idle()

    def subgrid_bounds_clicked(self):
        # If we don't have any grids plotted, then we can't know where to put
        # the lines
        if len(self.grid_params) == 0:
            return

        button = self.sender()
        # This gets called for both the on and off events, so it only
        # takes action on the check event.
        if not button.isChecked():
            return

        # Figure out which button this was....
        level = None
        for idx, bb in six.iteritems(self.bounds_buttons):
            if bb == button:
                level = idx
                break

        # Clear the existing lines
        if self.vlines is not None:
            self.vlines.remove()
            self.vlines = None
        if self.hlines is not None:
            self.hlines.remove()
            self.hlines = None
        if self.bold_vlines is not None:
            self.bold_vlines.remove()
            self.bold_vlines = None
        if self.bold_hlines is not None:
            self.bold_hlines.remove()
            self.bold_hlines = None

        # Only redraw lines if level >= 0 (here, -1 is magic number for None)
        if -1 != level:
            # Prefer to use the resolution from the current grid, but really,
            # they're constant across the map and are only duplicated per-file
            # because each grid file needs to be self-contained.
            # TODO: Make it an error for the levels to be different on load?
            #       (and have a single param for them?)
            step = self.hres * pow(2, level)

            xmin, xmax = self.map_ax.get_xlim()

            # Handle case where axes is using image coordinates
            y1, y2 = self.map_ax.get_ylim()
            ymin = min(y1, y2)
            ymax = max(y1, y2)

            # If we don't already have a grid for this level, we don't know
            # where its bounds are. Could probably compute from another layer,
            # but that's less important...
            if level in self.grid_params.keys():
                grid_xmin = self.grid_params[level].ll_x - 0.5*step
                dx = grid_xmin - xmin
                xstart = grid_xmin - step*np.floor(dx / step)

                grid_ymin = self.grid_params[level].ll_y - 0.5*step
                dy = grid_ymin - ymin
                ystart = grid_ymin - step*np.floor(dy / step)
            else:
                xstart = xmin
                ystart = ymin

            xcoords = np.arange(xstart, xmax, step)
            ycoords = np.arange(ystart, ymax, step)
            self.vlines = self.map_ax.vlines(xcoords, ymin, ymax, color='gray', lw=0.5)
            self.hlines = self.map_ax.hlines(ycoords, xmin, xmax, color='gray', lw=0.5)


            if self.send_levels is not None:
                grid_level = level + self.send_levels - 1
                grid_step = self.hres * pow(2, grid_level)
                # Try to properly align the bold lines to the actual boundaries
                # This is an annoying duplication of the above logic...
                if grid_level in self.grid_params:
                    grid_xmin = self.grid_params[grid_level].ll_x - 0.5*grid_step
                    dx = grid_xmin - xmin
                    xstart = grid_xmin - grid_step*np.floor(dx / grid_step)

                    grid_ymin = self.grid_params[grid_level].ll_y - 0.5*grid_step
                    dy = grid_ymin - ymin
                    ystart = grid_ymin - grid_step*np.floor(dy / grid_step)
                else:
                    print("WARNING: Trying to draw grid lines for level {}, and don't have a grid at level {}".format(level, grid_level))

                xcoords = np.arange(xstart, xmax, grid_step)
                ycoords = np.arange(ystart, ymax, grid_step)
                self.bold_vlines = self.map_ax.vlines(xcoords, ymin, ymax, linewidths=2)
                self.bold_hlines = self.map_ax.hlines(ycoords, xmin, xmax, linewidths=2)

        self.map_canvas.draw_idle()

    def handle_path(self, msg):
        """
        Update the path that's drawn on the grid to show vehicle's location.
        """
        rospy.logerr("handle_path!")
        if self.path_points is not None:
            self.path_points.remove()
            self.path_track.remove()
        # Typical AUV frame reports x/y as northing/easting, but we need ENU
        # for the map to show up as expected.
        northing = [ps.pose.position.x for ps in msg.poses]
        easting = [ps.pose.position.y for ps in msg.poses]
        self.path_points, = self.map_ax.plot(easting, northing, 'k.', ms=5)
        self.path_track, = self.map_ax.plot(easting, northing, 'k-', lw=1)
        self.map_canvas.draw_idle()

    def handle_update_grids(self, msg):
        self.update_grids()

    def handle_metadata(self, msg):
        self.send_levels = msg.send_levels
        self.num_points_label.setText("Num Points:  {}".format(msg.num_points))
        self.hres_label.setText("Horiz. Resolution: {}".format(msg.horizontal_resolution))
        self.vres_label.setText("Vert. Resolution:  {}".format(msg.vertical_resolution))
        subgrid_size = pow(2, msg.send_levels - 1)
        self.subgrid_size_label.setText("Subgrid size:      {} x {}".format(subgrid_size, subgrid_size))
        self.update()


    def update_grids(self):
        '''
        This will be called if/when the new file is created, triggered by a
        message from the grid receiver.
        '''
        self.grids.clear()
        self.grid_params.clear()
        for filename in os.listdir(self.grid_dir):
            filepath = "{}/{}".format(self.grid_dir, filename)
            try:
                with open (filepath, 'rb') as fp:
                    header = fp.read(28)
                    try:
                        tokens = struct.unpack('<iffiiff', header)
                    except Exception as ex:
                    # If the GUI is started while files are being written,
                    # we may encounter an invalid file.
                        print(ex)
                        continue

                    level = tokens[0]
                    hres = tokens[5]
                    vres = tokens[6]

                    if self.hres is None:
                        self.hres = hres
                        self.relabel_gui()
                    else:
                        if hres != self.hres:
                            raise Exception("Invalid horizontal resolution ({}) "
                                        "in {}. Current hres = {} ".format(hres, filepath, self.hres))
                    if self.vres is None:
                        self.vres = vres
                    else:
                        if vres != self.vres:
                            raise Exception("Invalid vertical resolution ({}) "
                                        "in {}. Current vres = {} ".format(vres, filepath, self.vres))


                    params = GridParameters(*tokens[1:5])

                # If it's an empty grid, don't save it
                    if params.rows == 1 or params.cols == 1:
                        continue

                    fp.seek(28)
                    data = np.fromfile(fp, dtype=np.int32, count=-1)

                # If the grid doesn't have any data (it's a level higher than
                # is being transmitted), then don't save it.
                    if np.min(data) == np.max(data):
                        continue
                    self.grid_params[level] = params
                    self.grids[level] = np.reshape(data, (params.rows, params.cols))
            except IsADirectoryError as es:
                print(es)
                continue

        # Try to set the color limits to the data bounds.
        if (len(self.grids) > 0):
            vmin = np.inf
            vmax = -np.inf
            for level, grid in six.iteritems(self.grids):
                good_idxs = np.where(grid!= 2**31 - 1)
                if len(good_idxs[0]) > 0:
                    vmin = min(vmin, self.vres * np.min(grid[good_idxs]))
                    vmax = max(vmax, self.vres * np.max(grid[good_idxs]))

            self.data_limits_label.setText("Grid min/max depth: {} -> {} m".format(vmin, vmax))
            if self.vmin is None:
                self.vmin = vmin
                self.vmax = vmax
                self.vmin_edit.setText(str(self.vmin))
                self.vmax_edit.setText(str(self.vmax))

            if len(self.bounds_buttons) < 2:
                self.add_bounds_buttons()

            self.plot_grids()

    def relabel_gui(self):
        '''
        After receiving the first hres information, can update the labels!
        '''
        for level, checkbox in six.iteritems(self.visibility_checkboxes):
            if level == -1:
                continue
            resolution = self.get_resolution(level)
            checkbox.setText("{} m".format(resolution))
        for level, button in six.iteritems(self.bounds_buttons):
            if level == -1:
                continue
            resolution = self.get_resolution(level)
            button.setText("{} m".format(resolution))
        self.update()

    def add_layer_checkboxes(self):
        for level in np.arange(self.max_display_level, -1, -1):
            checkbox = QtWidgets.QCheckBox("level {}".format(level))
            checkbox.setChecked(self.all_checkbox.isChecked())
            checkbox.toggled.connect(self.subgrid_visibility_clicked)
            self.subgrid_visibility_layout.addWidget(checkbox)
            self.visibility_checkboxes[level] = checkbox

    def add_bounds_buttons(self):
        for level in np.arange(self.max_display_level, -1, -1):
            button = QtWidgets.QRadioButton("level {}".format(level))
            button.setChecked(False)
            button.toggled.connect(self.subgrid_bounds_clicked)
            self.subgrid_bounds_layout.addWidget(button)
            self.bounds_buttons[level] = button

    def get_resolution(self, level):
        return self.hres * pow(2, level)

    def plot_grids(self):
        levels = sorted(self.grids.keys())

        # Don't want xlim/ylim to revert with every redraw
        if self.xlim is not None:
            self.xlim = self.map_ax.get_xlim()
        if self.ylim is not None:
            self.ylim = self.map_ax.get_ylim()

        for idx, im in six.iteritems(self.grid_images):
            im.remove()
        self.grid_images.clear()

        for level in levels[::-1]:
            # We create the checkboxes ahead of time; if layer is too
            # low-resolution to be in the list, then just don't display
            # it. (Adding GUI elements in reponse to a ROS callback
            # doesn't seem to work, due to parent being in another thread,
            # so if we display it, there'd be no good way to turn it off.)
            if level not in self.visibility_checkboxes:
                continue

            # Extent corresponds to edges of drawn image
            params = self.grid_params[level]
            cell_size = self.get_resolution(level)
            xmin = params.ll_x
            ymin = params.ll_y
            xmin -= cell_size / 2.
            ymin -= cell_size / 2.
            xmax = xmin + params.cols * cell_size
            ymax = ymin + params.rows * cell_size
            # NB: The sender currently uses image coordinates (rather than geographic)
            #     so origin is top left, and extent has to reflect that.
            # This is definitely on the fix-me list =)
            # (Will have to change in the sender and here)

            #extent = self.hres*np.array([xmin, xmax, ymax, ymin])
            extent = self.hres*np.array([xmin, xmax, ymin, ymax])

            # print("Level {}, extent: {}".format(level, extent))

            ma = np.ma.masked_where(self.grids[level] == 2**31 - 1,
                                    self.grids[level])
            # Scaling needs to happen after masking
            scaled = ma * self.vres
            im = self.map_ax.imshow(scaled, interpolation='none',
                                    origin='lower',  # had been defaulting to 'upper'
                                    extent=extent,
                                    vmin=self.vmin, vmax=self.vmax)

            im.set_visible(self.visibility_checkboxes[level].isChecked())
            self.grid_images[level] = im


        self.legend_ax.clear()
        colorbar_level = min(levels[-1], self.max_display_level)
        self.map_figure.colorbar(self.grid_images[colorbar_level],
                                 cax=self.legend_ax)
        self.map_ax.axis('equal')

        if self.xlim is not None:
            self.map_ax.set_xlim(self.xlim)
        if self.ylim is not None:
            self.map_ax.set_ylim(self.ylim)


        self.map_canvas.draw_idle()
        self.update()

        # If this is the first plot, initialize axis limits!
        if self.xlim is None:
            self.xlim = copy.copy(self.map_ax.get_xlim())
        if self.ylim is None:
            self.ylim = copy.copy(self.map_ax.get_ylim())

    def update_vlim(self):
        for level, im in six.iteritems(self.grid_images):
            im.set_clim(vmin=self.vmin, vmax=self.vmax)
        self.map_canvas.draw_idle()

    # NB: These don't include a validator for min < max; could probably do so
    #     in the TextEdit widget.
    def update_vmin(self):
        self.vmin = float(self.vmin_edit.text())
        self.update_vlim()

    def update_vmax(self):
        self.vmax = float(self.vmax_edit.text())
        self.update_vlim()


if __name__ == "__main__":
    rospy.init_node("quadtree_grid_gui")

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir", help="directory to load grids from. If unset, will be loaded from ROS parameter server")
    myargv = rospy.myargv(sys.argv)
    args = parser.parse_args(myargv[1:])

    app = QtWidgets.QApplication(sys.argv)
    gp = GridPrioritization(args.dir)

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())
