#! /usr/bin/env python3

# Copyright 2019 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function, division

import argparse
import collections
import numpy as np
import rospy
import signal
import six
import sys
import yaml

import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui  # Only used for setting font to bold.
import PyQt5.QtWidgets as QtWidgets

import ds_acomms_msgs.msg

import comms_manager

# for mypy; optional because this doesn't affect anything at runtime
try:
    from typing import Dict, List, Optional, Tuple, Type
except ImportError:
    pass


QueueRow = collections.namedtuple('QueueRow', ['code_label', 'topic_label',
                                               'class_label', 'mode_label',
                                               'remove_button'])
# Operational mode for streaming background data.
# There will be service calls for filling in the background data each way,
# along with (optional) lists of associated topics that should be relayed.
# TODO: Does the GUI handle background data that is only one-way, without
#       a corresponding service call for the other direction?
BackgroundMode = collections.namedtuple('BackgroundMode',
                                        ['subsea_service', 'topside_service',
                                         'subsea_topics', 'topside_topics'])

class TopicConfiguration(QtWidgets.QWidget):
    def __init__(self,
                 topic_lookup: comms_manager.TopicLookup,
                 comms_manager: comms_manager.CommsManager,
                 default_topics: List[int]
                 ) -> None:
        super(TopicConfiguration, self).__init__()
        self.topic_lookup = topic_lookup
        self.comms_manager = comms_manager
        self.default_topics = default_topics

        for topic in self.default_topics:
            if topic not in self.topic_lookup.topics.values():
                msg = "Default topic not in lookup table: {}".format(topic)
                rospy.logfatal(msg)
                raise Exception(msg)

        # TODO: Separate send-once from fixed-frequency?
        # self.send_once_topics = {}  # type: Dict[str, int]
        self.active_topics = {}  # type: Dict[bytes, int]

        self.setup_ui()

        # Set display to show default topics!
        self.reset_default()

    def setup_ui(self) -> None:
        code_width = 45
        topic_width = 400
        class_width = 275
        mode_width = 45

        # Header row for labelling for the the table
        topic_header_row = QtWidgets.QHBoxLayout()

        topic_header_code_label = QtWidgets.QLabel("Code")
        topic_header_topic_label = QtWidgets.QLabel("Topic")
        topic_header_class_label = QtWidgets.QLabel("Class")
        topic_header_mode_label = QtWidgets.QLabel("Period")

        topic_header_code_label.setMinimumWidth(code_width)
        topic_header_topic_label.setMinimumWidth(topic_width)
        topic_header_class_label.setMinimumWidth(class_width)
        topic_header_mode_label.setMinimumWidth(mode_width)

        topic_header_row.addWidget(topic_header_code_label)
        topic_header_row.addWidget(topic_header_topic_label)
        topic_header_row.addWidget(topic_header_class_label)
        topic_header_row.addWidget(topic_header_mode_label)
        topic_header_row.addStretch(1)

        # Scroll area for adding queue rows!
        # Holds another widget, provides horizontal and vertical scroll bars
        scroll_widget = QtWidgets.QScrollArea()
        scroll_widget.setWidgetResizable(True)
        scroll_widget.setFocusPolicy(QtCore.Qt.NoFocus)

        # ScrollArea requires a widget, not a layout, so this holds the Layout
        # (and adds a frame around it)
        scroll_frame = QtWidgets.QFrame()

        # NB: This seems like one too many layers of Layouts, but it was
        # the easiest way to make sure that the grid doesn't stretch awkwardly.
        # (Easy to add stretch at bottom of QVBoxLayout; I coulnd't find
        # that for a grid)
        scroll_layout = QtWidgets.QVBoxLayout()
        grid_layout = QtWidgets.QGridLayout()
        scroll_layout.addLayout(grid_layout)
        scroll_layout.addStretch(1)

        scroll_frame.setLayout(scroll_layout)
        scroll_widget.setWidget(scroll_frame)

        # Create all the widgets that we might want to use to display different
        # queues, then make them visible/invisible as needed.
        self.widgets = {}  # type: Dict[bytes, QueueRow]
        row_idx = 0
        for topic in sorted(self.topic_lookup.topics.values()):

            row_idx += 1
            codes = [key for key, value in six.iteritems(self.topic_lookup.topics)
                     if value == topic]
            code = codes[0]
            msg_class = self.topic_lookup.msg_classes[code]

            code_label = QtWidgets.QLabel("{}".format(ord(code)))
            grid_layout.addWidget(code_label, row_idx, 0)

            topic_label = QtWidgets.QLabel(topic)
            grid_layout.addWidget(topic_label, row_idx, 1)

            label_text = msg_class.__module__.replace(".msg._", ".")
            class_label = QtWidgets.QLabel(label_text)
            grid_layout.addWidget(class_label, row_idx, 2)

            mode_label = QtWidgets.QLabel()
            grid_layout.addWidget(mode_label, row_idx, 3)

            remove_button = QtWidgets.QPushButton("Remove")
            remove_button.setAutoDefault(True)
            fxn = lambda arg, cc=code: self.remove_queue(cc, arg)
            remove_button.clicked.connect(fxn)
            grid_layout.addWidget(remove_button, row_idx, 5)

            row = QueueRow(code_label, topic_label, class_label, mode_label,
                           remove_button)
            for widget in row:
                widget.setVisible(False)
            self.widgets[code] = row

        # Set up minimum column sizes to minimize required resizing
        grid_layout.setColumnMinimumWidth(0, code_width)
        grid_layout.setColumnMinimumWidth(1, topic_width)
        grid_layout.setColumnMinimumWidth(2, class_width)
        grid_layout.setColumnMinimumWidth(3, mode_width)
        # Use stretch to have space before remove button
        grid_layout.setColumnStretch(4, 1)

        # Row for adding a new queue

        add_row = QtWidgets.QHBoxLayout()

        self.code_label = QtWidgets.QLabel("")
        self.topic_combobox = QtWidgets.QComboBox()
        self.topic_combobox.addItem("")
        for topic in sorted(self.topic_lookup.topics.values()):
            self.topic_combobox.addItem(topic)
        # No callback -- data will be read by the "Add" callback

        self.class_label = QtWidgets.QLabel()

        self.mode_combobox = QtWidgets.QComboBox()
        for mode in range(11):
            self.mode_combobox.addItem("{}".format(mode))
        idx = self.mode_combobox.findText("1")
        self.mode_combobox.setCurrentIndex(idx)
        # No callback -- data will be read by the "Add" callback

        add_button = QtWidgets.QPushButton("Add")
        add_button.setAutoDefault(True)
        add_button.clicked.connect(self.add_queue)

        self.code_label.setMinimumWidth(code_width)
        self.topic_combobox.setMinimumWidth(topic_width)
        self.class_label.setMinimumWidth(class_width)
        self.mode_combobox.setMinimumWidth(mode_width)

        add_row.addWidget(self.code_label)
        add_row.addWidget(self.topic_combobox)
        add_row.addWidget(self.class_label)
        add_row.addWidget(self.mode_combobox)
        add_row.addWidget(add_button)
        add_row.addStretch(1)

        bytes_row = QtWidgets.QHBoxLayout()
        total_bytes_label = QtWidgets.QLabel("Minimum total bytes:")
        self.total_bytes = QtWidgets.QLabel("0")
        self.total_bytes.setMinimumWidth(60)

        average_bytes_label = QtWidgets.QLabel("Average bytes per transmission:")
        self.average_bytes = QtWidgets.QLabel("0")
        self.average_bytes.setMinimumWidth(60)

        bytes_row.addWidget(total_bytes_label)
        bytes_row.addWidget(self.total_bytes)
        bytes_row.addWidget(average_bytes_label)
        bytes_row.addWidget(self.average_bytes)

        main_vbox = QtWidgets.QVBoxLayout()
        main_vbox.addLayout(topic_header_row)
        main_vbox.addLayout(add_row)
        main_vbox.addWidget(scroll_widget)
        main_vbox.addLayout(bytes_row)
        self.setLayout(main_vbox)

    def add_queue(self) -> None:
        '''
        Pull queue code and mode from the current display. If they are valid,
        update model and view.
        '''
        topic = self.topic_combobox.currentText()
        mode = int(self.mode_combobox.currentText())

        self.add_topic(topic, mode)
        self.reset_new_queue()

    def add_topic(self, topic, mode):
        # type: (str, int) -> None
        '''
        Hook to programmatically add topic to the configuration.
        '''
        try:
            codes = [key for key, value in six.iteritems(self.topic_lookup.topics)
                     if value == topic]
            code = codes[0]

            self.active_topics[code] = mode
            self.update_code(code)
        except (ValueError, IndexError):
            # If topic/mode haven't been selected, just ignore the button click.
            pass

        self.update_byte_display()


    def reset_new_queue(self) -> None:
        '''
        Reset the params for the next queue to be added.
        '''
        self.code_label.setText("")
        idx = self.mode_combobox.findText("1")
        self.mode_combobox.setCurrentIndex(idx)
        self.topic_combobox.setCurrentIndex(0)
        self.class_label.setText("")

    def remove_topic(self, topic: str) -> None:
        '''
        Hook to programmatically remove topic from the configuration list.
        '''
        try:
            codes = [key for key, value in six.iteritems(self.topic_lookup.topics)
                     if value == topic]
            code = codes[0]
            self.remove_queue(code, None)
        except (ValueError, IndexError):
            pass

    def remove_queue(self, code: bytes, _) -> None:
        print("Asked to remove queue {}".format(ord(code)))
        self.active_topics.pop(code, None)
        self.update_code(code)
        self.update_byte_display()

    def update_code(self, code: bytes) -> None:
        '''
        Update a single row (corresponding to input code) of the input display.
        '''
        code_active = code in self.active_topics
        for widget in self.widgets[code]:
            widget.setVisible(code_active)
        if code_active:
            label = "{}".format(self.active_topics[code])
            self.widgets[code].mode_label.setText(label)

    def reset_last_sent(self, last_published) -> None:
        self.active_topics = {code: ord(mode) for code, mode in
                              zip(last_published.queue_topics,
                                  last_published.queue_periods)}
        print("Resetting to: {}".format(self.active_topics))
        for code in self.topic_lookup.topics.keys():
            try:
                self.update_code(code)
            except Exception as ex:
                print("Trying to update code failed: {}".format(ord(code)))
                raise(ex)
        self.update_byte_display()

    def reset_default(self) -> None:
        self.active_topics = {}
        for topic in self.default_topics:
            codes = [cc for cc, tt in six.iteritems(self.topic_lookup.topics) if tt == topic]
            code = codes[0]
            self.active_topics[code] = 1  # Default to mode 1

        print("Resetting to: {}".format(self.active_topics))
        for code in self.topic_lookup.topics.keys():
            try:
                self.update_code(code)
            except Exception as ex:
                print("Trying to update code failed: {}".format(ord(code)))
                raise(ex)
        self.update_byte_display()

    def update_byte_display(self) -> None:
        '''
        Running count of the minimum number of bytes required to transmit
        all selected messages at rate 1 and averaged over expected cycle
        (and leaving out the mode=0 ones).

        This will underestimate the length of any message that includes
        an array or a string.
        '''
        total_bytes = 0
        average_bytes = 0
        for code, mode in six.iteritems(self.active_topics):
            msg = self.topic_lookup.msg_classes[code]()
            msg_bytes = self.comms_manager.encode_message(code, msg)
            total_bytes += len(msg_bytes)
            if mode != 0:
                average_bytes += 1.0*len(msg_bytes) / mode
        self.total_bytes.setText("{}".format(total_bytes))
        self.average_bytes.setText("{}".format(int(np.ceil(average_bytes))))


class QueueConfiguration(QtWidgets.QWidget):
    def __init__(self, default_filename: str) -> None:
        super(QueueConfiguration, self).__init__()

        # At least for now, I want to be able to work on this without dealing
        # with the parameter server, so allowing command line specification
        self.topic_filename = rospy.get_param("/topic_lookup_filename", default_filename)
        if self.topic_filename is None:
            raise Exception("No topic filename found")
        self.topic_lookup = comms_manager.TopicLookup(self.topic_filename)

        # Using default parameter b/c it's OK if these are empty
        topside_str = rospy.get_param("~default_topside_topics", "[]")
        if topside_str is None:
            raise Exception("Could not find parameter default_topside_topics")
        try:
            self.default_topside_topics: List[int] = yaml.load(topside_str)
        except:
            rospy.logerr("Couldn't parse default topside topics array! {}"
                         .format(topside_str))
            raise

        subsea_str = rospy.get_param("~default_subsea_topics", "[]")
        if subsea_str is None:
            raise Exception("Could not find parameter default_subsea_topics")
        try:
            self.default_subsea_topics: List[int] = yaml.load(subsea_str)
        except:
            rospy.logerr("Couldn't parse default subsea topics array! {}"
                         .format(subsea_str))
            raise

        background_str = rospy.get_param("~background_data_modes", "{}")
        self.background_modes = {}  # type: Dict[str, BackgroundMode]
        try:
            background_dict = yaml.load(background_str)
            # Always want to be able to disable background data.
            self.background_modes["None"] = BackgroundMode("", "", [], [])
            for key, value in six.iteritems(background_dict):
                self.background_modes[key] = BackgroundMode(*value)
        except:
            rospy.logerr("Couldn't parse background modes! {}"
                         .format(subsea_str))
            raise
        self.background_mode = "None"
        assert(self.background_mode in self.background_modes)

        # Need a copy of the queue manager to determine serialization size
        self.comms_manager = comms_manager.CommsManager(self.topic_filename)

        self.default_data_service: bytes = b""
        self.subsea_data_service: bytes = self.default_data_service
        self.topside_data_service: bytes = self.default_data_service

        self.last_subsea_published = ds_acomms_msgs.msg.QueueDefinition()
        self.last_topside_published = ds_acomms_msgs.msg.QueueDefinition()

        self.subsea_pub = rospy.Publisher("~subsea_queue_definition",
                                          ds_acomms_msgs.msg.QueueDefinition,
                                          queue_size=1, latch=True)
        self.topside_pub = rospy.Publisher("~topside_queue_definition",
                                           ds_acomms_msgs.msg.QueueDefinition,
                                           queue_size=1, latch=True)

        self.setup_ui()
        # Publish the default so user doesn't have to do so manually
        self.publish_queue_definition()

    def publish_queue_definition(self):
        # type: () -> None
        '''
        Publish queue definition message based on GUI's current configuration.
        '''
        topside_msg = ds_acomms_msgs.msg.QueueDefinition()
        for code, mode in six.iteritems(self.topside_tab.active_topics):
            topside_msg.queue_topics += code
            topside_msg.queue_periods += bytes([mode])

        try:
            print("Publishing queue def! topside/subsea background data: {}, {}"
                  .format(self.topside_data_service, self.subsea_data_service))
        except Exception as ex:
            print(ex)

        topside_msg.background_data = self.topside_data_service
        self.topside_pub.publish(topside_msg)
        self.last_topside_published = topside_msg

        subsea_msg = ds_acomms_msgs.msg.QueueDefinition()
        for code, mode in six.iteritems(self.subsea_tab.active_topics):
            subsea_msg.queue_topics += code
            subsea_msg.queue_periods += bytes([mode])

        subsea_msg.background_data = self.subsea_data_service
        self.subsea_pub.publish(subsea_msg)
        self.last_subsea_published = subsea_msg


    def background_data_toggled(self) -> None:
        button = self.sender()
        if button.isChecked():
            print("Selected background data: {}".format(button.key))
            self.background_mode = button.key
            print("Services are: {}".format(self.background_modes[self.background_mode]))

            # Set {topside/subsea}_data_service
            bg_mode = self.background_modes[button.key]
            self.subsea_data_service = self.topic_lookup.code_from_data_service(bg_mode.subsea_service)
            self.topside_data_service = self.topic_lookup.code_from_data_service(bg_mode.topside_service)

            # Set {topside/subsea} topics (in the tab widgets...)
            for topic in self.background_modes[self.background_mode].topside_topics:
                self.topside_tab.add_topic(topic, 1)
            for topic in self.background_modes[self.background_mode].subsea_topics:
                self.subsea_tab.add_topic(topic, 1)

        else:
            print("Button deselected: {}".format(button.key))
            if button.key != self.background_mode:
                rospy.logwarn("Inconsistent state! unchecked button {}, "
                              "but previous mode was {}"
                              .format(button.key, self.background_mode))
            for topic in self.background_modes[self.background_mode].topside_topics:
                self.topside_tab.remove_topic(topic)
            for topic in self.background_modes[self.background_mode].subsea_topics:
                self.subsea_tab.remove_topic(topic)

            print("...subsea: {}".format(self.background_modes[self.background_mode].subsea_topics))
            print("...topside: {}".format(self.background_modes[self.background_mode].topside_topics))

    def reset_last_sent(self) -> None:
        '''
        Reset the GUI's view to correspond to the most recently published
        message.
        '''
        self.subsea_tab.reset_last_sent(self.last_subsea_published)
        self.topside_tab.reset_last_sent(self.last_topside_published)

        self.subsea_data_service = self.last_subsea_published.background_data
        self.topside_data_service = self.last_topside_published.background_data

    def reset_default(self) -> None:
        '''
        Reset the GUI's view to correspond to the default specified by the
        parameter server / launch file.
        '''
        self.subsea_tab.reset_default()
        self.topside_tab.reset_default()
        self.subsea_data_service = self.default_data_service
        self.topside_data_service = self.default_data_service

    def setup_ui(self) -> None:
        '''
        Create all the UI elements required, and hook up callbacks.

        TODOs:
        * add titles to the columns
        * Possibly move the add-queue buttons outside the scroll area?
        * add better default widths so you don't have to immediately resize and
          so addition of queues with longer topics doesn't always trigger resize
        '''
        self.setGeometry(150, 150, 1000, 200)
        self.setWindowTitle('Comms Manager Configuration')

        # To set full font/size/weight, could use:
        # setFont(QtGui.QFont("Times", 8, QtGui.QFont.Bold))
        # Instead, I just want to go from default to bold:
        data_service_section_label = QtWidgets.QLabel("Background Data Service")
        bold_font = data_service_section_label.font()
        bold_font.setWeight(QtGui.QFont.Bold)
        data_service_section_label.setFont(bold_font)

        # Select data service w/ drop-down
        background_row = QtWidgets.QHBoxLayout()
        for key in self.background_modes.keys():
            button = QtWidgets.QRadioButton(key)
            button.key = key
            button.setChecked(key == self.background_mode)
            button.toggled.connect(self.background_data_toggled)
            background_row.addWidget(button)
        background_row.addStretch(1)

        # TODO: Add separate section for send-once topics? Might also
        #       be good to treat them separately in the Queue Manager
        #       (append rather than replace, don't reset counts)

        ###########################

        topic_section_label = QtWidgets.QLabel("Transmitted Topics")
        topic_section_label.setFont(bold_font)

        # Tabs for Subsea / Topside
        tabs = QtWidgets.QTabWidget()
        self.subsea_tab = TopicConfiguration(self.topic_lookup,
                                             self.comms_manager,
                                             self.default_subsea_topics)
        self.topside_tab = TopicConfiguration(self.topic_lookup,
                                              self.comms_manager,
                                              self.default_topside_topics)

        tabs.addTab(self.subsea_tab, "Subsea")
        tabs.addTab(self.topside_tab, "Topside")

        # Publish/reset Buttons!

        button_row = QtWidgets.QHBoxLayout()

        reset_default_button = QtWidgets.QPushButton('Reset (default)')
        reset_default_button.setAutoDefault(True)
        reset_default_button.setToolTip('Reset GUI to default configuration')
        reset_default_button.clicked.connect(self.reset_default)

        reset_button = QtWidgets.QPushButton('Reset (last published)')
        reset_button.setAutoDefault(True)
        reset_button.setToolTip('Reset GUI to last-published configuration')
        reset_button.clicked.connect(self.reset_last_sent)

        publish_button = QtWidgets.QPushButton('Publish')
        publish_button.setAutoDefault(True)
        publish_button.setToolTip('Publish this configuration')
        publish_button.clicked.connect(self.publish_queue_definition)

        button_row.addStretch(1)
        button_row.addWidget(reset_default_button)
        button_row.addWidget(reset_button)
        button_row.addWidget(publish_button)

        # Final layout

        main_vbox = QtWidgets.QVBoxLayout()
        main_vbox.addWidget(data_service_section_label)
        main_vbox.addLayout(background_row)

        main_vbox.addWidget(topic_section_label)
        main_vbox.addWidget(tabs)
        main_vbox.addLayout(button_row)

        self.setLayout(main_vbox)
        self.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--filename",
                        help="lookup table from ID to message topics/classes")
    # rospy.myargv includes the full name of the executable as arg 0
    args = parser.parse_args(rospy.myargv()[1:])

    rospy.init_node('subsea_qm_gui')

    # TODO: Turn this into rqt plugin.
    app = QtWidgets.QApplication(sys.argv)
    qm = QueueConfiguration(args.filename)
    # Enable the GUI to exit gracefully on Ctrl-C, rather than requiring SIGKILL
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())
