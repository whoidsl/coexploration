# Copyright 2021 University of Washington Applied Physics Laboratory
# Author: Laura Lindzey
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


import importlib
import numpy as np
# import numpy.typing as npt
import rospy

from typing import List, Optional, Tuple

import coexploration_msgs.msg
import std_msgs.msg


class CoexScalarException(Exception):
    pass


def parse_message(msg: rospy.Message,
                  data_field: str,
                  data_index: Optional[int]
                 ) -> Optional[float]:
    """
    Parse scalar data from the message based on config params.
    """
    attrs = data_field.split('.')
    submsg = msg

    try:
        for attr in attrs:
            submsg = getattr(submsg, attr)
    except AttributeError as ex:
        errmsg = ("Unable to access field {} of message type {}"
                  .format(data_field, type(msg)))
        rospy.logfatal(errmsg)
        raise(CoexScalarException(errmsg))

    if data_index is not None:
        if data_index >= len(submsg):
            errmsg = ("Requested index {}, but {} has len {}"
                      .format(data_index, data_field, len(submsg)))
            rospy.logerr_throttle(5, errmsg)
            return
        dd = submsg[data_index]
    else:
        dd = submsg
    return dd

def filter_data(data,  # npt.NDArray[np.float32],
                msg: rospy.Message,
                max_length: Optional[int]
               ) -> List[coexploration_msgs.msg.ScalarData]:
    """
    Return list of Nx2 numpy arrays corresponding to data requested by msg.
    There will be separate arrays in the case that self.data doesn't have a
    consistent frequency s.t. the data had to be split.

    Implemented as a standalone function for easier testing.

    Input:
    * data: Nx2 np.array where the first column is time in seconds, and the
            second column is the values.
    * msg: ScalarDataRequest that this will be responding to
    * max_length: maximum number of values to include in a single message.
                  (Will return multiple messages if the requested slice is
                  longer than max_length.) None for no maximum.
    """
    if len(data) == 0:
        rospy.logwarn("scalar_sender: trying to filter data, but no data received")
        return []

    # Crop the data to the requested time bounds
    start_time = msg.start_time.to_sec()
    end_time = start_time + msg.duration
    idxs, = np.where(np.all([start_time <= data[:,0], data[:,0] <= end_time], axis=0))
    cropped_data = data[idxs, :]

    # TODO: Consider having a min segment length? Maybe something like 10 valid points?
    # If we don't have enough data to calcualte a rate, don't send anything
    nrows, ncols = cropped_data.shape
    if nrows < 2:
        rospy.logwarn("No data in requested time range {} - {}".format(start_time, end_time))
        return []

    # Detect gaps in the timestamps and split the data.
    dt = cropped_data[1:,0] - cropped_data[0:-1,0]
    jumps, = np.where(dt > 1.5*np.median(dt))
    # jumps gives the index of the last element in a constant-rate subset,
    # so add the final index so all segments will have an entry.
    jumps = np.append(jumps, len(dt))
    rospy.logdebug("jumps: {}".format(jumps))
    start_idx = 0
    segment_bounds = []
    for end_idx in jumps:
        segment_bounds.append((start_idx, end_idx))
        start_idx = end_idx + 1
    rospy.logdebug("bounds: {}".format(segment_bounds))

    # For each segment, create a message.
    responses = []

    for start_idx, end_idx in segment_bounds:
        # Don't send length-one segment.
        if start_idx == end_idx:
            continue
        rospy.logdebug("indices: {}, {}".format(start_idx, end_idx))
        rospy.logdebug("cropped data shape: {}".format(cropped_data.shape))
        segment_data = cropped_data[start_idx:end_idx+1, :]
        rospy.logdebug("segment data shape: {}".format(segment_data.shape))
        dt = segment_data[1:,0] - segment_data[:-1,0]

        # Always send uniformly spaced data, so figure out the skip that
        # will most closely approximate the requested rate.
        skip = max(1, int(np.round((1.0/msg.rate)/np.median(dt))))
        rospy.logdebug("For rate {} and dt {}, skip = {}".format(msg.rate, np.median(dt), skip))
        downsampled_data = segment_data[::skip,:]

        nrows, _ = downsampled_data.shape
        if max_length is None or nrows < max_length:
            resp = coexploration_msgs.msg.ScalarData()
            t0 = downsampled_data[0, 0]
            resp.start_time = rospy.Time(t0)
            resp.duration = downsampled_data[-1,0] - t0
            resp.values = list(downsampled_data[:,1])
            responses.append(resp)
        else:
            rospy.logdebug("Need to split the data!")
            start_idx = 0
            for end_idx in np.arange(max_length, nrows+max_length, max_length):
                rospy.logdebug("Start/end indices: {}, {}".format(start_idx, end_idx))
                resp = coexploration_msgs.msg.ScalarData()
                transmit_data = downsampled_data[start_idx:end_idx,:]
                t0 = transmit_data[0, 0]
                resp.start_time = rospy.Time(t0)
                resp.duration = transmit_data[-1,0] - t0
                resp.values = list(transmit_data[:,1])
                responses.append(resp)
                start_idx = end_idx

    return responses


def validate_params(msg_type_str, data_field, data_index, max_length):
    # type: (str, str, Optional[int], Optional[int]) -> type
    """
    Check that the input parameters specify a valid combination of message
    and scalar field.

    Returns the type of the specified message.

    Inputs:
    * msg_type_str: Rather than a hard-coded list of supported message
                    packages, dynamically imports the requested type.
                    Format is "package/message", e.g. "ds_sensor_msgs/Ctd"
    * data_field: Dot-separated path to the scalar of interest within the
                  message. e.g. for geometry_msgs/PoseStamped, z position
                  would be "pose.position.z"
    * data_index: `None` if data_field is a scalar; otherwise, integer
                  index within the list that is requested. For example, to
                  request raw[0] from a ds_core_msgs/A2D2, set data_index = 0.
    * max_length: Maximum number of data values to report in a single message.
                  Useful in the case that we want to guarantee that each
                  message fits in an acoustic packet.
    """
    # Check that we can import the specified message type
    msg_type: type
    try:
        pkg, msg_name = msg_type_str.split('/')
        msg_module = importlib.import_module(pkg + ".msg")
        msg_type = getattr(msg_module, msg_name)
    except Exception as ex:
        rospy.logfatal("Unable to load message: {}".format(msg_type_str))
        raise(CoexScalarException(ex))

    # Check that the message has a header -- required for timestamps
    msg = msg_type()
    if not hasattr(msg, "header"):
        errmsg = ("ScalarSender requires messages to have a header. "
                   "{} does not have an appropriately-named field."
                   .format(type(msg)))
        rospy.logfatal(errmsg)
        raise(CoexScalarException(errmsg))

    if type(msg.header) != std_msgs.msg.Header:
        # NB: didn't actually test this because defining a pathological
        #     message seemed like overkill just for test coverage, given that
        #     it'll just fail at startup anyways.
        errmsg = ("{}'s header is of the wrong type: {}"
                  .format(type(msg), type(msg.header)))
        rospy.logfatal(errmsg)
        raise(CoexScalarException(errmsg))

    # Check that the specified data_field exists within the message
    attrs = data_field.split('.')
    field = msg_type()
    for attr in attrs:
        if not hasattr(field, attr):
            errmsg = ("Message {} does not have attribute {} ({})"
                      .format(msg_type, data_field, attr))
            rospy.logfatal(errmsg)
            raise(CoexScalarException(errmsg))
        field = getattr(field, attr)

    # Finally, check that the requested data field is of a type that
    # matches the configuration.
    # ROS allows messages to have fields that are lists of unspecified
    # length, so it is not always possible to check that the index into
    # the list is valid before a message has been received.
    if data_index is None:
        # (tested by launching the orp node with no data_index set)
        if type(field) not in [float, int]:
            errmsg = ("Field {} in message {} is not a scalar! ({})"
                      .format(data_field, msg_type, type(field)))
            rospy.logfatal(errmsg)
            raise(CoexScalarException(errmsg))
    else:
        # (tested by specifying an index for temperature)
        if type(field) != list:
            errmsg = ("Requested index {} of non-list field {} in message {}"
                      .format(data_index, data_field, msg_type))
            rospy.logfatal(errmsg)
            raise(CoexScalarException(errmsg))
        # TODO: I couldn't figure out how to check the expected type of
        #       elements in a list. So, if the user asks for the n-th
        #       entry in a string[], that will be a runtime error when
        #       the code tries to convert the data to a np.array.

    # If max_length is specified, it must be positive.
    if max_length is not None and max_length < 1:
        errmsg = "Invalid value for max_length: {}".format(max_length)
        rospy.logfatal(errmsg)
        raise(CoexScalarException(errmsg))

    return msg_type


class ScalarSender(object):
    def __init__(self) -> None:
        """
        Construction WILL FAIL if the scalar_sender is unable to load
        the message or if the requested field does not exist or is of
        the wrong type.
        """
        self.data : List[Tuple[float, float]] = []

        self.setup_params()
        self.setup_connections()

    def setup_params(self) -> None:
        msg_type_str = rospy.get_param("~msg_type")
        self.data_field = rospy.get_param("~data_field")
        self.data_index = rospy.get_param("~data_index", default=None)
        self.max_length = rospy.get_param("~max_length", default=None)

        self.msg_type = validate_params(msg_type_str, self.data_field,
                                        self.data_index, self.max_length)

    def setup_connections(self) -> None:
        rospy.loginfo("ScalarSender.setup_connections")
        self.data_sub = rospy.Subscriber("~scalar_data", self.msg_type, self.data_callback)

        self.cmd_sub = rospy.Subscriber("~request",
                                        coexploration_msgs.msg.ScalarDataRequest,
                                        self.request_callback)

        self.pub = rospy.Publisher("~aggregated_data",
                                   coexploration_msgs.msg.ScalarData,
                                   queue_size=1)



    def data_callback(self, msg: rospy.Message) -> None:
        """
        Append scalar from message to data in memory.
        """
        tt = msg.header.stamp.to_sec()
        try:
            dd = parse_message(msg, self.data_field, self.data_index)
            if dd is not None:
                self.data.append((tt, dd))
        except Exception as ex:
            rospy.logerr("Unhandled exception in data_callback: {}".format(ex))
            return

        rospy.logdebug_throttle(5,"{}: {}: {}".format(self.msg_type, tt, dd))


    def request_callback(self, msg):
        # type: (coexploration_msgs.msg.ScalarDataRequest) -> None
        rospy.loginfo("ScalarSender.request_callback")
        try:
            data = np.array(self.data)
            responses = filter_data(data, msg, self.max_length)

            for resp in responses:
                rospy.loginfo("Publishing segment of size: {}".format(len(resp.values)))
                self.pub.publish(resp)
        except Exception as ex:
            rospy.logerr("Unhandled exception in request_callback: {}".format(ex))



if __name__ == "__main__":
    rospy.init_node('scalar_sender')
    ss = ScalarSender()
    rospy.spin()
