/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Nov 19, 2019.
//

#ifndef DS_QUADTREE_QUADTREE_H
#define DS_QUADTREE_QUADTREE_H

#include <array>                    // for std::array
#include <boost/coroutine2/all.hpp>  // For equivalent to generator
#include <cassert>                  // for std::assert
#include <cmath>                    // for std::round
#include <fstream>
#include <functional>               // For function pointers
#include <iostream>                 // for std::cout
#include <limits>                   // for std::numeric_limits
#include <list>                     // for std::list
#include <memory>                   // For smart pointers
#include <sstream>                  // for std::stringstream
#include <vector>                   // for std::vector

// This file does not have any ROS or DS dependencies; those are restricted
// to the node classes. In order to achieve this:
//
// * Any required timestamps are passed in by the calling function
// * Logging is handled by pointers to logging functions. They will default to
//   using std::cout, and when used in a ROS node, those function pointers
//   will be to ROS_DEBUG etc. See:
//   https://answers.ros.org/question/29874/putting-log-messages-from-non-ros-code-into-ros-logging-system/
//   I don't love this solution -- I would prefer one like the python logging
//   in smach, where you don't need to have the logging function belong
//   to a class.


// Throughout the code, we will be referring to different regions.
// These can be used in reference to the whole tree, or to a subtree
// rooted at any node:
//
// * domain: given the current center and level, what coordinates can be
//           added to the tree without adding another level.
// * bounding box: what are the x and y limits defining a tight bounding
//                 box for th enodes currently in the tree. This will often
//                 be smaller than the domain.
// * coverage: What grid cells in a given level will have a value given the
//             current nodes in the tree. Will not necessarily be compact.


// Additionally, we have conventions for referring to parts of the tree:
//
// * tree: The whole quadtree
// * subgrid: part of tree that can be sent in a single acoustic update.
//            includes a root + all nodes N levels down from that.
// * root: Either the root node of the tree or of a subgrid

namespace quadtree {

// Default callbacks for logging functions. These can be redirected
// to the equivalent ROS functions by using setLogMessageCallback
inline void defaultFatalMessageCallback(const std::string &msg) {
  std::cerr << "Quadtree Fatal: " << msg << std::endl;
}
inline void defaultErrorMessageCallback(const std::string &msg) {
  std::cerr << "Quadtree Error: " << msg << std::endl;
}
inline void defaultWarnMessageCallback(const std::string &msg) {
  std::cerr << "Quadtree Warn: " << msg << std::endl;
}
inline void defaultInfoMessageCallback(const std::string &msg) {
  std::cout << "Quadtree Info: " << msg << std::endl;
}
inline void defaultDebugMessageCallback(const std::string &msg) {
  std::cout << "Quadtree Debug: " << msg << std::endl;
}


struct Node {
  enum ChildIndex {
    UPPER_LEFT = 0,
    UPPER_RIGHT,
    LOWER_LEFT,
    LOWER_RIGHT,
    NUM_CHILDREN
  };

  /**
      @brief Children nodes. (Always exactly 4 for a quadtree)

      They are stored as a pointer to an array so that leaf nodes only
      have 8 bytes of memory for children, rather than 32, without having
      to wrangle multiple Node classes.

      NB: This may not have been the right choice -- I haven't checked the
      actual memory layout, and means we can't use attribute(packed).
   */
  std::unique_ptr<std::array<std::unique_ptr<Node>, 4>> children;

  /**
      @brief Mean value of points within this cell

     The no-data value will be float_max or int_max, depending
     on whether it is represented by a node in the tree or a cell in
     the grid. This requires some annoying magic-number checking, but
     I couldn't come up with a better option.
   */
  float mean_z;

  /**
      @brief How many points have gone into computing the mean.

      Given that the multibeam system provides < 1000 points/second,
      an int32 is sufficient for hundreds of hours of operation.
  */
  int32_t num_points;

  /**
     @brief Last time a new point updated this cell (seconds since Epoch)

     If this code is still being used in 2038, an int32 will overflow.
     I used int here to avoid uint math bugs, and because I didn't really want
     the extra 4 bytes in each node for int64.
  */
  int32_t updated_time;

  /**
     @brief Last time the subgrid rooted at this cell was published

     This is compared with updated_time and the requested time to only
     republish cells with new data.
  */
  int32_t published_time;

  /**
     @brief Default constructor.
  */
  Node() {
    this->mean_z = std::numeric_limits<float>::max();
    this->num_points = 0;
    this->updated_time = 0;
    this->published_time = 0;
  }

  // Convert from internal float representation to the int representation
  // used in the grid and in transferring updates. (Internal representation
  // has to be float to avoid math errors.)
  int get_value() {
    if (this->mean_z == std::numeric_limits<float>::max()) {
      return std::numeric_limits<int>::max();
    } else if (this->mean_z > std::numeric_limits<int>::max() ||
	       this->mean_z < std::numeric_limits<int>::min()) {
      // TODO: Given that this error message occurs in a context where the
      //       logging function pointers haven't been initialized, I'm stuck
      //       for how it should be redirected to the ROS macros.
      std::cout << "WARNING: mean_z is outside of int range. This is probably a bug" << std::endl;
      return std::numeric_limits<int>::max();
    } else {
      int rounded = (int)std::round(this->mean_z);
      if (rounded == std::numeric_limits<int>::max() ||
	  rounded == std::numeric_limits<int>::min()) {
      }
      return rounded;
    }
  }
};  //  __attribute__((packed));
// TODO: We care about memory usage for this one...so figure out how to
//       get packed to work.
//       1) This caused issues with using Node fields as inputs to
//          std::make_unique<..>, but was easily fixed with an intermediate
//          variable.
//       2) A warning about a non-POD type due to including a std::unique_ptr
//       ... so I'm not sure that packing even makes sense. (I'll trade memory
//       efficiency for smart pointers!)
//       AND, based on sizeof printouts, it seems that even without packing it's
//       doing as well as it could (24 bytes)

/**
Map updates are transmitted acoustically as "subgrids", each corresponding
to a section of the tree.

It consists of elevation values, along with the metadata required to
insert them in the topside tree.
 */
struct Subgrid {
  /** Level of sending tree's root node */
  int tree_level;

  /**
      @brief X coordinate of tree's root node, in units of grid cells.

      Information about the sending tree's root must be included with
      each subgrid because it determines the topology of the tree, and
      that needs to be kept synchronized between sender/receiver.
  */
  int tree_xx;

  /** Y coordinate of tree's root node, in units of grid cells. */
  int tree_yy;


  /** Level in tree that root node of the subgrid occupies */
  int level;

  /**
      @brief X coordinate of root node in subgrid, in units of grid cells.

      This is a bit annoying, because the offset between level0 and level1
      node positions will be 0.5 grid cells. So, trying to force all node
      coordinates to be ints will fail. However, it's OK for subgrids to be
      required to be at least 2 levels, after which the subgrid centers will
      line up with lower-level node boundaries, which will be integral.
  */
  int xx;

  /** Y coordinate of root node in subgrid, in units of grid cells. */
  int yy;

  /** Last time this subgrid was updated with new data. */
  int32_t timestamp;

  /**
      @brief cell values for this subgrid

      These will be converted to uint8 or uint16 and transmitted.
      (int_max is no-data value)
  */
  std::vector<int> data;

  Subgrid() {
    this->level = 0;
    this->tree_level = 0;
    this->tree_xx = 0;
    this->tree_yy = 0;
    this->xx = 0;
    this->yy = 0;
    this->timestamp = 0;
  }

  Subgrid(int tree_level, int tree_xx, int tree_yy, int ll, int xx, int yy,
	  int32_t ts) {
    this->tree_level = tree_level;
    this->tree_xx = tree_xx;
    this->tree_yy = tree_yy;
    this->level = ll;
    this->xx = xx;
    this->yy = yy;
    this->timestamp = ts;
  }

  /**
     @brief Returns whether the range of data in this quadtree fits in int8.

     Update subgrids are serialized as an offset and a 2^n x 2^n grid
     of deltas, where the range of deltas determines whether a uint8 or
     uint16 is used. This is the only compression that occurs.
  */
  bool IsSmall() const {
    // NB: This can't just use std::minmax_element because it needs to ignore
    //     the values used to flag no data.
    bool small = true;
    int min_val = std::numeric_limits<int>::max();
    int max_val = std::numeric_limits<int>::min();
    for (int val : data) {
      // in case of no data, subgrid malue is max int
      if (val == std::numeric_limits<int>::max()) {
	continue;
      }
      min_val = std::min(val, min_val);
      max_val = std::max(val, max_val);
    }
    // Needs to be strictly less than in order to preserve 255
    // for the small grid NaN mask.
    return (max_val - min_val) < 255;
  }

  /**
     @brief Returns minimum value of data, used as the offset for compression.
  */
  int GetOffset() const {
    return *std::min_element(data.begin(), data.end());
  }
};

/** @brief Description of single user-requested grid */
struct GridRequest {
  /**
      @brief X-coordinate of center of requested grid (cell units)

      The requested center is not required to align exactly with a node's
      coordinate (though in practice, it usually will, since the requests
      are programatically generated). Any input coordinate will be mapped
      to a grid containing it.
  */
  float cx;

  /** @brief Y-coordinate of center of requested grid. (cell units) */
  float cy;

  /**
      @brief What level to send (counting up from leaves at 0)
  */
  int level;

  /**
     @brief most recent timestamp that the requestor has data for this subgrid

     This is the only mechanism for acking that data was received; if the
     specified subgrid hasn't been updated since timestamp, it will not be sent.
   */
  int32_t timestamp;

  // Default constructor
  GridRequest() {
    this->cx = 0;
    this->cy = 0;
    this->level = 0;
    this->timestamp = 0;
  }

  // Required for use with emplace_back
  GridRequest(float xx, float yy, int ll, int32_t ts) {
    this->cx = xx;
    this->cy = yy;
    this->level = ll;
    this->timestamp = ts;
  }
};

class QuadTree {
  friend class QuadTreeTraversalTest_test_get_node_Test;

 public:
  /**
      @brief Used when constructing an explicit grid of values at a given level.

      This is only relevant for when the receiver saves grids, and for some
      unit tests.
  */
  typedef std::vector<std::vector<int>> grid_t;

  /** coroutine used for depth first search through tree.*/
  typedef boost::coroutines2::asymmetric_coroutine<
      std::tuple<Node*, int, float, float>>
      coro_dfs_t;

  /** coroutine used for visiting all leaf nodes within a subtree. */
  typedef boost::coroutines2::asymmetric_coroutine<Node*> coro_grid_t;

  // We want to switch between outputing to stdout and using the ROS
  // logging macros, depending on whether this library is being run
  // from within a ROS node or not.
  // Implementation roughly follows the example given here:
  // https://answers.ros.org/question/29874/putting-log-messages-from-non-ros-code-into-ros-logging-system/
  typedef std::function<void(const std::string&)> LogMessageCallback;

  explicit QuadTree(float h_res, float v_res, int send_levels);

  /**
     @brief Perform depth-first traversal of tree using co routines.

     This implements tree traversal as a coroutine, which means that
     multiple other methods can depend on it without having to reimplement
     the error-prone coordinate and level tracking.

     The page describing coroutine motivation has a very relevant example:
     https://www.boost.org/doc/libs/1_58_0/libs/coroutine/doc/html/coroutine/motivation.html

     NB: Whenever we update to 18.04 / Melodic, Boost will update from
         1.58 -> 1.62 or 1.65. In that jump, coroutine2 was added, but I
         think that coroutine will continue to work. The jump *after* that
         is likely to be a problem. (Hopefully it'll move into std::?)

     The coroutine yields a tuple of Node*, its level and its coordinates
     relative to the root of the tree. This is useful because the node itself
     doesn't store those values. The alternative would be to store a pointer
     to parent, and compute them when needed.

     @param root Node to start the depth-first traversal at
     @param level absolute level of input node (counting up from leaves at 0)
     @param xc x-position of root's center in cell units
     @param yc y-position of root's center in cell units
     @param out
  */
  void TraverseDFS(Node* root, int level, float xc, float yc,
                   coro_dfs_t::push_type& out);

  /**
     @brief Perform breadth-first traversal of tree using co routines

     This is useful for any functions that want to terminate the traversal
     early. Since memory usage is a concern, the implementation is done in an
     iterative-deepening style

     @param root Node where the traversal starts
     @param level absolute level of input root node
     @param xc x-position of root's center in cell units
     @param yc y-position of root's center in cell units
     @param out
  */
  void TraverseBFS(Node* root, int level, float xc, float yc,
                   coro_dfs_t::push_type& out);

  /**
     @brief Traverse all nodes at a given level

     @param target_level level to traverse
     @param root Node where the traversal starts
     @param level absolute level of input root node
     @param xc x-position of root's center in cell units
     @param yc y-position of root's center in cell units
     @param out
  */
  void TraverseLevel(int target_level, Node* root, int level, float xc,
                     float yc, coro_dfs_t::push_type& out);

  /**
     @brief Perform traversal of all descendants of input node at a given depth.

     This is useful in the sender for creating a subgrid, and in the receiver
     for inserting new data.

     @param root node to start the traversal at
     @param level how many levels below node to start yielding nodes
     @param out
  */
  void TraverseSubgrid(Node* node, int level, coro_grid_t::push_type& out);

  /**
     @brief Construct a 2-D matrix out of the data at a given level in the tree

     WARNING: This has the potential to use a huge amount of memory.
     It should never be used in the node that runs on the robot; it is
     included for testing and for topside visualization.

     TODO: Maybe add an argument for max# cells we're prepared to handle,
     and have it return success? (Punting on this for now because all typical
     survey patterns will be fine.)

     @param grid_level which level of the tree the grid should represent
  */
  std::unique_ptr<grid_t> MakeGrid(int grid_level);

  /**
     @brief Save all levels of tree as  grids in binary format

     Output filenames will be level_%d.bin
     It is assumed that output_dir has already been created.

     @param output_dir fully-resolved path to output directory
  */
  void SaveGrids(std::string output_dir, int min_level, int max_level);

  /** accessor for current height of tree */
  int num_levels() const { return this->num_levels_; };

  /** accessor for how many levels are being sent in each subgrid update */
  int send_levels() const { return this->send_levels_; };

  /** accessor for how many data points have been added to the tree */
  int num_points() const {
    if (this->root_) {
      return this->root_->num_points;
    } else {
      return 0;
    }
  };
  float x_center() const { return this->x_center_; };
  float y_center() const { return this->y_center_; };

  /**
     @brief return pointer to node in input level whose span includes point

     Will return nullptr if no such node exists.

     TODO: Returning a non-const pointer to an internal tree node shouldn't
           be part of this class's public interface.
	   QuadTreeReceiverNode currently uses this to get the coordinates
	   of the node closest node to an input set of coordinates.

     TODO: Mixing return styles is ugly.

     @param xx
     @param yy
     @param level
  */
  Node* GetNode(float xx, float yy, int level, float* nodex, float* nodey);

  /**
     @brief Set logger for a given log level.

     @param level - one of "fatal", "error", "warn", "info", "debug";
                    matching ROS's available log levels.
     @param cb - callback to be called for that log level.
  */
  void setLogMessageCallback(std::string level, LogMessageCallback cb);

 protected:

  /**
     @brief Calculate coordinate bounds for a given level of the tree.

     This returns bounds of the resulting grid, which is distinct from the
     region spanned by the tree if it were dense.
     (This requires a leaf node at the boundary, rather than calculating
     from center + cell size + levels.)

     return value indicates whether valid bounds were computed
     for the input level (e.g. it's possible for a tree to not have any
     nodes at level 0, in which case the bounds will be meaningless)

     WARNING: This is slow, since it computes the bounds by traversing the
         whole tree. DO NOT USE in the sender.

     @return success
     @param level
  */
  bool GetCoordBounds(int level, float* min_x_coord, float* max_x_coord,
                      float* min_y_coord, float* max_y_coord);
  bool GetTreeExtrema(int level, float* x_min, float* x_max, float* y_min,
		      float* y_max);

  /**
     @brief Calculate the offset between node centers between levels.

     This function gives the offset to a child node from a parent node.
     To find the offset to parent from child, call with child's level + 1,
     and reverse the direction of deltas.

     @param[in] level parent node's level
     @param[in] child_index Used to determine cardinal direction of step
     @param[out] dx offset along x axis
     @param[out] dy offset along y axis
  */
  void GetLevelOffset(const int level, const int child_index, float* dx,
                      float* dy);

  /**
      @brief return the index to child node corresponding to input direction.

      @param dx delta from node's X center to other point (pt = center + delta)
      @param dy delta from node's Y center to other point
  */
  int GetChildIndex(float dx, float dy);

  /**
     @brief Convert from world coordinates to cell coordinates

     This conversts from world coordinates (assumed to be meters in some
     orthogonal reference frame) to map coordinates (in units of cells).

     NB: does NOT center cell coords at root of tree, just rescales

     @param xx[in] input x position (world coordinates, meters)
     @param yy[in] input y position (world coordinates, meters)
     @param zz[in] input z position (world coordinates, meters)
     @param xc[out] output x position (map coordinates, cells)
     @param yc[out] output y position (map coordinates, cells)
     @param zc[out] output z position (map coordinates, cells)
  */
  void CellFromWorld(float xx, float yy, float zz, float* xc, float* yc,
                     float* zc);

  /** X position of the root node's center, in world coordinates (meters) */
  float x_center_;
  /** Y position of the root node's center, in world coordinates (meters) */
  float y_center_;

  /**
      @brief How many levels are in the tree.

      If no points have been added to the tree, num_levels = 0.
      NB: This is always positive, but using a uint would mean having to
          convert to a signed int before using it in some of the math since
          pow(1, level - 1) overflows ...
  */
  int num_levels_;

  /**
      @brief How many levels of a subtree will be transmitted in an update.

      How many levels of the tree to include in the subgrid that is sent
      in a single frame. This should be chosen based on packet size.

      The micromodem's 256 byte packet can fit an 8x8 grid with metadata,
      so send_levels = 3.
  */
  int send_levels_;

  /** Root of the tree */
  std::unique_ptr<Node> root_;

  // TODO: I don't love this naming scheme -- they act like functions, name them as such?
  LogMessageCallback fatal_;
  LogMessageCallback error_;
  LogMessageCallback warn_;
  LogMessageCallback info_;
  LogMessageCallback debug_;

 private:

  /** @brief x/y dimension of each leaf cell (meters) */
  float horizontal_resolution_;

  /**
      @brief vertical resolution of elevation data (meters)

     The acoustic updates transmit updates using uints, and this is used
     to convert elevations from the input units to counts.
     For sea floor elevations, 0.1 m is a good value.
  */
  float vertical_resolution_;

};  // class QuadTree



}  // namespace quadtree

#endif  // DS_QUADTREE_QUADTREE_H
