#! /usr/bin/env python2
"""

"""


import cv2
import numpy as np
import os
os.environ['TZ'] = 'UTC'
import rosparam
import rospy
import subprocess
import time

import std_msgs.msg  # For Time

def get_timestamp(filename):
    '''
    returns posix timestamp from the images filename.
    example input:
    sentry.20191103.205441377280.11098.tif
    '''
    _, day_str, time_str, _, _ = filename.split('.')
    foo = time.strptime("{} {}".format(day_str, time_str[0:6]), "%Y%m%d %H%M%S")
    tt = time.mktime(foo)
    return tt


def make_output_filename(timestamp):
    '''

    '''
    time_struct = time.gmtime(timestamp)
    # This will properly zero-pad for a length 9
    # HOWEVER, progressive imagery has a max image_id of 1000000
    # This is defined in jpeg2000-image.proto, but it is not trivial to
    # increase it 1000x, because then the actual message size may exceed
    # the maximum allowed. Would have to tweak other maximum values...
    #time_string = time.strftime("%j%H%M%S", time_struct)
    # So, for now, assuming that we won't request images with duplicate timestamps over 24 hours.
    time_string = time.strftime("%H%M%S", time_struct)
    filename = "image_{}.tif".format(time_string)
    return filename


# This implements color_correction::gray_world::run2
def gray_world(img):
    scale_factor = compute_scaling_factors(img)
    scaled_img = img*scale_factor
    # I think that the C++ version of this forces it to stay 8UC3 type?
    # YUP. That wound up being VERY important for the apparent color balance.
    scaled_img[scaled_img > 255] = 255
    return scaled_img.astype('uint8')

def compute_scaling_factors(img):
    '''
    This reimplements the scaling calculations performed by:
    void color_correction::gray_world::process(Mat src1,float *ml,float *ma,float *mb,int p,int m)
    (assuming p = 1 and m = 2, as is hard-coded in pipeline.cpp)
    '''
    nrows, ncols, _ = img.shape
    npixels = 1.0 * nrows * ncols

    scaled_sums = np.sum(img, axis=(0, 1)) / npixels
    max_val = np.max(scaled_sums)
    return max_val / scaled_sums

def apply_clahe(img):
    lab_img = cv2.cvtColor(img.astype('uint8'), cv2.COLOR_BGR2LAB)

    clip_limit = 1.0
    grid_size = (10, 10)
    clahe = cv2.createCLAHE(clip_limit, grid_size)
    ll, aa, bb = cv2.split(lab_img)
    ll = ll + 2  # Original comment was "make entire image a little lighter"
    new_ll = clahe.apply(ll)
    eq_lab_img = cv2.merge([new_ll, aa, bb])
    eq_bgr_img = cv2.cvtColor(eq_lab_img, cv2.COLOR_LAB2BGR)
    return eq_bgr_img


def process_image(raw_filepath, proc_filepath):
    '''
    Perform debayering, grey-world color correction and histogram equalization
    Params:
    * raw_filepath:  path to image, as taken directly from the datapod
    * proc_filepath: Where to put the processed image
    '''
    rospy.loginfo("Processing image {} -> {}".format(raw_filepath, proc_filepath))
    raw_img = cv2.imread(raw_filepath, cv2.IMREAD_GRAYSCALE)
    bgr_img = cv2.cvtColor(raw_img, cv2.COLOR_BAYER_GB2BGR)
    gray_img = gray_world(bgr_img)
    clahe_img = apply_clahe(gray_img)
    cv2.imwrite(proc_filepath, clahe_img)


class ImageFetcher(object):
    def __init__(self):

        try:
            # /home/sentry/progressive_imagery_test/raw
            self.local_raw_dir = rospy.get_param("~local_raw_dir")
            # TODO: This should probably a parameter shared with PI sender (common/img_dir)
            # /home/sentry/progressive_imagery_test/tx
            self.local_proc_dir = rospy.get_param("~local_proc_dir")
            # sentry@datapod
            self.remote_server = rospy.get_param("~remote_server")
            # "/home/sentry/disk
            self.remote_dir = rospy.get_param("~remote_dir")
        except KeyError as ex:
            # Allowing this to raise, since it only happens on launch, and if
            # it's not configured, there's no point ...
            rospy.logfatal("sentry_photoproc requires parameters!")
            raise(ex)

        try:
            os.listdir(self.local_raw_dir)
        except OSError as ex:
            # Likewise, allowing this to raise to fail on launch.
            rospy.logfatal("nonexistant local directory! {}".format(self.local_raw_dir))
            raise(ex)

        try:
            os.listdir(self.local_proc_dir)
        except OSError as ex:
            # Likewise, allowing this to raise to fail on launch.
            rospy.logfatal("nonexistant local directory! {}".format(self.local_proc_dir))
            raise(ex)

        # Setup subscribers
        self.sub = rospy.Subscriber("~image_time", std_msgs.msg.Time, self.time_callback)

        self.remote_times = np.array([])
        self.remote_images = []

    def time_callback(self, msg):
        '''
        Handles the whole pipeline from figuring out available raw images
        to copying it and processing it.

        '''
        # Only perform the remote `ls` command if we don't already have the
        # data cached. This assumes that images are created remotely in
        # temporal order, which ... seems safe.
        if len(self.remote_times) == 0 or msg.data.secs > np.max(self.remote_times):
            rospy.loginfo("Requesting newer image than we have; re-querying remote directory")
            self.remote_images = self.list_images()
            self.remote_times = np.array([get_timestamp(img) for img in self.remote_images])

        if len(self.remote_images) == 0:
            rospy.logerr("Image Sender has no remote images; cannot process request")
            return

        # If there are multiple minima, the FIRST will be returned by np.argmin
        dt = self.remote_times - msg.data.secs
        img_idx = np.argmin(np.abs(dt))
        debug_msg = "Image Sender trying to copy {}".format(self.remote_images[img_idx])
        rospy.logdebug(debug_msg)

        success = self.copy_image(self.remote_images[img_idx])
        if not success:
            rospy.logerr("Image Fetcher did not copy requested image")
            return
        rospy.loginfo("Copied image: {}".format(self.remote_images[img_idx]))

        proc_filename = make_output_filename(self.remote_times[img_idx])
        raw_filepath = "{}/{}".format(self.local_raw_dir, self.remote_images[img_idx])
        proc_filepath = "{}/{}".format(self.local_proc_dir, proc_filename)

        process_image(raw_filepath, proc_filepath)

    def list_images(self):
        # type: (None) -> List[str]
        '''
        returns list of image files on the remote directory
        '''
        remote_images = []
        ssh_cmd = ["ssh", self.remote_server, "ls {}".format(self.remote_dir)]
        try:
            result = subprocess.check_output(ssh_cmd)
            filenames = result.split('\n')
            for ff in filenames:
                if ff.startswith("sentry") and ff.endswith(".tif"):
                    remote_images.append(ff)
        except:
            pass

        return remote_images

    def copy_image(self, filename):
        '''
        Copy image from remote server to local directory, in preparation for processing.
        '''
        # TODO: Before copying image, check if that file already exists in the raw dir.
        if filename in os.listdir(self.local_raw_dir):
            rospy.logwarn("{} already copied; skipping".format(filename))
            return False

        scp_cmd = ["scp",
                   # "-l", # limit in kbit/sec; unfortunately, didn't help with datapod dropping frames.
                   # "40000", # ~5MB/sec
                   "{}:{}/{}".format(self.remote_server, self.remote_dir, filename),
                   "{}/".format(self.local_raw_dir)]
        try:
            subprocess.check_call(scp_cmd)
            return True
        except subprocess.CalledProcessError:
            return False


if __name__ == "__main__":
    os.nice(10) # Don't want this to hog CPU!
    rospy.init_node("sentry_photoproc")
    img_fetcher = ImageFetcher()
    rospy.spin()
