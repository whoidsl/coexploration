#! /usr/bin/env python3

# Copyright 2022 University of Washington Applied Physics Lab
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import copy
from dataclasses import dataclass
import rospy
#import rospy.timer
from typing import Dict

from ds_acomms_msgs.msg import ModemData
from ds_acomms_msgs.srv import ModemDataRequest
from ros_acomms.msg import QueueStatus
from codec_config_parser import QueueParams, QueueId, MessageCodecId, get_queue_params, get_message_codecs

@dataclass
class BackgroundParams:
    publish_topic: str
    service: str
    num_bytes: int
    goal_length: int


class QueueMonitor(object):
    """
    Class that monitors the current length of queues and calls the appropriate
    data service in order to keep them at the desired length, so they're ready
    for transmission.

    There's a potential timing issue, where in a message callback based system
    we could be processing the next queue node status *before* the just-published
    messages have had a chance to propagate into the published queue status.

    This implementation attempts to get around the problem by running at
    a fixed rate, rather than for every message received.
    """
    def __init__(self) -> None:

        self.setup_params()

        self.queue_status = None
        self.status_sub = rospy.Subscriber("queue_status", QueueStatus,
                                        self.queue_status_callback, queue_size=1)
        self.timer = rospy.Timer(rospy.Duration(self.timer_period), self.fill_queues)


    def setup_params(self) -> None:
        self.timer_period = rospy.get_param("~timer_period", 1.0)

        subsea_codec_config = rospy.get_param("~codec_config")
        self.queue_params = get_queue_params(subsea_codec_config[0])
        self.id_from_topic = {pp.subscribe_topic: queue_id for queue_id, pp in self.queue_params.items()}
        print(self.id_from_topic)

        self.publishers: Dict[QueueId, rospy.Publisher] = {}
        self.service_proxies: Dict[QueueId, rospy.ServiceProxy] = {}
        self.background_params: Dict[QueueId, BackgroundParams] = {}

        background_data_config = rospy.get_param("~background_data")
        for data_params in background_data_config:
            publish_topic = data_params['publish_topic']
            try:
                queue_id = self.id_from_topic[publish_topic]
            except Exception as ex:
                rospy.logwarn("Could not find queue for topic: {}".format(publish_topic))
                continue
            service_name = data_params['service']
            self.background_params[queue_id] = BackgroundParams(publish_topic,
                                                                service_name,
                                                                data_params['num_bytes'],
                                                                data_params['goal_length'])
            self.publishers[queue_id] = rospy.Publisher(publish_topic, ModemData, queue_size=10)
            self.service_proxies[queue_id] = rospy.ServiceProxy(service_name, ModemDataRequest)


    def queue_status_callback(self, msg: QueueStatus) -> None:
        self.queue_status = msg

    def fill_queues(self, _event: rospy.timer.TimerEvent) -> None:
        if self.queue_status is None:
            return
        msg = copy.copy(self.queue_status)  # Don't want it changing under us while we run the following slow code
        for queue_summary in msg.queue_summaries:
            queue_id = queue_summary.queue_id
            queue_length = queue_summary.message_count
            # Figure out desired length
            # If there are slots available, call associated service call N times and publish the data

            # Ignore all queues that aren't in our list of background data.
            if queue_id not in self.background_params:
                continue
            params = self.background_params[queue_id]

            empty_slots = params.goal_length - queue_length

            if empty_slots > 0:
                # rospy.logwarn("Queue {} has {} / {} messages".format(queue_id, queue_length, params.goal_length))
                # rospy.loginfo("Trying to fill {} slots with {}".format(empty_slots, params.publish_topic))
                try:
                    rospy.wait_for_service(params.service, timeout=0.5)
                except rospy.ROSException:
                    rospy.logwarn_throttle_identical(300, "queue {} timed out (monitoring topic {}".format(params.service, params.publish_topic))
                    continue


                for _ in range(empty_slots):
                    msg = ModemData()
                    try:
                        background_data = self.service_proxies[queue_id](self.background_params[queue_id].num_bytes)
                    except Exception as ex:
                        errmsg = ("Could not call service {}\n{}"
                                  .format(self.background_params[queue_id].service, ex))
                        rospy.logwarn_throttle_identical(300, errmsg)
                        break
                    try:
                        msg.payload = background_data.data
                    except rospy.ServiceException as ex:
                        rospy.logwarn("service {} did not process request: {}".format(queue_service, ex))
                        break
                    # rospy.loginfo("Publishing {} bytes!".format(len(msg.payload)))
                    self.publishers[queue_id].publish(msg)




if __name__ == "__main__":
    rospy.init_node("ros_acomms_queue_monitor")
    qm = QueueMonitor()
    rospy.spin()
