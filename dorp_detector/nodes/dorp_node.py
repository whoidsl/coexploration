#!/usr/bin/env python3

# Copyright 2021 University of Washington Applied Physics Laboratory
# Author: Laura Lindzey
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Node that subscribes to ORP data and publishes peaks.

TODO:
* add tests for calc_dorp_dt and find_peaks that check they handle not enough data / data that violates assumptions / floating point errors. (We can trust the data types in the subscribed messages, but can't trust that they have valid data or that they arrive in order.)
* Add warning if orp data comes in out-of-order? (Or with a bad timestamp?) ... I worry about adding warnings in cases that won't ever be exercised.
* Go back to the analysis notebook, and generate plots showing expected output every 30 mins in the dive.
"""


import numpy as np
import rospy
import threading

import coexploration_msgs.msg  # for DorpHits
import ds_hotel_msgs.msg  # for A2D2
import ds_sensor_msgs.msg  # for DepthPressure

import dorp_detector
import dorp_detector.orp_utils as orp_utils


class DorpDetector(object):
    def __init__(self):
        # Data volumes are so low that this will be ~5 MB for a 10 hour dive.
        # So, it is fine to just cache all data in memory.
        self.orp_data = None  # Nx2 np.array, cols are [time, raw orp]
        # As I understand it, rospy runs Subscriber callbacks and Timer
        # callbacks in separate threads.
        self.orp_lock = threading.Lock()

        # How many peaks of dORP data to publish?
        self.num_pub_peaks = rospy.get_param("~num_pub_peaks", default=10)

        # A dORP/dt value is only considered a hit if
        # dorp/dt < -detection_factor * stddev(dorp/dt)
        self.detection_factor = rospy.get_param("~detection_factor", default=5)

        # Period (in seconds) for publishing the top N hits
        self.pub_period = rospy.get_param("~pub_period", default=5)

        self.submerged = False  # Whether the vehicle is deeper than the threshold
        self.depth_count = 0  # How many consecutive messages have been deeper than threshold
        # How deep we must be before processing ORP data.
        self.depth_threshold = rospy.get_param("~depth_threshold", default=100)

        # Use paro because it has a depth field, while CTD simply has pressure.
        self.depth_sub = rospy.Subscriber("~paro_data",
                                          ds_sensor_msgs.msg.DepthPressure,
                                          self.depth_callback)
        self.orp_sub = rospy.Subscriber("~orp_data",
                                        ds_hotel_msgs.msg.A2D2,
                                        self.orp_callback)
        self.dorp_pub = rospy.Publisher("~dorp_peaks",
                                        coexploration_msgs.msg.DorpHits,
                                        queue_size=1)
        self.dorp_timer = rospy.Timer(rospy.Duration(self.pub_period), self.pub_callback)


    def depth_callback(self, msg):
        """
        Monitor vehicle depth; when deep enough for other sensors to be valid,
        set flag and unsubscribe.
        """
        if msg.depth > self.depth_threshold:
            rospy.loginfo("Depth callback! depth = {}".format(msg.depth))
            self.depth_count += 1
        if self.depth_count > 5:
            rospy.loginfo("Received 5 depth messages above threshold! Unregistering.")
            self.submerged = True
            self.depth_sub.unregister()

    def orp_callback(self, msg):
        """
        Append new ORP data to the array.
        """
        # Ignore data while still on deck and at the start of the descent
        if not self.submerged:
            return

        with self.orp_lock:
            # ORP only uses the first channel of the 4 available.
            if self.orp_data is None:
                self.orp_data = np.array([[msg.header.stamp.to_sec(), msg.raw[0]]])
            else:
                new_orp = [[msg.header.stamp.to_sec(), msg.raw[0]]]
                self.orp_data = np.append(self.orp_data, new_orp, axis=0)

    def pub_callback(self, _):
        """
        Periodically calculate dORP/dt, and publish the top N hits.
        The actual calculation is fast enough that there's no reason
        to try to cache previous computations; just re-run on the whole
        data set each time.
        """
        if self.orp_data is None:
            return
        # Ideally, we'll catch any errors before this. However, I'm always worried
        # about math errors, and we don't want calculation of dorp to cause the rest
        # of the node to fail.
        with self.orp_lock:
            try:
                dorpdt = orp_utils.calc_dorp_dt(self.orp_data)
            except Exception as ex:  # Tested error handling by throwing Exception
                rospy.logerr("Exception in orp_utils.calc_dorp_dt: {}".format(ex))
                return
        if dorpdt is None:
            # Error message will be logged by orp_utils
            return
        clustered_peaks = orp_utils.find_clustered_peaks(dorpdt, self.detection_factor)
        if len(clustered_peaks) == 0:
            return
        peaks = orp_utils.select_peaks(clustered_peaks, self.num_pub_peaks)
        if peaks is None:
            return

        # TODO: Don't publish if peaks message is empty?
        # However, in that case, the topside operator won't know what's going on.
        # I kind of think it would be better to publish the N most negative
        # extrema along with the stddev, just to differentiate working-but-boring
        # from panic-cuz-its-broken.

        msg = coexploration_msgs.msg.DorpHits()
        try:
            npeaks, _ = peaks.shape
        except Exception as ex:  # Tested this error handling with replayed data
            rospy.logerr("Peaks ({}) is not a 2d ndarray! Exception: {}".format(peaks, ex))
            return

        for idx in np.arange(npeaks):
            stamp = rospy.Time.from_seconds(peaks[idx,0])
            msg.stamps.append(stamp)
            msg.dorps.append(peaks[idx,1])
        self.dorp_pub.publish(msg)




if __name__ == "__main__":
    rospy.init_node("dorp_detector")
    dd = DorpDetector()
    rospy.spin()
