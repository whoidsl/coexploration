/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

//
// Created by llindzey on Dec 13, 2019.
//

#include "quadtree/quadtree_receiver_ros.h"

namespace quadtree {

QuadTreeReceiverNode::QuadTreeReceiverNode() {
  setup();
}

QuadTreeReceiverNode::~QuadTreeReceiverNode() = default;

void QuadTreeReceiverNode::setupParameters() {
  auto nh = ros::NodeHandle("~");
  if (!nh.getParam("grid_directory", grid_directory_)) {
    ROS_FATAL("Unable to find parameter grid_directory");
    ROS_BREAK();
  }
  boost::filesystem::create_directories(grid_directory_);
}

void QuadTreeReceiverNode::setupPublishers() {
  auto nh = ros::NodeHandle("~");
  const int queue_size = 1;
  saved_grids_pub_ = nh.advertise<std_msgs::Empty>("saved_grids", queue_size,
						   false);
  if (!saved_grids_pub_) {
    ROS_FATAL("Unable to create publisher. Exiting.");
    ROS_BREAK();
  }
}

void QuadTreeReceiverNode::setupSubscriptions() {
  auto nh = ros::NodeHandle("~");
  metadata_sub_ = nh.subscribe("metadata", 10,
			       &QuadTreeReceiverNode::handleMetadata, this);

  subgrid_sub_ = nh.subscribe("subgrids", 10,
			      &QuadTreeReceiverNode::handleSubgrid, this);

  region_sub_ = nh.subscribe("requested_region", 1,
			     &QuadTreeReceiverNode::handleRegionRequest, this);
}

void QuadTreeReceiverNode::setupServices() {
  auto nh = ros::NodeHandle("~");
  auto data_cb = boost::bind(&QuadTreeReceiverNode::dataRequestCallback, this, _1, _2);
  data_server_ =
      nh.advertiseService<ds_acomms_msgs::ModemDataRequest::Request,
                          ds_acomms_msgs::ModemDataRequest::Response>(
          "data_request", data_cb);
}

void QuadTreeReceiverNode::setupTimers() {
  auto nh = ros::NodeHandle("~");
  bool oneshot = true;
  bool autostart = false;
  save_grids_timer_ = nh.createTimer(ros::Duration(1.0),
                                     &QuadTreeReceiverNode::saveGrids,
                                     this, oneshot, autostart);
}

void QuadTreeReceiverNode::setup() {
  setupParameters();
  setupSubscriptions();
  setupPublishers();
  setupTimers();
  setupServices();
}

void QuadTreeReceiverNode::handleMetadata(
    const coexploration_msgs::QuadtreeMetadata& msg) {
  if (tree_ == nullptr) {
    ROS_INFO("Got first metadata!");
    tree_ = std::make_unique<quadtree::QuadTreeReceiver> (
        msg.horizontal_resolution, msg.vertical_resolution, msg.send_levels);
    tree_->setLogMessageCallback("fatal", RosFatalMessageCallback);
    tree_->setLogMessageCallback("error", RosErrorMessageCallback);
    tree_->setLogMessageCallback("warn", RosWarnMessageCallback);
    tree_->setLogMessageCallback("info", RosInfoMessageCallback);
    tree_->setLogMessageCallback("debug", RosDebugMessageCallback);
    ROS_INFO("Constructed Tree!");
  }
}

void QuadTreeReceiverNode::handleSubgrid(
    const ds_acomms_msgs::ModemData& msg) {
  if (tree_ == nullptr) {
    ROS_WARN("Can't update tree before receiving metadata!");
    return;
  }
  // ROS_INFO("handleSubgrid");
  quadtree::Subgrid subgrid;
  bool success = deserializeSubgrid(msg.payload, &subgrid);

  if (success) {
    // want to save grids a little after the last update comes in.
    save_grids_timer_.stop();
    tree_->AddSubgrid(subgrid);
    save_grids_timer_.start();
  } else {
    ROS_WARN("Couldn't deserialize subgrid!");
  }
}

void QuadTreeReceiverNode::saveGrids(const ros::TimerEvent&) {
  if (!tree_) {
    // ROS_INFO("No tree yet; can't save subgrids!");
    return;
  }
  ROS_INFO("SaveGrids");
  int min_save_level = 0;
  int max_save_level = 10;
  tree_->SaveGrids(grid_directory_, min_save_level, max_save_level);

  std_msgs::Empty saved_msg;
  saved_grids_pub_.publish(saved_msg);
}

void QuadTreeReceiverNode::handleRegionRequest(
    const coexploration_msgs::QuadtreeRegion& msg) {
  if (tree_ == nullptr) {
    ROS_INFO("Can't handle Region Request -- don't have tree yet!");
    return;
  }
  ROS_INFO("Got Region Request");
  // Needs to be a list of x, y, level, timestamp
  int subgrid_level = (int)msg.level + tree_->send_levels() - 1;
  float step = powf(2.0, subgrid_level);
  float nodex, nodey;
  for (float xx = msg.x0; xx <= msg.x0 + msg.dx; xx += step) {
    for (float yy = msg.y0; yy <= msg.y0 + msg.dy; yy += step) {
      auto node = tree_->GetNode(xx, yy, msg.level, &nodex, &nodey);
      if (node) {
	requests_.emplace(nodex, nodey, subgrid_level, node->updated_time);
      } else {
	requests_.emplace(xx, yy, subgrid_level, 0);
      }
      ROS_INFO_STREAM("... adding level " << subgrid_level << " subgrid at " << xx << ", " << yy);
    }
  }
}

// NB: This implementation is very inefficient; it only fits one request per set.
//     Better to serialize where the first byte is the number of requests,
//     then the rest are the data.
bool QuadTreeReceiverNode::dataRequestCallback(
    const ds_acomms_msgs::ModemDataRequest::Request& request,
    ds_acomms_msgs::ModemDataRequest::Response& response) {
  // ROS_INFO("dataRequestCallbacK");
  // How many bytes required to represent the request.
  // TODO: These constants should be handled by whatever handles serialization
  int data_length = 1 + 3*4;  // int8 + 3x int32

  int num_requests = (int) std::floor((1.0*request.bytes_requested - 1) / data_length);
  // ROS_INFO_STREAM("Can fit " << num_requests << " requests into " << request.bytes_requested << " bytes.");

  num_requests = std::min(num_requests, (int) requests_.size());

  if (num_requests <= 0) {
    // ROS_INFO_STREAM("Unable to fit any grid requests into " << request.bytes_requested << " bytes");
    return false;
  }

  serialize8(num_requests, &response.data);

  for (int ii = 0; ii < num_requests; ii++) {
    quadtree::GridRequest next_request = requests_.front();
    bool success = serializeRequest(next_request, &response.data);
    requests_.pop();
    if (!success) {
      return false;
    }
  }
  return true;
}

}  // namespace quadtree
