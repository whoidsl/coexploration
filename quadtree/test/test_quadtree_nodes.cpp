/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Dec 13, 2019.
//

#include <gtest/gtest.h>
#include <quadtree/quadtree.h>
#include <quadtree/quadtree_sender_ros.h>
#include <quadtree/quadtree_receiver_ros.h>


#include <coexploration_msgs/QuadtreeMetadata.h>
#include <std_msgs/Empty.h>

// Test node-level functionality.
// So far, it has been easier to just set up the conditions and directly
// call the appropriate ROS callbacks. This probably isn't quite as good as
// setting up a full rostest, since it leaves out some of the pipeline, but
// it at least lets me reproduce bugs found via interactive testing.

namespace quadtree {

class QuadTreeSenderNodeTest : public ::testing::Test {
 protected:
  std::unique_ptr<QuadTreeSenderNode> sender_node;
  std::unique_ptr<ros::NodeHandle> nh;
  void SetUp() override {
    sender_node = std::make_unique<QuadTreeSenderNode>();
    nh = std::make_unique<ros::NodeHandle>("~");
    // I dislike default parameters, because it's too easy to make a typo
    // in a launch file and never realize that the parameter you're setting
    // isn't having any effect. So ... set them for testing without a launch
    // file.
    nh->setParam("horizontal_resolution", 1.0);
    nh->setParam("vertical_resolution", 1.0);
    nh->setParam("min_stream_level", 3);
    nh->setParam("max_stream_level", 7);
    nh->setParam("send_levels", 3);  // 4x4
    // TODO: This is specific to LEL's computer; consider checking a test
    //       grid into the repo?
    nh->setParam("grid_filename", "/home/llindzey/Documents/NOAAcoexploration/multibeam/sentry546/sentry536_20190930_0357_rnv_tide_1.00x1.00_BV02.grd");
  }
};

TEST_F(QuadTreeSenderNodeTest, test_save_large_grid) {
  sender_node->setup();
  sender_node->tree_->SaveGrids("large_grids", 0, 12);
}

TEST_F(QuadTreeSenderNodeTest, test_request_data_no_tree) {
  // Test that calling the dataRequestCallback before getting any data fails
  // gracefully.
  // TODO: Update this test once I implement map initialization ... it may
  //       no longer be valid for auto-loading from file. (It may also need
  //       to be moved to a ROS test, if map loading is controlled by rosparam)
  // TODO: figure out how to get the whole test class as a friend class,
  //       rather than just this single test.
  sender_node->setup();
  ds_acomms_msgs::ModemDataRequest::Request request;
  ds_acomms_msgs::ModemDataRequest::Response response;
  request.bytes_requested = 240;
  bool success = sender_node->dataRequestCallback(request, response);
  ASSERT_FALSE(success);
}

// TODO:
// * Test smaller/larger bytes_requested

class QuadTreeReceiverNodeTest : public ::testing::Test {
 protected:
  std::unique_ptr<QuadTreeReceiverNode> receiver_node;
  std::unique_ptr<ros::NodeHandle> nh;
  void SetUp() override {
    receiver_node = std::make_unique<QuadTreeReceiverNode>();
    nh = std::make_unique<ros::NodeHandle>("~");
    // TODO: Make this something in /tmp? Or
    nh->setParam("grid_directory", "/home/llindzey/Documents/NOAAcoexploration/multibeam/rx_grids");
  }
};

TEST_F(QuadTreeReceiverNodeTest, test_request_grids) {
  // One bug found during development was requesting grids before the tree
  // had valid data in it, or requesting a level that hadn't yet been populated.
  receiver_node->setup();

  coexploration_msgs::QuadtreeMetadata metadata_message;
  metadata_message.horizontal_resolution = 1.0;
  metadata_message.vertical_resolution = 1.0;
  metadata_message.send_levels = 3;
  receiver_node->handleMetadata(metadata_message);

  // Using actual values from failing system test...
  Subgrid subgrid;
  subgrid.tree_level = 13;
  subgrid.tree_xx = 1461;
  subgrid.tree_yy = 2017;
  subgrid.level = 7;
  subgrid.xx = 885;
  subgrid.yy = 2209;
  subgrid.timestamp = 2;
  for (int ii=0; ii < pow(2, pow(2, metadata_message.send_levels - 1)); ii++) {
    subgrid.data.push_back(0);
  }
  receiver_node->tree_->AddSubgrid(subgrid);

  std_msgs::Empty empty_msg;
  receiver_node->handleSaveGrids(empty_msg);
}

}  // namespace quadtree

int main(int argc, char** argv) {
  ros::init(argc, argv, "test_quadtree_nodes");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
