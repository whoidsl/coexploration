/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Jan 24, 2020.
//

#ifndef DS_QUADTREE_QUADTREE_SENDER_H
#define DS_QUADTREE_QUADTREE_SENDER_H

#include <quadtree/quadtree.h>

namespace quadtree {

/**
    @brief Quadtree implementation specialized for sending acoustic updates.

    This QuadTree implementation was originally designed for representing
    a multibeam grid of the sea floor and sending incremental acoustic updates
    to the scientists monitoring the robot topside.

    It was designed for use with the Micromodem2, which imposes constraints:
    * maximum message size of 256 bytes (per frame)
    * messages are often dropped

    A quadtree representation was chosen to support non-compact surveys
    (Sentry will often perform patch surveys with transits in between)
    and because it naturally represents the map at varying resolutions.

    Each node tracks the mean elevation of all points that are within the
    node's extent. We use mean for this because it is easy to incrementally
    update, and  will be good enough for approximating elevations. If this
    was meant to be a final bathymetric product (rather than a mid-dive
    representation) a different metric would be more appropriate.

    The leaf nodes are 1x1 cells horizontal resolution, so the full extent
    of space in the tree is horizontal_resolution * 2 ^ (num_levels - 1)
*/
class QuadTreeSender : public QuadTree {
  friend class QuadTreeInternalsTest_AddPoint_Test;

 public:
  /**
     @brief Construct a QuadTreeSender.

     None of these parameters can be changed after construction,
     since the acoustic updates assume that these are constants.

     @param h_res horizontal resolution controlling cell size (meters)
     @param v_res vertical resolution for stored elevations (meters)
     @param send_levels how many levels to include in each transmitted subgrid
     @param min_level minimum level of tree that will be streamed
     @param max_level maximum level of tree that will be streamed
  */
  explicit QuadTreeSender(float h_res, float v_res, int send_levels,
                          int min_level, int max_level, int32_t send_delay);

  /**
     @brief Add single point to the quadtree;

     Adding a point will cause the tree to grow if necessary,
     and then creates any intermediate nodes required to provide
     a path to the leaf.

     This is the only place in the class that world-frame units
     are used. They are immediately converted to cell units
     (this is a scaling - no offsets are applied).

     Timestamps are passed in to avoid a dependence on ROS.

     @todo This does NOT check tree size before growing it; consider
           implementing a check on the maximum number of nodes in the tree
           and refusing updates that would violate that.

     @param xx input point's X coordinate, in world frame and coordinates
     @param yy
     @param zz
     @param tt posix timestamp for input point
  */
  void AddPoint(float xx, float yy, float zz, int32_t tt);

  /**
     @brief Add levels to the tree until the input point is in the span

     If the cell is outside the span of the tree given the current root
     node and number of levels, create a new root node and push existing
     tree down into one of new root's children.

     It is ambiguous which child the root should be, and no attempt is made
     to be clever. This uses the heuristic that if the new point is in the
     upper right quadrant of the existing center, then the old tree is pushed
     into the lower left childe. (And likewise all other quadrants.)

     This is only used for adding a new point to the tree.
     Updating a tree with a subgrid uses a different method, since
     the receiver needs to guarantee topological consistency with
     the sender.

     @param xc x coordinate of new point (cell units)
     @param yc y coordinate of new point (cell units)
  */
  void GrowTree(float xc, float yc);

  /**
      @brief Return next subgrid for acoustic transmission.

      First sends any subgrids that the user has specifically requested.

      After that queue is empty, "stream" data at increasingly higher
      resolution. Priority is based on level, and then on age.
      Subgrids will be re-sent if updated with new data.

      @param small If true, choose the highest-priority subgrid with 8-bit range
      @param timestamp seconds since the epoch; passed in to avoid ROS
     dependency
   */
  std::unique_ptr<Subgrid> GetNextSubgrid(const bool small,
                                          const int32_t timestamp);

  /**
     @brief Append new grid requests to the priority list.

     For now, the requests are treated as a FIFO queue. However, there is
     concern that LIFO may be more intuitive for the topside users.
     (would you rather have to send your priorities in reverse order but
     not worry about a delay, or have to clear the queue?)

     @param requests List of requests, appended in-order
  */
  void AddPriorityRequests(const std::list<GridRequest>& requests);

  /**
      @brief clear list of priorities

      The quadtree uses a FIFO queue for the prioritized list of user-requested
      grids. This allows the user to clear the list if the subsea node can't
      keep up with requests and has accumulated a delay/queue.
  */
  void ClearPriorities();

  int num_sent() { return sent_subgrid_count_; };

 private:
  /**
      @brief Insert the given data into the tree, updating all nodes on the
     path.

      @param xc x coordinate of new point (cell units)
      @param yc y coordinate of new point (cell units)
      @param zc z coordinate of new point (map units)
      @param tt posix timestamp for point (passed in to avoid ROS dependency)
  */
  void UpdateTree(float xc, float yc, float zc, int32_t tt);

  /**
      @brief Use user-specified priorities to determine next subgrid to send

      @param small If true, choose the highest-priority subgrid with 8-bit range
      @param timestamp seconds since the epoch; passed in to avoid ROS
     dependency
   */
  std::unique_ptr<Subgrid> GetNextRequestedSubgrid(const bool small,
                                                   const int32_t timestamp);

  /**
      @brief Determine which subgrid to send next in the absense of priorities

      For now, this uses a simple heuristic on what to send:
      * Send the higher-level nodes first
      * With the longest gap since being sent
        (ties broken by longest gap since being updated)
      * That has new data since it was last streamed
        Each subgrid is only streamed once: if the packet is dropped,
        it is up to the topside operator to request that it be re-sent

        Obviously, there's a lot of room for improvement here. For example:
        - require minimum fraction of grid to be occupied (don't waste bandwidth
          on the ragged edges of a multibeam survey)
        - track some sense of "information change" since the last update, rather
          than a binary "updated"
        - ...

      @param small If true, choose the highest-priority subgrid with 8-bit range
      @param timestamp seconds since the epoch; passed in to avoid ROS
     dependency
   */
  std::unique_ptr<Subgrid> GetNextStreamedSubgrid(const bool small,
                                                  const int32_t timestamp);

  /**
      @brief Returns the subgrid coresponding to the input node.

      Helper function shared by GetNext{Requested,Streamed}Subgrid.
      Additional arguments are passed through to populate the returned subgrid.

      @param node
      @param level
      @param cx
      @param cy
  */
  std::unique_ptr<Subgrid> GetSubgrid(Node* node, int level, float cx,
                                      float cy);

  /**
      @brief whether subgrid rooted at input node will fit in smaller type.

      In order to compress the subgrids, we send them as an offset and deltas.
      If all of the deltas require more than 8 bits to be represented, we
      can save 64 bytes in the update encoding.
  */
  bool SubgridIsSmall(Node* node);

  /**
      @brief Highest-resolution level in the tree that will be streamed.

      The minimum resolution should be chosen based on the expected
      acoustic bandwidth and terrain coverage rate.
      Size of cell = 2 ^ (level - 1)
  */
  int min_streaming_level_;

  /**
      @brief Lowest-resolution level in the tree that will be streamed.
  */
  int max_streaming_level_;

  /**
      @brief Required time delay between subgrid update and transmission.

      This can be tuned to prevent the same subgrids from being resent
      repeatedly as they are actively updated.
  */
  int32_t send_delay_;


  /**
      @brief List of subgrids that have been requested from topside.
  */
  std::list<GridRequest> requested_grids_;

  int sent_subgrid_count_;

};  // class QuadTreeSender

}  // namespace quadtree

#endif  // DS_QUADTREE_QUADTREE_SENDER_H
