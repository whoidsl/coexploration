### Overview

comms_manager is an acoustic API for ROS, enabling arbitrary messages to
be transmitted and rebroadcast through an acoustic modem.
The operator selects a TDMA schedule for topside/subsea modems, as well
as which messages should be relayed and at what frequency.

As many messages as possible will be packed into each frame, going in 
priority order and with no attempt to solve the knapsack problem.
This will result in unused bytes in a packet if the next message is
too large or if all requested messages have been encoded.

In addition to being able to relay topics, the spare bytes in each
packet can be used to stream background data. Any background data provider
is expected to adhere to the same interface: a service call requesting
a given number of bytes. Progressive Imagery uses this to transmit images,
and Quadtree to send maps.

ROS topic names are too long to include in the encoded data, so we rely
on a lookup table that is shared by all comms managers to encode topic
name and message type in a single byte.
If more than 256 topics are needed, it would be relatively straightforward
to modify the comms manager to use two bytes for this mapping.
The lookup table should be checked into the repo, and the full path to
it will be a parameter, which is logged in Sentry's setup. For backwards
compatability of rosbags, treat the files as write-only, and create a new
one any time the lookup table needs to change.

![Alt text](design_docs/comms_manager.png)

The comms_manager package contains both the communications manager
itself and the various developmental guis used to interface with it:

* **comms_manager.py** - Runs both topside and subsea, creates appropriate
                       subscriptions and bundles the data into a modem message.
* **tdma_gui.py** - Runs topside, allows changing the assigned topside/subsea
                TDMA slots.
* **queue_gui.py** - Runs topside, allows the user to specify what topics
                     should be sent up by the subsea comms manager.
* **generate_topic_lookup.py** - script to generate the lookup table, utility 
                     that will run manually.
* **comms_echoer.py** - utility monitoring tool to echo all received messages
                    to the terminal; run manually on the command line.
* **mock_micromodem.py** - alternative to using a hardware test box, forwards
                       micromodem messages between two ROS networks over UDP.
		               NYI: probabilistic dropping of frames/packets.

### Setup

The first step in using the comms_manager is to generate a lookup table that 
maps a one byte code to a topic name / message type pair. 
The table is generated from a set of existing bag files, along with optional
configuration files. This process is more cumbersome than I would like, but
I didn't come up with anything better that will synchronize a lookup table
across multiple independent `rosmaster`s.

Inputs:

* directory containing bag files logged from your running system. Any message
  in one of the bag files will be included in the lookup table.
* list of bytes that cannot be used as the code for a topic. This is useful if
  the modem is being shared with a legacy system that uses the first byte of data
  in a frame to route message types. For example, when running in parallel with 
  Sentry's existing system, JTALK uses 0x64 and 0x66 as the first byte, and ignores
  any other values. NUI also had some reserved bytes.
* list of topic prefixes to ignore. The comms_manager will optionally prepend
  a namespace to indicate topics that it's listening to / republishing, which is
  useful for enabling isolation between topside/subsea when running both in
  simulation on the same computer and for clarity when replaying bags
  after a dive.
* Config file with topics that need to be included even if they're not in the
  input bag files. This is required for boostrapping, and to guarantee that
  topics used for controlling the comms manager are represented.
  [Example file for Sentry](https://bitbucket.org/whoidsl/coexploration/src/master/comms_manager/src/comms_manager/sentry_coex_topics.txt) -- first column is topic name, second is message type.
* Config file with background data topics. (There isn't a way to infer these
  pairings from a bagfile.)
  [Example file for Sentry](https://bitbucket.org/whoidsl/coexploration/src/master/comms_manager/src/comms_manager/sentry_coex_services.txt)
  -- first column is the service name, and second column is the topic that the resulting data will be published to.

Example command:
```
cd <workspace>/src/coexploration/comms_manager/src/comms_manager/
./generate_topic_lookup.py --bagdir /home/llindzey/SentryData/2019-valentine/dives/sentry547/nav-sci/raw/rosbag --skip_bytes 0x64 0x66 --skip_prefixes /transmit /republished --topics_file sentry_coex_topics.txt --services_file sentry_coex_services.txt --output test.txt
```

I don't love the dependencies between this and the overall launch file:

* `--skip-prefixes` needs to match the "publication_prefix" and "subscription_prefix" parameters used when launching the topside comms_manager.
* `--topics_file` and `--services_file` need to match the topics that are arguments in the launch file.
However, I haven't found a cleaner way around the lookup table needing to be generated before the launch file is run...

### TDMA Configuration

On Sentry, the modem drivers will attempt to transmit data as soon as the message is received; the communications manager is responsible for ensuring that topside and subsea adhere to a TDMA schedule.

![Alt text](design_docs/tdma_gui.png)

GUI for setting the TDMA slots.

There are separate editable text panes for topside and subsea, and a selection of preset configurations that are useful for testing.

Before transmitting the new configuration, the user must enter a slant range to the vehicle and click "Update".
This will show when each modem is allowed to transmit, as well as the time periods when the data will be in-flight.
The graphical representation does not include any safety margin -- if gaps are not clear, then the TDMA configuration is probably too risky.
Additionally, note that the slant-range based margins aren't programmatically enforced. 
The user is required to look at the timing plot, but is allowed to ignore it.
After "Update" has been clicked, "Publish" will be enabled. Click it to actually publish the ROS messages that control the comms_managers.

### Topic Configuration and Background Data


![Alt text](design_docs/queue_gui.png)

The Comms Manager GUI allows the user to specify which topics should be relayed, and at what frequency:

* Use the Subsea tab to configure what should be sent subsea -> topside, and the Topside tab for topside -> subsea.
* Choose a topic from the dropdown menu
* Choose the corresponding period; this will be in units of transmit cycles. 
  So, a value of "1" indicates to include the most recent message of that topic with every packet,
  while "5" would include it in every 5-th packet. 
* Click "Add"
* When you're happy with the set of topics, click "Publish"

Each received message is encoded and transmitted at most once: 
if messages are coming in slower than the requested encoding rate, that topic will be skipped rather than publishing repeats.

Default topics can be set in the launch file using the `default_topside_topics` and `default_subsea_topics` arguments.

Additionally, there are radio buttons along the top to select what background data to stream.
The background data servers may have associated messages that should be streamed 
(e.g. metadata, updates to request different data, etc.); these messages will be automatically added to the list of topics.

If you want to add additional services that use the background data mechanism to transfer data,
modify the `background_data_modes` argument to configure the names that should appear in the GUI, 
along with what their services and topics are.


### Known Limitations

* If a message can't be encoded into a single frame, it is dropped.
  So far, this hasn't been a significant limitation at rate 5 (however, it is easy for a queue definition to overflow a rate1 packet)
* Does not implement a remote procedure call interface. This would be a fairly straightforward extension.
* Does not attempt to compress the messages. Instead, it uses ROS's default encoding that dramatically 
  inflates the message size (we saw 3x for some messages), which is obviously suboptimal.
  Implementing optional annotations for desired precision in the message files is on the wishlist (a la goby), 
  but message compression hasn't been a limiting factor yet.
* Does not implement replay protection, which could become important in a more complicated network. 
  Philosophically, I think this belongs in the modules that subscribe to the messages rather than the 
  module that is relaying received data. Any data that is time-critifffcal should be transmitted with a corresponding timestamp.
* Assumes that there will be <= 256 individual topics in the system. If this becomes a limiting factor,
  it is easy to modify comms_manager to use two bytes for the codes. (Only subtlety is in properly handling reserved bytes.)


### Automated Testing

The tests are split into unit tests and rostests.
They are both triggered by `catkin_make run_tests_comms_manager`.

To run unit tests standalone:

`$ ./unittest_qm.py`

To run the ROS tests standalone:
(NB -- using roslaunch prevents the test node from starting)
`$ roslaunch rostest.test`
This can be run with unittest command line arguments.
e.g. `-v -f` to forces quit at first failure.
`$ ./rostest_qm.py`


### Code Quality

Additionally, I've been trying out some Python development tools with this.

1) Coverage checking using coverage.py

This will check which lines of code are actually executed by a suite of unit tests.
It is limited in that it can't guarantee that all logical paths have been followed,
but for mission-critical python code I like to at least make sure that all of my 
error message have been exercised lest a badly formatted attempt to handle an error
wind up bringing down the whole program.

`coverage run --source=comms_manager unittest_qm.py`

`coverage html`

(open htmlcov/index.html in browser)

This is informative, but doesn't exercise the ROS interfaces.
There are a few places where coverage checking shows that I haven't
tested the error handling.

2) MyPy type checking

`mypy --py2 --check-untyped-defs --ignore-missing-imports comms_manager.py`

MyPy doesn't support a single-char datatype, so I'm using bytes for that
(str would match the python2 implementation, but not the intention)


3) Linting

Pylint doesn't really support < 3.4, so use flake8

`python -m pip install flake8`

`flake8 src/comms_manager/`


### Python 3

Unfortunately, kinetic only supports python2, so we're stuck with that,
and building Melodic from source on 18.04 isn't yet straightforward
enough to be worth doing just to test this code.

So, I have started trying to make the code python3-compatible, using
[six](https://six.readthedocs.io/) and [future](https://docs.python.org/2/library/__future__.html):

- `from __future__ import print_function, division`
  - Forces `print(...)` syntax, rather than `print ...`
  - Forces `/` to always mean floating point division; use `//` for floor
- use `six.iteritems(dd)` rather than `dd.iteritems()` or `dd.items()`

Known outstanding issues:

- bytes representation -- Maybe also be explicit about b''? (and import unicode_literals from __future__)
- Relatedly, StringIO has changed; look at BytesIO
