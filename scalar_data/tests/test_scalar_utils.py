#! /usr/bin/env python3


import sys, os
sys.path = [os.path.abspath(os.path.dirname(__file__))] + sys.path

import numpy as np
import unittest

import rospy
import coexploration_msgs.msg
import ds_hotel_msgs.msg
import ds_sensor_msgs.msg

# TODO: Really, the utils that I want to test should be in src/scalar_data, then
#   imported by both the node and the test code.
from ..nodes import scalar_sender

class ValidateParamsTest(unittest.TestCase):
    """
    Test parameter validation.

    Note: I don't love that these tests introduce a dependency on the
          ds_msgs package.
    """
    def test_orp_data(self):
        """ Test handling of data that indexes into a list."""
        msg_type = "ds_hotel_msgs/A2D2"
        data_field = "raw"
        data_index = 0
        max_length = None
        try:
            _ = scalar_sender.validate_params(msg_type, data_field, data_index, max_length)
        except Exception:
            self.fail("Should not raise exception!")

    def test_ctd_data(self):
        """ Test simplest case -- no nesting, no indexing."""
        msg_type = "ds_sensor_msgs/Ctd"
        data_field = "temperature"
        data_index = None
        max_length = None
        try:
            _ = scalar_sender.validate_params(msg_type, data_field, data_index, max_length)
        except Exception:
            self.fail("Should not raise exception!")

    def test_dvl_data(self):
        """ Test nested field."""
        msg_type = "ds_sensor_msgs/Dvl"
        data_field = "velocity.x"
        data_index = None
        max_length = None
        try:
            _ = scalar_sender.validate_params(msg_type, data_field, data_index, max_length)
        except Exception:
            self.fail("Should not raise exception!")

    def test_nonexistant_package_name(self):
        msg_type = "no_such_package/Dvl"
        data_field = "velocity.x"
        data_index = None
        max_length = None
        self.assertRaises(scalar_sender.CoexScalarException, scalar_sender.validate_params,
                          msg_type, data_field, data_index, max_length)

    def test_no_package_name(self):
        msg_type = "Dvl"
        data_field = "velocity.x"
        data_index = None
        max_length = None
        self.assertRaises(scalar_sender.CoexScalarException, scalar_sender.validate_params,
                          msg_type, data_field, data_index, max_length)

    def test_too_many_slashes(self):
        msg_type = "ds_sensor_msgs/msg/Dvl"
        data_field = "velocity.x"
        data_index = None
        max_length = None
        self.assertRaises(scalar_sender.CoexScalarException, scalar_sender.validate_params,
                          msg_type, data_field, data_index, max_length)

    def test_invalid_msg_name(self):
        msg_type = "ds_sensor_msgs/DopplerVelocityLog"
        data_field = "velocity.x"
        data_index = None
        max_length = None
        self.assertRaises(scalar_sender.CoexScalarException, scalar_sender.validate_params,
                          msg_type, data_field, data_index, max_length)

    def test_no_header(self):
        msg_type = "ds_core_msgs/Abort"
        data_field = "ttl"
        data_index = None
        max_length = None
        self.assertRaises(scalar_sender.CoexScalarException, scalar_sender.validate_params,
                          msg_type, data_field, data_index, max_length)

    def test_invalid_field_name(self):
        msg_type = "ds_sensor_msgs/Ctd"
        data_field = "temp"
        data_index = None
        max_length = None
        self.assertRaises(scalar_sender.CoexScalarException, scalar_sender.validate_params,
                          msg_type, data_field, data_index, max_length)

    def test_nonscalar_field(self):
        msg_type = "ds_hotel_msgs/A2D2"
        data_field = "raw"
        data_index = None
        max_length = None
        self.assertRaises(scalar_sender.CoexScalarException, scalar_sender.validate_params,
                          msg_type, data_field, data_index, max_length)

    def test_index_into_scalar(self):
        msg_type = "ds_sensor_msgs/Ctd"
        data_field = "temperature"
        data_index = 0
        max_length = None
        self.assertRaises(scalar_sender.CoexScalarException, scalar_sender.validate_params,
                          msg_type, data_field, data_index, max_length)



class RequestedFrequencyTest(unittest.TestCase):
    """
    Check that the returned data has been subsampled to match the requested rate.
    """
    def setUp(self):
        tt = np.arange(10, 40, 0.1)
        # TODO: Separate test for changing the rates; for what I'm trying to test here, deterministic is easier.
        # tt = tt + 0.01*(np.random.rand(len(tt)) - 0.5)
        dd = np.random.randint(0, 100, size=len(tt))
        self.data = np.transpose(np.array([tt, dd]))  # Nx2 array
        self.max_length = None

        self.req = coexploration_msgs.msg.ScalarDataRequest()
        self.req.start_time = rospy.Time(20)
        self.req.duration = 5.0
        self.req.rate = 10.0

    def test_freq_10(self):
        """Simplest case: Data frequency is 10Hz, and requested at 10Hz"""
        messages = scalar_sender.filter_data(self.data, self.req, self.max_length)
        self.assertEqual(len(messages), 1)

        resp = messages[0]
        period = resp.duration / (len(resp.values) - 1)
        self.assertAlmostEqual(period, 1.0/self.req.rate)

    def test_freq_20(self):
        """Data frequency is 10Hz, but requested at 20Hz. Should return 10Hz"""
        self.req.rate = 20.0
        messages = scalar_sender.filter_data(self.data, self.req, self.max_length)
        resp = messages[0]
        period = resp.duration / (len(resp.values) - 1)
        self.assertAlmostEqual(period, 0.10)

    def test_freq_8(self):
        """Data frequency is 10Hz, but requested at 8Hz. Should return 10Hz."""
        self.req.rate = 8.0
        messages = scalar_sender.filter_data(self.data, self.req, self.max_length)
        resp = messages[0]
        period = resp.duration / (len(resp.values) - 1)
        self.assertAlmostEqual(period, 0.10)

    def test_freq_6(self):
        """Data frequency is 10Hz, but requested at 6Hz. Should return at 5Hz"""
        self.req.rate = 6.0
        messages = scalar_sender.filter_data(self.data, self.req, self.max_length)
        resp = messages[0]
        period = resp.duration / (len(resp.values) - 1)
        self.assertAlmostEqual(period, 0.2)
        pass

    def test_freq_5(self):
        """Data frequency is 10Hz, but requested at 5Hz. Should return at 5Hz"""
        self.req.rate = 5.0
        messages = scalar_sender.filter_data(self.data, self.req, self.max_length)
        resp = messages[0]
        period = resp.duration / (len(resp.values) - 1)
        self.assertAlmostEqual(period, 1.0/self.req.rate)
        pass

    def test_freq_3(self):
        """Data frequency is 10Hz, but requested at 3Hz. Should return at 3.3Hz"""
        self.req.rate = 3.0
        messages = scalar_sender.filter_data(self.data, self.req, self.max_length)
        resp = messages[0]
        period = resp.duration / (len(resp.values) - 1)
        self.assertAlmostEqual(period, 0.3)


class EmptyResponseTest(unittest.TestCase):
    """
    Test cases where there is no valid data for a given request,
    or where there is only a single value in bounds.
    """
    def setUp(self):
        tt = np.arange(20, 40, 0.1)
        dd = np.random.randint(0, 100, size=len(tt))
        self.data = np.transpose(np.array([tt, dd]))  # Nx2 array
        self.max_length = None

        # Make sure that the bounds on the request span any deletions
        # that happen for the tests
        self.req = coexploration_msgs.msg.ScalarDataRequest()
        self.req.start_time = rospy.Time(0)
        self.req.duration = 5.0
        self.req.rate = 10.0

    def test_interval_before_data(self):
        """ Requested time + duration is completed before first timestamp in data"""
        responses = scalar_sender.filter_data(self.data, self.req, self.max_length)
        self.assertEqual(0, len(responses))

    def test_interval_after_data(self):
        """ Requested time + duration is entirely after data"""
        self.req.start_time = rospy.Time(50.0)
        responses = scalar_sender.filter_data(self.data, self.req, self.max_length)
        self.assertEqual(0, len(responses))

    def test_negative(self):
        """ Requested duration is negative -- should return nothing."""
        self.req.start_time = rospy.Time(30.0)
        self.req.duration = -20
        responses = scalar_sender.filter_data(self.data, self.req, self.max_length)
        self.assertEqual(0, len(responses))


class DataNotReceivedTest(unittest.TestCase):
    """
    Test case where sender hasn't yet received data.
    (Unlikely to be an issue during dives, but came up in testing.)
    """
    def setUp(self):
        self.max_length = None

        # Make sure that the bounds on the request span any deletions
        # that happen for the tests
        self.req = coexploration_msgs.msg.ScalarDataRequest()
        self.req.start_time = rospy.Time(0)
        self.req.duration = 5.0
        self.req.rate = 10.0

    def test_empty_data(self):
        data = []
        responses = scalar_sender.filter_data(data, self.req, self.max_length)
        self.assertEqual(0, len(responses))

    def test_len1_data(self):
        tt = np.arange(20, 21)
        dd = np.random.randint(0, 100, size=len(tt))
        data = np.transpose(np.array([tt, dd]))  # Nx2 array
        responses = scalar_sender.filter_data(data, self.req, self.max_length)
        self.assertEqual(0, len(responses))


class DataGapTest(unittest.TestCase):
    def setUp(self):
        tt = np.arange(0, 100, 0.1)
        tt = tt + 0.01*(np.random.rand(len(tt)) - 0.5)
        dd = np.random.randint(0, 100, size=len(tt))
        self.data = np.transpose(np.array([tt, dd]))  # Nx2 array
        self.max_length = None

        # Make sure that the bounds on the request span any deletions
        # that happen for the tests
        self.req = coexploration_msgs.msg.ScalarDataRequest()
        self.req.start_time = rospy.Time(5)
        self.req.duration = 90.0
        self.req.rate = 10.0

    def test_no_gaps(self):
        responses = scalar_sender.filter_data(self.data, self.req, self.max_length)
        self.assertEqual(1, len(responses))

    def test_one_gap(self):
        self.data = np.delete(self.data, range(200, 250), axis=0)
        responses = scalar_sender.filter_data(self.data, self.req, self.max_length)
        self.assertEqual(2, len(responses))

    def test_two_gaps(self):
        self.data = np.delete(self.data, range(150, 175), axis=0)
        self.data = np.delete(self.data, range(225, 250), axis=0)
        responses = scalar_sender.filter_data(self.data, self.req, self.max_length)
        self.assertEqual(3, len(responses))

    def test_len1_segment(self):
        """
        Check that length-one segment is not sent.
        """
        # Original data had len = 300
        self.data = np.delete(self.data, range(150, 175), axis=0)
        self.data = np.delete(self.data, range(151, 250), axis=0)
        responses = scalar_sender.filter_data(self.data, self.req, self.max_length)
        self.assertEqual(2, len(responses))

    def test_len2_segment(self):
        """
        Check that length-two segment is sent.
        """
        # Original data had len = 300
        self.data = np.delete(self.data, range(150, 175), axis=0)
        self.data = np.delete(self.data, range(152, 250), axis=0)
        responses = scalar_sender.filter_data(self.data, self.req, self.max_length)
        self.assertEqual(3, len(responses))


class MaxLengthTest(unittest.TestCase):
    """
    Test splitting a message (in support of bounding the message size that
    will be transmitted acoustically).
    """
    def setUp(self):
        tt = np.arange(0, 100, 0.1)
        dd = np.random.randint(0, 100, size=len(tt))
        self.data = np.transpose(np.array([tt, dd]))  # Nx2 array

        # Make sure that the bounds on the request span any deletions
        # that happen for the tests
        self.req = coexploration_msgs.msg.ScalarDataRequest()
        self.req.start_time = rospy.Time(10)
        self.req.duration = 10.0
        self.req.rate = 10.0

    def test_max_length(self):
        """Split into two"""
        # Request should be for 101 values
        max_length = 60
        responses = scalar_sender.filter_data(self.data, self.req, max_length)
        self.assertEqual(2, len(responses))
        self.assertEqual(max_length, len(responses[0].values))

    def test_max_length_with_gap(self):
        """single gap, one side of which needs to be further split"""
        data = np.delete(self.data, range(100, 200), axis=0)
        self.req.start_time = rospy.Time(5.0)
        self.req.duration = 25.0
        max_length = 60
        responses = scalar_sender.filter_data(data, self.req, max_length)
        self.assertEqual(3, len(responses))
        self.assertGreater(max_length, len(responses[0].values))
        self.assertEqual(max_length, len(responses[1].values))

    def test_max_length_with_short_remainder(self):
        """Splitting at max_length leaves length-one message"""
        # Request should be for 101 values
        max_length = 50
        responses = scalar_sender.filter_data(self.data, self.req, max_length)
        self.assertEqual(3, len(responses))
        self.assertEqual(max_length, len(responses[0].values))
        self.assertEqual(max_length, len(responses[1].values))
        self.assertEqual(1, len(responses[2].values))

    def test_max_exact(self):
        """Splitting at max_length yields exactly two max_length messages"""
        self.req.duration = 9.95  # requesting 100 data points
        max_length = 50
        responses = scalar_sender.filter_data(self.data, self.req, max_length)
        self.assertEqual(2, len(responses))
        self.assertEqual(max_length, len(responses[0].values))
        self.assertEqual(max_length, len(responses[1].values))

class MessageParsingTest(unittest.TestCase):
    def setUp(self):
        # Needed so calls to rospy.log*_throttle dont raise exceptions
        rospy.init_node("test_scalar_utils")

    def test_no_nesting_no_index(self):
        """ Test simplest case -- no nesting, no indexing."""
        data_field = "temperature"
        data_index = None
        msg = ds_sensor_msgs.msg.Ctd()
        msg.temperature = 42.0
        dd = scalar_sender.parse_message(msg, data_field, data_index)
        self.assertAlmostEqual(dd, msg.temperature)

    def test_no_nesting_and_index(self):
        data_field = "raw"
        data_index = 0
        msg = ds_hotel_msgs.msg.A2D2()
        msg.raw = [1, 2, 3, 4]
        dd = scalar_sender.parse_message(msg, data_field, data_index)
        self.assertAlmostEqual(dd, msg.raw[0])

    def test_nesting_no_index(self):
        data_field = "velocity.x"
        data_index = None
        msg = ds_sensor_msgs.msg.Dvl()
        msg.velocity.x = 3.0
        dd = scalar_sender.parse_message(msg, data_field, data_index)
        self.assertAlmostEqual(dd, msg.velocity.x)

    def test_invalid_field(self):
        """
        Test invalid field name

        We've already validated the parameters at setup, so the only way this
        would be an issue is if the code was plumbed up incorrectly, in which
        case we DO want it to die, because there's no way to automatically
        recover.
        """
        data_field = "temp"
        data_index = None
        msg = ds_sensor_msgs.msg.Ctd()
        msg.temperature = 42.0
        self.assertRaises(scalar_sender.CoexScalarException,
                          scalar_sender.parse_message,
                          msg, data_field, data_index)

    def test_invalid_index(self):
        """
        Test requesting an index beyond the range of the array.

        Not fatal because ROS allows publishing varying-length arrays, but
        I can't think of a case where this would be done on purpose.
        """
        data_field = "raw"
        data_index = 6
        msg = ds_hotel_msgs.msg.A2D2()
        msg.raw = [1, 2, 3, 4]
        dd = scalar_sender.parse_message(msg, data_field, data_index)
        self.assertIsNone(dd)




if __name__ == "__main__":
    unittest.main()
