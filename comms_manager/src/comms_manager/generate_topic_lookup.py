#! /usr/bin/env python2

# Copyright 2019 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function, division

import argparse
import copy
import os
import rosbag
import six

# for mypy; optional because this doesn't affect anything at runtime
try:
    from typing import Dict, List, Optional, Tuple, Type
except ImportError:
    pass


def list_files(bagdir):
    '''
    Generate a list of files to use for the topic-generation.
    Rather than processing ALL bagfiles, only process the biggest
    of each type.
    '''
    file_sizes = {}  # type: Dict[str, int]
    biggest_files = {}  # type: Dict[str, str]
    for fname in os.listdir(bagdir):
        if ".bag" not in fname:
            continue
        fpath = '{}/{}'.format(bagdir, fname)
        # Get basenames (sensors, actuators, etc ...)
        prefix = fname.split("-")[0][0:-5]
        file_size = os.path.getsize(fpath)
        if prefix not in file_sizes or file_size > file_sizes[prefix]:
            file_sizes[prefix] = file_size
            biggest_files[prefix] = fpath
    return biggest_files


def load_bagfile_topics(read_files, config_topics, skip_prefixes):
    '''
    Given list of bag files, generate dict mapping all topic names
    to corresponding message type.

    Will skip any topics starting with a prefix in skip_prefixes.
    '''
    # Type: (List[str], Dict[str, str], List[str]) -> Dict[str, str]

    topics = copy.copy(config_topics)
    for _, fpath in six.iteritems(read_files):
        bag = rosbag.bag.Bag(fpath)
        topictypes = bag.get_type_and_topic_info()
        for topic, tt in six.iteritems(topictypes.topics):
            if topic in topics:
                print("Already encountered topic {}".format(topic))
                if tt.msg_type != topics[topic]:
                    print("ERROR: type mismatch! {} vs. {}"
                          .format(tt.msg_type, topics[topic]))
                continue

            if any([topic.startswith(prefix) for prefix in skip_prefixes]):
                print("skipping topside topic: {}".format(topic))
                continue

            topics[topic] = tt.msg_type
    return topics


def save_lookup_table(services, topics, skip_bytes, filename):
    '''
    Save topics to file, formatted as expected.
    '''
    # 0-indexed; we increment at the start of each loop
    count = -1
    with open(filename, 'w') as fp:
        sorted_services = services.keys()
        sorted_services.sort()
        for service in sorted_services:
            topic = services[service]

            count += 1
            while count in skip_bytes:
                print("Skipping 0x{:x}".format(count))
                count += 1

            fp.write("{} DATA_SRV {} {}\n".format(count, service, topic))

        sorted_topics = topics.keys()
        sorted_topics.sort()
        for topic in sorted_topics:
            msg_type = topics[topic]

            # Make sure we don't add a second entry for topics published as
            # part of a background data service/topic pair.
            if topic in services.values():
                print("Skipping {} because it's a background data topic"
                      .format(topic))
                if msg_type != "ds_acomms_msgs/ModemData":
                    print("ERROR: topic {}'s type {} doesn't match background "
                          "data".format(topic, msg_type))
                continue

            count += 1
            while count in skip_bytes:
                print("Skipping 0x{:x}".format(count))
                count += 1
            fp.write("{} TOPIC {} {}\n".format(count, topic, msg_type))

            if count >= 255:
                print("WARNING: number of lookup tables has exceeded storing "
                      "the codes as a single byte!!")


def load_config(filename):
    '''
    load a plain text file containing two columns, and return a dict
    where the first column are the keys and the second column are the values.

    This is used both for config topics and services.
    '''
    dd = {}
    try:
        for line in open(filename, "r"):
            if line.startswith('#'):
                continue
            if 0 == len(line.strip()):
                continue
            # This will raise an exception if the file is ill-formatted.
            key, val = line.split()
            dd[key] = val
    except Exception as ex:
        print("Could not read topics from input: {}".format(filename))
        print(ex)
    return dd


def main(bagdir, config_topics_filename, config_services_filename,
         output_filename, skip_bytes, skip_prefixes):
    files = list_files(bagdir)

    # topic name -> message type
    config_topics = load_config(config_topics_filename)
    bag_topics = load_bagfile_topics(files, config_topics, skip_prefixes)

    # data service -> republished topic
    config_services = load_config(config_services_filename)

    save_lookup_table(config_services, bag_topics, skip_bytes, output_filename)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--bagdir", required=True,
                        help="Full path to directory containing bag files")
    parser.add_argument("--output", required=True,
                        help="File to save generated topics file to")
    parser.add_argument("--skip_bytes", nargs="*",
                        help="space-separated list of hex-encoded reserved "
                        "bytes. e.g. 0x64")
    parser.add_argument("--skip_prefixes", nargs="*",
                        help="space-separated list of prefixes indicating a "
                        "topic that does not belong in the lookup table")

    parser.add_argument("--topics_file", required=True,
                        help="Path to file containing topics that might not "
                        "be in the bag file but that must be included in the "
                        "output lookup table.")
    parser.add_argument("--services_file", required=True,
                        help="Path to file specifying the background data "
                        "services that must be included in the lookup table.")

    args = parser.parse_args()

    skip_bytes = []  # type: List[int]
    if args.skip_bytes is not None:
        try:
            skip_bytes = [int(nn, 16) for nn in args.skip_bytes]
        except:
            print("Invalid input for skip_bytes: {}".format(args.skip_bytes))
            raise

    main(args.bagdir, args.topics_file, args.services_file, args.output,
         skip_bytes, args.skip_prefixes)
