/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Jan 24, 2020.
//

#include "quadtree/quadtree.h"
#include "quadtree/quadtree_receiver.h"

namespace quadtree {

QuadTreeReceiver::QuadTreeReceiver(float h_res, float v_res, int send_levels)
  : QuadTree(h_res, v_res, send_levels), received_subgrid_count_(0) {}

// NB: It is possible for a partial update to happen, if a bad subgrid has a valid
//     root but invalid subgrid. That *shouldn't* ever happen, since that would be
//     inconsistent with the sender's status...
bool QuadTreeReceiver::AddSubgrid(const Subgrid& subgrid) {
  received_subgrid_count_ += 1;

  bool success = GrowTree(subgrid.tree_level, subgrid.tree_xx, subgrid.tree_yy);
  if (!success) {
    error_("QuadTreeReceiver: AddSubgrid failed due to GrowTree failure");
    return false;
  }

  float nodex, nodey;
  auto node = MakeOrFindNode(subgrid.xx, subgrid.yy, subgrid.level, &nodex, &nodey);

  if (nodex != subgrid.xx || nodey != subgrid.yy) {
    // NB: This will add failures given that the tree will always wind up centered at 0.5, 0.5 on the first level, and is purposely doing that so bounds between nodes are integral. So ... only allow this on the lowest-level nodes? Or just don't allow depth 1 subgrids at all?
    error_("Quadtree::AddSubgrid: received subgrid doesn't align with existing tree");
    return false;
  }
  MakeSubgridChildren(send_levels_ - 1, node);
  CopySubgrid(subgrid, node);
  return true;
}

bool QuadTreeReceiver::GrowTree(int level, float xc, float yc) {
  if (level < num_levels_ - 1) {
    std::stringstream ss;
    ss << "QuadTreeReceiver::GrowTree: Can't grow tree to level " << level
       << "; root is at level " << num_levels_ - 1;
    error_(ss.str());
    return false;
  } else if (level == num_levels_ - 1) {
    if (xc == x_center_ && yc == y_center_) {
      return true;
    } else {
      std::stringstream ss;
      ss << "QuadTreeReceiver::GrowTree Can't grow tree with root (" << xc
	 << ", " << yc << ") . Levels match, but existing root is at ("
	 << x_center_ << ", " << y_center_ << ")";
      error_(ss.str());
      return false;
    }
  } else if (num_levels_ == 0) {
    // No need to try to grow new root down to existing one; just initialize.
    root_ = std::make_unique<Node>();

    num_levels_ = level + 1;
    x_center_ = xc;
    y_center_ = yc;
    return true;
  }

  // This could check compatability of new root and existing root
  // in order to exit early, but its easier to try to connect the trees
  // and return false if it fails.
  auto new_root = std::make_unique<Node>();
  Node* ptr = new_root.get();
  int curr_level = level;
  float curr_xx = xc;
  float curr_yy = yc;
  int child_index = -1;
  while (curr_level > num_levels_ - 1) {
    // child from previous iteration, except for the first time through.
    if (child_index >= 0) {
      if (!ptr->children->at(child_index)) {
	ptr->children->at(child_index) = std::make_unique<Node>();
      }
      ptr = ptr->children->at(child_index).get();
    }
    // Figure out deltas to next child
    child_index = GetChildIndex(x_center_ - curr_xx, y_center_ - curr_yy);
    float dx, dy;
    GetLevelOffset(curr_level, child_index, &dx, &dy);
    curr_level -= 1;
    curr_xx += dx;
    curr_yy += dy;

    if (!ptr->children) {
      ptr->children = std::make_unique<std::array<std::unique_ptr<Node>, 4>>();
    }
  }

  if (curr_xx == x_center_ && curr_yy == y_center_) {
    ptr->children->at(child_index) = std::move(root_);
    root_ = std::move(new_root);
    x_center_ = xc;
    y_center_ = yc;
    num_levels_ = level + 1;
    return true;
  } else {
    // This could be a case where the current tree is outside the domain
    // of the root, or where the levels aren't aligned.
    std::stringstream ss;
    ss << "QuadTreeReceiver::GrowTree: Couldn't match new root to tree. "
       << "Existing root: (" << x_center_ << ", " << y_center_ << ") "
       << "closest node under new root: (" << curr_xx << ", " << curr_yy << ")";
    error_(ss.str());
    return false;
  }
}

// TODO: This traversal logic exactly mirrors that in QuadTree::GetNode
  //     and QuadTreeReceiver::GrowTree.
// Maybe combine the two functions, with an argument for "make intermediate
// nodes"?
  // TODO: Mixing return types is ugly!
Node* QuadTreeReceiver::MakeOrFindNode(float xx, float yy, int level, float* nodex, float* nodey) {
  // Unlike GetNode, this should always be called after GrowTree,
  // so xx, yy will be within the span of the tree.
  if (level >= num_levels_ || level < 0) {
    // TODO: Really shouldn't be using asserts. Report failure instead?
    assert(false);
  }
  Node* ptr = root_.get();
  int curr_level = num_levels_ - 1;
  auto curr_x = x_center_;
  auto curr_y = y_center_;

  while (curr_level > level) {
    int child_index = GetChildIndex(xx - curr_x, yy - curr_y);

    float dx, dy;
    GetLevelOffset(curr_level, child_index, &dx, &dy);
    curr_level -= 1;
    curr_x += dx;
    curr_y += dy;

    if (!ptr->children) {
      ptr->children = std::make_unique<std::array<std::unique_ptr<Node>, 4>>();
    }
    if (!ptr->children->at(child_index)) {
      ptr->children->at(child_index) = std::make_unique<Node>();
    }
    ptr = ptr->children->at(child_index).get();
  }
  *nodex = curr_x;
  *nodey = curr_y;
  return ptr;
}

void QuadTreeReceiver::MakeSubgridChildren(int level, Node* node) {
  if (0 == level) {
    // Level 0 nodes don't have children -- if this is reached, it's an
    // error within the library.
    return;
  }

  if (!node) {
    // Another internal error -- at least log it!
    return;
  }

  if (!node->children) {
    node->children = std::make_unique<std::array<std::unique_ptr<Node>, 4>>();
  }
  for (int idx = 0; idx < Node::NUM_CHILDREN; idx++) {
    if (!node->children->at(idx)) {
      node->children->at(idx) = std::make_unique<Node>();
    }
    MakeSubgridChildren(level - 1, node->children->at(idx).get());
  }
}

void QuadTreeReceiver::CopySubgrid(const Subgrid& subgrid, Node* node) {
  // Don't apply stale updates.
  // (However, we really shouldn't get them, if sender is working.)
  if (node->updated_time >= subgrid.timestamp) {
    std::stringstream ss;
    ss << "stale update. Node/subgrid time = "
       << node->updated_time  << " / " << subgrid.timestamp
       << ", subgrid position: " << subgrid.xx << ", " << subgrid.yy
       << ", subgrid level: " << subgrid.level << " / " << subgrid.tree_level;
    info_(ss.str());
    return;
  }

  node->updated_time = subgrid.timestamp;
  // TraverseSubgrid is used both to create the subgrid and to unpack it,
  // so the order of elements will be correct.
  coro_grid_t::pull_type traverser([&](coro_grid_t::push_type& out) {
    TraverseSubgrid(node, send_levels_ - 1, out);
  });
  int idx = 0;
  for (Node* leaf : traverser) {
    int zz = subgrid.data.at(idx);
    if (zz == std::numeric_limits<int>::max()) {
      leaf->mean_z == std::numeric_limits<float>::max();
    } else {
      leaf->mean_z = 1.0*zz;
    }
    idx += 1;
  }
}

}  // namespace quadtree
