#! /usr/bin/env python3

# Copyright 2019 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


'''
The core idea behind this comms_manager is that transporting data between
topside and subsea should only require defining a message/service and
ensuring that a node that publishes/provides that interface is running
on the vehicle. (And that the topic is in the lookup table.)

Neither the comms manager itself nor the GUI used to control it
need to be modified when new messages are added.
'''
from __future__ import print_function, division

import collections
import copy
import importlib
import six
import io

import rospy

# for mypy; optional because this doesn't affect anything at runtime
try:
    from typing import Dict, List, Optional, Tuple, Type
except ImportError:
    pass

import ds_acomms_msgs.msg
import ds_acomms_msgs.srv

# The local service to query for data, and the remote topic to publish
# the result on.
ServicePair = collections.namedtuple('ServicePair', ['service', 'topic'])


class TopicLookup(object):
    '''
    The comms manager requires a lookup table mapping bytes to topics and
    services. This class loads that lookup table in from a flat file.
    (Other than that, it is used more as a NamedTuple/struct.)

    The Lookup table file includes a line for each type of
    data that can be requested. Format is:
    [single byte id, data type, <type specific fields>]

    Data Type is one of {TOPIC, DATA_SRV}
    - TOPIC emulates the existing interaction modalities, subsampling
            data from a topic published on Sentry. Fields are:
      [ID, TOPIC, channel/topic, message type]
    - DATA_SRV is used for packing the message with binary data from a
            single node. It expects to make a service call with type
            ModemDataRequest to the specified service, and to republish
            the resulting ModemData to the specified topic. Fields are:
      [ID, DATA_SRV, service, channel/topic]

    '''

    def __init__(self, filename):
        # type: (str) -> None
        '''
        Initializes from input filename.

        Raises an exception on invalid lookup file (e.g. duplicated topics).
        This should be OK for on-robot code because loading topic lookup happens
        at startup, when any errors can be fixed.
        '''
        self.topics = {}  # type: Dict[bytes, str]
        self.data_services = {}  # type: Dict[bytes, ServicePair]
        self.msg_classes = {}  # type: Dict[bytes, Type[rospy.Message]]
        self.srv_classes = {}  # type: Dict[bytes, Type[rospy.Message]]

        for line in open(filename, 'r'):
            tokens = line.split()
            # TODO: Add test case where lookup tables go above 255? Right now,
            #   this will raise ValueError, which seems OK since it is always
            #   called at startup.
            code = chr(int(tokens[0]))
            if code in self.topics or code in self.data_services:
                rospy.logfatal("Duplicate code ({})in lookup file {}!"
                               .format(code, filename))
                raise(Exception)

            if tokens[1] == 'TOPIC':
                topic, msg_type = tokens[2:4]
                if topic in self.topics.values():
                    rospy.logfatal("Duplicate topic name: {}".format(topic))
                    raise(Exception)
                self.topics[code] = topic
                pkg, tt = msg_type.split('/')
                msg_module = importlib.import_module(pkg + ".msg")
                try:
                    self.msg_classes[code] = getattr(msg_module, tt)
                except AttributeError as ex:
                    rospy.logfatal(
                        "Unable to load message {} from {}".format(tt, pkg))
                    raise(ex)

            elif tokens[1] == 'DATA_SRV':
                service, topic = tokens[2:4]
                # On the receiving end, the CommsManager will handle the ModemData
                # response exactly like any other message.
                if topic in self.topics.values():
                    rospy.logfatal(
                        "DATA_SRV topic is duplicate: {}".format(topic))
                    raise(Exception)
                for sp in self.data_services.values():
                    if sp.service == service:
                        rospy.logfatal(
                            "DATA_SRV is duplicate: {}".format(service))
                        raise(Exception)
                self.data_services[code] = ServicePair(service, topic)
                self.topics[code] = topic
                self.msg_classes[code] = ds_acomms_msgs.msg.ModemData
                self.srv_classes[code] = ds_acomms_msgs.srv.ModemDataRequest
            else:
                rospy.logerr("Invalid line in topics file! \n {}".format(line))

    def code_from_data_service(self, service):
        # type: (str) -> bytes
        '''
        Given the service name, return the corresponding code from the lookup
        table for background data services.

        If service isn't found, returns empty string.
        '''
        output_code = b""
        for code, data_srv in six.iteritems(self.data_services):
            if service == data_srv[0]:
                output_code = code
                print("For service {}, code is {}"
                      .format(service, ord(code)))
                break
        return output_code


def slot_overlaps(new_slot, slots):
    # type: (Tuple[float, float, int], List[Tuple[float, float, int]]) -> bool
    # TODO: These type signatures are getting ugly -- I should definitely
    #       refactor to use a class for Slots.
    '''
    Check whether input new_slot overlaps with any of the existing
    slots.
    '''
    if len(slots) == 0:
        return False
    new_start, new_duration, _ = new_slot
    new_end = new_start + new_duration
    for start, duration, _ in slots:
        if new_start >= start and new_start < start+duration:
            return True
        if new_end >= start and new_end < start+duration:
            return True
        if start >= new_start and start < new_end:
            return True
        if start + duration >= new_start and start + duration < new_end:
            return True
    return False


def tdma_msg_is_valid(msg):
    # type: (rospy.Message) -> bool
    '''
    Check whether the input message represents a valid schedule:
    * times within the range of 0-60 seconds after the top of the minute
    * slots don't overlap

    Used by the comms_manager to check incoming message before updating its
    publication schedule and by the GUI for checking construction of message
    before publishing it.
    '''
    new_slots = []  # type: List[Tuple[float, float, int]]

    for slot in zip(msg.starts, msg.durations, msg.rates):
        start, duration, rate = slot
        if start < 0 or start >= 60:
            rospy.logerr("Invalid start time {} in input slot message: {}"
                         .format(start, msg))
            return False
        if duration < 0 or start + duration > 60:
            rospy.logerr("Invalid duration {} in input slot message: {}"
                         .format(duration, msg))
            return False
        if slot_overlaps(slot, new_slots):
            rospy.logerr("Overlapping slots in message: {}".format(msg))
            return False
        # TODO: I dislike having this list hardcoded here...
        if rate not in [msg.DEFAULT, msg.MM1, msg.MM4, msg.MM5]:
            rospy.logerr("Invalid rate {} in message: {} (slot: {})"
                         .format(rate, msg, slot))
            return False
        new_slots.append(slot)
    return True


# TODO: Could also make encode a member function
def decode(data,  # type: List[bytes]
           msg_classes  # type: Dict[bytes, Type[rospy.Message]]
           ):
    # type: (...) -> Dict[bytes, rospy.Message]

    messages = {}  # type: Dict[bytes, rospy.Message]
    sio = io.StringIO()
    for elem in data:
        sio.write(elem)

    sio.seek(0)
    while sio.tell() < sio.len:
        # Even an empty message has at least 3 bytes
        # (1 code, 2 num_bytes)
        if sio.tell() > sio.len - 3:
            rospy.logerr("Message incomplete but more bytes in data. "
                         "Original data: {}, Leftover: {}"
                         .format(data, sio.read()))
            break

        code = sio.read(1)
        # When running with Sentry/NUI/etc, the vehicle directly sends
        # messages to the modem using a small set of reserved first bytes.
        # Comms_manager can run in parallel to this, assuming the lookup
        # table was generated with --skip_bytes
        if code not in msg_classes.keys():
            rospy.logwarn("Cannot decode message with invalid code {} at "
                          "index {}. data: {}. This is normal if running "
                          "in parallel with Sentry's legacy QueueManager."
                          .format(code, sio.tell(), data))
            break

        n1 = ord(sio.read(1))
        n2 = ord(sio.read(1))
        num_bytes = (n1 << 8) + n2

        if sio.tell() > sio.len - num_bytes:
            rospy.logerr("Too few bytes left in data ({}) to read message "
                         "({}). Data: {}, remaining:{}"
                         .format(sio.len - sio.tell(), num_bytes, data,
                                 sio.read()))
            break

        # TODO: Is this actually an error? Maybe messages should be an array
        #       of tuples, rather than a dict? (The current setup will cause
        #       problems with the planned update to request multiple background
        #       data chunks from quadtree in a single frame)
        if code in messages:
            rospy.logerr("Got repeat message code; this should not happen!"
                         " Full data: {}, remaining bytes: {}"
                         .format(data, sio.read()))
            break

        msg = msg_classes[code]()
        msg_data = sio.read(num_bytes)
        try:
            msg.deserialize(msg_data)
            messages[code] = msg
        except Exception as ex:
            # I didn't see a list of all possible exceptions that could be
            # raised. Typically, this would be genpy.DeserializationError,
            # but in testing I also got MemoryError (if num_bytes was
            # corrupted).
            rospy.logerr("Could not deserialize {}\n Exception was {}"
                         .format(data, ex))
            break

    return messages


class CommsManager(object):
    def __init__(self, filename=None):
        # type: (Optional[str]) -> None
        '''
        For unit testing, pass the filename in at construction.
        '''

        if filename is None:
            filename = rospy.get_param('/topic_lookup_filename')
        self.topic_lookup = TopicLookup(filename)

        # Workaround to enable unittesting w/o rosparam
        self.subscription_prefix = ""
        self.publication_prefix = ""

        # Each key in this dict is a code for a topic that should be
        # transmitted topside, and the value describes how often it should be
        # sent.
        self.active_topics = {}  # type: Dict[bytes, int]

        # Which service to use to pack the rest of the message with data.
        # Can be None, or a code.
        self.active_service = None  # type: Optional[str]

        # The TDMA slots that the CommsManager is allowed to transmit.
        # * First element of tuple is starting time of slot, in seconds after
        #   the start of the minute.
        # * Second element of tuple is duration of slot, in seconds
        self.tdma_slots = []  # type: List[Tuple[float, float, int]]
        # Timestamp for previous transmission to modem
        self.prev_pub_time = 0  # type: float

        # TODO: These should be parameters for the comms manager.
        #       For now, they're hardcoded to be micromodem-specific.
        # Valid rates
        self.rates = [1, 4, 5]  # type: List[int]
        self.default_rate = 1

        # How many bytes are in each frame at a given rate
        # These were NOMINAL bytes. However, sentry544 and experimentation
        # afterwards showed that Sentry's micromodem gave errors at anything
        # more than 248 bytes for rate 5, so subtracting 8 from all of them.
        # self.frame_bytes = {1:64, 4:256, 5:256}  # type: Dict[int, int]
        self.frame_bytes = {1: 56, 4: 248, 5: 248}  # type: Dict[int, int]

        # How many frames are in a packet at a given rate
        self.num_frames = {1: 3, 4: 2, 5: 8}  # type: Dict[int, int]

        # Publishers for all topics that have appeared in a set of bytes to be
        # decoded. They are created as-needed on receipt of modem data, and
        # currently-unused ones are not ever removed.
        self.publishers = {}  # type: Dict[bytes, rospy.Publisher]

        # Subscribers for all topics that have appeared in a Queue Definition
        # to be transmitted. Created as-needed, and not cleaned up.
        self.subscribers = {}  # type: Dict[bytes, rospy.Subscriber]

        self.service_proxies = {}  # type: Dict[bytes, rospy.ServiceProxy]

        # Dict of the most recently received data for all subscribed topics
        self.received_data = {}  # type: Dict[bytes, rospy.Message]

        # How many times encode has been called
        self.encode_count = 0  # type: int

        # What was the last publication cycle that included a message of each type.
        # (Used to determine whether a message is due to be sent)
        self.queues_last_sent = {
            code: None for code in self.topic_lookup.msg_classes}
        # type: Dict[bytes, Optional[int]]

        # How many times data from queue has been encoded since the previous
        # update to queue definitions. Used to ensure appropriate frequency.
        self.queues_sent_count = {
            code: 0 for code in self.topic_lookup.msg_classes}
        # type: Dict[bytes, int]

    def setup(self):
        # type: () -> None
        self.setup_parameters()
        self.setup_publishers()
        self.setup_subscribers()
        self.setup_timers()

    def setup_parameters(self):
        # type: () -> None
        self.publication_prefix = rospy.get_param("~publication_prefix", "")
        self.subscription_prefix = rospy.get_param("~subscription_prefix", "")
        self.local_addr = rospy.get_param('~local_addr')
        self.remote_addr = rospy.get_param('~remote_addr')

    def setup_publishers(self):
        # type: () -> None
        # NB: To match full ds_ros implementation, this should probably set up
        # a timer and publisher for status messages
        self.modem_pub = rospy.Publisher('~tx',
                                         ds_acomms_msgs.msg.AcousticModemData,
                                         queue_size=1)
        self.ack_pub = rospy.Publisher("~acks_expected",
                                       ds_acomms_msgs.msg.AcksExpected,
                                       queue_size=1)

    def setup_subscribers(self):
        # type: () -> None
        self.queue_sub = rospy.Subscriber('~queue_definition',
                                          ds_acomms_msgs.msg.QueueDefinition,
                                          self.queue_definition_callback)
        self.modem_sub = rospy.Subscriber('~rx',
                                          ds_acomms_msgs.msg.AcousticModemData,
                                          self.modem_data_callback)
        self.tdma_sub = rospy.Subscriber('~tdma_slots',
                                         ds_acomms_msgs.msg.TDMASlots,
                                         self.tdma_slots_callback)

    def setup_timers(self):
        # type: () -> None
        # Callback to check whether it's time to publish in the next TDMA slot
        self.tdma_timer = rospy.Timer(rospy.Duration(0.5), self.timer_callback)

    def timer_callback(self, event):
        # type: (rospy.timer.TimerEvent) -> None
        '''
        Determine whether to send data to modem based on whether the current
        time is within the TDMA slot and whether a transmission has already
        occurred in this slot.

        At the start of a slot, publish "AcksExpected" so any nodes that
        were waiting for a message know that they didn't get it this cycle.
        '''
        tt = rospy.Time.now()
        # A bit hacky, but works so long as the epoch of rostime is at the
        # start of a minute. The only time that isn't true is in simulation,
        # in which case you'd have to sync sim time across two ROS networks.
        # I briefly looked at time.gmtime(tt.to_sec()) but it truncates its
        # tm_sec field to integers.
        seconds = tt.to_sec() % 60
        dt = tt.to_sec() - self.prev_pub_time
        slot_start, slot_rate = self.seconds_in_slot(seconds)
        # If current time is in a slot, and we haven't already published
        # within that slot.
        # TODO: Would like to track duration of transmission and send
        #       multiple messages in a slot if it's long enough.
        if slot_start is not None and (seconds - slot_start) < dt:
            ack_msg = ds_acomms_msgs.msg.AcksExpected()
            self.ack_pub.publish(ack_msg)

            # TODO: Adding services makes this potentially a non-instantaneous
            #       operation. Track time required to call it, and report errors
            #       if longer than 1/4 second?
            # msg = ds_acomms_msgs.msg.MicromodemData()
            msg = ds_acomms_msgs.msg.AcousticModemData()
            msg.local_addr = self.local_addr
            msg.remote_addr = self.remote_addr
            msg.payload = msg
            payload_length += len(msg.payload)

            # TODO: the comms manager should probably be constructed with
            #       parameters for valid rates, num frames and max frame length
            #       for each rate, transmission duration.
            '''
            if slot_rate in self.rates:
                msg.rate = slot_rate
            elif msg.rate == ds_acomms_msgs.msg.TDMASlots.DEFAULT:
                msg.rate = self.default_rate
            nbytes = self.frame_bytes[msg.rate]
            nframes = self.num_frames[msg.rate]
            payload_length = 0
            for idx in range(nframes):
                frame = ds_acomms_msgs.msg.ModemData()
                first_frame = (idx == 0)
                frame.payload = self.encode_active(max_bytes=nbytes,
                                                   first_frame=first_frame)
                if len(frame.payload) == 0:
                    break
                msg.frames.append(frame)
                payload_length += len(frame.payload)
            '''

            # QUESTION: Should this send an empty message during the TDMA slot
            #   just to say "I'm alive!"?
            if payload_length > 0:
                self.modem_pub.publish(msg)
            self.prev_pub_time = tt.to_sec()

    def seconds_in_slot(self, seconds):
        # type: (float) -> Tuple[Optional[float], Optional[int]]
        '''
        Return whether the input number of seconds is inside a TDMA slot,
        and if so, the starting second for that slot.
        '''
        # unpacking here would fail if tdma_slots is empty
        for slot in self.tdma_slots:
            start, duration, rate = slot
            if seconds >= start and seconds < start + duration:
                return start, rate
        return None, None

    def tdma_slots_callback(self, msg):
        # type: (ds_acomms_msgs.msg.TDMASlots) -> None
        '''
        Right now, only updates the transmission slots if input is valid.
        TODO: Should this publish any feedback for "rejected message"?
        '''
        if tdma_msg_is_valid(msg):
            self.tdma_slots = zip(msg.starts, msg.durations, msg.rates)

    def modem_data_callback(self, msg):
        # type: (ds_acomms_msgs.msg.MicromodemData) -> None
        '''
        Handles each frame of the message independently.
        '''
        for frame in msg.frames:
            messages = decode(frame.payload, self.topic_lookup.msg_classes)
            for code, message in six.iteritems(messages):
                if code not in self.publishers:
                    topic = self.publication_prefix + self.topic_lookup.topics[code]
                    # With a queue size of 1, some of the background data
                    # messages were being dropped.
                    pub = rospy.Publisher(topic, self.topic_lookup.msg_classes[code],
                                          queue_size=10, latch=True)

                    self.publishers[code] = pub
                    # NB: The first message may well be dropped. If this
                    #     becomes important, may want to add a sleep here, but
                    #     that's rather hacky too.

                self.publishers[code].publish(message)

    def queue_definition_callback(self, msg):
        # type: (ds_acomms_msgs.msg.QueueDefinition) -> None
        '''
        Handle receipt of new queue description.
        TODO: Yet another place where I need to split the send-once list
              from the ongoing queue list.

        Available modes:
        * 0:     send once
        * 1 - N: send every N'th transmission
        '''
        # First, handle the new service definitions. There's no performance
        # penalty for having service proxies open, so go ahead and create it.
        # (But don't call wait_for_server!)
        if len(msg.background_data) == 0:
            self.active_service = None
        else:
            code = msg.background_data[0]
            if len(msg.background_data) > 1:
                rospy.logerr("Multiple background data services. Using first. "
                             "Input QueueDefinition: {}".format(msg))

            if code in self.topic_lookup.data_services:
                service = self.topic_lookup.data_services[code].service
                if code not in self.service_proxies:
                    srv_class = self.topic_lookup.srv_classes[code]
                    self.service_proxies[code] = rospy.ServiceProxy(service,
                                                                    srv_class)
                self.active_service = code
            else:
                rospy.logerr("Invalid SRV code {} in Queue definition "
                             "message:\n {}".format(ord(code), msg))
                self.active_service = None

        new_def = {}  # type: Dict[bytes, int]
        for code, mode_char in zip(msg.queue_topics, msg.queue_periods):
            # We do math with the modes, so internal use converts to int
            mode = ord(mode_char)
            if code in new_def:
                rospy.logerr("Repeated TOPIC code {} in queue definition "
                             "message:\n {}".format(ord(code), msg))
                continue
            if code not in self.topic_lookup.msg_classes.keys():
                rospy.logerr("Invalid TOPIC code {} in queue definition "
                             "message:\n {}" .format(ord(code), msg))
                continue

            new_def[code] = mode

        prev_active = copy.copy(self.active_topics)
        self.active_topics = {}
        for code, mode in six.iteritems(new_def):
            # Make sure that a single-shot doesn't fire until new data has been
            # received
            if mode == 0:
                self.queues_sent_count[code] = 0
                self.received_data[code] = None

            # Only reset counters if new queue definition changes a relevant
            # parameter:
            # * Adding new non-single-shot topic
            # * Changing a topic's frequency, and not to single-shot
            def_changed = code not in prev_active or prev_active[code] != mode
            if mode != 0 and def_changed:
                # print("Initializing code {}".format(ord(code)))
                self.queues_sent_count = {code: 0 for code in self.topic_lookup.msg_classes}

            self.active_topics[code] = mode
            if code not in self.subscribers:
                cb = lambda msg, code=code: self.data_callback(code, msg)
                sub_topic = self.subscription_prefix + self.topic_lookup.topics[code]
                sub = rospy.Subscriber(sub_topic, self.topic_lookup.msg_classes[code], cb)
                self.subscribers[code] = sub

        # I'm torn about removing subscribers that aren't currently active.
        # It could lead to a fair bit of subscribe/disconnect churn, and we'd
        # have to be careful w/r/t guaranteeing sufficient persistence for
        # connections to be made (note how rostopic sleeps for a few seconds
        # before and after publishing...)

    def data_callback(self, code, msg):
        # type: (bytes, rospy.Message) -> None
        self.received_data[code] = msg

    def prioritize_queues(self):
        # Type: (object) -> Dict[bytes, float]
        '''
        Determine relative priorities for each queue.

        If a message is not due to be sent, its code will not appear in this dict.
        '''
        priorities = {}  # type: Dict[bytes, int]
        for code, mode in six.iteritems(self.active_topics):
            # If we haven't received the message yet, can't encode it.
            if code not in self.received_data or self.received_data[code] is None:
                rospy.logdebug("Tried to encode message {} but haven't yet "
                               "received any.".format(code))
                continue

            # Desired periods are a lower bound; if message isn't due yet,
            # don't encode.
            last_sent = self.queues_last_sent[code]
            if last_sent is not None and last_sent + mode > self.encode_count:
                continue

            # If this was a single-shot message and it has been sent,
            # don't send again.
            if mode == 0 and self.queues_sent_count[code] > 0:
                continue

            # Relative priority is based on how what fraction of the desired
            # frequency each message has achieved.
            # We calculate it based on frequency IF this message were sent
            # in order to obtain correct ordering at startup.
            # Single-shot commands go to the front of the line, since we assume
            # they're the one the operator cares about most.
            if mode == 0:
                priorities[code] = -1
            else:
                # This is somewhat hacky:
                # * add 1 to the count so that more frequent messages are sent
                #   first after receiving a new queue definition.
                priorities[code] = (self.queues_sent_count[code] + 1) * mode
        return priorities

    def encode_message(self, code, msg):
        # type: (bytes, rospy.Message) -> Optional[List[bytes]]
        '''
        Encode a single message.
        '''
        data = []  # type: List[bytes]
        sio = io.StringIO()
        msg.serialize(sio)

        num_bytes = sio.len
        if num_bytes >= 2**16:
            rospy.logerr("Saved message is too big; we only support "
                         "two-byte num_bytes. code {}, msg {}"
                         .format(code, self.received_data[code]))
            return None

        data.append(code)
        data.append(chr(num_bytes >> 8))
        data.append(chr(num_bytes % 256))
        sio.seek(0)
        data_bytes = sio.read(sio.len)
        data.extend(data_bytes)
        return data

    # num_bytes is optional for now in order to enable testing ALL
    # message types, including those that don't serialize into a single
    # frame size.
    # In operations, it should never be None.
    def encode_active(self, max_bytes=None, first_frame=True):
        # type: (Optional[int], Optional[bool]) -> bytes
        '''
        Generate the raw data that should be transmitted with the next message.

        For each ROS message being relayed, the format is:
        * 1 byte  - code
        * 2 bytes - number of serialized bytes in message (MSB first)
        * N bytes - serialized message

        For now, serialization is the super-inefficient method provided by ROS.
        e.g. sys.getsizeof(sensor_msgs.msg.Imu) == 112, but serialized = 312
        '''
        # encode_active is called once per frame, but we only want to
        # update the encode_count once per packet.
        if first_frame:
            self.encode_count += 1

        priorities = self.prioritize_queues()
        sorted_priorities = sorted(priorities.items(), key=lambda kv: kv[1])

        data = []  # type: List[bytes]
        for code, priority in sorted_priorities:
            msg_data = self.encode_message(code, self.received_data[code])
            if msg_data is None:
                # No need to logwarn here, because self.encode_message will
                # have output a more informative one.
                continue
            if max_bytes is not None and len(msg_data) > max_bytes:
                rospy.logwarn("Could not pack message w/ code: {}. Needed {} "
                              "bytes, but this frame only has {} total"
                              .format(code, len(msg_data), max_bytes))
                continue
            if max_bytes is not None and len(msg_data) + len(data) > max_bytes:
                rospy.logdebug("Reached max_bytes.")
                rospy.logdebug("Could not add code {} to frame; trying next "
                               "priority. Required {} bytes, only have {} left"
                               .format(code, len(msg_data), max_bytes))
                continue

            data.extend(msg_data)
            self.queues_last_sent[code] = self.encode_count
            self.queues_sent_count[code] += 1
            # Don't re-send this message until a new one is receied
            self.received_data[code] = None

        # Try filling in background data!
        if self.active_service is not None:
            # Figure out how many bytes to request
            if max_bytes is None:
                bytes_remaining = 256
            else:
                bytes_remaining = max_bytes - len(''.join(data))
            # Try to get the data from the service
            # TODO: Magic number!
            encode_overhead = 7  # (bytes)
            num_requested = bytes_remaining - encode_overhead
            try:
                rospy.loginfo("{} Requesting {} bytes from {}".format(rospy.get_name(), num_requested, ord(self.active_service)))
                resp = self.service_proxies[self.active_service](num_requested)
                if len(resp.data) > 0:
                    msg = ds_acomms_msgs.msg.ModemData()
                    msg.payload = resp.data
                    bg_data = self.encode_message(self.active_service, msg)
                    if bg_data is not None:
                        data.extend(bg_data)
                    else:
                        rospy.logerr("Unable to encode service's response: {}"
                                     .format(resp.data))
                else:
                    rospy.loginfo("...Received an empty response!")
            except:
                service = self.topic_lookup.data_services[self.active_service].service
                rospy.loginfo("Requesting {:3d} bytes from {} {} failed."
                              .format(num_requested, ord(self.active_service),
                                      service))

        return ''.join(data)


if __name__ == "__main__":
    rospy.init_node('comms_manager')
    qm = CommsManager()
    qm.setup()
    rospy.spin()
