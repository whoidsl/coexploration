#! /usr/bin/env python2.7
'''
Actually run all the tests in the suite.
'''

import rospy
import rostest
import rostest_qm

if __name__=="__main__":
    PKG = "comms_manager"
    rospy.init_node('rostest_comms_manager')
    rostest.rosrun(PKG, 'rostest_comms_manager', 'rostest_qm.RostestCommsManager')
