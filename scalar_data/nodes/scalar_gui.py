# Copyright 2021 University of Washington Applied Physics Laboratory
# Author: Laura Lindzey
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import collections
import datetime
import numpy as np
import numpy.typing as npt
import rospy
import signal
import sys
from typing import Dict, Optional

import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui
import PyQt5.QtWidgets as QtWidgets

import matplotlib
import matplotlib.dates as mdates
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.patches import Rectangle

import coexploration_msgs.msg

ScalarDataType = collections.namedtuple(
    'ScalarDataType', ['data_label',  # Label to put on the plot's axis
                       'subscribe_topic',  # Topic with aggregated data
                       'request_topic', ]
)


class ScalarGui(QtWidgets.QWidget):
    def __init__(self) -> None:
        super(ScalarGui, self).__init__()

        self.raw_data: Dict[str, Optional[npt.NDArray[np.float64]]] = {}

        self.setup_params()

        # TODO: Unlike the raw data, peaks aren't aggregated, since nominally,
        #       each message is the self-contained top N peaks. However,
        #       this means that earlier, smaller, peaks will disappear unless
        #       the GUI keeps track of them.
        # TODO: Add a "threshold" to the message showing the cutoff for the detector
        #  (QUESTION: better to just use the minimum reported value? Or is the threshold useful in the case of an empty message? Or does it even make sense to filter returend points on threshold, instead always sending at least one s.t. we know the pipeline is working?
        if self.dorp_enabled:
            self.dorp_peaks: Optional[npt.NDArray[np.float64]] = None

        self.setup_ui()
        self.setup_connections()

    def setup_params(self) -> None:
        self.dorp_enabled = rospy.get_param("~enable_dorp")
        topic_config = rospy.get_param("~topic_config")
        print("Configuring topics: {}".format(topic_config))
        self.topics = {}
        for topic_dict in topic_config:
            try:
                data_key = topic_dict['name']
                label = topic_dict['data_label']
                sub_topic = topic_dict['subscribe_topic']
                req_topic = topic_dict['request_topic']
                self.topics[data_key] = ScalarDataType(label, sub_topic,
                                                       req_topic)
                self.raw_data[data_key] = None

            except KeyError as ex:
                rospy.logfatal("Malformed topic_config parameter!")

        if self.dorp_enabled and 'orp' not in self.topics:
            rospy.logwarn("Can't display dORP data without an orp axis")
            self.dorp_enabled = False

    def setup_ui(self) -> None:
        self.setGeometry(1000, 100, 900, 600)
        self.setWindowTitle('Scalar Data GUI')

        ######################
        # Create widget for plotting the data
        self.time_figure = Figure()
        num_axes = len(self.topics)

        # This only works if we change the plotted data to datetimes, and then
        # I had issues with retrieving the bounds in seconds.
        # self.dorp_ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=5))
        # self.dorp_ax.xaxis.set_minor_locator(mdates.MinuteLocator())
        # self.dorp_ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
        # self.dorp_ax.set_xlabel("Time (UTC) HH:MM")

        # We want to be able to request scalar data for the whole dive, even though
        # we don't have a good way of knowing when it started. So, assume GUI is
        # started at the start of dive, and allow zooming/scrolling as necessary.
        # (When replaying a bagfile, may not have clock data until bagfile playing,
        # so wait for the first timer callback to actually set it)
        self.first_time = None

        ff = matplotlib.ticker.FuncFormatter(lambda x, pos: "{}/{}\n{:02d}:{:02d}:{:02d}".format(
            datetime.datetime.utcfromtimestamp(x).month,
            datetime.datetime.utcfromtimestamp(x).day,
            datetime.datetime.utcfromtimestamp(x).hour,
            datetime.datetime.utcfromtimestamp(x).minute,
            datetime.datetime.utcfromtimestamp(x).second))

        self.data_axes = {}
        self.first_ax = None
        for idx, (key, topic) in enumerate(self.topics.items()):
            if self.first_ax is None:
                self.first_ax = self.time_figure.add_subplot(num_axes, 1, num_axes-idx)
                self.first_ax.set_xlabel("Time")
                self.first_ax.xaxis.set_major_formatter(ff)
                self.first_ax.tick_params(axis='x', labelrotation=45)

                self.data_axes[key] = self.first_ax
            else:
                self.data_axes[key] = self.time_figure.add_subplot(
                    num_axes, 1, num_axes-idx, sharex=self.first_ax)
                self.data_axes[key].tick_params(axis='x', which='both',
                                                bottom=False, labelbottom=False,
                                                top=False)

            self.data_axes[key].set_ylabel(topic.data_label,
                                           rotation='horizontal',
                                           horizontalalignment='right')
        self.time_figure.subplots_adjust(hspace=0.2)

        if self.dorp_enabled:
            self.dorp_ax = self.data_axes["orp"].twinx()
            self.dorp_ax.set_ylabel("dORP / dt")

        self.time_canvas = FigureCanvas(self.time_figure)
        self.time_canvas.mpl_connect('button_press_event', self.on_mouse_press)
        self.time_canvas.mpl_connect(
            'button_release_event', self.on_mouse_release)
        self.toolbar = NavigationToolbar(self.time_canvas, self)
        time_col = QtWidgets.QVBoxLayout()
        time_col.addWidget(self.time_canvas)
        time_col.addWidget(self.toolbar)
        # TODO: Toolbar? We need to be able to zoom horizontally at a minimum, and possibly vertically.

        # handle to the plotted data to it can be updated.
        self.data_artists: Dict[str, Optional[matplotlib.lines.Line2D]] = {}
        for data_key in self.data_axes.keys():
            self.data_artists[data_key] = None
        self.peaks_artist: Optional[matplotlib.lines.Line2D] = None


        ######################
        # Create widget for selecting a region

        # Checkbox to enable click-and-drag
        # text entry box for rate
        # Text display indicating "Requesting _ s of data at _ Hz will be ~ _ points.
        # Button to send
        # button to clear selection

        # Start time, in seconds since the epoch, for selected region
        self.selection_start_time = None
        self.duration = None  # Duration in seconds of selected retgion
        self.corner1 = None  # Corner where user clicked, if selection is active
        self.corner2 = None  # Corner where user released, if selection is active
        self.selection_artist = None  # Rectangle used to draw selection on the axis

        self.active_selection_label = "None"
        self.selection_vbox = QtWidgets.QVBoxLayout()
        self.selection_buttons = {}
        self.selection_buttons["None"] = QtWidgets.QRadioButton("None")
        callback = lambda x, key="None": self.handle_selection_toggled(x, key)
        self.selection_buttons["None"].toggled.connect(callback)
        self.selection_vbox.addWidget(self.selection_buttons["None"])
        for key in self.topics.keys():
            self.selection_buttons[key] = QtWidgets.QRadioButton(key)
            self.selection_vbox.addWidget(self.selection_buttons[key])
            callback = lambda x, key=key: self.handle_selection_toggled(x, key)
            self.selection_buttons[key].toggled.connect(callback)
        self.selection_buttons["None"].setChecked(True)

        self.data_rate = 1.0
        rate_label = QtWidgets.QLabel("Requested Rate (Hz): ")
        self.rate_edit = QtWidgets.QLineEdit("{}".format(self.data_rate))
        self.rate_edit.setValidator(QtGui.QDoubleValidator(0, 100, 2))
        self.rate_edit.editingFinished.connect(self.rate_text_edited)
        rate_row = QtWidgets.QHBoxLayout()
        rate_row.addWidget(rate_label)
        rate_row.addWidget(self.rate_edit)

        self.data_label = QtWidgets.QLabel()
        self.format_data_label()

        clear_button = QtWidgets.QPushButton("Clear")
        clear_button.clicked.connect(self.clear_button_clicked)
        send_button = QtWidgets.QPushButton("Send")
        send_button.clicked.connect(self.send_button_clicked)
        button_row = QtWidgets.QHBoxLayout()
        button_row.addWidget(clear_button)
        button_row.addWidget(send_button)

        control_col = QtWidgets.QVBoxLayout()
        control_col.addStretch(1)
        control_col.addLayout(self.selection_vbox)
        control_col.addLayout(rate_row)
        control_col.addWidget(self.data_label)
        control_col.addLayout(button_row)

        main_hbox = QtWidgets.QHBoxLayout()
        main_hbox.addLayout(time_col, stretch=1)
        main_hbox.addLayout(control_col)

        self.setLayout(main_hbox)
        self.show()

    def setup_connections(self) -> None:
        # TODO: I'd rather have this be more general -- it should take a dict
        #       matching name to topic, and subscribe to all of them.
        #       However, as a first step, I'm going to hard-code the dORP pieces.
        #       The dictionary + turning them on/off can come later.
        if self.dorp_enabled:
            self.dorp_peak_sub = rospy.Subscriber("~dorp_peaks",
                                                coexploration_msgs.msg.DorpHits,
                                                self.peaks_callback)

        self.publishers = {}
        self.subscribers = {}
        self.subscribers["orp"] = rospy.Subscriber("~orp_data",
                                                   coexploration_msgs.msg.AggregatedScalarData,
                                                   lambda msg: self.data_callback(msg, "orp"))
        self.publishers["orp"] = rospy.Publisher("~orp_requests",
                                                 coexploration_msgs.msg.ScalarDataRequest,
                                                 queue_size=1)

        for key, topic in self.topics.items():
            # This isn't quite the standard way of handling topics with remapping,
            # since I couldn't figure out how to do that in combination with accpting
            # a list of scalar channels to handle at run time.
            self.publishers[key] = rospy.Publisher(topic.request_topic,
                                                   coexploration_msgs.msg.ScalarDataRequest,
                                                   queue_size=1)
            self.subscribers[key] = rospy.Subscriber(topic.subscribe_topic,
                                                     coexploration_msgs.msg.AggregatedScalarData,
                                                     lambda msg, kk=key: self.data_callback(msg, kk))

        # We want to update the axes even without incoming data
        self.rescale_xlim_timer = rospy.Timer(rospy.Duration(10.0),
                                              self.rescale_xlim)

    def redraw(self) -> None:
        """
        The data is simple enough that we replot all data, regardless of
        whether the thing that changed was orp, peaks, or selected region.
        """
        if self.dorp_enabled:
            if self.peaks_artist is not None:
                self.peaks_artist.remove()
            if self.dorp_peaks is not None:
                times = [datetime.datetime.utcfromtimestamp(
                    tt) for tt in self.dorp_peaks[:, 0]]
                self.peaks_artist, = self.dorp_ax.plot(self.dorp_peaks[:, 0],  # times,
                                                    self.dorp_peaks[:, 1], 'ro')
                # TODO: Consider also plotting the threshold applicable to those peaks?
                #    It might be nice to see how it evolves over time, and make

        for data_key, _artist in self.data_artists.items():
            if self.data_artists[data_key] is not None:
                self.data_artists[data_key].remove()
                self.data_artists[data_key] = None
            data = self.raw_data[data_key]
            if data is not None:
                self.data_artists[data_key], = self.data_axes[data_key].plot(
                    data[:, 0], data[:, 1], 'k.')

        self.draw_selection_rectangle()

        # This will work until we start allowing zooming. We want the "home"
        # limits to be start -> present
        if self.first_time is not None:
            self.first_ax.set_xlim([self.first_time, rospy.Time.now().to_sec()])

        self.format_data_label()

        self.time_canvas.draw_idle()

    def rescale_xlim(self, _event) -> None:
        """
        We want the default xlimits to reflect the current time span, so scalar
        data can be requested at any point in the dive.
        """
        # This will work until we start allowing zooming. We want the "home"
        # limits to be start -> present
        if self.first_time is None:
            self.first_time = rospy.Time.now().to_sec()
        else:
            self.first_ax.set_xlim([self.first_time, rospy.Time.now().to_sec()])
            self.time_canvas.draw_idle()

    # TODO: Consider having this pull directly from the appropriate member variables and update teh label.
    def format_data_label(self):
        # type(Optional[float]) -> None
        npts = None
        nbytes = None
        text: str = ""
        if self.duration is None:
            text = "No region selected"
        else:
            npts = int(self.duration * self.data_rate)
            # TODO: Better estimate of ros_acomms's packing! This assumes no compression.
            nbytes = 16 + 8 + 8 * npts
            text = ("Requesting {:.2f} seconds of {} data at {:.1f} Hz. \n"
                    "This will be ~ {} points, or ~ {} bytes"
                    .format(self.duration, self.active_selection_label,
                            self.data_rate, npts, nbytes))
        self.data_label.setText(text)

    def rate_text_edited(self) -> None:
        self.data_rate = float(self.rate_edit.text())
        self.format_data_label()

    def handle_selection_toggled(self, state, key):
        # type: (bool, str) -> None
        """
        No need to take any action -- this simply changes the behavior of
        click-and-drag in the main plotting window.
        """
        print("Selection toggled! key = {} state = {}".format(key, state))
        print(type(state))
        if state and key != self.active_selection_label:
            print("...and this button is selected! previous selection = {}"
                  .format(self.active_selection_label))
            self.active_selection_label = key
            self.selection_start_time = None
            self.duration = None
            self.redraw()

    def clear_button_clicked(self, _: bool) -> None:
        self.selection_start_time = None
        self.duration = None
        self.maybe_remove_rect()

    def send_button_clicked(self, _: bool) -> None:
        if self.duration is None:
            rospy.logwarn("Cannot send data request if region not selected")
            return
        msg = coexploration_msgs.msg.ScalarDataRequest()
        msg.start_time = rospy.Time(self.selection_start_time)
        msg.duration = self.duration
        msg.rate = self.data_rate
        self.publishers[self.active_selection_label].publish(msg)

    def on_mouse_press(self, event):
        # type: (matplotlib.backend_bases.MouseEvent) -> None
        print("Mouse pressed! event={}, active = {}".format(event, self.active_selection_label))
        if self.active_selection_label == "None":
            return
        self.corner1 = (event.xdata, event.ydata)

    def on_mouse_release(self, event):
        # type: (matplotlib.backend_bases.MouseEvent) -> None
        print("Mouse released! event={}, active = {}".format(event, self.active_selection_label))
        if self.active_selection_label == "None":
            return
        # If mouse is released outside of the canvas, these wil be None.
        if event.xdata is None or event.ydata is None:
            return

        self.maybe_remove_rect()

        self.corner2 = (event.xdata, event.ydata)
        try:
            self.selection_start_time, self.duration = self._get_selection_bounds()
        except Exception as ex:
            print("Tried to get time bounds, failed with exception:")
            print(ex)
            return
        self.redraw()

    def draw_selection_rectangle(self) -> None:
        if self.duration is None:
            return
        if self.selection_artist is not None:
            self.selection_artist.remove()

        xmin = self.selection_start_time
        xmax = self.selection_start_time + self.duration
        ax = self.data_axes[self.active_selection_label]
        self.selection_artist = ax.axvspan(xmin, xmax, color='gray', alpha=0.5)

    def _get_selection_bounds(self):
        # type () -> Tuple[float, float]
        rospy.loginfo("Getting bounds! corner1 = {}, corner2 = {}".format(
            self.corner1, self.corner2))
        t0 = min(self.corner1[0], self.corner2[0])
        dt = np.abs(self.corner1[0] - self.corner2[0])
        return t0, dt

    def maybe_remove_rect(self) -> None:
        if self.selection_artist is not None:
            try:
                self.selection_artist.remove()
                self.selection_artist = None
                self.time_canvas.draw_idle()
            except Exception as ex:
                print("Exception while removing rectangle: {}".format(ex))

    def peaks_callback(self, msg):
        # type: (coexploration_msgs.msg.DorpHits) -> None
        if len(msg.stamps) == 0:
            rospy.logdebug("scalar_gui.peaks_callback received empty DorpHits")
            return
        rospy.logwarn("Got peaks!")

        tt = [rt.to_sec() for rt in msg.stamps]
        min_time = np.min(tt)
        if self.first_time is None:
            # Want to start plot a few mins before first peak so user is able
            # to select context around it.
            self.first_time = min_time - 120.0
        else:
            self.first_time = min(self.first_time, min_time)

        self.dorp_peaks = np.transpose(np.array([tt, msg.dorps]))
        self.redraw()

    def data_callback(self, msg, data_key):
        # type: (coexploration_msgs.msg.AggregatedScalarData, str) -> None
        """ Replace existing data with updated """
        tt = [rt.to_sec() for rt in msg.timestamps]
        min_t = np.min(tt)
        if self.first_time is None:
            self.first_time = min_t - 120
        else:
            self.first_time = min(min_t - 120, self.first_time)
        self.raw_data[data_key] = np.transpose(np.array([tt, msg.values]))
        self.redraw()


if __name__ == "__main__":
    rospy.init_node("scalar_gui")
    app = QtWidgets.QApplication(sys.argv)
    sg = ScalarGui()
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())
