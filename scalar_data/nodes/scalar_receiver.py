# Copyright 2021 University of Washington Applied Physics Laboratory
# Author: Laura Lindzey
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import numpy as np
import rospy

import coexploration_msgs.msg

class ScalarReceiver(object):
    def __init__(self) -> None:
        # This node aggregates all data received from the subsea sender and
        # republishes it so that the GUIs only need to update based on the
        # most recent message. (In practice, they wind up being restarted
        # during a dive, so are not the best place to cache data.)
        self.data: Optional[npt.NDArray[np.float64]] = None

        self.setup_connections()

    def setup_connections(self) -> None:
        self.data_sub = rospy.Subscriber("~scalar_data",
                                         coexploration_msgs.msg.ScalarData,
                                         self.data_callback)
        self.aggregated_pub = rospy.Publisher("~aggregated_data",
                                              coexploration_msgs.msg.AggregatedScalarData,
                                              queue_size=1, latch=True)
    def data_callback(self, msg):
        #type: (coexploration_msgs.msg.ScalarData) -> None
        t0 = msg.start_time.to_sec()
        if len(msg.values) == 1:
            tt = [t0]
            dd = msg.values
        else:
            dt = msg.duration / (len(msg.values) - 1)
            dd = msg.values
            tt = [t0 + dt*ii for ii in range(len(msg.values))]
        if len(tt) != len(dd):
            rospy.logerr("Length mismatch! len(stamps) = {}, len(data) = {}"
                         .format(len(tt), len(dd)))
            return

        new_data = np.transpose(np.array([tt, dd]))

        if self.data is None:
            self.data = new_data
        else:
            self.data = np.append(self.data, new_data, axis=0)
            # Sort by timestamps, which are first column
            idxs = self.data.argsort(axis=0)
            self.data = self.data[idxs[:,0],:]

        self.publish_data()

    def publish_data(self):
        msg = coexploration_msgs.msg.AggregatedScalarData()
        msg.timestamps = [rospy.Time(tt) for tt in self.data[:,0]]
        msg.values = [dd for dd in self.data[:,1]]
        self.aggregated_pub.publish(msg)


if __name__ == "__main__":
    rospy.init_node('scalar_sender')
    sr = ScalarReceiver()
    rospy.spin()
