## ros_acomms_shims

In order for coexploration to work with ros_acomms managing the acoustic
channel, we need a few nodes that live between the coexploration utilities
and the ros_acomms stack. These duplicate some of the functionality found
in the comms_manager.

### queue_monitor.py

This node is responsible for monitoring the state of the queue_manager's
queues and pulling data from quadtree / progressive_imagery to balance
having data available for the next transmission with avoiding long queues.
This is required in order to avoid extreme latency in response to user data
requests.

It is run subsea, and can be configured with:
- monitored_queues:
    - queue_name
    - desired queue length
    - service call to get data
    - bytes of data to request at a time
    - topic to publish data to


### queue_configuration_gui.py

This node provides a GUI for the topside scientist to enable/disable queues
and update their priorities.

All configuration comes from the codec config yaml files.
