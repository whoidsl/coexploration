import threading
import socket
import time
import datetime
import serial
import logging

class UDPSerialServer(object):
    def __init__(self, name='UDPSerialNMEAServer',
                 udp_port=10000,
                 serial_port="/dev/ttyUSB0", baud_rate=19200,
                 log_path=None, log_level='INFO'):

        self.udp_client_addresses = dict()
        self.udp_port = udp_port

        self._serialport = serial.Serial(serial_port, baud_rate, timeout=0.1)
        self._incoming_line_buffer = ""

        self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.udp_socket.bind(('', self.udp_port))

        self.udp_listener_thread = threading.Thread(target=self._listen_udp, daemon=True)
        self.serial_listener_thread = threading.Thread(target=self._listen_serial, daemon=True)

    def start(self):
        self.udp_listener_thread.start()
        self.serial_listener_thread.start()

    def stop(self):
        self.udp_socket.close()
        self._serialport.close()

    def _listen_udp(self):
        while True:
            try:
                bytes, address = self.udp_socket.recvfrom(1024)
                msg = bytes.decode('utf8')
                print("UDP msg rx (dest:", self._serialport.port,"):",msg)

                self.udp_client_addresses[address] = datetime.datetime.now()

                self._send_serial(msg.rstrip('\n') + '\n')

            except Exception as e:
                print(e)

    def _listen_serial(self):
        while True:
            if self._serialport.isOpen():
                msg = self._readline()
                # We are connected, so pass through to NMEA
                if (msg):
                    self._send_udp(msg)
                    #print(msg)
                    print("serial rx (dest:",self.udp_port,"):",msg)

            else:  # not connected
                time.sleep(0.5)  # Wait half a second, try again.

    def _send_udp(self, msg):
        for address in self.udp_client_addresses:
            last_rx = self.udp_client_addresses[address]
            if datetime.datetime.now() - last_rx < datetime.timedelta(days=1):
                # If we've receivd from this client in the last day, continue to send messages
                self.udp_socket.sendto(msg.encode('utf8'), address)
            else:
                # Else remove this address from our list of clients
                self.udp_client_addresses.pop(address)

    def _send_serial(self, msg):
        self._serialport.write(msg.encode('utf-8'))

    def _readline(self):
        """Returns a \n terminated line from the modem.  Only returns complete lines (or None on timeout)"""
        rl = self._serialport.readline().decode('utf-8')

        if rl == "":
            return None

        # Make sure we got a complete line.  Serial.readline may return data on timeout.
        if rl[-1] != '\n':
            self._incoming_line_buffer += rl
            return None
        else:
            if self._incoming_line_buffer != "":
                rl = self._incoming_line_buffer + rl
            self._incoming_line_buffer = ""
            return rl


class UDPSerialClient(object):
    def __init__(self, udp_ip, udp_port,
                 name='UDPSerialClient',
                 log_path=None, log_level='INFO'):

        self.udp_send_ip = udp_ip
        self.udp_send_port = udp_port

        self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.udp_socket.bind(('', self.udp_send_port+1))

        self.udp_listener_thread = threading.Thread(target=self._listen_udp, daemon=True)

    def start(self):
        self.udp_listener_thread.start()

    def stop(self):
        self.udp_socket.close()

    def _listen_udp(self):
        while True:
            try:
                bytes, address = self.udp_socket.recvfrom(1024)
                msg = bytes.decode('utf8')

                print("Received NMEA: " + msg)

            except Exception as e:
                print(e)

    def write(self, msg):
        self.udp_socket.sendto(msg.encode('utf8'), (self.udp_send_ip, self.udp_send_port))


if __name__ == '__main__':
    server = UDPSerialServer(udp_port=54321, serial_port="/dev/ttyUSB0", baud_rate=19200)
    client = UDPSerialClient(udp_ip='127.0.0.1', udp_port=12345)
    server.start()
    client.start()
    while True:
        time.sleep(3)
