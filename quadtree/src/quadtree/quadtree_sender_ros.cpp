/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

//
// Created by llindzey on Dec 12, 2019.
//

#include "quadtree/quadtree_sender_ros.h"

namespace quadtree {

QuadTreeSenderNode::QuadTreeSenderNode() {
  setup();
}

QuadTreeSenderNode::~QuadTreeSenderNode() = default;

void QuadTreeSenderNode::setupParameters() {
  auto nh = ros::NodeHandle("~");

  if (!nh.getParam("horizontal_resolution", horizontal_resolution_)) {
    ROS_FATAL("Unable to find parameter horizontal_resolution");
    ROS_BREAK();
  }
  if (!nh.getParam("vertical_resolution", vertical_resolution_)) {
    ROS_FATAL("Unable to find parameter vertical_resolution");
    ROS_BREAK();
  }
  // TODO: It might make more sense to define min/max stream level as referring
  // to leaf nodes, not roots...
  if (!nh.getParam("min_stream_level", min_stream_level_)) {
    ROS_FATAL("Unable to find parameter min_stream_level");
    ROS_BREAK();
  }
  if (!nh.getParam("max_stream_level", max_stream_level_)) {
    ROS_FATAL("Unable to find parameter max_stream_level");
    ROS_BREAK();
  }

  if (!nh.getParam("send_levels", send_levels_)) {
    ROS_FATAL("Unable to find parameter send_levels");
    ROS_BREAK();
  }

  if (!nh.getParam("use_prestaged_map", use_prestaged_map_)) {
    ROS_FATAL("Unable to find parameter use_prestaged_map");
    ROS_BREAK();
  }

  if (!nh.getParam("send_delay", send_delay_)) {
    ROS_FATAL("Unable to find parameter send_delay");
    ROS_BREAK();
  }

  nh.param<std::string>("grid_filename", grid_filename_, "");
  nh.param<std::string>("map_frame", map_frame_, "");

  if (use_prestaged_map_) {
    if ("" == grid_filename_) {
      ROS_FATAL("Prestaged map requires grid_filename_ parameter must be supplied");
      ROS_BREAK();
    }
  } else {
    if ("" == map_frame_) {
      ROS_FATAL("Map from pointclouds requires setting map_frame_ parameter");
      ROS_BREAK();
    }
  }
}

void QuadTreeSenderNode::setupPublishers() {
  auto nh = ros::NodeHandle("~");
  const int queue_size = 1;
  metadata_pub_ = nh.advertise<coexploration_msgs::QuadtreeMetadata>(
      "metadata", queue_size, false);
  if (!metadata_pub_) {
    ROS_FATAL("Unable to create publisher. Exiting.");
    ROS_BREAK();
  }
}

void QuadTreeSenderNode::setupSubscriptions() {
  auto nh = ros::NodeHandle("~");

  clear_sub_ = nh.subscribe("clear", 1, &QuadTreeSenderNode::handleClear, this);
  request_data_sub_ = nh.subscribe("incoming_data", 1,
				  &QuadTreeSenderNode::handleDataRequest, this);
  if (!use_prestaged_map_) {
    pointcloud_sub_ = nh.subscribe("pointcloud", 1,
				   &QuadTreeSenderNode::handlePointCloud, this);
    multibeam_raw_sub_ = nh.subscribe("multibeam_raw", 1,
                                      &QuadTreeSenderNode::handleRaw, this);
  }
}

void QuadTreeSenderNode::setupServices() {
  auto nh = ros::NodeHandle("~");

  auto data_cb =
      boost::bind(&QuadTreeSenderNode::dataRequestCallback, this, _1, _2);
  data_server_ =
      nh.advertiseService<ds_acomms_msgs::ModemDataRequest::Request,
                          ds_acomms_msgs::ModemDataRequest::Response>(
          "data_request", data_cb);
}

void QuadTreeSenderNode::setupTimers() {
  auto nh = ros::NodeHandle("~");

  // TODO: Publish once a minute. (publishing more frequently now for testing)
  auto cb1 = boost::bind(&QuadTreeSenderNode::publishMetadata, this, _1);
  metadata_timer_ = nh.createTimer(ros::Duration(1.0), cb1);

}

void QuadTreeSenderNode::setup() {
  setupParameters();
  setupSubscriptions();
  setupPublishers();
  setupTimers();
  setupServices();

  tree_ = std::make_unique<quadtree::QuadTreeSender>(
      horizontal_resolution_, vertical_resolution_, send_levels_,
      min_stream_level_, max_stream_level_, send_delay_);

  tree_->setLogMessageCallback("fatal", RosFatalMessageCallback);
  tree_->setLogMessageCallback("error", RosErrorMessageCallback);
  tree_->setLogMessageCallback("warn", RosWarnMessageCallback);
  tree_->setLogMessageCallback("info", RosInfoMessageCallback);
  tree_->setLogMessageCallback("debug", RosDebugMessageCallback);

  // If using prestaged map, initialize map from existing grid
  if (use_prestaged_map_) {
    loadGrid();
  }
}

void QuadTreeSenderNode::loadGrid() {
  if ("" == grid_filename_) {
    return;
  }

  ROS_INFO_STREAM("Loading map from file: " << grid_filename_);
  // Register all GDAL drivers
  GDALAllRegister();

  auto ptr = (GDALDataset*)GDALOpen(grid_filename_.c_str(), GA_ReadOnly);
  dataset_.reset(ptr);

  if (nullptr == dataset_) {
    ROS_WARN_STREAM("Unable to load dataset from file: " << grid_filename_);
  } else {
    ROS_INFO("Loaded dataset!");
  }

  // Hmmm. No projection attached to the output of our GMT gridding? I guess
  // I can just assume 1m x 1m grids, but that seems extremely sloppy.
  ROS_INFO_STREAM("QuadTreeSenderNode::LoadGrid. Projection is: "
		  << dataset_->GetProjectionRef() << ". Dataset has "
		  << dataset_->GetRasterCount() << "layers, "
		  << "and raster dimensions are " << dataset_->GetRasterXSize()
		  << " x " << dataset_->GetRasterYSize());

  auto band = dataset_->GetRasterBand(1);  // 1-indexed
  int foo;
  double nodata = band->GetNoDataValue(&foo);

  ROS_INFO_STREAM("QuadTreeSenderNode::loadGrid: Got band! Min/Max: "
		  << band->GetMinimum(&foo) << " / " << band->GetMaximum(&foo)
		  << ". No-Data value = " << nodata);

  float val;
  int nan_count = 0;
  int numx = dataset_->GetRasterXSize();
  int numy = dataset_->GetRasterYSize();
  // Due to the required time delay before streaming, if the time is set to
  // "now" there will be a confusing no-messages period at startup.
  int32_t tt = ros::Time::now().sec - 24*60*60;
  for (int xx = 0; xx < numx; xx++) {
    for (int yy = 0; yy < numy; yy++) {
      band->RasterIO(GF_Read, xx, yy, 1, 1, &val, 1, 1, GDT_Float32, 0, 0);
      if (std::isnan(val)) {
        nan_count += 1;
        continue;
      }
      tree_->AddPoint(xx, yy, -1 * val, tt);
    }
  }
  ROS_INFO_STREAM("QuadTreeSenderNode::loadGrid: Raster had " << nan_count
		  << "/" << numx * numy << " NaN cells.");
  ROS_INFO_STREAM("Done with setup!");
}

void QuadTreeSenderNode::handleDataRequest(const ds_acomms_msgs::ModemData& msg) {
  std::list<quadtree::GridRequest> requests;
  ROS_INFO("handleDataRequest");
  bool success = deserializeRequests(msg.payload, &requests);
  if (success) {
    tree_->AddPriorityRequests(requests);
    ROS_INFO("Successfully added priorities!");
  }
}

void QuadTreeSenderNode::handlePointCloud(const PointCloud::ConstPtr& msg) {
  PointCloud pc;
  ros::Time time;
  pcl_conversions::fromPCL(msg->header.stamp, time);
  bool success = tf_listener_.waitForTransform(msg->header.frame_id, map_frame_, time, ros::Duration(1));
  if (success) {
    pcl_ros::transformPointCloud(map_frame_, *msg, pc, tf_listener_);
    for (const auto& pt : pc.points) {
      tree_->AddPoint(pt.x, pt.y, pt.z, time.sec);
    }
  } else {
    ROS_WARN("Unable to transform point cloud into map frame");
  }
}

void QuadTreeSenderNode::handleRaw(const ds_multibeam_msgs::MultibeamRaw& msg) {
  // First, determine whether it's possible to perform the required transform
  bool success = tf_listener_.waitForTransform(msg.header.frame_id, map_frame_,
                                               msg.header.stamp,
					       ros::Duration(1));
  if (success) {
    // It doesn't look like the message format itself enforces that
    // all vectors are the same length.
    if ((msg.beamflag.size() != msg.twowayTravelTime.size()) ||
        (msg.beamflag.size() != msg.angleAlongTrack.size()) ||
        (msg.beamflag.size() != msg.angleAcrossTrack.size())) {
      ROS_WARN_STREAM("Received MultibeamRaw with inconsistent field lengths at"
                      << msg.header.stamp.toSec());
    } else {
      // Construct point cloud in sensor frame, assuming:
      // * angleAlongTrack is positive with positive x (like pitch)
      // * angleAcrossTrack is positive to negative y (like roll)
      PointCloud::Ptr msg_pc (new PointCloud);
      msg_pc->header.frame_id = msg.header.frame_id;
      for (int idx=0; idx < msg.beamflag.size(); idx++) {
        if (msg.beamflag[idx] != msg.BEAM_OK) {
          continue;
        }
        float twtt = msg.twowayTravelTime[idx];
        float roll = msg.angleAcrossTrack[idx];
        float pitch = msg.angleAlongTrack[idx];
	float range = 0.5 * twtt * msg.soundspeed;

	tf::Matrix3x3 mat;
	mat.setRPY(roll, pitch, 0);
	tf::Vector3 vec = mat * tf::Vector3(0, 0, range);
	msg_pc->push_back(pcl::PointXYZ(vec[0], vec[1], vec[2]));
      }

      PointCloud pc;  // Transformed pointcloud
      pcl_ros::transformPointCloud(map_frame_, *msg_pc, pc, tf_listener_);
      for (const auto& pt : pc.points) {
        tree_->AddPoint(pt.x, pt.y, pt.z, msg.header.stamp.sec);
      }
    }
  } else {
    ROS_WARN("Unable to transform point cloud into map frame");
  }
}

void QuadTreeSenderNode::handleClear(const std_msgs::Empty& msg) {
  ROS_INFO("Received request to clear the priority queue");
  tree_->ClearPriorities();
}

void QuadTreeSenderNode::publishMetadata(const ros::TimerEvent& event) {
  coexploration_msgs::QuadtreeMetadata msg;
  msg.num_points = tree_->num_points();
  msg.horizontal_resolution = horizontal_resolution_;
  msg.vertical_resolution = vertical_resolution_;
  msg.send_levels = send_levels_;
  metadata_pub_.publish(msg);
}

bool QuadTreeSenderNode::dataRequestCallback(
    const ds_acomms_msgs::ModemDataRequest::Request& request,
    ds_acomms_msgs::ModemDataRequest::Response& response) {

  bool small;
  if (request.bytes_requested < kSmallSubgridBytes) {
    ROS_INFO_STREAM("Requested " << request.bytes_requested << " bytes. "
		    << "Minimum subgrid size is " << kSmallSubgridBytes);
    return false;  // can't even get a small subgrid out
  } else if (request.bytes_requested >= kSubgridBytes) {
    small = false;  // Enough space for a regular subgrid
  } else {
    small = true;
  }
  int32_t timestamp = ros::Time::now().sec;
  auto subgrid = tree_->GetNextSubgrid(false, timestamp);
  if (!subgrid) {
    return false;
  }

  bool success = serializeSubgrid(*subgrid.get(), &response.data);
  // TODO: Shouldn't be an assert ... but need tests ensuring lengths are never
  // exceeded.
  assert(response.data.size() <= request.bytes_requested);

  return success;
}

}  // namespace quadtree
