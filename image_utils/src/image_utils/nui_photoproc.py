#! /usr/bin/env python3
"""
Given a request from topside with a timestamp, find the image captured
closest to that time, convert it to an appropriate format, and copy to
the progressive imagery transmit directory.

Author: Laura Lindzey, 2022
"""
import numpy as np
import os
os.environ['TZ'] = 'UTC'
import rospy
import subprocess

import std_msgs.msg  # For Time


class ImageFetcher(object):
    def __init__(self):

        # /data/coex/cam
        self.capture_image_dir = rospy.get_param("~capture_image_dir")
        # /data/coex/pi_data/tx
        self.transmit_image_dir = rospy.get_param("~transmit_image_dir")

        try:
            os.listdir(self.capture_image_dir)
        except OSError as ex:
            # Likewise, allowing this to raise to fail on launch.
            rospy.logfatal("Please create capture_image_dir = {}".format(self.capture_image_dir))
            raise(ex)

        try:
            os.listdir(self.transmit_image_dir)
        except OSError as ex:
            # Likewise, allowing this to raise to fail on launch.
            rospy.logfatal("Please create capture_image_dir = {}".format(self.transmit_image_dir))
            raise(ex)

        # Setup subscribers
        self.sub = rospy.Subscriber("~requested_image_time", std_msgs.msg.Time, self.time_callback)

        # Maps filename in capture_image_dir to its timestamp
        self.image_times = {}


    def time_callback(self, msg):
        '''
        Handles the whole pipeline from figuring out available raw images
        to copying it and processing it.

        '''
        filenames = [ff for ff in os.listdir(self.capture_image_dir) if ff.endswith("jpg")]
        for ff in filenames:
            if ff not in self.image_times:
                filepath = "{}/{}".format(self.capture_image_dir, ff)
                self.image_times[ff] = os.path.getmtime(filepath)
        if len(self.image_times) == 0:
            rospy.logerr("Image Sender has no remote images; cannot process request")
            return

        closest_image = min(self.image_times,
                            key=lambda x: np.abs(msg.data.secs - self.image_times[x]))
        closest_time = self.image_times[closest_image]
        rospy.loginfo("Found image {} (time = {}), which is {} seconds from request {}"
                      .format(closest_image, closest_time,
                              closest_time - msg.data.secs, msg.data.secs))

        self.process_image(closest_image)


    def process_image(self, filename):
        input_filepath = "{}/{}".format(self.capture_image_dir, filename)
        tiff_filename = filename.replace("jpg", "tiff")
        output_filepath = "{}/{}".format(self.transmit_image_dir, tiff_filename)

        # NUI's cameras log to jpg, which progressive_imagery doesn't support.
        # So, convert to tiff (seems goofy...)
        convert_cmd = ["convert", input_filepath, output_filepath]
        try:
            result = subprocess.check_output(convert_cmd)
        except Exception as ex:
            print(ex)
        rospy.loginfo("Copied {} to {}!".format(input_filepath, output_filepath))


if __name__ == "__main__":
    os.nice(10) # Don't want this to hog CPU!
    rospy.init_node("sentry_photoproc")
    img_fetcher = ImageFetcher()
    rospy.spin()
