/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Jan 24, 2020.
//

#ifndef DS_QUADTREE_QUADTREE_RECEIVER_H
#define DS_QUADTREE_QUADTREE_RECEIVER_H

#include <quadtree/quadtree.h>

namespace quadtree {

/**
    @brief Quadtree implementation specialized for receiving acoustic updates.

    This assumes that the canonical representation of the map is held by
    the remote quadtree sender, and incrementally updates its own
    representation based on received subgrids.

    This means that its topology needs to match the remote tree's;
    subgrid updates include information about the other tree's root in
    order to enable this.

    Due to the potential for maps getting out of sync temporally, intermediate
    averages are NOT updated in the receiver tree -- it only contains
    timestamped data, exactly as received.
*/
class QuadTreeReceiver : public QuadTree {

 public:
  /**
     @brief Construct a QuadTreeSender.

     None of these parameters can be changed after construction,
     since the acoustic updates assume that these are constants,
     agreed up on with the sender.

     @param h_res horizontal resolution controlling cell size (meters)
     @param v_res vertical resolution for stored elevations (meters)
     @param send_levels how many levels to include in each transmitted subgrid
  */
  QuadTreeReceiver(float h_res, float v_res, int send_levels);


  /**
     @brief Update quadtree with the contents of the received subgrid.

     The update involves both growing the tree and updating node values.

     This will report an error if the received subgrid does not match the
     existing topology of the received one.
  */
  bool AddSubgrid(const Subgrid& subgrid);

  /**
     @brief Add root as specified coordinates, connecting to the existing tree.

     Updating the receiver's tree with a subgrid requires keeping
     the topology synchronized with the sender's tree. This is enabled
     by also transmitting the sender's current root level and position,
     which uniquely specifies the topology of all lower levels.

     @param level level of root
     @param xc x coordinate of new root (cell units)
     @param yc y coordinate of new root (cell units)
  */
  bool GrowTree(int level, float xc, float yc);


  int num_received() { return received_subgrid_count_; };

 private:
  // Return a pointer to the internal node that is the root of this subgrid
  Node* MakeOrFindNode(float xx, float yy, int level, float* nodex, float* nodey);

  // Make sure that all of the children nodes for a subgrid of this depth exist.
  // (Unlike the Sender tree, empty nodes are allowed to exist, and will have
  // int max as their value.)
  void MakeSubgridChildren(int level, Node* node);

  void CopySubgrid(const Subgrid& subgrid, Node* node);

  // How many subgrids have been received
  int received_subgrid_count_;

};  // class QuadTreeReceiver

}  // namespace quadtree

#endif  // DS_QUADTREE_QUADTREE_RECEIVER_H
