/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Dec 13, 2019.
//
#pragma once

#include <quadtree/quadtree.h>
#include <quadtree/quadtree_sender.h>

#include <gdal/gdal_priv.h>
#include <queue>

#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <tf/transform_listener.h>

#include "coexploration_msgs/QuadtreeMetadata.h"

#include "ds_acomms_msgs/ModemData.h"
#include "ds_acomms_msgs/ModemDataRequest.h"
#include "ds_multibeam_msgs/MultibeamRaw.h"
#include "sensor_msgs/PointCloud2.h"
#include "std_msgs/Empty.h"

#include "quadtree/quadtree_serialization.h"
#include "quadtree/quadtree_ros.h"

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

namespace quadtree {

class QuadTreeSenderNode {
  friend class QuadTreeSenderNodeTest_test_request_data_no_tree_Test;
  friend class QuadTreeSenderNodeTest_test_save_large_grid_Test;

 public:
  explicit QuadTreeSenderNode();
  ~QuadTreeSenderNode();

 protected:
  void setupParameters();
  void setupPublishers();
  void setupSubscriptions();
  void setupServices();
  void setupTimers();
  void setup();

  bool dataRequestCallback(
      const ds_acomms_msgs::ModemDataRequest::Request& request,
      ds_acomms_msgs::ModemDataRequest::Response& response);

 private:
  typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

  void publishMetadata(const ros::TimerEvent& event);
  void handleClear(const std_msgs::Empty& msg);
  void handleDataRequest(const ds_acomms_msgs::ModemData& msg);
  void handlePointCloud(const PointCloud::ConstPtr& msg);
  void handleRaw(const ds_multibeam_msgs::MultibeamRaw& msg);
  void loadGrid();

  std::unique_ptr<quadtree::QuadTreeSender> tree_;

  // Parameters controlling the quadtree...
  float horizontal_resolution_;
  float vertical_resolution_;
  int min_stream_level_;
  int max_stream_level_;
  int send_levels_;
  int32_t send_delay_;

  // Whether we're using pre-staged map or building from pointclouds.
  // This is useful for testing the transfer algorithm development.
  bool use_prestaged_map_;

  // Fully-specified path to input grid, if running with prestaged map
  std::string grid_filename_;
  // If non-empty, use this as the map frame and build map from pointclouds
  std::string map_frame_;
  tf::TransformListener tf_listener_;

  // QUESTION: Why does GdalDatasetPrivate use <GDALDataset, void (*)(void*)>
  std::unique_ptr<GDALDataset> dataset_;

  // pub/sub!
  ros::Subscriber request_sub_;
  ros::Subscriber clear_sub_;
  ros::Subscriber pointcloud_sub_;
  ros::Subscriber multibeam_raw_sub_;
  // For serialized data sent using the streaming data mechanism in
  // comms_manager
  ros::Subscriber request_data_sub_;
  ros::ServiceServer data_server_;
  ros::Publisher metadata_pub_;

  ros::Timer metadata_timer_;
  ros::Timer stats_timer_;
};  // class QuadTreeSenderNode

}  // namespace quadtree