#!/bin/python

import argparse
import hashlib
import os
from binascii import hexlify
from time import sleep
import sys
from sys import exit
from serial import Serial
from serial.tools import list_ports

# Use tqdm for a pretty interface, if it is available
try:
    from tqdm import tqdm
    has_tqdm = True
except ImportError:
    tnrange = range
    has_tqdm = False

# Use EasyGUI if it is available
try:
    import easygui
    has_easygui = True
except ImportError:
    has_easygui = False

class ModemUpdater(object):
    def __init__(self, port, update_baud_rate=115200, initial_baud_rate=None, uart=None, verbose=False):
        self.serial = Serial(port=port, baudrate=19200, timeout=1)
        self.bytes_per_message = 1013
        self.verbose = verbose

        if initial_baud_rate:
            self.serial.baudrate = initial_baud_rate
            self.initial_baud_rate = initial_baud_rate
        else:
            rate = self.detect_baud_rate()
            if rate is not None:
                print("Baud rate: {}".format(rate))
            else:
                self.error_out("Failed to detect modem baud rate.  Check serial port?")
            self.initial_baud_rate = rate

        if uart:
            self.uart = uart
        else:
            self.uart = self.detect_uart()
            if self.uart is not None:
                print("Connected to UART {}".format(self.uart))
            else:
                self.error_out("Failed to detect connected UART.  Check serial port, and try specifying a UART (e.g. "
                               "--uart 1).")

        self.update_baud_rate = update_baud_rate

        booted_slot = self.get_booted_slot()
        if booted_slot is None:
            self.error_out("Error reading currently booted firmware slot.  Please try again.")
        else:
            print("Booted slot {}".format(booted_slot))
            self.booted_slot = booted_slot

    def error_out(self, message):
        print(message)
        # Try to set the baud rate back
        self.set_baud_rate(self.initial_baud_rate)
        exit(2)

    def wait_for_strings(self, msg_strings, retries=10, progress_bar=None):
        if not has_tqdm:
            progress_bar = None
        if progress_bar:
            retry_progress = tqdm(desc=progress_bar, total=retries, bar_format="{desc}: |{bar}|")
        for i in range(retries):
            line = self.readline()
            if line:
                if any(m in line for m in msg_strings):
                    if progress_bar:
                        retry_progress.update(retries)
                        retry_progress.close()
                    return line
            if progress_bar:
                retry_progress.update(1)
        if progress_bar:
            retry_progress.close()
        return None

    def do_update(self, file_name, slot=None):
        if not slot:
            if self.booted_slot == 1:
                slot = 2
            else:
                slot = 1

        file_size = os.path.getsize(file_name)
        packets_to_send = (file_size + (self.bytes_per_message - 1)) // self.bytes_per_message

        with open(file_name, 'rb') as firmware_file:
            firmware_blob = firmware_file.read(file_size)

        sha_hash = hashlib.sha1(firmware_blob).hexdigest()

        # Switch the baud rate
        self.set_baud_rate(self.update_baud_rate)

        # Disable NMEA logging (in case it is running)
        self.write_string("$CCCFG,log.nmea.level,0\r\n")

        mfw_msg = "$UPMFW,{slot},{data_loc},{filesize},{reboot},{shahash},{filename}\r\n".format(
            slot=slot,
            data_loc=0,
            filesize=file_size,
            reboot=0,
            shahash=sha_hash,
            filename="test.fw"
        )

        self.write_string(mfw_msg)
        response = self.wait_for_strings(["UPMFWA", "UPERR"], retries=20)
        if response is None:
            self.error_out("Timeout waiting for $UPMFWA, please try again")
        if "UPERR" in response:
            self.error_out("Error updating firmware: {}".format(response))
        
        if has_tqdm:
            upload_progress = tqdm(desc="Uploading firmware", total=file_size, unit="byte", unit_scale=True)

        for pos in range(0, file_size, self.bytes_per_message):
            data_bytes = firmware_blob[pos:pos + self.bytes_per_message]
            data_string = hexlify(bytes(data_bytes)).decode('ascii')
            self.write_string("$UPDAT,{}\r\n".format(data_string))
            response = self.wait_for_strings(["UPDATA", "UPERR"])
            if response is None:
                self.error_out("Timeout waiting for $UPDATA, please try again")
            if "UPERR" in response:
                self.error_out("Error updating firmware: {}".format(response))

            if has_tqdm:
                upload_progress.update(self.bytes_per_message)
        
        if has_tqdm:
            upload_progress.close()

        response = self.wait_for_strings(["UPDONE", "UPERR"], retries=300, progress_bar="Writing flash")
        if response is None:
            self.error_out("Timeout waiting for $UPDONE, please try again")
        if "UPERR" in response:
            self.error_out("Error updating firmware: {}".format(response))

        # Reset the baud rate back to the original
        self.set_baud_rate(self.initial_baud_rate)

        # Reboot into new firmware
        self.write_string("$CCRST,{}\r\n".format(slot))
        # Wait a second
        sleep(5)
        new_baud = self.detect_baud_rate()
        if new_baud is None:
            self.error_out("Error booting into new firmware (no response)")

        booted_slot = self.get_booted_slot()
        if booted_slot != slot:
            self.error_out("Didn't boot into new firmware.  Try manually?")

        self.print_firmware_summary()

    def set_baud_rate(self, baud_rate):
        self.write_string("$CCCFG,uart{0}.bitrate,{1}\r\n".format(self.uart, baud_rate))
        self.wait_for_strings(["CACFG"])
        self.serial.baudrate = baud_rate
        print("Setting serial port baud rate to {}".format(baud_rate))

        # Give it a few tries
        for i in range(10):
            sleep(0.1)
            self.serial.flushInput()
            self.write_string("$CCCFQ,SRC\r\n")
            line = self.readline()
            if line:
                if "$CACFG,SRC" in line:
                    return
        self.error_out("Error setting baud rate, try again")

    def write_string(self, data):
        if self.verbose:
            #print("< ", data)
            pass
        self.serial.write(data.encode('ascii'))

    def readline(self):
        try:
            line = self.serial.readline().decode('ascii')
            if self.verbose:
                print("> ", line.strip())
            return line
        except UnicodeDecodeError:
            # garbage characters...
            return None

    def detect_baud_rate(self):
        rates = [19200, 115200, 921600, 9600, 4800, 38400, 57600, 230400, 460800]
        if has_tqdm:
            rates = tqdm(rates, desc="Detecting baud rate", bar_format="{desc}: |{bar}| {n_fmt}/{total_fmt}")
        for rate in rates:
            self.serial.baudrate = rate

            sleep(0.1)
            self.serial.flushInput()
            self.write_string("$CCCFQ,SRC\r\n")
            line = self.readline()
            if line:
                if "$CACFG,SRC" in line:
                    if has_tqdm:
                        rates.clear()
                        rates.close()
                    return rate
        return None

    def detect_uart(self):
        uarts = [1, 2, 3, 4]
        if has_tqdm:
            uarts = tqdm(uarts, desc="Detecting connected UART", bar_format="{desc}: |{bar}| {n_fmt}/{total_fmt}")
        for uart in uarts:
            sleep(0.1)
            self.serial.flushInput()
            uart_mask = 0x01 << (uart - 1)
            self.write_string("$CCPST,{},0,0,0,0,,UART {}\r\n".format(uart_mask, uart))
            line = self.readline()
            if line:
                if "$CAPST" in line:
                    if has_tqdm:
                        uarts.clear()
                        uarts.close()
                    return uart
        return None

    def get_parameter(self, parameter_name):
        self.serial.flushInput()
        self.write_string("$CCCFQ,{}\r\n".format(parameter_name))
        line = self.readline()
        if line:
            if "$CACFG,{}".format(parameter_name) in line:
                args = line.replace('*', ',').split(',')
                return args[2]
        return None

    def get_booted_slot(self):
        slot = self.get_parameter("info.booted_slot")
        try:
            return int(slot)
        except:
            return 0

    def print_firmware_summary(self):
        print("Firmware version: {}".format(self.get_parameter("info.firmware_version")))


def run_update(serial_port, file_name, slot=None, update_baud_rate=115200, initial_baud_rate=None,
               uart=None, verbose=False):
    mu = ModemUpdater(serial_port, update_baud_rate=update_baud_rate,
                      initial_baud_rate=initial_baud_rate, uart=uart, verbose=verbose)
    mu.do_update(file_name, slot=slot)


def run_gui():
    file_name = easygui.fileopenbox("Select uModem2 firmware file", filetypes=['*.ldr'])
    if not file_name:
        exit(1)
    list_of_ports = list_ports.comports()
    port_names = [p.device for p in list_of_ports]
    serial_port = easygui.choicebox("Select serial port", choices=port_names)
    if not serial_port:
        exit(1)
        
    # The fast option causes more problems than it is worth.  
    # It can be uncommented if you know that the user has a good serial interface
    #fast = easygui.boolbox("Use fast serial (921600bps) rate for update?\nThis requires a compatible serial port, "
    #                       "but speeds up the firmware upload.", ["Yes", "No"])
    #if fast:
    #    update_baud_rate = 921600
    #else:
    update_baud_rate = 115200

    run_update(serial_port, file_name, update_baud_rate=update_baud_rate)
    input("Press Enter to exit.")

if __name__ == '__main__':
    ap = argparse.ArgumentParser(description='Update Micromodem-2 Firmware.')
    ap.add_argument("serial_port", help="Serial port name or path (e.g. COM1 on Windows or /dev/ttyS1 on Linux)")
    ap.add_argument("file_name", help="Path to firmware .ldr file")
    ap.add_argument("-b", "--current_baud_rate", type=int,
                    help="Set current baud rate of modem serial port (disable autobauding)")
    ap.add_argument("-s", "--slot", default='auto',
                    choices=['auto', 'Slot-1', 'Slot-2', 'Recovery', 'Bootloader', 'Coproc'],
                    help="Firmware Slot to Flash. Default is the not-currently-booted slot.")
    ap.add_argument("-u", "--uart", default='auto', choices=['auto', '1', '2', '3', '4'],
                    help="Current UART connected to on the modem. Default is auto")
    ap.add_argument("-r", "--reboot", action='store_true', help="Force the modem to reboot on completion.")
    ap.add_argument("-f", "--fast", action='store_true',
                    help="Upload firmware at 921600 bps (requires capable serial port and modem already running "
                         "firmware newer than 2.0.16000)")
    ap.add_argument("-v", "--verbose", action='store_true',
                    help="Display all messages from modem instead of progress bar.")

    if len(sys.argv) == 1:
        if has_easygui:
            # Run GUI if no arguments passed
            print("Running GUI interface.  To see command-line options, pass the -h parameter.")
            run_gui()
            exit(1)
        else:
            print("GUI library not available.  To use it, install easygui (pip install easygui).")

    args = ap.parse_args()

    # 0=Recovery, 1 = Slot 1, 2 = Slot 2
    if args.slot in ('Slot-1', '1'):
        slot = 1
    elif args.slot in ('Slot-2', '2'):
        slot = 2
    elif args.slot == 'Recovery':
        slot = 0
    elif args.slot == 'Bootloader':
        slot = 3
    elif args.slot == 'Coproc':
        slot = 4
    else:
        slot = None

    if args.uart == 'auto':
        uart = None
    else:
        uart = int(args.uart)

    if args.reboot:
        reboot = 1
    else:
        reboot = 0

    if args.fast:
        update_baud_rate = 921600
    else:
        update_baud_rate = 115200

    verbose = args.verbose

    if not has_tqdm:
        print("tqdm is not installed, so you can't see nice progress bars (using verbose mode instead)")
        print("To get progress bars, install tqdm (pip install tqdm).")
        verbose = True

    run_update(serial_port=args.serial_port, file_name=args.file_name, slot=slot, uart=uart,
               update_baud_rate=update_baud_rate, initial_baud_rate=args.current_baud_rate, verbose=verbose)
