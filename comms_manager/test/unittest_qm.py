#! /usr/bin/env python2.7

from __future__ import print_function

import copy
import importlib
import numpy as np
import time
import types # for ModuleType
import unittest

import rospkg
import rospy
import sensor_msgs.msg
import std_msgs.msg

import ds_acomms_msgs.msg

# NB: I'm not sure what the recommended way is to install a ROS python
#   package in a way that allows imports for testing purposes but doesn't
#   expect anything else to use them. So, I went the full setup/install route.
#   * http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
#   * https://answers.ros.org/question/59163/how-do-i-install-a-python-module-with-a-different-name-than-the-catkin-package/

import comms_manager


# for mypy; should probably  make this optional
try:
    from typing import Dict, List, Tuple, Type
except:
    pass

# I played around a bit with actually running mypy rather than just using its
# type syntax for documentation:
# llindzey@llindzey-t480s:~/sentry_ws/src/noaa_coexploration$ mypy --py2 --check-untyped-defs --ignore-missing-imports test_qm.py
#test_qm.py:86: error: Incompatible types in assignment (expression has type "List[Any]", variable has type "bool")
#test_qm.py:93: error: Incompatible types in assignment (expression has type "List[Any]", variable has type "bool")
#test_qm.py:101: error: Incompatible types in assignment (expression has type "List[str]", variable has type "bool")
#test_qm.py:103: error: Incompatible types in assignment (expression has type "str", variable has type "bool")
# ... It really didn't like re-using the same dummy variable. This is discussed in this issue:
# https://github.com/python/mypy/issues/1174


def set_random_data(msg):
    # type: (rospy.Message) -> None
    # NB: I'm not sure if this is the correct way to specify to mypy that
    #     we expect `issubclass(type(msg), rospy.Message)` to evaluate to True
    '''
    Iterate over all the fields in the input message and fill them up with
    random data.
    '''
    for slot, slot_type in zip(msg.__slots__, msg._slot_types):

        # If the field is an array, determine how many elements to fill it with.
        # (If specified, use that; otherwise, random)
        is_array = False
        if '[' in slot_type:
            is_array = True
            slot_type, array_len_str = slot_type.rstrip(']').split('[')
            try:
                array_len = int(array_len_str)
            except ValueError:
                array_len = np.random.randint(5, 15)

        # Go through all the base classes defined by ROS.
        # Any that still have Exception("NYI") are b/c they don't show up in
        # our message definitions, so that branch of code hadn't triggered
        # failures yet ...
        if slot_type in ['bool']:
            if is_array:
                bool_data = [bool(np.random.randint(0, 2)) for ii in range(array_len)]
            else:
                bool_data = bool(np.random.randint(0, 2))
            setattr(msg, slot, bool_data)

        # NB: 'char' is the deprecated alias for uint8
        # NB: for the python interface, ROS treats uint8 as a bytes type
        # Serialization fails if a single field of uint8 is a chr, but the types don't match
        # if I set uint8[] to be an array of ints instead of this str.
        elif slot_type in ['uint8', 'char']:
            if is_array:
                char_data = ''.join(chr(np.random.randint(0, 100))
                                    for ii in range(array_len))
            else:
                char_data = np.random.randint(0, 2**6)
            setattr(msg, slot, char_data)

        # NB: 'byte' is the deprecated aliases for int8
        # NB: For python2, int64/uint64 will be type long, not int. In python3 they're all type int.
        elif slot_type in ['int8', 'int16', 'uint16', 'int32', 'uint32',
                           'int64', 'uint64', 'byte']:
            if is_array:
                int_data = [np.random.randint(0, 100) for ii in range(array_len)]
            else:
                int_data = np.random.randint(0, 2**6)
            setattr(msg, slot, int_data)

        elif slot_type in ['float32', 'float64']:
            if is_array:
                float_data = [np.random.normal(0, 100) for ii in range(array_len)]
            else:
                float_data = np.random.normal(0, 100)
            setattr(msg, slot, float_data)

        elif slot_type == 'string':
            strlen = np.random.randint(5, 15)
            if is_array:
                string_data = [''.join([chr(np.random.randint(32,127))
                                        for ii in range(strlen)])
                               for jj in range(array_len)]
            else:
                string_data = ''.join([chr(np.random.randint(32,127))
                                       for ii in range(strlen)])
            setattr(msg, slot, string_data)

        elif slot_type == 'time':
            if is_array:
                raise Exception("NYI: Array of time")
            # Not using rospy.Time.now() because it requires init_node()
            tt = time.time() + np.random.normal(0, 60)
            time_data = rospy.Time(tt)
            setattr(msg, slot, time_data)

        elif slot_type == 'duration':
            if is_array:
                raise Exception("NYI: Array of duration")
            dt = np.abs(np.random.normal(0, 60))
            duration_data = rospy.Duration(dt)
            setattr(msg, slot, duration_data)

        # We have nested mesage definitions. Yay!!
        else:
            if is_array:
                pkg, msg_type = slot_type.split('/')
                msg_module = importlib.import_module(pkg + ".msg")
                data = [getattr(msg_module, msg_type)() for ii in range(array_len)]
                for elem in data:
                    set_random_data(elem)
                setattr(msg, slot, data)
            else:
                set_random_data(getattr(msg, slot))



ROS_PRIMITIVE_SLOTS = ['char', 'byte', 'bool', 'int8', 'uint8', 'int16', 'uint16', 'int32', 'uint32', 'int64', 'uint64', 'float32', 'float64', 'string', 'time', 'duration']

def message_almost_equals(msg1, msg2):
    '''
    Check if the two input messages are approximately equivalent, based on the
    data types of each field.

    Performs a depth-first search traversing all fields in the message,
    instantly return False at a mismatch.

    NB: This will also need to be updated for any additional precision loss due
    to chosen encoding / bit packing. For now, the ROS encoding should maintain
    full precision.
    '''
    if type(msg1) != type(msg2):
        return False

    for slot, slot_type in zip(msg1.__slots__, msg1._slot_types):
        if '[' in slot_type:
            slot_type = slot_type.split('[')[0]
            data1 = getattr(msg1, slot)
            data2 = getattr(msg2, slot)
        else: # create arrays so rest of function can be identical
            data1 = [getattr(msg1, slot)]
            data2 = [getattr(msg2, slot)]

        for d1, d2 in zip(data1, data2):

            if type(d1) != type(d2):
                # Not actually a cause for alarm ...
                # rospy.rostime.Time -> genpy.rostime.Time
                pass

            if slot_type in ROS_PRIMITIVE_SLOTS:
                if not primitive_almost_equals(d1, d2, slot_type):
                    return False
            else:
                if not message_almost_equals(d1, d2):
                    return False
    # Depth-first search completed and didn't find any mismatches
    return True


def primitive_almost_equals(elem1, elem2, slot_type):
    '''
    Helper function for testing message equality; checks whether two fields
    that are a primitive type are approximately the same.

    NB: If we change how serialization works, this will the the function that
    actually specifies it on a
    '''
    if slot_type == 'float32':
        equals = np.isclose(elem1, elem2, rtol=1e-6)
    else:
        equals = (elem1 == elem2)

    return equals


def make_char(int_arr):
    # type (List[int]) -> List[char]
    '''
    Helper function for generating messages with the correct types.
    This is only necessary when directly passing constructed messages to
    the ROS callbacks; it appears that pub/sub handles converting the array
    of ints to chars.
    '''
    # NB: This will raise a ValueError is any of the inputs is < 0 or > 255.
    #     Since it'll only be used in test code, this is fine.
    return ''.join(map(chr, int_arr))


class TestMessageEquality(unittest.TestCase):
    '''
    Various test frameworks won't automagically apply their "almost equals" to a
    ROS message, so we have to implement it ourselves. So, I wanted to test
    the utility before using it to test the actual code.
    '''
    def test_floats(self):
        '''
        float precisions are what required the custom implementation of "almost equals",
        so generate cases where == fails...
        '''
        msg1 = std_msgs.msg.Float32(-173.58638005)

        msg2 = std_msgs.msg.Float32(-173.58637464)

        self.assertTrue(message_almost_equals(msg1, msg2), msg="Messages should be approx equal: \n {} \n {}".format(msg1, msg2))


    def test_base_cases(self):
        '''
        Use the messages in std_msgs to check that the base cases work correctly ...
        These are sanity checks that pass with using the basic equality operator msg1 == msg2.
        '''
        for msg_type in ['Bool', 'Byte', 'Char', 'Duration', 'String', 'Time',
                         'Float32', 'Float64', 'Int16', 'Int32', 'Int64', 'Int8']:
            #print("Testing message type: {}".format(msg_type))
            msg1 = getattr(std_msgs.msg, msg_type)()
            set_random_data(msg1)
            msg2 = copy.deepcopy(msg1)
            self.assertTrue(message_almost_equals(msg1, msg2), msg="Messages not equal: \n{}\n\n{}".format(msg1, msg2))

            # Make sure they're different!
            if msg_type == 'Bool':
                msg2.data = not msg1.data
            elif msg_type in ['Duration', 'Time', 'String']:
                set_random_data(msg2)
            else:
                msg2.data = msg1.data + 1
            self.assertFalse(message_almost_equals(msg1, msg2), msg="Messages equal: \n{}\n\n{}".format(msg1, msg2))


        for msg_type in ['ByteMultiArray', 'Float32MultiArray', 'Float64MultiArray',
                         'Int16MultiArray', 'Int32MultiArray', 'Int64MultiArray',
                         'Int8MultiArray']:
            msg1 = getattr(std_msgs.msg, msg_type)()
            set_random_data(msg1)
            msg2 = copy.deepcopy(msg1)
            self.assertTrue(message_almost_equals(msg1, msg2), msg="Messages not equal: \n{}\n\n{}".format(msg1, msg2))

            if msg_type == 'Bool':
                msg2.data[0] = not msg1.data[0]
            elif msg_type in ['Duration', 'Time', 'String']:
                set_random_data(msg2)
            else:
                msg2.data[0] = msg1.data[0] + 1
            self.assertFalse(message_almost_equals(msg1, msg2), msg="Messages equal: \n{}\n\n{}".format(msg1, msg2))


class TestDecoding(unittest.TestCase):
    def setUp(self):
        rp = rospkg.RosPack()
        qm_path = rp.get_path('comms_manager')
        self.filename = "{}/test/sentry_topics.txt".format(qm_path)

        self.qm1 = comms_manager.CommsManager(self.filename)
        self.topic_lookup = comms_manager.TopicLookup(self.filename)

    def test_single_random_message_round_trip(self):
        '''
        For each topic that appears in the sentry system, generates a message
        with random values and checks that it round-trips correctly through
        the encode/decode functions.
        '''
        for code in self.topic_lookup.msg_classes.keys():
            # Configure the queue manager
            queue_msg = ds_acomms_msgs.msg.QueueDefinition()
            queue_msg.queue_topics = [code]
            queue_msg.queue_periods = make_char([1])
            self.qm1.queue_definition_callback(queue_msg)

            # Generate an appropriate random message
            msg = self.topic_lookup.msg_classes[code]()
            try:
                set_random_data(msg)
            except Exception as ex:
                print("Trying to roundtrip message {}, a {}".format(code, type(msg)))
                raise(ex)

            self.qm1.data_callback(code, msg)

            data = self.qm1.encode_active()

            roundtripped_messages = comms_manager.decode(data, self.topic_lookup.msg_classes)

            self.assertTrue(message_almost_equals(msg, roundtripped_messages[code]),
                            msg="Messages didn't match. code: {}, \n input: {} output: \n {}".format(ord(code), msg, roundtripped_messages[code]))

    def test_too_little_data(self):
        '''
        Check that decode fails gracefully when it isn't given as much
        data as the num_bytes chars are told to expect.
        '''
        for code in self.topic_lookup.msg_classes.keys():
            # Configure the queue manager
            queue_msg = ds_acomms_msgs.msg.QueueDefinition()
            queue_msg.queue_topics = [code]
            queue_msg.queue_periods = make_char([1])
            self.qm1.queue_definition_callback(queue_msg)

            # Generate an appropriate random message
            data_msg = self.topic_lookup.msg_classes[code]()
            set_random_data(data_msg)
            self.qm1.data_callback(code, data_msg)

            data = self.qm1.encode_active()

            # Empty message -- this doesn't even generate an error message
            messages = comms_manager.decode([], self.topic_lookup.msg_classes)
            self.assertEqual(len(messages), 0)

            # only code, no payload
            messages = comms_manager.decode([data[0]], self.topic_lookup.msg_classes)
            self.assertEqual(len(messages), 0)

            # only code + num_bytes
            messages = comms_manager.decode(data[0:2], self.topic_lookup.msg_classes)
            self.assertEqual(len(messages), 0)

            messages = comms_manager.decode(data[0:3], self.topic_lookup.msg_classes)
            self.assertEqual(len(messages), 0)

            # code, num_bytes, half of message
            messages = comms_manager.decode(data[:-3], self.topic_lookup.msg_classes)
            self.assertEqual(len(messages), 0)


    def test_too_much_data(self):
        '''
        Check that decode fails gracefully when it receives a buffer that has
        been packed with 0s after the real data.
        (Useful for handling the micromodem's frames)
        '''
        for code in self.topic_lookup.msg_classes.keys():
            # Configure the queue manager
            queue_msg = ds_acomms_msgs.msg.QueueDefinition()
            queue_msg.queue_topics = [code]
            queue_msg.queue_periods = make_char([1])
            self.qm1.queue_definition_callback(queue_msg)

            # Generate an appropriate random message
            data_msg = self.topic_lookup.msg_classes[code]()
            set_random_data(data_msg)
            self.qm1.data_callback(code, data_msg)

            data = self.qm1.encode_active()
            for ii in range(25):
                data += chr(0)

            messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
            self.assertEqual(len(messages), 1)


    def test_invalid_code(self):
        '''
        Check that decode fails gracefully when the first byte isn't valid
        '''
        valid_codes = self.topic_lookup.msg_classes.keys()
        code = 255 # Max value ... test will start failing if we have that
        # many valid codes, which would require adding a second code byte
        self.assertFalse(code in valid_codes)

        data = [code, 0, 1, 0]
        messages = comms_manager.decode(data, self.topic_lookup.msg_classes)

    def test_repeated_message(self):
        '''
        Check that decode fails gracefully when there's a repeated message
        type. (This may be supported in the future, but not currently.)
        '''
        for code in self.topic_lookup.msg_classes.keys():
            # Configure the queue manager
            queue_msg = ds_acomms_msgs.msg.QueueDefinition()
            queue_msg.queue_topics = [code]
            queue_msg.queue_periods = make_char([1])
            self.qm1.queue_definition_callback(queue_msg)

            # Generate an appropriate random message
            data_msg = self.topic_lookup.msg_classes[code]()
            set_random_data(data_msg)
            self.qm1.data_callback(code, data_msg)

            data = self.qm1.encode_active()
            data = data + data
            messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
            # 1st one should have been accepted, 2nd trashed
            # Really, to test this satisfactorily I need to be checking for
            # publication of an error message on roslog; current test just
            # checks that the attempt to check for errors doesn't have a bug =)
            self.assertEqual(len(messages), 1)


    def test_encode_multiple_messages(self):
        '''
        Test that we can encode/decode multiple messages in a single bytestring.
        '''
        # Pick two codes at random to send at once (no size limit)
        valid_codes = self.topic_lookup.msg_classes.keys()
        for _ in range(50):
            code1, code2 = np.random.choice(valid_codes, 2, replace=False)
            msg1 = self.topic_lookup.msg_classes[code1]()
            msg2 = self.topic_lookup.msg_classes[code2]()
            set_random_data(msg1)
            set_random_data(msg2)
            msg = ds_acomms_msgs.msg.QueueDefinition()
            msg.queue_topics = [code1, code2]
            msg.queue_periods = make_char([1, 1])
            self.qm1.queue_definition_callback(msg)
            self.qm1.data_callback(code1, msg1)
            self.qm1.data_callback(code2, msg2)

            data = self.qm1.encode_active()
            messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
            self.assertTrue(code1 in messages)
            self.assertTrue(message_almost_equals(msg1, messages[code1]))
            self.assertTrue(code2 in messages)
            self.assertTrue(message_almost_equals(msg2, messages[code2]))


class TestQueueDefinition(unittest.TestCase):
    '''
    Tests for parsing input of the queueDefinition message and ensuring that
    it is consistent with the lookup table loaded at startup.

    NB: There are similar tests in rostests that check error message publication

    Errors handled:
    * repeated codes -> just keep overwriting, but publish an error message
    * invalid code -> shouldn't be possible if user is using GUI to interact,
         but need to be robust to it anyways. print error message, keep on
         with the next code in the list.
    '''
    def setUp(self):
        rp = rospkg.RosPack()
        qm_path = rp.get_path('comms_manager')
        filename = "{}/test/test_topics.txt".format(qm_path)
        self.qm = comms_manager.CommsManager(filename)

    def test_repeated_codes(self):
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.queue_topics = make_char([1, 2, 1])
        msg.queue_periods = make_char([1, 1, 1])
        self.qm.queue_definition_callback(msg)
        self.assertEquals(len(self.qm.active_topics), 2)

    def test_invalid_topic_code(self):
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.queue_topics = make_char([1,2,3,255])
        msg.queue_periods = make_char([1,1,1,1])
        self.qm.queue_definition_callback(msg)
        self.assertEquals(len(self.qm.active_topics), 3)

    def test_too_many_data_services(self):
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.background_data = make_char([6, 7])
        self.qm.queue_definition_callback(msg)
        # make sure it doesn't raise any errors; it should use the first one
        self.assertEquals(self.qm.active_service, chr(6))

    def test_invalid_service_code1(self):
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.background_data = make_char([6]) # valid ID
        self.qm.queue_definition_callback(msg)
        self.assertEquals(self.qm.active_service, chr(6))
        msg.background_data = make_char([255]) # Value out of range of lookup table
        self.qm.queue_definition_callback(msg)
        self.assertIsNone(self.qm.active_service)

    def test_invalid_service_code2(self):
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.background_data = make_char([6]) # valid ID
        self.qm.queue_definition_callback(msg)
        self.assertEquals(self.qm.active_service, chr(6))
        msg.background_data = make_char([1]) # ID belongs to a TOPIC
        self.qm.queue_definition_callback(msg)
        self.assertIsNone(self.qm.active_service)

    def test_service_code_set_to_none(self):
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.background_data = make_char([6]) # valid ID
        self.qm.queue_definition_callback(msg)
        self.assertEquals(self.qm.active_service, chr(6))
        msg.background_data = []
        self.qm.queue_definition_callback(msg)
        self.assertIsNone(self.qm.active_service)


class TestTdmaSlots(unittest.TestCase):
    '''
    Tests for parsing input of the tdmaSlots message.

    This is kind of ugly -- some of the tests directly access tdma_msg_is_valid,
    while others check the length of the QM's tdma slots after updating.
    The real goal is to (1) check that a well-formatted message updates
    correctly and (2) check that various malformed messages are rejected.
    '''
    def setUp(self):
        rp = rospkg.RosPack()
        qm_path = rp.get_path('comms_manager')
        filename = "{}/test/test_topics.txt".format(qm_path)
        self.qm = comms_manager.CommsManager(filename)

    def test_good(self):
        '''
        tests that valid messages are parsed
        '''
        # Single slot, lasting 15 seconds
        msg1 = ds_acomms_msgs.msg.TDMASlots([0], [15], [0])
        self.qm.tdma_slots_callback(msg1)
        self.assertEquals(1, len(self.qm.tdma_slots))

        # Two slots, each 5 seconds
        msg2 = ds_acomms_msgs.msg.TDMASlots([0, 30], [5, 5], [0, 0])
        self.qm.tdma_slots_callback(msg2)
        self.assertEquals(2, len(self.qm.tdma_slots))

    def test_overlapping_slots_start(self):
        '''
        start of subsequent slot is in existing slots
        '''
        msg = ds_acomms_msgs.msg.TDMASlots([0, 15], [20, 15], [0, 0])
        # This and subsequent tests might be better off testing
        # if an error is detected by the comms_manager when
        # fed a bad slot, but for now, just testing the
        # overlap calculations...
        self.assertFalse(comms_manager.tdma_msg_is_valid(msg))

    def test_overlapping_slots_end(self):
        '''
        End of 2nd slot is inside 1st; 3rd is OK.
        '''
        msg = ds_acomms_msgs.msg.TDMASlots([30, 20, 0], [15, 15, 15], [0, 0, 0])
        self.assertFalse(comms_manager.tdma_msg_is_valid(msg))

    def test_overlapping_slots_contains1(self):
        '''
        2nd slot entirely contains 1st slot.
        '''
        msg = ds_acomms_msgs.msg.TDMASlots([30, 20], [10, 30], [0, 0])
        self.assertFalse(comms_manager.tdma_msg_is_valid(msg))

    def test_overlapping_slots_contains2(self):
        '''
        1st slot entirely contains 2nd slot.
        '''
        msg = ds_acomms_msgs.msg.TDMASlots([30, 40], [20, 5], [0, 0])
        self.assertFalse(comms_manager.tdma_msg_is_valid(msg))

    def test_out_of_range_neg_start(self):
        '''
        slot starts before 0 seconds
        '''
        msg1 = ds_acomms_msgs.msg.TDMASlots([-5], [15], [0])
        self.qm.tdma_slots_callback(msg1)
        self.assertEquals(0, len(self.qm.tdma_slots))

    def test_out_of_range_neg_duration(self):
        '''
        slot has negative duration
        '''
        msg1 = ds_acomms_msgs.msg.TDMASlots([0], [-15], [0])
        self.qm.tdma_slots_callback(msg1)
        self.assertEquals(0, len(self.qm.tdma_slots))

    def test_out_of_range_high_start(self):
        '''
        start of slot is at time > 60 seconds
        '''
        msg1 = ds_acomms_msgs.msg.TDMASlots([66], [10], [0])
        self.qm.tdma_slots_callback(msg1)
        self.assertEquals(0, len(self.qm.tdma_slots))

    def test_out_of_range_too_long(self):
        '''
        end of slot goes beyond 60 seconds
        '''
        msg1 = ds_acomms_msgs.msg.TDMASlots([55], [10], [0])
        self.qm.tdma_slots_callback(msg1)
        self.assertEquals(0, len(self.qm.tdma_slots))


class TestEncodingBandwidthLimited(unittest.TestCase):
    '''
    Tests for the logic of which data to encode in what order, in cases
    where we have more bytes to send than available bandwidth.
    '''
    def setUp(self):
        rp = rospkg.RosPack()
        qm_path = rp.get_path('comms_manager')
        filename = "{}/test/test_topics.txt".format(qm_path)
        # qm1 handles encoding
        self.qm1 = comms_manager.CommsManager(filename)
        self.topic_lookup = comms_manager.TopicLookup(filename)

    def test_single_shot_priority(self):
        '''
        Test that a single-shot message (mode = 0) is immediately added
        to the encoded message after it has been received.
        '''
        codes = make_char([1, 2, 3, 4])
        def_msg = ds_acomms_msgs.msg.QueueDefinition()
        def_msg.queue_topics = codes
        def_msg.queue_periods = make_char([1, 0, 2, 1])
        self.qm1.queue_definition_callback(def_msg)

        code_count = {code: 0 for code in codes}
        data_msg = std_msgs.msg.Float32(123.456)
        max_len = 16 # Fit two queues in each encoded message

        # At start, only provide data for the constant-frequency messages
        for ii in range(10):
            for code in map(chr, [1, 3, 4]):
                self.qm1.data_callback(code, data_msg)
            data = self.qm1.encode_active(max_len)
            messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
            for key, value in messages.iteritems():
                code_count[key] += 1

        self.assertEqual(8, code_count[chr(1)])
        self.assertEqual(4, code_count[chr(3)])
        self.assertEqual(8, code_count[chr(4)])

        self.qm1.data_callback(chr(2), data_msg)
        data = self.qm1.encode_active(max_len)
        messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
        self.assertTrue(chr(2) in messages)

        # Now provide it for all of them, but since chr(2) was already
        # encoded once, it shouldn't ever come up again.
        for ii in range(10):
            for code in codes:
                self.qm1.data_callback(code, data_msg)
            data = self.qm1.encode_active(max_len)
            messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
            for key, value in messages.iteritems():
                code_count[key] += 1
        self.assertEqual(16, code_count[chr(1)])
        self.assertEqual(0, code_count[chr(2)])
        self.assertEqual(8, code_count[chr(3)])
        self.assertEqual(16, code_count[chr(4)])

    def test_multiple_messages_bandwidth_limited(self):
        '''
        Test proper cycling through the desired queues when all configured
        messages cannot fit into a single transmission, and that the
        resulting message frequencies are equal.
        '''
        data_msg = std_msgs.msg.Float32(123.456)
        codes = make_char([1, 2, 3])
        def_msg = ds_acomms_msgs.msg.QueueDefinition()
        def_msg.queue_topics = codes
        def_msg.queue_periods = make_char([1, 1, 1])
        self.qm1.queue_definition_callback(def_msg)

        # First, figure out how big a single message is...
        # NB: since actually using the encode_active() function changes its
        #   state, this test broke when I tried to calculate how many
        #   bytes are required to pack the message. Instead, hard-code
        #   it, knowing that it'll be brittle to changing encoding schemes.
        msg_len = 8
        max_len = int(np.round(2.5*msg_len))

        # In this case, it is undefined what order the messages should be
        # relayed in. So, we test:
        # * Each set of messages doesn't duplicate codes
        # * After two sets, each code is represented at least once
        # * After three sets, each code is represented exactly twice

        code_count = {code: 0 for code in codes}

        for code in codes:
            self.qm1.data_callback(code, data_msg)
        data = self.qm1.encode_active(max_len)
        messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
        for key, value in messages.iteritems():
            code_count[key] += 1
        self.assertEqual(2, len(set(messages.keys())))

        for code in codes:
            self.qm1.data_callback(code, data_msg)
        data = self.qm1.encode_active(max_len)
        messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
        self.assertEqual(2, len(set(messages.keys())))
        for key, value in messages.iteritems():
            code_count[key] += 1
        for code in codes:
            self.assertTrue(code_count[code] > 0)

        for code in codes:
            self.qm1.data_callback(code, data_msg)
        data = self.qm1.encode_active(max_len)
        messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
        self.assertEqual(2, len(set(messages.keys())))
        for key, value in messages.iteritems():
            code_count[key] += 1

        for code in codes:
            self.assertEquals(2, code_count[code])


    def test_multiple_messages_different_start_times(self):
        '''
        Test that after adding a new message to the queue definition
        all relative frequencies are correct.
        '''
        data_msg = std_msgs.msg.Float32(123.456)

        # First, figure out how big a single message is...
        # NB: since actually using the encode_active() function changes its
        #   state, this test broke when I tried to calculate how many
        #   bytes are required to pack the message. Instead, hard-code
        #   it, knowing that it'll be brittle to changing encoding schemes.
        msg_len = 8
        max_len = int(np.round(1.5*msg_len))

        # In this case, it is undefined what order the messages should be
        # relayed in. So, we test:
        # * Each set of messages doesn't duplicate codes
        # * After two sets, each code is represented at least once
        # * After three sets, each code is represented exactly twice

        code_count = {code: 0 for code in make_char([1,2,3])}


        # Initialize queue manager with codes 1&2;
        # check that resulting encoded messages evenly split
        queue_msg = ds_acomms_msgs.msg.QueueDefinition()
        queue_msg.queue_topics = make_char([1, 2])
        queue_msg.queue_periods = make_char([1, 1])
        self.qm1.queue_definition_callback(queue_msg)

        for ii in range(50):
            for code in queue_msg.queue_topics:
                self.qm1.data_callback(code, data_msg)
            data = self.qm1.encode_active(max_len)
            messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
            for key, value in messages.iteritems():
                code_count[key] += 1
        print("After 50 iterations, code counts = {}".format(code_count))
        self.assertEqual(code_count[chr(1)], code_count[chr(2)])

        # Update queues to have codes 1,2,&3
        queue_msg.queue_topics = make_char([1, 2, 3])
        queue_msg.queue_periods = make_char([1, 1, 1])
        self.qm1.queue_definition_callback(queue_msg)

        for ii in range(75):
            for code in queue_msg.queue_topics:
                self.qm1.data_callback(code, data_msg)
            data = self.qm1.encode_active(max_len)
            messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
            for key, value in messages.iteritems():
                code_count[key] += 1
        print("After 50 iterations, code counts = {}".format(code_count))
        self.assertEqual(code_count[chr(1)], code_count[chr(2)])
        self.assertEqual(25, code_count[chr(3)])


    def test_encode_frequency(self):
        '''
        Test that messages with modes 1 and 3 alternate appropriately
        '''
        codes = make_char([2,1])
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.queue_topics = codes
        msg.queue_periods = make_char([1, 3])

        self.qm1.queue_definition_callback(msg)
        self.qm1.data_callback(chr(1), std_msgs.msg.Float32(42.))
        self.qm1.data_callback(chr(2), std_msgs.msg.Float32(42.))

        max_bytes = 8 # size of single Float32 encoded message
        code_count = {code: 0 for code in codes}

        # At start, the more-frequent one should go first
        data = self.qm1.encode_active(max_bytes)
        messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
        self.assertTrue(chr(2) in messages)
        for key, value in messages.iteritems():
            code_count[key] += 1

        for _ in range(99):
            self.qm1.data_callback(chr(1), std_msgs.msg.Float32(42.))
            self.qm1.data_callback(chr(2), std_msgs.msg.Float32(42.))
            data = self.qm1.encode_active(max_bytes)
            messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
            for key, value in messages.iteritems():
                code_count[key] += 1

        #print("After 100 iterations, counts are: {}".format(code_count))
        self.assertEquals(code_count[chr(2)], 3*code_count[chr(1)])

    def test_message_too_big_doesnt_crash(self):
        '''
        Check that a too-big message doesn't crash queue manager
        '''
        # Right now, comms_manager can handle data with 2**16 = 65536 bytes
        # (Doesn't mean they can be transmitted, but they'll be
        # correctly encoded.) A 240x360 image has 86400 bytes, so us that.
        img_msg = sensor_msgs.msg.Image()
        img_msg.height = 240
        img_msg.width = 320
        img_msg.data = ''.join([chr(np.random.randint(0, 255)) for _ in range(img_msg.height * img_msg.width)])

        def_msg = ds_acomms_msgs.msg.QueueDefinition()
        def_msg.queue_topics = [chr(5)]
        def_msg.queue_periods = [chr(1)]
        self.qm1.queue_definition_callback(def_msg)
        self.qm1.data_callback(chr(5), img_msg)
        data = self.qm1.encode_active()

        messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
        self.assertEquals(len(messages), 0)

    def test_message_too_big_doesnt_block(self):
        '''
        Check that a too-big message doesn't block queue manager

        We uncovered a bug on sentry542 where once a too-big message was
        requested, the other queues were never sent beause it always had
        priority.
        '''
        # Right now, comms_manager can handle data with 2**16 = 65536 bytes
        # (Doesn't mean they can be transmitted, but they'll be
        # correctly encoded.) A 240x360 image has 86400 bytes, so us that.
        img_msg = sensor_msgs.msg.Image()
        img_msg.height = 240
        img_msg.width = 320
        img_msg.data = ''.join([chr(np.random.randint(0, 255)) for _ in range(img_msg.height * img_msg.width)])

        float_msg = std_msgs.msg.Float32()
        float_msg.data = 123.456

        def_msg = ds_acomms_msgs.msg.QueueDefinition()
        def_msg.queue_topics = [chr(5), chr(1)]
        def_msg.queue_periods = [chr(1), chr(1)]
        self.qm1.queue_definition_callback(def_msg)

        self.qm1.data_callback(chr(1), float_msg)
        data = self.qm1.encode_active(max_bytes=64)
        messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
        self.assertEquals(len(messages), 1)

        for _ in range(5):
            self.qm1.data_callback(chr(5), img_msg)
            self.qm1.data_callback(chr(1), float_msg)
            data = self.qm1.encode_active(max_bytes=64)

            messages = comms_manager.decode(data, self.topic_lookup.msg_classes)
            self.assertEquals(len(messages), 1)


class TestEncoding(unittest.TestCase):
    '''
    Tests for the logic of which data to encode in what order.

    # TODO: Do we want to add the criteria that a given message is published
        only once? This would make the topside queue manager easier to deal
        with -- it would have most topics set to frequency 1, and send them
        whenever it gets a new message.
    '''
    def setUp(self):
        rp = rospkg.RosPack()
        qm_path = rp.get_path('comms_manager')
        filename = "{}/test/test_topics.txt".format(qm_path)
        # qm1 handles encoding
        self.qm1 = comms_manager.CommsManager(filename)
        # qm2 handles decoding
        self.qm2 = comms_manager.CommsManager(filename)

    def test_not_yet_received(self):
        '''
        Test that encode_active() doesn't fail if a relevant message hasn't been
        received yet, and that after receipt it reports data.
        '''
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.queue_topics = make_char([1])
        msg.queue_periods = make_char([1])
        self.qm1.queue_definition_callback(msg)
        data = self.qm1.encode_active()
        self.assertEquals(len(data), 0) # Haven't received any messages yet!
        self.qm1.data_callback(chr(1), std_msgs.msg.Float32(123.456))
        data = self.qm1.encode_active()
        self.assertTrue(len(data) > 0)

    def test_send_after(self):
        '''
        Test that encode properly handles mode 0 (send the NEXT received message)
        '''
        # Initially have it set to send the most recent
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.queue_topics = make_char([1])
        msg.queue_periods = make_char([1])
        self.qm1.queue_definition_callback(msg)
        self.qm1.data_callback(chr(1), std_msgs.msg.Float32(123.456))
        data = self.qm1.encode_active()
        self.assertTrue(len(data) > 0)

        # After resetting to send next, don't encode the already-waiting message
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.queue_topics = make_char([1])
        msg.queue_periods = make_char([0])
        self.qm1.queue_definition_callback(msg)
        data = self.qm1.encode_active()
        self.assertEquals(len(data), 0)

        # After receiving message, it should be encoded
        self.qm1.data_callback(chr(1), std_msgs.msg.Float32(123.456))
        data = self.qm1.encode_active()
        self.assertTrue(len(data) > 0)

        # And, after it has been encoded once, it doesn't appear in future calls
        data = self.qm1.encode_active()
        self.assertEquals(len(data), 0)

    def test_encode_excess_bandwidth(self):
        '''
        Test that a single message with mode > 1 isn't encoded every time
        '''
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.queue_topics = make_char([1])
        msg.queue_periods = make_char([2])
        self.qm1.queue_definition_callback(msg)

        # For a period of 2, the message should be encoded every other call
        # to encode_active.
        for ii in range(2):
            self.qm1.data_callback(chr(1), std_msgs.msg.Float32(42.))
            data = self.qm1.encode_active()
            self.assertTrue(len(data) > 0)
            # Without being given a new message, this test would pass for the
            # wrong reason -- each received message is only encoded once.
            self.qm1.data_callback(chr(1), std_msgs.msg.Float32(42.))
            data = self.qm1.encode_active()
            self.assertEquals(len(data), 0)

    def test_remove_queue(self):
        '''
        Test that a subsequent queue definition removing a queue results in
        that queue not getting sent in the next encoding.
        '''
        queue_msg = ds_acomms_msgs.msg.QueueDefinition()
        queue_msg.queue_topics = make_char([1])
        queue_msg.queue_periods = make_char([1])
        self.qm1.queue_definition_callback(queue_msg)

        # Check that first queue is encoded...
        for ii in range(3):
            self.qm1.data_callback(chr(1), std_msgs.msg.Float32(42.))
            data = self.qm1.encode_active()
            self.assertTrue(len(data) > 0)

        # Remove queue; check nothing encoded
        queue_msg.queue_topics = []
        queue_msg.queue_periods = []
        self.qm1.queue_definition_callback(queue_msg)
        for ii in range(3):
            self.qm1.data_callback(chr(1), std_msgs.msg.Float32(42.))
            data = self.qm1.encode_active()
            self.assertEqual(len(data), 0)

    def test_already_sent_message(self):
        '''
        Test that if a message has been sent, it won't be repeated.
        (That topic will be skipped when encoding until a new messages shows up)
        This is particularly relevant for topside->subsea comms ... it's
        unnecessarily loud to keep retransmitting TDMA and Queue updates the
        whole time that comms manager is running.
        '''
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.queue_topics = make_char([1])
        msg.queue_periods = make_char([1])
        self.qm1.queue_definition_callback(msg)

        # Before receiving message, nothing should be encoded
        data = self.qm1.encode_active()
        self.assertEquals(len(data), 0)

        for ii in range(3):
            # After message receipt, first encode should succeed
            self.qm1.data_callback(chr(1), std_msgs.msg.Float32(42.))
            data = self.qm1.encode_active()
            self.assertTrue(len(data) > 0)
            # Subsequent call to encode should fail
            data = self.qm1.encode_active()
            self.assertEquals(len(data), 0)


class UnittestCommsManager(unittest.TestSuite):
    '''
    Trying to integrate unit tests into rostest without also making them terribly unwieldy.

    I don't like having to add them by hand, but I haven't found a way to
    auto-discover when run through catkin_make run_tests.
    (running this as a standalone script, dir() will give a list of all the
    classes and can hackily filter based on Test ... but not with catkin)
    '''
    def __init__(self):
        super(UnittestCommsManager, self).__init__()
        # Need to add tests to the suite, but I don't want to do it by hand and have to keep it updated.
        # Can't use `dir` b/c when imported, they won't be there.

        self.addTest(unittest.makeSuite(TestEncoding))
        self.addTest(unittest.makeSuite(TestEncodingBandwidthLimited))
        self.addTest(unittest.makeSuite(TestDecoding))
        self.addTest(unittest.makeSuite(TestMessageEquality))
        self.addTest(unittest.makeSuite(TestQueueDefinition))
        self.addTest(unittest.makeSuite(TestTdmaSlots))


if __name__ == "__main__":
    '''
    Failure modes that I need to test for:
    * multiple copies of message in same buffer (does decode need to return Dict[int, List[rospy.Message]]?)

    '''
    unittest.main()
