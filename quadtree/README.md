### Overview

The quadtree package provides a library for transferring multi-resolution multibeam grids from subsea to topside.

The map is stored as a quadtree, where each node contains the average elevation of all points that fall within it. Additionally, nodes track the timestamps of when they were last transmitted and when they were last updated. Grids are transmitted in chunks we call "subgrids" whose dimensions are determined by the acomms message size.

By default, NxN subgrids are streamed starting with the lowest resolution data first.
The user can request a region to be prioritized; the request includes which subgrids to be sent, along with the most recent timestamp from topside. (The timestamp provides an ack mechanism -- data for a subgrid is only transmitted if the subsea data is newer.)

A more detailed description of the rationale behind the design of this library for multibeam grid transfer
can be found in the [original design document / proposal](design_docs/multibeam_map_transfer.pdf).

![Alt text](design_docs/quadtree.png)

The above diagram shows the nodes and topics involved in quadtree map transfer.

Depending on what instrument is being used, it may be necessary to perform a mbclean-like operation on the data in order to
remove outliers before gridding. (Data from the Kongsberg EM2040 has not required this step.)
The map sender will create a grid using all points received in a `PointCloud2` message, 
while for a `ds_multimbeam_msgs/MultibeamRaw` message, it will ignore any beam whose flag is not `BEAM_OK`.


### Instructions

##### Notes on Input Data

To run in simulation with MultibeamRaw messages from the Kongsberg, you can use test data from the 2019-engineering cruise.
However, at that point, the driver was not including a `frame_id` in the message headers, so you'll need to modify the bag file to add that.
I've done it for a single bagfile, and uploaded it to the [Downloads](https://bitbucket.org/whoidsl/coexploration/downloads/) portion of this repository:

~~~
import rosbag
in_bag = rosbag.Bag('sonars_2019-10-13-06-30-25_6.bag', 'r')
out_bag = rosbag.Bag('kongsberg_2019-10-13-06-30-25_6.bag', 'w')
for topic, msg, tt in in_bag.read_messages(topics=['/sentry/sonars/kongsberg/mbraw']):
    msg.header.frame_id = '/base_link'
    out_bag.write(topic, msg, t=tt)
print("Found {} sonar messages".format(count))
out_bag.close()
~~~

The full globals bagfile was too large to upload, so the example data was filtered to only include the `/tf` topic:

~~~
rosbag filter globals_2019-10-13-06-30-26_6.bag tf_2019-10-13-06-30-26_6.bag "topic=='/tf'"
~~~

To run an example with a pre-staged map, use any of the .grd files output by Sentry's post-processing chain.

##### Running the Quadtree

See the [launch README.md](../launch/README.md) for how to launch all the relevant nodes and start the map transfer.

![Alt text](design_docs/POS_map.png)

The above figure shows the quadtree GUI after letting it transfer the grid created by the Pontoon of Science's bagfile.
I like this example because the borders of the map clearly show the variable spatial resolution obtained with a quadtree. 
The "Visible Layers" column of checkboxes indicates that the lowest resolution cell that is being displayed is 16 x 16 m.
(For this dataset, watercolumn was < 10 m, and the vertical origin was oddly chosen.)

The Quadtree GUI will superimpose all levels of the map on top of each other, with highest-resolution on top, 
and where any cells that don't have data are transparent.

![Alt text](design_docs/sentry504_16m.png)

The above figure is using the publicly-available and navadjusted grid file from dive sentry504. 
This is early in the transfer process, as the highest resolution data available is 16 m.
I have zoomed in to a feature of interest in the center (using the magnifying glass icon in the bottom left toolbar), 
and selected the "16 m" radio button in the "Request Level" column to overlay the boundaries of grid cells. 
Darker lines show the subgrid boundaries, which are groups of 8x8 cells whose updates are transmitted in a single message.

NB: The colorscale does not auto-adjust; the user must do so by updating the values in the Vmin/Vmax text boxes in the lower right of the GUI.

![Alt text](design_docs/sentry504_select_region.png)

In order to request higher resolution:

* Deselect the magnifying glass (to avoid zooming at the same time)
* Check the "Select Region Active" box
* drag a rectangle around the region you want in more detail
* Select the desired resolution (here, we are requesting 4 m)
* Click "Send Selection"

![Alt text](design_docs/sentry504_4m.png)

After requesting a selection, you should see higher-resolution blocks start to fill in the area.
Once it is finished, click "Clear Selection" to get rid of the grey rectangle. 
I like to leave the selection active until all packets have arrived -- if any are dropped, just click "Send Selection" again.
The requests include timestamp info, so this will only use bandwidth on transmitting blocks that were missing.


![Alt text](design_docs/sentry541_map.png)

The above map was created using `MultibeamRaw` messages from sentry dive 541 on the 2019-engineering cruise.
(This is only one hour worth of data.)
The offsets between adjacent lines are expected, and are particularly clear due to the low relief in this region.
In the final post-processed map, they would be eliminated by applying tide corrections and running mbnavadjust.




### Misc

##### Code formatting

I follow the google style guide: https://google.github.io/styleguide/cppguide.html

NB: ROS diverges from Google's style guide in a few ways. For all of these, I'm using Google's choice:

* ROS disallows cuddled braces; Google uses them
* ROS uses 120 chars per line; Google 80
* ROS uses camelCase for function names; Google uses CamelCase => Unfortunately, for the nodes, we already have DsProcess using camelCase for setup*

clang-format is a tool for automatically changing code to match a styleguide. It can be invoked with the command:

~~~
 find . -name "*.cpp" -or -name "*.h" | xargs clang-format-3.8 -i -style=google $1
~~~

TODO: At a minimum, add a git hook to run this. I also like what Zac did using CI to enforce styling.

##### Documentation

The ds_ros codebase uses doxygen style comments.
To generate them for a given package, run `rosdoc_lite /path/to/package`. Then, open `doc/html/index.html` in your web browser.
