#! /usr/bin/env python2.7

'''
Focuses on testing the ROS interfaces.
This needs to be run after launching the test nodes: `roslaunch rostest.test`
'''


import time
import unittest

import rospkg
import rospy
import rosgraph_msgs.msg
import std_msgs.msg

import comms_manager
from unittest_qm import make_char, message_almost_equals
import ds_acomms_msgs.srv  # ModemDataRequest
import ds_acomms_msgs.msg  # ModemData, MicromodemData


class DataServiceProvider(object):
    '''
    Helper class that provides data for packing the transmitted message.
    (Returns empty response if too few bytes are requested.)
    '''
    def __init__(self, service, num_bytes=128):
        self.server = rospy.Service(service,
                                    ds_acomms_msgs.srv.ModemDataRequest,
                                    self.handle_request)
        assert num_bytes < 256
        self.num_bytes = num_bytes

    def handle_request(self, req):
        resp = ds_acomms_msgs.srv.ModemDataRequestResponse()
        if req.bytes_requested >= self.num_bytes:
            resp.data = "".join([chr(ii) for ii in range(self.num_bytes)])
        return resp

    def shutdown(self):
        '''
        This is required so the next testcase can create a DataServiceProvider
        using the same name. (Otherwise, get "already registered" error)
        '''
        self.server.shutdown()


class DataServiceBlocker(object):
    '''
    Helper class that creates a service that will block.
    '''
    def __init__(self, service):
        self.server = rospy.Service(service,
                                    ds_acomms_msgs.srv.ModemDataRequest,
                                    self.handle_request)

    def handle_request(self, req):
        rr = rospy.Rate(1.0)
        while True:
            rr.sleep()

    def shutdown(self):
        '''
        This is required so the next testcase can create a DataServiceProvider
        using the same name. (Otherwise, get "already registered" error)
        '''
        self.server.shutdown()


class RosoutMonitor(object):
    '''
    Helper class that subscribes to /rosout and counts the number of ERROR
    messages published by the given node name.
    '''
    def __init__(self, node_name):
        self.node_name = node_name
        self.error_count = 0

        # Needs to be after initial definition of error_count; maybe move to setup function?
        self.sub = rospy.Subscriber('/rosout', rosgraph_msgs.msg.Log, self.rosout_callback)

        # /rosout latches (and I don't think I can change that) so if an error
        # was previously published, the callback will be called immediately
        # after subscription. Give it time to do that, then reset the error
        # count so it'll be valid for the test that is about to start.
        # TODO: This is hacky!
        time.sleep(0.1)
        self.error_count = 0

    def rosout_callback(self, msg):
        if msg.level == msg.ERROR and msg.name == self.node_name:
            self.error_count += 1


class BytesMonitor(object):
    '''
    Helper class that subscribes to MicromodemData and caches most recent message.
    '''
    def __init__(self, topic):
        self.sub = rospy.Subscriber(topic, ds_acomms_msgs.msg.MicromodemData,
                                    self.data_callback)
        self.rx_data = None
    def data_callback(self, msg):
        self.rx_data = msg
    def clear(self):
        self.rx_data = None


class DataMonitor(object):
    '''
    Subscribes to all topics in input filename and caches the most recent message in each.
    '''
    def __init__(self, filename, prefix=None):
        topic_lookup = comms_manager.TopicLookup(filename)

        self.rx_data = {} # type: Dict[char, rospy.Message]

        self.subscribers = {}
        for code, topic in topic_lookup.topics.iteritems():
            if prefix is not None:
                topic = prefix + topic
            msg_class = topic_lookup.msg_classes[code]
            cb = lambda msg,code=code: self.data_callback(code, msg)
            self.subscribers[code] = rospy.Subscriber(topic, msg_class, cb)
        time.sleep(0.1)
        self.clear()

    def data_callback(self, code, msg):
        self.rx_data[code] = msg

    def clear(self):
        self.rx_data = {}


class TestPublishEncoded(unittest.TestCase):
    '''
    Test that comms manager publishes modem data when given queue definition,
    tdma slot, and incoming data.
    '''
    def setUp(self):
        rp = rospkg.RosPack()
        qm_path = rp.get_path('comms_manager')
        filename = "{}/test/test_topics.txt".format(qm_path)
        self.bytes_monitor = BytesMonitor('/up/modem_data')
        self.data_monitor = DataMonitor(filename, '/topside')

        self.def_pub = rospy.Publisher('/subsea/queue_definition',
                                       ds_acomms_msgs.msg.QueueDefinition,
                                       queue_size=1, latch=True)
        self.tdma_pub = rospy.Publisher('/subsea/tdma_slots',
                                        ds_acomms_msgs.msg.TDMASlots,
                                        queue_size=1, latch=True)
        self.data_pub = rospy.Publisher('/test/a', std_msgs.msg.Float32,
                                        queue_size=1, latch=True)

    def test_subsea_to_topside_pipeline(self):
        '''
        Using the ROS interface to configure the subsea comms_manager,
        listen to the data republished by the topside comms_manager.
        '''
        tdma_msg = ds_acomms_msgs.msg.TDMASlots()
        tdma_msg.starts = range(0, 60, 3)
        tdma_msg.durations = [2 for _ in tdma_msg.starts]
        tdma_msg.rates = [0 for _ in tdma_msg.starts]
        self.tdma_pub.publish(tdma_msg)

        def_msg = ds_acomms_msgs.msg.QueueDefinition()
        def_msg.queue_topics = make_char([1,2,3,4])
        def_msg.queue_periods =  make_char([1,1,1,1])
        self.def_pub.publish(def_msg)

        time.sleep(0.2) # Give time for the queue definition to update

        data_msg = std_msgs.msg.Float32(123.456)
        self.data_monitor.clear()
        self.data_pub.publish(data_msg)
        time.sleep(4.0) # The TDMA period is 3 seconds

        self.assertTrue(chr(1) in self.data_monitor.rx_data)
        self.assertTrue(message_almost_equals(self.data_monitor.rx_data[chr(1)], data_msg))


    def test_publish_encoded(self):

        tdma_msg = ds_acomms_msgs.msg.TDMASlots()
        tdma_msg.starts = range(0, 60, 3)
        tdma_msg.durations = [2 for _ in tdma_msg.starts]
        tdma_msg.rates = [0 for _ in tdma_msg.starts]
        self.tdma_pub.publish(tdma_msg)

        def_msg = ds_acomms_msgs.msg.QueueDefinition()
        def_msg.queue_topics = make_char([1,2,3,4])
        def_msg.queue_periods = make_char([1,1,1,1])
        self.def_pub.publish(def_msg)

        time.sleep(0.2) # Give time for the queue definition to update

        # Q: Do I need to let the queue manager set up subscriptions before publishing the data_msg?
        #   or is it more realistic to just have it published continually?

        data_msg = std_msgs.msg.Float32(123.456)
        self.bytes_monitor.clear()
        self.data_pub.publish(data_msg)
        time.sleep(4.0) # The TDMA period is 3 seconds
        self.assertTrue(self.bytes_monitor.rx_data is not None)

    def test_tdma_rate(self):
        '''
        Test that when set up as in the previous one the transmitted data arrives at 1/3 hz
        '''
        pass

class TestInvalidTDMASlots(unittest.TestCase):
    '''
    Check that an ill-formed TDMA slot message leads to publication of an error
    '''
    def setUp(self):
        # NB: These need to match the params in the launchfile that are used
        #     for the nodes. Probably better to clean that up =)
        self.tdma_pub = rospy.Publisher('/topside/tdma_slots',
                                        ds_acomms_msgs.msg.TDMASlots,
                                        queue_size=1)

        self.rosout_monitor = RosoutMonitor('/topside_qm')
        rp = rospkg.RosPack()
        qm_path = rp.get_path('comms_manager')
        filename = "{}/test/test_topics.txt".format(qm_path)
        topic_lookup = comms_manager.TopicLookup(filename)
        self.topics = topic_lookup.topics
        time.sleep(1.0)

    def test_good_slots(self):
        '''
        First check that a good message doesn't raise any errors.
        '''
        msg = ds_acomms_msgs.msg.TDMASlots([0, 30], [15, 15], [0, 0])
        self.assertEquals(self.rosout_monitor.error_count, 0)
        self.tdma_pub.publish(msg)
        time.sleep(0.2)
        self.assertEquals(self.rosout_monitor.error_count, 0)

    def test_bad_slots(self):
        '''
        Overlapping slots.
        '''
        msg = ds_acomms_msgs.msg.TDMASlots([0, 10], [15, 15], [0, 0])
        self.assertEquals(self.rosout_monitor.error_count, 0)
        self.tdma_pub.publish(msg)
        time.sleep(0.2)
        self.assertEquals(self.rosout_monitor.error_count, 1)

    def test_invalid_rates(self):
        '''
        Message with invalid rates.
        '''
        msg= ds_acomms_msgs.msg.TDMASlots([0, 30], [15, 15], [100, 100])
        self.assertEquals(self.rosout_monitor.error_count, 0)
        self.tdma_pub.publish(msg)
        time.sleep(0.2)
        self.assertEquals(self.rosout_monitor.error_count, 1)


class TestInvalidQueueDefinition(unittest.TestCase):
    def setUp(self):
        # NB: These need to match the params in the launchfile that are used
        #     for the nodes. Probably better to clean that up =)
        self.definition_pub = rospy.Publisher('/topside/queue_definition',
                                              ds_acomms_msgs.msg.QueueDefinition,
                                              queue_size=1)
        self.rosout_monitor = RosoutMonitor('/topside_qm')
        rp = rospkg.RosPack()
        qm_path = rp.get_path('comms_manager')
        filename = "{}/test/test_topics.txt".format(qm_path)
        topic_lookup = comms_manager.TopicLookup(filename)
        self.topics = topic_lookup.topics
        time.sleep(1.0)

    def tearDown(self):
        '''
        Really, we should start up a new comms manager for each of these
        tests. So, this is a hack to clean up after this test, rather than
        leaving the comms manager trying to listen for unavailable data.
        '''
        msg = ds_acomms_msgs.msg.QueueDefinition()
        self.definition_pub.publish(msg)

    def test_good_definition(self):
        '''
        First check that a good message doesn't raise any errors.
        '''
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.queue_topics = [1]
        msg.queue_periods = [1]
        self.assertEquals(self.rosout_monitor.error_count, 0)
        self.definition_pub.publish(msg)
        time.sleep(0.2)
        self.assertEquals(self.rosout_monitor.error_count, 0)


    def test_repeated_code(self):
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.queue_topics = [1, 1]
        msg.queue_periods = [1, 1]
        self.assertEquals(self.rosout_monitor.error_count, 0)
        self.definition_pub.publish(msg)
        # Enough time for message to be published and response received.
        # This is a bit hacky -- I don't know how to get this to be truly
        # deterministic. Empirically, 0.1 is too fast and 0.2 works running
        # on my laptop.
        time.sleep(0.2)
        self.assertEquals(self.rosout_monitor.error_count, 1)

    def test_invalid_code(self):
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.queue_topics = [255]
        msg.queue_periods = [1]
        self.assertEquals(self.rosout_monitor.error_count, 0)
        self.definition_pub.publish(msg)
        time.sleep(0.5)
        self.assertEquals(self.rosout_monitor.error_count, 1)

    def test_invalid_service_code1(self):
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.background_data = [255]
        self.assertEquals(self.rosout_monitor.error_count, 0)
        self.definition_pub.publish(msg)
        time.sleep(0.5)
        self.assertEquals(self.rosout_monitor.error_count, 1)

    def test_invalid_service_code2(self):
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.background_data = [1]
        self.assertEquals(self.rosout_monitor.error_count, 0)
        self.definition_pub.publish(msg)
        time.sleep(0.5)
        self.assertEquals(self.rosout_monitor.error_count, 1)

    def test_too_many_services(self):
        msg = ds_acomms_msgs.msg.QueueDefinition()
        msg.background_data = make_char([6, 7])
        self.assertEquals(self.rosout_monitor.error_count, 0)
        self.definition_pub.publish(msg)
        time.sleep(0.2)
        self.assertEquals(self.rosout_monitor.error_count, 1)


class TestMessageDecoding(unittest.TestCase):
    def setUp(self):
        rp = rospkg.RosPack()
        qm_path = rp.get_path('comms_manager')
        filename = "{}/test/test_topics.txt".format(qm_path)
        self.qm = comms_manager.CommsManager(filename)
        self.data_monitor = DataMonitor(filename, prefix='/topside')

        self.data_pub = rospy.Publisher('/up/modem_data',
                                        ds_acomms_msgs.msg.MicromodemData,
                                        queue_size=1)
        time.sleep(0.2)

    def test_message_decoding(self):
        '''
        # TODO: This test is VERY flaky. I haven't yet figured out why it sometimes
        #   succeeds and sometimes fails.
        #   * It seems independent of adding additional sleeps after initial
        #     subscrption / after publication
        #   * It seems to be independent of how many times in a row I've run
        #     it w/o restarting the nodes. (Well --- it fails more often on the first run, and succeeds more often on successive ones)
        #   * Additionally, the messages comms_manager sometimes prints about "Inbound TCP/IP connection failed: connection from sender terminated before handshake header received. 0 bytes were received. Please check sender for additional details" are uncorrelated with test successs/failure.
        '''
        # First, use the already-tested comms manager interfaces to construct
        # the data bytes message
        def_msg = ds_acomms_msgs.msg.QueueDefinition()
        def_msg.queue_topics = make_char([1,2,3])
        def_msg.queue_periods = make_char([1,1,1])
        data_msg1 = std_msgs.msg.Float32(100.1)
        data_msg2 = std_msgs.msg.Float32(200.2)
        data_msg3 = std_msgs.msg.Float32(300.3)

        self.qm.queue_definition_callback(def_msg)
        self.qm.data_callback(chr(1), data_msg1)
        self.qm.data_callback(chr(2), data_msg2)
        self.qm.data_callback(chr(3), data_msg3)

        bytes_msg = ds_acomms_msgs.msg.MicromodemData()
        bytes_msg.frames.append(ds_acomms_msgs.msg.ModemData())
        bytes_msg.frames[0].payload = self.qm.encode_active()

        self.data_pub.publish(bytes_msg)
        time.sleep(0.3)
        self.assertTrue(chr(1) in self.data_monitor.rx_data)
        self.assertTrue(message_almost_equals(data_msg1, self.data_monitor.rx_data[chr(1)]))
        self.assertTrue(chr(2) in self.data_monitor.rx_data)
        self.assertTrue(message_almost_equals(data_msg2, self.data_monitor.rx_data[chr(2)]))
        self.assertTrue(chr(3) in self.data_monitor.rx_data)
        self.assertTrue(message_almost_equals(data_msg3, self.data_monitor.rx_data[chr(3)]))


class TestRateSelection(unittest.TestCase):
    '''
    Checks that the published MicromodemData messages have the requested
    rate, in response to directly having queue definitions and slots set.

    This is largely a copy-paste job from test_subsea_to_topside_pipeline.
    '''
    def setUp(self):
        self.bytes_monitor = BytesMonitor("/up/modem_data")
        self.def_pub = rospy.Publisher('/subsea/queue_definition',
                                       ds_acomms_msgs.msg.QueueDefinition,
                                       queue_size=1, latch=True)
        self.tdma_pub = rospy.Publisher('/subsea/tdma_slots',
                                        ds_acomms_msgs.msg.TDMASlots,
                                        queue_size=1, latch=True)
        self.data_pub = rospy.Publisher('/test/a', std_msgs.msg.Float32,
                                        queue_size=1, latch=True)

    def test_rate1(self):
        self.rate_helper(1, 1)

    def test_rate4(self):
        self.rate_helper(4, 4)

    def test_rate5(self):
        self.rate_helper(5, 5)

    def test_rate_default(self):
        '''
        Test that rate defaults to 1
        NOTE: This is a micromodem-specific test.
        '''
        self.rate_helper(0, 1)

    def rate_helper(self, input_rate, expected_rate):
        '''
        Helper function for testing various commanded rates.
        The separation between input_rate and expected_rate makes
        it possible to test the default behavior.
        '''
        tdma_msg = ds_acomms_msgs.msg.TDMASlots()
        tdma_msg.starts = range(0, 60, 3)
        tdma_msg.durations = [2 for _ in tdma_msg.starts]
        tdma_msg.rates = [input_rate for _ in tdma_msg.starts]
        self.tdma_pub.publish(tdma_msg)

        def_msg = ds_acomms_msgs.msg.QueueDefinition()
        def_msg.queue_topics = make_char([1,2,3,4])
        def_msg.queue_periods =  make_char([1,1,1,1])
        self.def_pub.publish(def_msg)

        time.sleep(0.2) # Give time for the queue definition to update

        data_msg = std_msgs.msg.Float32(123.456)
        self.data_pub.publish(data_msg)
        time.sleep(4.0) # The TDMA period is 3 seconds

        self.assertTrue(self.bytes_monitor.rx_data is not None)
        self.assertEqual(expected_rate, self.bytes_monitor.rx_data.rate)
        self.assertGreater(len(self.bytes_monitor.rx_data.frames), 0)
        self.assertGreater(len(self.bytes_monitor.rx_data.frames[0].payload), 0)


class TestBackgroundData(unittest.TestCase):
    '''
    In all cases, the provided background data is 128 bytes (requiring 132 bytes
    with the header added...)

    NB: This only handles a single frame. Does NOT yet do micromodem-style multiple
    frames per transmission.

    NB: Doesn't test case where service response contains more bytes than requested;
    that should be handled in the code, because the QM should be robust to
    poorly-implemented nodes.
    '''
    '''
    TODO:
    Test for packing background data into remaining space:
    * If too big for empty frame, don't raise error, just ignore
    * If current frame isn't empty and request fails, try requesting again on next frame
    * Request more data with space remaining after current frame -- only
      increment frame after failure, not immediately.
    '''

    def setUp(self):
        rp = rospkg.RosPack()
        qm_path = rp.get_path('comms_manager')
        filename = "{}/test/test_topics.txt".format(qm_path)
        self.qm = comms_manager.CommsManager(filename)

        self.good_service = DataServiceProvider('/test/data_srv/a')  # ID is 6
        self.block_service = DataServiceBlocker('/test/data_srv/b')  # ID is 7
        # /test/data_srv/c with ID 8 is NOT started, but called in the tests.

        self.data_monitor = DataMonitor(filename, '/topside')

        self.def_pub = rospy.Publisher('/subsea/queue_definition',
                                       ds_acomms_msgs.msg.QueueDefinition,
                                       queue_size=1, latch=True)
        self.tdma_pub = rospy.Publisher('/subsea/tdma_slots',
                                        ds_acomms_msgs.msg.TDMASlots,
                                        queue_size=1, latch=True)
        self.data_pub = rospy.Publisher('/test/a', std_msgs.msg.Float32,
                                        queue_size=1, latch=True)

        time.sleep(0.2)

    def tearDown(self):
        self.good_service.shutdown()
        self.block_service.shutdown()

    def test_server_not_started(self):
        '''
        Test that an unavailable background data service doesn't prevent
        a message from being transmitted.

        (This test is almost identical to test_subsea_to_topside_pipeline,
        with the addition of a non-functional service call)
        '''
        # Set up to relay topic 1 and service 8
        tdma_msg = ds_acomms_msgs.msg.TDMASlots()
        tdma_msg.starts = range(0, 60, 3)
        tdma_msg.durations = [2 for _ in tdma_msg.starts]
        tdma_msg.rates = [0 for _ in tdma_msg.starts]
        self.tdma_pub.publish(tdma_msg)

        def_msg = ds_acomms_msgs.msg.QueueDefinition()
        def_msg.queue_topics = make_char([1])
        def_msg.queue_periods =  make_char([1])
        def_msg.background_data = make_char([8])  # We didn't start this one
        self.def_pub.publish(def_msg)

        time.sleep(0.2) # Give time for the queue definition to update

        # Publish a data messages!
        data_msg = std_msgs.msg.Float32(123.456)
        self.data_monitor.clear()
        self.data_pub.publish(data_msg)
        time.sleep(4.0) # The TDMA period is 3 seconds

        # Make sure that we get messages with topic 1 (but no background data)
        self.assertTrue(chr(1) in self.data_monitor.rx_data)
        self.assertTrue(message_almost_equals(self.data_monitor.rx_data[chr(1)], data_msg))


    # ROS doesn't support timeouts in service calls, which makes it very
    # difficult to cleanly handle a server blocking.
    # For now, this failure mode isn't handled -- the comms manager only recovers when
    # the service provided by this test shuts down.

    # def test_server_times_out(self):
    #     '''
    #     Test that a server that never responds times out and doesn't prevent
    #     a message from being transmitted.

    #     (This test is almost identical to test_subsea_to_topside_pipeline
    #     and test_server_not_started, with the addition of a non-functional
    #     but extant service call)
    #     '''
    #     # Set up to relay topic 1 and service 7
    #     tdma_msg = ds_acomms_msgs.msg.TDMASlots()
    #     tdma_msg.starts = range(0, 60, 3)
    #     tdma_msg.durations = [2 for _ in tdma_msg.starts]
    #     tdma_msg.rates = [0 for _ in tdma_msg.starts]
    #     self.tdma_pub.publish(tdma_msg)

    #     def_msg = ds_acomms_msgs.msg.QueueDefinition()
    #     def_msg.queue_topics = make_char([1])
    #     def_msg.queue_periods =  make_char([1])
    #     def_msg.background_data = make_char([7])  # This one blocks...
    #     self.def_pub.publish(def_msg)

    #     time.sleep(0.2) # Give time for the queue definition to update

    #     # Publish a data message!
    #     data_msg = std_msgs.msg.Float32(123.456)
    #     self.data_monitor.clear()
    #     self.data_pub.publish(data_msg)
    #     time.sleep(15.0) # The TDMA period is 3 seconds

    #     print(self.data_monitor.rx_data)
    #     # Make sure that we get messages with topic 1 (but no background data)
    #     self.assertTrue(chr(1) in self.data_monitor.rx_data)
    #     self.assertTrue(message_almost_equals(self.data_monitor.rx_data[chr(1)], data_msg))


    def test_empty_buffer(self):
        '''
        check that we encode the returned data, given that we started with an
        empty buffer.
        '''
        pass

    def test_partial_buffer(self):
        '''
        check that background data is encoded when buffer isn't empty
        but does have enough space.
        (test with 132 bytes remaining)
        '''
        pass

    def test_full_buffer(self):
        '''
        check that background data is NOT encoded when buffer is full.
        (Check for both exactly full, and with 131 bytes remaining.)
        '''
        pass

    def test_background_data_published(self):
        '''
        Check that we actually publish background data.
        '''
        pass

    def test_background_data_rate1(self):
        '''
        Check that it doesn't fail when empty buffer is too small.
        '''
        pass

    def test_background_data_rate4(self):
        '''
        Check that it properly fills both frames.
        '''
        pass

    def test_background_data_rate5(self):
        '''
        Check that it properly fills all frames.
        '''
        pass



class RostestCommsManager(unittest.TestSuite):
    def __init__(self):
        super(RostestCommsManager, self).__init__()
        # As in unittest_qm, I'm unhappy with having to manually add the
        # test class names here, but still haven't figured out how to
        # handle auto-discovery with ROS.
        self.addTest(unittest.makeSuite(TestInvalidQueueDefinition))
        self.addTest(unittest.makeSuite(TestInvalidTDMASlots))
        self.addTest(unittest.makeSuite(TestMessageDecoding))
        self.addTest(unittest.makeSuite(TestPublishEncoded))
        self.addTest(unittest.makeSuite(TestBackgroundData))



if __name__=="__main__":
    '''
    This runs the tests standalone; for integration into rostest,
    see rostest_qm_node.
    '''
    rospy.init_node('rostest_comms_manager')
    unittest.main()
