#! /usr/bin/env python3

# Copyright 2019 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function, division

import rospy
import select
import socket
import io

import ds_acomms_msgs.msg


class MockMicromodem(object):
    '''
    A pair of MockMicromodems communicate over UDP to simulate the disconnected
    network we'll have topside/subsea.

    This does NOT fully simulate micromodem operation:
    * Doesn't include any propagation delays
    * Doesn't include transmission duration
    * Doesn't include Rx/Tx interference

    TODO: Add probabilistically dropped messages and frames.
    '''
    def __init__(self):
        self.rx_port = rospy.get_param("~rx_port", None)
        self.tx_port = rospy.get_param("~tx_port", None)
        self.tx_host = rospy.get_param("~tx_host", None)

        if None in [self.rx_port, self.tx_port, self.tx_host]:
            msg = ["un-set param in mock_micromodem!",
                   "rx_port: {}".format(self.rx_port),
                   "tx_port: {}".format(self.tx_port),
                   "tx_host: {}".format(self.tx_host)]
            raise Exception('\n'.join(msg))

        # Topic is named because this is the data that the modem will transmit
        self.sub = rospy.Subscriber("~tx",
                                    ds_acomms_msgs.msg.MicromodemData,
                                    self.modem_data_callback)

        # Publisher for data that the modem has received
        self.pub = rospy.Publisher("~rx",
                                   ds_acomms_msgs.msg.MicromodemData,
                                   queue_size=1)

        self.modem_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.modem_socket.bind(('', self.rx_port))
        self.timer = rospy.Timer(rospy.Duration(0.1),
                                 self.socket_timer_callback)

    def socket_timer_callback(self, _):
        # type: (rospy.TimerEvent) -> None
        '''
        Check if there's data waiting on the UDP interface. If so,
        forward it via the ROS interface.

        TODO: This is where the selective dropping of packets/frames
              should be implemented.
        '''
        timeout = 0.1  # seconds
        # Input to select is (read_list, write_list, x_list, timeout),
        # where the lists are FDs to check for readiness
        read_list = [self.modem_socket]
        write_list, x_list = [], []
        read_ready, _, _ = select.select(read_list, write_list, x_list, timeout)
        if len(read_ready) > 0:
            data = self.modem_socket.recv(4096)
            msg = ds_acomms_msgs.msg.MicromodemData()
            msg.deserialize(data)
            self.pub.publish(msg)

    def modem_data_callback(self, msg):
        # type: (ds_acomms_msgs.msg.MicromodemData) -> None
        '''
        Receive MicromodemData via the ROS interface and forward it
        via a UDP socket.

        NB: Currently does NO checking w/r/t valid rates / packet lengths.
        '''
        sio = io.StringIO()
        msg.serialize(sio)
        sio.seek(0)
        data = sio.read(sio.len)
        if len(data) > 4096:
            # Shouldn't ever happen since the largest Micromodem message
            # is 8 frames of 256 bytes each, but I've seen ROS's serialization
            # blow messages up to a ridiculous extent.
            rospy.logerr("WTF? serialized incoming message is too long: {}"
                         .format(msg))
            return
        self.modem_socket.sendto(data, (self.tx_host, self.tx_port))


if __name__ == "__main__":
    rospy.init_node("mock_micromodem")
    mm = MockMicromodem()
    rospy.spin()
