# Example config file specifying topics required for Sentry's comms_manager
# and coexploration setup to function.
# Format of each line is: [TOPIC] [MESSAGE_TYPE]

# Topics for the comms manager itself.
# The equivalent of these 3 is required to bootstrap from existing logs.
/sentry/acomms/manager/queue_definition ds_acomms_msgs/QueueDefinition
/sentry/acomms/manager/tdma_slots ds_acomms_msgs/TDMASlots
/sentry/acomms/manager/acks_expected ds_acomms_msgs/AcksExpected

# Topics for requesting an image
/sentry/coexploration/image_time std_msgs/Time

# Topics for interacting with Progressive Imagery
/sentry/coexploration/progressive_imagery/data ds_acomms_msgs/ModemData
/sentry/coexploration/progressive_imagery/queue_update progressive_imagery/QueueAction
/sentry/coexploration/progressive_imagery/transfer_progress progressive_imagery/TransferProgress
/sentry/coexploration/progressive_imagery/queue_update progressive_imagery/QueueAction

# Topics for the Quadtree Map Sender
/sentry/coexploration/quadtree/metadata coexploration_msgs/QuadtreeMetadata
