/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Jan 24, 2020.
//

#include "quadtree/quadtree.h"
#include "quadtree/quadtree_sender.h"

namespace quadtree {

QuadTreeSender::QuadTreeSender(float h_res, float v_res, int send_levels,
                               int min_level, int max_level, int32_t send_delay)
    : QuadTree(h_res, v_res, send_levels),
      min_streaming_level_(min_level),
      max_streaming_level_(max_level),
      send_delay_(send_delay),
      sent_subgrid_count_(0) {}

void QuadTreeSender::AddPoint(float xx, float yy, float zz, int32_t tt) {
  // Convert from world coordinates to cell coordinates
  float xc, yc, zc;
  CellFromWorld(xx, yy, zz, &xc, &yc, &zc);
  // Make sure that the tree spans the new point
  GrowTree(xc, yc);
  // Finally, update the tree with the new point
  UpdateTree(xc, yc, zc, tt);
}

void QuadTreeSender::GrowTree(float xc, float yc) {
  if (!root_) {
    root_ = std::make_unique<Node>();

    num_levels_ = 1;
    // This forces level 0 grid cells to have boundaries at integral
    // coordinates, and all centers for level > 0 will also be integral.
    // Uses ceil s.t. cells are open at min boundary and closed at max.
    x_center_ = std::ceil(xc) - 0.5;
    y_center_ = std::ceil(yc) - 0.5;
  }

  // While input point is outside of tree's span, add more levels
  float dx = xc - x_center_;
  float dy = yc - y_center_;
  // absolute value of offset between center of current cell and
  // its boundaries / center of cell one level up
  float radius = powf(2, num_levels_ - 2);

  // NB, bounds are asymmetrical (open to neg, closed to pos)
  while ((dx <= -radius) || (dx > radius) ||
         (dy <= -radius) || (dy > radius)) {
    std::unique_ptr<Node> new_root = std::make_unique<Node>();
    // The existing tree will be put in the new root caddy-corner to where
    // the new point will wind up. (So, this is OPPOSITE from GetChildIndex)
    int child_index;
    if (dx > 0) {
      if (dy > 0) {
        child_index = Node::LOWER_LEFT;
      } else {
        child_index = Node::UPPER_LEFT;
      }
    } else {
      if (dy > 0) {
        child_index = Node::LOWER_RIGHT;
      } else {
        child_index = Node::UPPER_RIGHT;
      }
    }

    float x_step, y_step;
    // NB: offset from this to parent requires +1 and reversing direction
    GetLevelOffset(num_levels_, child_index, &x_step, &y_step);
    x_center_ -= x_step;
    y_center_ -= y_step;

    new_root->mean_z = root_->mean_z;
    new_root->num_points = root_->num_points;
    new_root->updated_time = root_->updated_time;
    new_root->children =
        std::make_unique<std::array<std::unique_ptr<Node>, 4>>();
    new_root->children->at(child_index) = std::move(root_);

    root_ = std::move(new_root);
    num_levels_ += 1;
    dx = xc - x_center_;
    dy = yc - y_center_;
    radius = pow(2, num_levels_ - 2);
  }
}

// This assumes that the tree's span will contain xc, yc. (GrowTree was already
// called)
// NB: It does NOT guarantee it; if the point it outside the span of the tree,
//     it'll just get added to whichever leaf node is closest.
void QuadTreeSender::UpdateTree(float xc, float yc, float zc, int32_t tt) {
  auto curr_x = x_center_;
  auto curr_y = y_center_;

  Node* ptr = root_.get();
  // I don't love using the level to determine when to stop iterating,
  // but we may need to add more interior nodes in order to add the point.
  for (int level = num_levels_ - 1; level >= 0; level--) {
    // Update the mean/count at the current level
    float scale1 = 1.0 * ptr->num_points / (1 + ptr->num_points);
    float scale2 = 1.0 / (1 + ptr->num_points);
    ptr->mean_z = scale1 * ptr->mean_z + scale2 * zc;
    ptr->num_points += 1;
    ptr->updated_time = tt;

    // If this is a leaf node, skip the "determine and add children" step.
    if (0 == level) {
      break;
    }

    // Figure out where to go for the next level
    int child_idx = GetChildIndex(xc - curr_x, yc - curr_y);
    float dx, dy;
    GetLevelOffset(level, child_idx, &dx, &dy);
    curr_x += dx;
    curr_y += dy;

    // Add node if needed
    if (!ptr->children) {
      ptr->children = std::make_unique<std::array<std::unique_ptr<Node>, 4>>();
    }
    if (!ptr->children->at(child_idx)) {
      ptr->children->at(child_idx) = std::make_unique<Node>();
    }

    ptr = ptr->children->at(child_idx).get();
  }  // end iterating over level
}

void QuadTreeSender::AddPriorityRequests(
    const std::list<GridRequest>& requests) {
  for (const auto& new_request : requests) {
    // Check if requested level is allowed for this tree
    if (new_request.level < send_levels_ - 1) {
      warn_("QuadTreeSender::AddPriorityRequest: rejecting request -- level "
	    "too small for full grid");
      continue;
    }
    if (new_request.level >= num_levels_) {
      warn_("QuadTreeSender::AddPriorityRequest: rejecting request -- level "
	    "too big for full grid");
      continue;
    }

    // Check if request is within span of tree and has data
    float nx, ny;
    Node* node =
        GetNode(new_request.cx, new_request.cy, new_request.level, &nx, &ny);
    if (node == nullptr) {
      std::stringstream ss;
      ss << "Skipping request to add node at level " << new_request.level
	 << " with x,y = " << new_request.cx << ", " << new_request.cy
	 << " because it is not in tree's span";
      warn_(ss.str());
      continue;
    }

    // Check if there's new data
    if (new_request.timestamp >= node->updated_time) {
      debug_("Skipping request due to no new data!");
      continue;
    }

    // Check if list already has this point; if so, just update timestamp.
    bool duplicate = false;
    for (auto it = requested_grids_.begin(); it != requested_grids_.end();
         it++) {
      if (nx == it->cx && ny == it->cy && new_request.level == it->level) {
        auto new_timestamp = std::max(it->timestamp, new_request.timestamp);
        it->timestamp = new_timestamp;
        duplicate = true;
        break;
      }
    }
    if (duplicate) {
      continue;
    }

    requested_grids_.emplace_back(nx, ny, new_request.level,
                                  new_request.timestamp);
  }
}

void QuadTreeSender::ClearPriorities() { requested_grids_.clear(); }

std::unique_ptr<Subgrid> QuadTreeSender::GetNextSubgrid(
    const bool small, const int32_t curr_timestamp) {
  // First, check if a priority node can be sent.
  auto requested_subgrid = GetNextRequestedSubgrid(small, curr_timestamp);
  if (requested_subgrid != nullptr) {
    return requested_subgrid;
  }
  // Otherwise, send one based on the streaming heuristic
  auto streamed_subgrid = GetNextStreamedSubgrid(small, curr_timestamp);
  return streamed_subgrid;
}

std::unique_ptr<Subgrid> QuadTreeSender::GetNextRequestedSubgrid(
    const bool small, const int32_t curr_timestamp) {
  if (requested_grids_.empty()) {
    return nullptr;
  }

  Node* send_node = nullptr;
  std::list<GridRequest>::iterator it;
  float gx, gy;  // coordinates for the center of the grid
  for (it = requested_grids_.begin(); it != requested_grids_.end(); it++) {
    send_node = GetNode(it->cx, it->cy, it->level, &gx, &gy);
    if (send_node == nullptr) {
      warn_("Cannot find node for requested coordinates");
      continue;
    }
    if (small && !SubgridIsSmall(send_node)) {
      warn_("Requested small subgrid, but provided one is not small!");
    }
    // We can handle this request!
    break;
  }

  if (send_node) {
    sent_subgrid_count_ += 1;
    send_node->published_time = curr_timestamp;
    auto subgrid = GetSubgrid(send_node, it->level, gx, gy);
    requested_grids_.erase(it);
    return subgrid;
  }
  return nullptr;
}

std::unique_ptr<Subgrid> QuadTreeSender::GetNextStreamedSubgrid(
    const bool small, const int32_t curr_timestamp) {
  // Range of levels to stream.
  // These are the level of the subgrid's root node, not the leaf data
  // that will be sent. Thus, minimum level is determined by grid width
  int min_level = std::max(min_streaming_level_, send_levels_ - 1);
  int max_level = std::min(max_streaming_level_, num_levels_ - 1);

  coro_dfs_t::pull_type traverser([&](coro_dfs_t::push_type& out) {
    // For transmitting a grid, we want absolute coordinates, so seed
    // traversal with current grid center.
    TraverseBFS(root_.get(), num_levels_ - 1, x_center_, y_center_, out);
  });

  // Variables for current node that is being visited
  Node* node;
  int level;
  float cx, cy;
  // Current-best node
  Node* send_node = nullptr;
  int send_level = -1;
  float send_cx, send_cy;

  int send_age = std::numeric_limits<int>::min();
  int send_dt = std::numeric_limits<int>::min();

  int num_nodes_visited = 0;

  int num_level_too_high = 0;
  int num_level_too_low = 0;
  int num_not_updated = 0;
  int num_before_delay = 0;
  int num_not_small = 0;
  int num_not_older = 0;

  for (auto node_tup : traverser) {
    std::tie(node, level, cx, cy) = node_tup;
    num_nodes_visited += 1;

    if (level > max_level) {
      num_level_too_high += 1;
      continue;
    }

    // Terminate search if all following nodes will be on a too-low level
    if (level < min_level || level < send_level) {
      num_level_too_low += 1;
      break;
    }
    // Don't re-send.
    // TODO: These timestamps may be equal if this cell is actively being
    //       updated by a new scan. So, it would be better to also require
    //       a delta between the current time and the updated time.
    if (node->updated_time <= node->published_time) {
      num_not_updated += 1;
      continue;
    }

    // Only send a node if it hasn't been updated recently. This heuristic
    // avoids sending the currently-active region multiple times.
    if (node->updated_time + send_delay_ >= curr_timestamp) {
      if (num_before_delay == 0) {
	std::stringstream ss;
	ss << "Skipping node with updated_time = " << node->updated_time
	   << " because it is not " << send_delay_
	   << " before the current timestamp of " << curr_timestamp;
	// info_(ss.str());
      }
      num_before_delay += 1;
      continue;
    }

    // Send highest level first, ties broken by most out-of-date / oldest cells.
    int pub_dt = curr_timestamp - node->published_time;
    int update_dt = curr_timestamp - node->updated_time;
    // TODO: Write test that fails if this is the if statement....
    // if (pub_dt > send_age || (pub_dt == send_age && update_dt > send_dt)) {
    if (level > send_level || pub_dt > send_age ||
        (pub_dt == send_age && update_dt > send_dt)) {
      // If small grid requested, check that the range of z values will
      // fit into a uint8_t.
      if (small && !SubgridIsSmall(node)) {
        num_not_small += 1;
        continue;
      }
      send_age = pub_dt;
      send_dt = update_dt;
      send_node = node;
      send_level = level;
      send_cx = cx;
      send_cy = cy;
    } else {
      num_not_older += 1;
    }
  }

  if (send_node) {
    sent_subgrid_count_ += 1;
    send_node->published_time = curr_timestamp;
    return GetSubgrid(send_node, send_level, send_cx, send_cy);
  } else {
    std::stringstream ss;
    ss << "Visited " << num_nodes_visited << " nodes and couldn't find subgrid to stream" << std::endl;
    ss << " ... num_level_too_high " << num_level_too_high << std::endl;
    ss << " ... num_level_too_low " << num_level_too_low << std::endl;
    ss << " ... num_not_updated " << num_not_updated << std::endl;
    ss << " ... num_before_delay " << num_before_delay << std::endl;
    ss << " ... num_not_small " << num_not_small << std::endl;
    ss << " ... num_not_older " << num_not_older << std::endl;
    //  TOO TALKATIVE
    // info_(ss.str());

    return nullptr;
  }
}

// TODO: In this case, send_node should be a const, but that didn't match
//       the constness of TraverseSubgrid. See if I can clean that up ...
std::unique_ptr<Subgrid> QuadTreeSender::GetSubgrid(Node* send_node, int level,
                                                    float cx, float cy) {
  std::unique_ptr<Subgrid> subgrid{};

  // Can't pass packed struct field into std::make_unique constructor
  int tt = send_node->updated_time;
  // NB: the only time cx & cy won't be integers is if level = 0, which
  //     shouldn't ever happen.
  int xx = (int)std::round(cx);
  int yy = (int)std::round(cy);
  subgrid = std::make_unique<Subgrid>(num_levels()-1, x_center(), y_center(),
				      level, xx, yy, tt);

  coro_grid_t::pull_type subgrid_traverser([&](coro_grid_t::push_type& out) {
    TraverseSubgrid(send_node, send_levels_ - 1, out);
  });

  for (Node* node : subgrid_traverser) {
    if (node) {
      int value = node->get_value();
      subgrid->data.push_back(value);
    } else {
      subgrid->data.push_back(std::numeric_limits<int>::max());
    }
  }

  return subgrid;
}

bool QuadTreeSender::SubgridIsSmall(Node* root_node) {
  coro_grid_t::pull_type range_traverser([&](coro_grid_t::push_type& out) {
    TraverseSubgrid(root_node, send_levels_ - 1, out);
  });
  int min_z = std::numeric_limits<int>::max();
  int max_z = std::numeric_limits<int>::min();
  int zz;

  for (Node* node : range_traverser) {
    if (node) {
      zz = node->get_value();
      min_z = std::min(min_z, zz);
      max_z = std::max(max_z, zz);
    }
  }
  // TODO: assert that min/max have changed? Otherwise, this could overflow.
  //       (However, that should never happen -- if this is called, there is at
  //       least one child that will be traversed.)
  if (max_z < min_z) {
    warn_("QuadTreeSender::SubgridIsSmall: called on empty subgrid.");
    return false;
  }
  int dz = max_z - min_z;
  return dz < 255;
}

}  // namespace quadtree
