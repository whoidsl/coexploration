# Coexploration Catkin Workspace Install Guide 

Requirements:
Ubuntu 20.04 is recommended as that's where development has taken place.
This guide is assuming 20.04

# Install:
First, choose a location for your catkin workspace to live.

Create your workspace with "mkdir coex_workspace && cd coex_workplace"

Make a source directory as well with "mkdir src"

To save your git credentials throughout git downloading, first run: git config --global credenial.helper cache

## Now let's download

wstool init src $ROSINSTALLFILE

After completion we must now have to check out dependencies are satisfied

rosdep check --from-paths src --ignore-src

Some apt dependencies may be listed, so you can just install with "sudo apt install <pkg>"
For example, you require:
sudo apt install ros-noetic-joy python3-scipy python-numpy

IF you have other needed dependencies, you may install them with
rosdep install --from-paths src --ignore-src

Note on Ubuntu 20.04 a ppa for DCCL may be necessary

Follow these instructions in the github page to install: https://github.com/GobySoft/dccl

# Extra non listed dependencies.

Scalar data node requires numpy >= 1.20 or may crash. It has been observed ubuntu 20.04 may ship lower by default.
How to install?
pip install -U numpy

<<<<<<< HEAD
There are also some extra dependencies for the progressive_imagery library that rosdep does not seem to resolve correctly.  
# Listed apt packages below
=======
There are also some extra dependencies for the progressive_imagery library that rosdep does not seem to resolve correctly.
#Listed apt packages below
>>>>>>> 037e2385689960457bb4b5be0baa9681e2cf8e15
sudo apt update
sudo apt install libprotobuf-dev protobuf-compiler \
     libdccl3-dev dccl3-compiler \
     libopenjp2-7-dev \
     libboost-dev libboost-filesystem-dev \
     libmagick++-dev

## documentation dependencies
sudo apt install doxygen texlive-latex-extra
```

Run catkin_make to build your workspace!







