# Copyright 2021 University of Washington Applied Physics Laboratory
# Author: Laura Lindzey
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Functions for calculating dORP/dt and finding peaks in the data.

Pulled out of the node for easier testing + analysis.
"""

import numpy as np
import scipy.signal


def calc_dorp_dt(orp):
    """
    Calculate dorp/dt, following the implementation in dsros_process_orp.m:
        [orpfilt_b, orpfilt_a] = butter(3, 0.1);
        orp.dorpdt = filtfilt(orpfilt_b, orpfilt_a, [0; diff(orp.v)'./diff(orp.hdr_t')])';

    This function takes 5 ms to run on a full dive's data, so there's no reason to
    try to run it sequentially / on a windowed set of data.

    Input:
    * orp: N x 2 np.array. Columns are [time (seconds), ORP values in raw volts].
    Output:
    * dorpdt: (N-1) x 2 np.array. Columns are [time (seconds), filtered dORP/dt].
    """
    nrows, ncols = orp.shape
    if nrows < 20:
        errmsg = "Could not calculate dORP with only {} points".fomat(nrows)
        rospy.logerr_throttled(1, errmsg)
        return
    dorp = np.diff(orp, axis=0)

    # TODO: We need to be careful to NEVER raise a floating point error in any of the
    #     math that runs on Sentry. However, we do want to log it. It looks like
    #     one option is to use `np.seterr` s.t. 1/0 = 0, but I'm not sure if you can
    #     then catch it and report it via roslog?
    #     We DEFINITELY want numpy to handle it s.t. one bad value just leads to a
    #     neighborhood of invalid, rather than no data for the whole dive.
    dorpdt = np.divide(dorp[:,1], dorp[:,0])
    # # Prepend 0 to keep dorpdt the same shape as the timestamps.
    # dorpdt = np.insert(dorpdt, 0, 0, axis=0)

    # NB: the scipy docs say to prefer sosfiltfilt, but the Matlab code was
    #     using the coefficients method.
    b, a = scipy.signal.butter(3, 0.1)
    dorpdt_filtered = scipy.signal.filtfilt(b, a, dorpdt)

    # Output needs to carry along the timestamps
    out = np.zeros(dorp.shape)
    out[:,0] = orp[1:,0]
    out[:,1] = dorpdt_filtered

    return out


def find_peaks(dorp, threshold_factor):
    """
    Finds the time and value of unique dORP peaks, requiring their magnitude
    to be threshold_factor * stddev.

    Returned data will be Nx2 np.array, with first column time and 2nd column dorp/dt.
    The array will be sorted to have the most negative dorp/dt first.
    """
    peak_idxs = []
    npts, _ = dorp.shape
    stddev = np.std(dorp[:,1])
    for idx in np.arange(1, npts-1):
        is_minima = (dorp[idx,1] < dorp[idx-1, 1] and dorp[idx,1] < dorp[idx+1, 1])
        is_hit = dorp[idx,1] < -threshold_factor * stddev
        if is_minima and is_hit:
            peak_idxs.append(idx)
    peaks = dorp[peak_idxs,:]
    # Sort in order of dorp/dt, to make it easier to send the N largest.
    peaks = peaks[peaks[:,1].argsort()]

    return peaks

def find_clustered_peaks(dorpdt, detection_factor):
    # type: (np.ndarray, int) -> List(np.ndarray)
    """
    Cluster locations where dorp exceeds the detection factor
    and return peaks.

    Returns list of arrays, where each Nx2 array contains the peaks for
    a single cluster. Peaks within a cluster are sorted largest->smallest,
    and clusters are sorted by largest peak.
    """
    # Find all "hits", where dorpdt is below the threshold
    threshold = -1.0 * detection_factor * np.std(dorpdt[:,1])
    hit_idxs, = np.where(dorpdt[:,1] < threshold)

    # Find "segments" of contiguous hits
    hit_idx_diffs = np.diff(hit_idxs)
    seg_endpoints, = np.where(hit_idx_diffs > 1)
    seg_endpoints = np.append(seg_endpoints, len(hit_idx_diffs))
    hit_idx_segments = []
    start_idx = 0
    for ii in seg_endpoints:
        end_idx = ii+1
        hit_idx_segments.append((start_idx, end_idx))
        start_idx = end_idx

    # Find peaks within each cluster. This does mean that a peak with a
    # single value below the threshold won't be included, but I think that's fine.
    clustered_peaks = []
    for ss, ee in hit_idx_segments:
        orp_indices = hit_idxs[ss:ee]
        cluster = dorpdt[orp_indices, :]
        factor = 0  # We've already filtered these points, don't farther limit peaks
        peaks = find_peaks(cluster, factor)

        # Handle case where this cluster has no peaks.
        # This is most likely to occur when streaming data and the peak has
        # not yet been reached.
        if len(peaks) == 0:
            continue
        #print("Peaks, before sorting (shape={})".format(peaks.shape))
        #print(peaks)
        sort_idxs = np.argsort(peaks[:,1], axis=0)
        peaks = peaks[sort_idxs, :]
        peaks = peaks.reshape((-1, 2))
        #print("Peaks, after sorting (shape={}".format(peaks.shape))
        #print(peaks)
        clustered_peaks.append(peaks)

    if len(clustered_peaks) > 1:
        clustered_peaks.sort(key=lambda x: np.min(x[:,1]))

    return clustered_peaks

def select_peaks(clustered_peaks, num_to_transmit):
    # type: (List(np.array), int) -> Optional[np.array]
    """
    Cycle through clusters, choosing largest remaining peak from each cluster.
    """
    num_clusters = len(clustered_peaks)
    if num_clusters == 0:
        return None
    try:
        max_num_peaks = np.max([len(peaks[:,1]) for peaks in clustered_peaks])
    except Exception as ex:
        print(clustered_peaks)
        raise(ex)
    #print("Selecting peaks from {} clusters. Max peaks/cluster = {}"
    #      .format(num_clusters, max_num_peaks))

    selected_peaks = []

    for peak_idx in range(max_num_peaks):
        if len(selected_peaks) >= num_to_transmit:
            break
        for cluster_idx in range(num_clusters):
            if len(selected_peaks) >= num_to_transmit:
                break
            peaks = clustered_peaks[cluster_idx]
            if peak_idx >= len(peaks):
                #print("Skipping {}-{}".format(cluster_idx, peak_idx))
                continue
            #print("Add: cluster {}, peak {}".format(cluster_idx, peak_idx))
            selected_peaks.append(peaks[peak_idx])
    selected_peaks = np.array(selected_peaks)
    return selected_peaks
