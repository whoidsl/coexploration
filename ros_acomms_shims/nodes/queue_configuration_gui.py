#! /usr/bin/env python3

# Copyright 2022 University of Washington Applied Physics Lab
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from cProfile import label
import rospy
import signal
import sys
from typing import Dict, Optional
import yaml

import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui  # Only used for setting font to bold.
import PyQt5.QtWidgets as QtWidgets

from codec_config_parser import QueueParams, QueueId, MessageCodecId, get_queue_params, get_message_codecs
from ros_acomms.msg import QueueEnable, QueueStatus, QueuePriority
from rospy.msg import AnyMsg

# This widget displays the info from a single queue
# TODO: Maybe don't abstract this out, and instead put a grid inside the
#    scroll layout? That might be better, because then we can control that
#    the column width matches the headers.
class QueueWidget(QtWidgets.QWidget):
    def __init__(self,
                 queue_id: int,
                 sub_topic: str,
                 msg_class: AnyMsg,
                 enabled: bool,
                 priority: int,
                 enable_publisher: rospy.Publisher,
                 priority_publisher: rospy.Publisher,
                 ) -> None:
        super(QueueWidget, self).__init__()
        self.queue_id = queue_id
        self.sub_topic = sub_topic
        self.msg_class = msg_class
        self.enable_pub = enable_publisher
        self.priority_pub = priority_publisher
        # Variables that have been set by a queue_status callback
        self.received_enable = enabled
        self.received_priority = priority
        # Variables set by the user.
        self.commanded_enable = enabled
        self.commanded_priority = priority

        self.setup_widgets()

    def setup_widgets(self) -> None:
        self.queue_id_label = QtWidgets.QLabel("{}".format(self.queue_id))
        self.sub_topic_label = QtWidgets.QLabel(self.sub_topic)
        class_label_text = self.msg_class.__module__.replace(".msg._", ".")
        self.class_label = QtWidgets.QLabel(class_label_text)
        self.enable_checkbox = QtWidgets.QCheckBox()
        self.enable_checkbox.clicked.connect(self.enable_checkbox_clicked)
        self.enable_checkbox.setChecked(self.commanded_enable)
        enable_label_text = "True" if self.received_enable else "False"
        self.enable_label = QtWidgets.QLabel(enable_label_text)
        self.priority_line_edit = QtWidgets.QLineEdit()
        self.priority_line_edit.setValidator(QtGui.QIntValidator(0, 100))
        self.priority_line_edit.setFixedWidth(35)
        self.priority_line_edit.editingFinished.connect(self.priority_edited)
        self.priority_label = QtWidgets.QLabel("{}".format(self.received_priority))

    def priority_edited(self) -> None:
        priority = int(self.priority_line_edit.text())
        msg = QueuePriority()
        msg.queue_id = self.queue_id
        msg.priority = priority
        self.priority_pub.publish(msg)

    def enable_checkbox_clicked(self) -> None:
        enable = self.enable_checkbox.isChecked()
        msg = QueueEnable()
        msg.queue_id = self.queue_id
        msg.enable = enable
        self.enable_pub.publish(msg)

    def update_received_values(self, is_enabled: bool, priority: int) -> None:
        self.received_enable = is_enabled
        self.received_priority = priority
        enable_label_text = "True" if self.received_enable else "False"
        self.enable_label.setText(enable_label_text)
        self.priority_label.setText("{}".format(self.received_priority))

# TODO: better name? This is the widget that includes all queues from
#   a single node (message_queue_node)
class QueueNodeWidget(QtWidgets.QWidget):
    def __init__(self,
                 label: str,
                 status_topic: str,
                 priority_command_topic: str,
                 enable_command_topic: str,
                 params: Dict[int, QueueParams]) -> None:
        super(QueueNodeWidget, self).__init__()
        self.label = label  # e.g. "subsea", "topside"
        self.status_topic = status_topic
        self.priority_command_topic = priority_command_topic
        self.enable_command_topic = enable_command_topic
        self.params = params

        # Publishers need to be set up *before* the GUI, because they're passed
        # to other classes.
        self.priority_pub = rospy.Publisher(self.priority_command_topic, QueuePriority, queue_size=10)
        self.enable_pub = rospy.Publisher(self.enable_command_topic, QueueEnable, queue_size=10)

        self.setup_ui()

        # Subscribers need to be set up *after* the GUI is ready to handle them
        # Flag whether the displayed status are from the config file
        # or have been received from the node itself.
        self.received_status = False
        self.status_sub = rospy.Subscriber(self.status_topic, QueueStatus,
                                           self.queue_status_callback,
                                           queue_size=1)

    def setup_ui(self) -> None:
        queue_node_label = QtWidgets.QLabel(self.label)
        bold_font = queue_node_label.font()
        bold_font.setWeight(QtGui.QFont.Bold)
        queue_node_label.setFont(bold_font)

        id_width = 75
        sub_topic_width = 450
        class_width = 300
        cmd_enable_width = 75
        fbk_enable_width = 75
        cmd_priority_width = 75
        fbk_priority_width = 75

        # Header row for labelling for the the table
        queue_header_row = QtWidgets.QHBoxLayout()

        queue_header_id_label = QtWidgets.QLabel("Queue ID")
        queue_header_topic_label = QtWidgets.QLabel("Sub Topic")
        queue_header_class_label = QtWidgets.QLabel("Class")
        queue_header_cmd_enable_label = QtWidgets.QLabel("Requested\nEnable")
        queue_header_cmd_enable_label.setAlignment(QtCore.Qt.AlignCenter)
        self.queue_header_fbk_enable_label = QtWidgets.QLabel("Config\nEnable")
        self.queue_header_fbk_enable_label.setAlignment(QtCore.Qt.AlignCenter)
        queue_header_cmd_priority_label = QtWidgets.QLabel("Requested\nPriority")
        queue_header_cmd_priority_label.setAlignment(QtCore.Qt.AlignCenter)
        self.queue_header_fbk_priority_label = QtWidgets.QLabel("Config\nPriority")
        self.queue_header_fbk_priority_label.setAlignment(QtCore.Qt.AlignCenter)

        queue_header_id_label.setMinimumWidth(id_width)
        queue_header_topic_label.setMinimumWidth(sub_topic_width)
        queue_header_class_label.setMinimumWidth(class_width)
        queue_header_cmd_enable_label.setMinimumWidth(cmd_enable_width)
        self.queue_header_fbk_enable_label.setMinimumWidth(fbk_enable_width)
        queue_header_cmd_priority_label.setMinimumWidth(cmd_priority_width)
        self.queue_header_fbk_priority_label.setMinimumWidth(fbk_priority_width)

        queue_header_row.addWidget(queue_header_id_label)
        queue_header_row.addWidget(queue_header_topic_label)
        queue_header_row.addWidget(queue_header_class_label)
        queue_header_row.addWidget(queue_header_cmd_enable_label)
        queue_header_row.addWidget(self.queue_header_fbk_enable_label)
        queue_header_row.addWidget(queue_header_cmd_priority_label)
        queue_header_row.addWidget(self.queue_header_fbk_priority_label)
        queue_header_row.addStretch(1)

        # Scroll area for adding queue rows!
        # Holds another widget, provides horizontal and vertical scroll bars
        scroll_widget = QtWidgets.QScrollArea()
        scroll_widget.setWidgetResizable(True)
        scroll_widget.setFocusPolicy(QtCore.Qt.NoFocus)

        # ScrollArea requires a widget, not a layout, so this holds the Layout
        # (and adds a frame around it)
        scroll_frame = QtWidgets.QFrame()

        # NB: This seems like one too many layers of Layouts, but it was
        # the easiest way to make sure that the grid doesn't stretch awkwardly.
        # (Easy to add stretch at bottom of QVBoxLayout; I coulnd't find
        # that for a grid)
        scroll_layout = QtWidgets.QVBoxLayout()
        grid_layout = QtWidgets.QGridLayout()
        scroll_layout.addLayout(grid_layout)
        scroll_layout.addStretch(1)

        scroll_frame.setLayout(scroll_layout)
        scroll_widget.setWidget(scroll_frame)

        self.queue_widgets: Dict[QueueId, QueueParams] = {}
        grid_row: int = 1
        for queue_id, param in self.params.items():
            # rospy.logwarn("Creating QueueWidget (id = {}) with param = {}".format(queue_id, param))
            self.queue_widgets[queue_id] = QueueWidget(queue_id,
                                                       param.subscribe_topic,
                                                       param.msg_class,
                                                       param.is_active,
                                                       param.priority,
                                                       self.enable_pub,
                                                       self.priority_pub)
            grid_layout.addWidget(self.queue_widgets[queue_id].queue_id_label,
                                  grid_row, 0)
            grid_layout.addWidget(self.queue_widgets[queue_id].sub_topic_label,
                                  grid_row, 1)
            grid_layout.addWidget(self.queue_widgets[queue_id].class_label,
                                  grid_row, 2)
            grid_layout.addWidget(self.queue_widgets[queue_id].enable_checkbox,
                                  grid_row, 3, alignment=QtCore.Qt.AlignCenter)
            grid_layout.addWidget(self.queue_widgets[queue_id].enable_label,
                                  grid_row, 4)
            grid_layout.addWidget(self.queue_widgets[queue_id].priority_line_edit,
                                  grid_row, 5)
            grid_layout.addWidget(self.queue_widgets[queue_id].priority_label,
                                  grid_row, 6)

            grid_row += 1

        # Set up minimum column sizes to minimize required resizing
        grid_layout.setColumnMinimumWidth(0, id_width)
        grid_layout.setColumnMinimumWidth(1, sub_topic_width)
        grid_layout.setColumnMinimumWidth(2, class_width)
        grid_layout.setColumnMinimumWidth(3, cmd_enable_width)
        grid_layout.setColumnMinimumWidth(4, fbk_enable_width)
        grid_layout.setColumnMinimumWidth(5, cmd_priority_width)
        grid_layout.setColumnMinimumWidth(6, fbk_priority_width)
        grid_layout.setColumnStretch(6, 5)

        main_vbox = QtWidgets.QVBoxLayout()
        main_vbox.addWidget(queue_node_label)
        main_vbox.addLayout(queue_header_row)
        main_vbox.addWidget(scroll_widget)
        self.setLayout(main_vbox)

    def queue_status_callback(self, msg: QueueStatus) -> None:
        for queue_summary in msg.queue_summaries:
            queue_id = queue_summary.queue_id
            enabled = queue_summary.enabled
            priority = queue_summary.priority
            self.queue_widgets[queue_id].update_received_values(enabled, priority)
        if not self.received_status:
            self.queue_header_fbk_enable_label.setText("Received\nEnable")
            self.queue_header_fbk_priority_label.setText("Received\nPriority")
            self.received_status = True

class QueueConfigurationGUI(QtWidgets.QWidget):
    def __init__(self) -> None:
        super(QueueConfigurationGUI, self).__init__()
        self.setup_params()
        self.setup_ui()

    def setup_ui(self) -> None:
        width = 1230
        height = 800
        xpos = 150
        ypos = 150
        self.setGeometry(xpos, ypos, width, height)
        self.setWindowTitle("Message Queue Node Configuration")
        # Create main window
        # For each message queue node add a widget
        main_vbox = QtWidgets.QVBoxLayout()
        for message_queue_node in self.message_queue_nodes:
            codec_file = message_queue_node["codec_filename"]
            codec_config = yaml.load(open(codec_file, "r"))
            params = get_queue_params(codec_config[0])
            widget = QueueNodeWidget(message_queue_node["name"],
                                     message_queue_node["status_topic"],
                                     message_queue_node["priority_command_topic"],
                                     message_queue_node["enable_command_topic"],
                                     params)
            main_vbox.addWidget(widget)
        self.setLayout(main_vbox)
        self.show()

    def setup_params(self) -> None:
        self.message_queue_nodes = rospy.get_param("~message_queue_nodes")




if __name__ == "__main__":
    rospy.init_node("ros_acomms_queue_configuration_gui")
    app = QtWidgets.QApplication(sys.argv)
    qm = QueueConfigurationGUI()
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())
    # rospy.spin()


