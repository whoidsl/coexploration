/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Nov 19, 2019.
//

#include <gtest/gtest.h>
#include <quadtree/quadtree.h>
#include <quadtree/quadtree_receiver.h>
#include <quadtree/quadtree_sender.h>

namespace quadtree {  // required for adding tests as friend to QuadTree class

// Helper functions ....
// Creates a tree corresponding to a grid with width x width cells,
// where each leaf node has a single point with unique elevation.
// returns timestamp later than last addpoint call.
int BuildDenseTree(QuadTreeSender* tree, int width) {
  // Adding a diagonal first is the easiest way to guarantee the tree
  // topology. (Since valid topology -> grid is many-to-one)
  for (int ii = 0; ii < width; ii++) {
    tree->AddPoint(ii, ii, ii + ii * width, 0);
  }
  // Now, add the points.
  for (int ii = 0; ii < width; ii++) {
    for (int jj = 0; jj < width; jj++) {
      if (ii != jj) {
        tree->AddPoint(ii, jj, jj + ii * width, 1);
      }
    }
  }
  return width * width + 1;
};

// Creates a tree corresponding to a grid with width x width cells,
// where each leaf node has a single point with unique elevation.
int BuildDiagonalTree(QuadTreeSender* tree, int width) {
  // Adding a diagonal first is the easiest way to guarantee the tree
  // topology. (Since valid topology -> grid is many-to-one)
  for (int ii = 0; ii < width; ii++) {
    int val = width * (ii + 1);
    tree->AddPoint(ii, ii, val, val);
  }
  return width * width;
};

// Any of the tests that require being a friend class to QuadTree go in here.
// I initially tried to implement this without white-box testing, but tests
// using only the public interfaces require too-big chunks of implementation
// to be used in TDD.
class QuadTreeInternalsTest : public ::testing::Test {
  /*
    These tests are the only ones allowed access to the tree's internals.
  */

 protected:
  // No default constructor, so need to store a pointer to it...
  std::unique_ptr<quadtree::QuadTreeSender> tree;

  void SetUp() override {
    float horiz_resolution = 1.0;
    float vert_resolution = 1.0;
    int min_level = 2;
    int max_level = 6;
    int send_levels = 3;
    int32_t send_delay = 0;
    tree = std::make_unique<quadtree::QuadTreeSender>(
        horiz_resolution, vert_resolution, send_levels, min_level, max_level, send_delay);
  }

  void TearDown() override {}
};

TEST_F(QuadTreeInternalsTest, test_subgrid_small) {
  // We don't care about tree coordinates...can be same as subgrid
  int ll = 2;
  int xx = 0;
  int yy = 0;

  // all less than 255
  auto subgrid1 = quadtree::Subgrid(ll, xx, yy, ll, xx, yy, 0);
  for (int ii = 0; ii < 255; ii ++) {
    subgrid1.data.push_back(ii);
  }
  ASSERT_TRUE(subgrid1.IsSmall());

  // range less than 255, but there's an offset
  auto subgrid2 = quadtree::Subgrid(ll, xx, yy, ll, xx, yy, 0);
  for (int ii = 0; ii < 255; ii ++) {
    subgrid2.data.push_back(1000+ii);
  }
  ASSERT_TRUE(subgrid2.IsSmall());

  // range is 255
  auto subgrid3 = quadtree::Subgrid(ll, xx, yy, ll, xx, yy, 0);
  for (int ii = 0; ii <= 255; ii ++) {
    subgrid3.data.push_back(ii);
  }
  ASSERT_FALSE(subgrid3.IsSmall());
}

TEST_F(QuadTreeInternalsTest, AddPoint) {
  /*
     Test that adding points in a line along y=0 grows the tree
     The resulting tree will have the structure:
     (Showing mean_z at each node)
     1  2  3  4  5
     \ /   \ /   |
     1.5   3.5   5
       \   /     |
        2.5      5
           \    /
            4.5
   X-center is easy to visualize; Y-center gets offset with each new level.

   TODO: Delete this test? It was useful at the very start, but because it's
         testing private representation it's a pain to maintain.
  */
  // With first point, the tree should be centered at that point.
  tree->AddPoint(1, 0, 1, 111);
  ASSERT_EQ(1, tree->num_levels_);
  ASSERT_EQ(0.5, tree->x_center_);
  ASSERT_EQ(-0.5, tree->y_center_);
  ASSERT_EQ(111, tree->root_->updated_time);

  // Adding a second point should add another level, with the center in
  // between the points.
  // center at 1, 0
  // - -
  // 1 2
  tree->AddPoint(2, 0, 2, 222);
  ASSERT_EQ(1, tree->x_center_);
  ASSERT_EQ(0, tree->y_center_);
  ASSERT_EQ(2, tree->root_->num_points);
  ASSERT_EQ(1.5, tree->root_->mean_z);
  ASSERT_EQ(2, tree->num_levels());
  ASSERT_EQ(1, tree->root_->children->at(Node::LOWER_LEFT)->mean_z);
  ASSERT_EQ(2, tree->root_->children->at(Node::LOWER_RIGHT)->mean_z);
  ASSERT_EQ(222, tree->root_->updated_time);
  ASSERT_EQ(111, tree->root_->children->at(Node::LOWER_LEFT)->updated_time);
  ASSERT_EQ(222, tree->root_->children->at(Node::LOWER_RIGHT)->updated_time);

  // Center at 2, -1
  // - - - -
  // 1 2 3 -
  // - - - -
  // - - - -
  tree->AddPoint(3, 0, 3, 333);
  ASSERT_EQ(2, tree->x_center_);
  ASSERT_EQ(-1, tree->y_center_);
  ASSERT_EQ(3, tree->root_->num_points);
  ASSERT_EQ(2, tree->root_->mean_z);
  ASSERT_EQ(3, tree->num_levels());

  ASSERT_EQ(1, tree->root_->children->at(Node::UPPER_LEFT)
                   ->children->at(Node::LOWER_LEFT)
                   ->mean_z);
  ASSERT_EQ(2, tree->root_->children->at(Node::UPPER_LEFT)
                   ->children->at(Node::LOWER_RIGHT)
                   ->mean_z);
  ASSERT_EQ(3, tree->root_->children->at(Node::UPPER_RIGHT)
                   ->children->at(Node::LOWER_LEFT)
                   ->mean_z);
  ASSERT_EQ(222, tree->root_->children->at(Node::UPPER_LEFT)->updated_time);
  ASSERT_EQ(333, tree->root_->children->at(Node::UPPER_RIGHT)
                     ->children->at(Node::LOWER_LEFT)
                     ->updated_time);
  ASSERT_EQ(333, tree->root_->children->at(Node::UPPER_RIGHT)->updated_time);
  ASSERT_EQ(333, tree->root_->updated_time);

  // Center at 2, -1
  // - - - -
  // 1 2 3 4
  // - - - -
  // - - - -
  tree->AddPoint(4, 0, 4, 444);
  ASSERT_EQ(2, tree->x_center_);
  ASSERT_EQ(4, tree->root_->num_points);
  ASSERT_EQ(2.5, tree->root_->mean_z);
  ASSERT_EQ(3, tree->num_levels());
  ASSERT_EQ(4, tree->root_->children->at(Node::UPPER_RIGHT)
                   ->children->at(Node::LOWER_RIGHT)
                   ->mean_z);

  // Center at 4, 1
  // - - - - - - - -
  // - - - - - - - -
  // - - - - - - - -
  // - - - - - - - -
  // - - - - - - - -
  // 1 2 3 4 5 - - -
  // - - - - - - - -
  // - - - - - - - -
  tree->AddPoint(5, 0, 5, 555);
  ASSERT_EQ(4, tree->x_center_);
  ASSERT_EQ(5, tree->root_->num_points);
  ASSERT_EQ(3, tree->root_->mean_z);
  ASSERT_EQ(4, tree->num_levels());
}

class QuadTreeConstructionTest : public ::testing::Test {
  /*
    These tests check construction of a tree, using the public interfaces.
  */

 protected:
  std::unique_ptr<quadtree::QuadTreeSender> tree;

  void SetUp() override {
    float horiz_resolution = 1.0;
    float vert_resolution = 1.0;
    int min_level = 2;
    int max_level = 6;
    int send_levels = 3;
    tree = std::make_unique<quadtree::QuadTreeSender>(
        horiz_resolution, vert_resolution, send_levels, min_level, max_level, 0);
  }
};

TEST_F(QuadTreeConstructionTest, test_first_point) {
  // Test that adding the first point to a tree properly initializes it
  ASSERT_EQ(0, tree->num_levels());
  uint32_t t0 = 1;
  int zz = 2;
  tree->AddPoint(0, 0, zz, t0);
  ASSERT_EQ(1, tree->num_levels());

  auto grid = tree->MakeGrid(0);
  ASSERT_EQ(1, grid->size());
  ASSERT_EQ(1, grid->at(0).size());
  ASSERT_EQ(zz, grid->at(0).at(0));
}

TEST_F(QuadTreeConstructionTest, test_computing_mean) {
  // Adding multiple points to the same cell updates mean_z
  ASSERT_EQ(0, tree->num_levels());
  uint32_t t0 = 1;
  int z0 = 2;
  tree->AddPoint(0, 0, z0, t0);
  ASSERT_EQ(1, tree->num_levels());

  auto grid = tree->MakeGrid(0);
  ASSERT_EQ(z0, grid->at(0).at(0));

  // add second point slightly offset from first
  // (first was at top-right of cell, so have to subtract)
  float z1 = 3.2;
  tree->AddPoint(-0.1, -0.1, z1, t0);
  ASSERT_EQ(1, tree->num_levels());  // should be in same spot
  // Average should be 2.6, which rounds to 3.
  auto grid2 = tree->MakeGrid(0);
  ASSERT_EQ(3, grid2->at(0).at(0));

  // add third point slightly offset from first
  float z2 = 2.0;
  tree->AddPoint(-0.1, 0, z2, t0);
  ASSERT_EQ(1, tree->num_levels());  // should be in same spot
  // Average should be 2.4, which rounds to 2.
  auto grid3 = tree->MakeGrid(0);
  ASSERT_EQ(2, grid3->at(0).at(0));
}

TEST_F(QuadTreeConstructionTest, test_second_cell) {
  // Test that adding a second point to the tree properly grows it
  ASSERT_EQ(0, tree->num_levels());
  uint32_t t0 = 1;
  tree->AddPoint(0, 0, 1, t0);
  ASSERT_EQ(1, tree->num_levels());
  tree->AddPoint(1, 0, 2, t0);
  ASSERT_EQ(2, tree->num_levels());

  auto grid = tree->MakeGrid(0);
  ASSERT_EQ(1, grid->size());  // should be 1 row x 2 cols
  ASSERT_EQ(2, grid->at(0).size());
  ASSERT_EQ(1, grid->at(0).at(0));  // pt 1
  ASSERT_EQ(2, grid->at(0).at(1));  // pt 2
}

TEST_F(QuadTreeConstructionTest, test_adding_line) {
  // test adding points in a line; make sure resulting grid is 1xN
  // (Also tests using negative world coordinates as input)
  ASSERT_EQ(0, tree->num_levels());
  uint32_t t0 = 1;
  tree->AddPoint(-2, 0, 1, t0);
  ASSERT_EQ(1, tree->num_levels());
  tree->AddPoint(-1, 0, 2, t0);
  ASSERT_EQ(2, tree->num_levels());
  tree->AddPoint(0, 0, 3, t0);
  tree->AddPoint(1, 0, 4, t0);
  ASSERT_EQ(3, tree->num_levels());
  tree->AddPoint(2, 0, 5, t0);
  tree->AddPoint(3, 0, 6, t0);
  ASSERT_EQ(4, tree->num_levels());

  auto grid = tree->MakeGrid(0);
  ASSERT_EQ(1, grid->size());  // should be 1 row x 6 cols
  ASSERT_EQ(6, grid->at(0).size());
  for (int ii = 1; ii <= 6; ii++) {
    ASSERT_EQ(ii, grid->at(0).at(ii - 1));
  }
}

TEST_F(QuadTreeConstructionTest, test_add_skipping_levels) {
  // test adding new point requiring multiple new levels at once.
  ASSERT_EQ(0, tree->num_levels());
  uint32_t t0 = 1;
  tree->AddPoint(-2, 0, 1, t0);
  tree->AddPoint(3, 0, 6, t0);
  ASSERT_EQ(4, tree->num_levels());

  auto grid = tree->MakeGrid(0);
  ASSERT_EQ(1, grid->size());  // should be 1 row x 6 cols
  ASSERT_EQ(6, grid->at(0).size());
  ASSERT_EQ(1, grid->at(0).at(0));  // Only first and last cell should be filled
  ASSERT_EQ(6, grid->at(0).at(5));
  for (int ii = 2; ii <= 5; ii++) {
    // using maxint for the no-data value
    ASSERT_EQ(std::numeric_limits<int>::max(), grid->at(0).at(ii - 1));
  }
}

TEST_F(QuadTreeConstructionTest, test_negative_elevations) {
  // Test that a grid is correctly returned
  ASSERT_EQ(0, tree->num_levels());
  uint32_t t0 = 1;
  int npts = 10;
  for (int ii = 0; ii < npts; ii++) {
    tree->AddPoint(ii - 5, 0, ii - 5, t0);
  }

  auto grid = tree->MakeGrid(0);
  ASSERT_EQ(1, grid->size());  // should be 1 row x 6 cols
  ASSERT_EQ(npts, grid->at(0).size());
  for (int ii = 0; ii < npts; ii++) {
    ASSERT_EQ(ii - 5, grid->at(0).at(ii));
  }
}

TEST_F(QuadTreeConstructionTest, test_points_on_boundary) {
  // The tree nodes have open negative boundaries and closed positive ones.
  // These tests check that growing the tree and traversing it for insertion
  // are consistent.

  tree->AddPoint(0.5, 0.5, 0, 0);
  ASSERT_EQ(1, tree->num_levels());

  // Tree has 1x1 horizontal resolution.
  // Adding points on positive boundary shouldn't cause tree to grow.
  tree->AddPoint(1.0, 1.0, 0, 0);
  ASSERT_EQ(1, tree->num_levels());
  tree->AddPoint(1.0, 0.5, 0, 0);
  ASSERT_EQ(1, tree->num_levels());

  // Adding point on negative x boundary, with a y value that causes
  // the tree will grow in positive y / negative x
  // (new tree will center at (-0.5, 0.5)
  tree->AddPoint(0, 0.2, 0, 0);
  ASSERT_EQ(2, tree->num_levels());
  // And, on negative y boundary; tree should have to grow again
  // in negative 1, positive x.
  // (new tree will center at 0.5, -0.5)
  tree->AddPoint(0, -1, 0, 0);
  ASSERT_EQ(3, tree->num_levels());
}


//////////////////////////////////////////////////////////////////////////

class QuadTreeResolutionTest : public ::testing::Test {
  /*
    These tests check functionality with non-unit resolution.
  */
 protected:
  void SetUp() override {}
};

TEST_F(QuadTreeResolutionTest, test_smaller_horizontal_resolution) {
  // Test that smaller horizontal resolution works (using points on a diagonal)
  // This also tests multi-resolution grid creation
  float horiz_resolution = 0.5;
  float vert_resolution = 1.0;
  int min_level = 2;
  int max_level = 6;
  int send_levels = 3;

  quadtree::QuadTreeSender tree = quadtree::QuadTreeSender(
      horiz_resolution, vert_resolution, send_levels, min_level, max_level, 0);

  ASSERT_EQ(0, tree.num_levels());
  uint32_t t0 = 1;
  tree.AddPoint(-1, -1, 1, t0);
  ASSERT_EQ(1, tree.num_levels());
  tree.AddPoint(-0.5, -0.5, 2, t0);
  ASSERT_EQ(2, tree.num_levels());
  tree.AddPoint(0, 0, 3, t0);
  tree.AddPoint(0.5, 0.5, 4, t0);
  ASSERT_EQ(3, tree.num_levels());
  tree.AddPoint(1, 1, 5, t0);
  tree.AddPoint(1.5, 1.5, 6, t0);
  ASSERT_EQ(4, tree.num_levels());

  auto grid = tree.MakeGrid(0);
  ASSERT_EQ(6, grid->size());  // should be 6 rows x 6 cols
  ASSERT_EQ(6, grid->at(0).size());
  for (int ii = 1; ii <= 6; ii++) {
    ASSERT_EQ(ii, grid->at(ii - 1).at(ii - 1));
  }

  auto grid1 = tree.MakeGrid(1);
  ASSERT_EQ(3, grid1->size());  // should be 3 rows x 3 cols
  ASSERT_EQ(3, grid1->at(0).size());
  for (int ii = 1; ii <= 3; ii++) {
    ASSERT_EQ((int)std::round(0.5*(4*ii-1)), grid1->at(ii - 1).at(ii - 1));
  }

  auto grid2 = tree.MakeGrid(2);
  ASSERT_EQ(2, grid2->size());  // should be 2 rows x 2 cols
  ASSERT_EQ(2, grid2->at(0).size());
  ASSERT_EQ(3, grid2->at(0).at(0));
  ASSERT_EQ(6, grid2->at(1).at(1));
}

TEST_F(QuadTreeResolutionTest, test_larger_horizontal_resolution) {
  // Test that larger horizontal resolution works (using points on a diagonal)
  float horiz_resolution = 2.0;
  float vert_resolution = 1.0;
  int min_level = 2;
  int max_level = 6;
  int send_levels = 3;

  quadtree::QuadTreeSender tree = quadtree::QuadTreeSender(
      horiz_resolution, vert_resolution, send_levels, min_level, max_level, 0);

  ASSERT_EQ(0, tree.num_levels());
  uint32_t t0 = 1;
  tree.AddPoint(-4, -4, 1, t0);
  ASSERT_EQ(1, tree.num_levels());
  tree.AddPoint(-2, -2, 2, t0);
  ASSERT_EQ(2, tree.num_levels());
  tree.AddPoint(0, 0, 3, t0);
  tree.AddPoint(2, 2, 4, t0);
  ASSERT_EQ(3, tree.num_levels());
  tree.AddPoint(4, 4, 5, t0);
  tree.AddPoint(6, 6, 6, t0);
  ASSERT_EQ(4, tree.num_levels());

  auto grid = tree.MakeGrid(0);
  ASSERT_EQ(6, grid->size());  // should be 6 rows x 6 cols
  ASSERT_EQ(6, grid->at(0).size());
  for (int ii = 1; ii <= 6; ii++) {
    ASSERT_EQ(ii, grid->at(ii - 1).at(ii - 1));
  }
}

TEST_F(QuadTreeResolutionTest, test_smaller_vertical_resolution) {
  float horiz_resolution = 2.0;
  float vert_resolution = 0.1;
  int min_level = 2;
  int max_level = 6;
  int send_levels = 3;

  quadtree::QuadTreeSender tree = quadtree::QuadTreeSender(
      horiz_resolution, vert_resolution, send_levels, min_level, max_level, 0);
  tree.AddPoint(0, 0, 1, 0);
  auto grid = tree.MakeGrid(0);
  ASSERT_EQ(10, grid->at(0).at(0));
  tree.AddPoint(0, 0, 2, 0);
  grid = tree.MakeGrid(0);
  ASSERT_EQ(15, grid->at(0).at(0));
}

TEST_F(QuadTreeResolutionTest, test_larger_vertical_resolution) {
  float horiz_resolution = 2.0;
  float vert_resolution = 10;
  int min_level = 2;
  int max_level = 6;
  int send_levels = 3;

  quadtree::QuadTreeSender tree = quadtree::QuadTreeSender(
      horiz_resolution, vert_resolution, send_levels, min_level, max_level, 0);
  tree.AddPoint(0, 0, 1, 0);
  auto grid = tree.MakeGrid(0);
  ASSERT_EQ(0, grid->at(0).at(0));
  tree.AddPoint(0, 0, 10, 0);
  grid = tree.MakeGrid(0);
  ASSERT_EQ(1, grid->at(0).at(0));  // 11/2 rounds up
  tree.AddPoint(0, 0, 20, 0);
  grid = tree.MakeGrid(0);
  ASSERT_EQ(1, grid->at(0).at(0));  // 31/3 rounds down
  tree.AddPoint(0, 0, 30, 0);
  grid = tree.MakeGrid(0);
  ASSERT_EQ(2, grid->at(0).at(0));  // 61/4 rounds up
}

//////////////////////////////////////////////////////////

class QuadTreeTraversalTest : public ::testing::Test {
  /*
    Tests various tree traversal functionality.
    - GetNode
   */
 protected:
  std::unique_ptr<quadtree::QuadTreeSender> tree;
  void SetUp() override {
    float horiz_resolution = 1.0;
    float vert_resolution = 1.0;
    int min_level = 0;
    int max_level = 4;
    int send_levels = 3;
    int send_delay = 0;
    tree = std::make_unique<quadtree::QuadTreeSender>(
        horiz_resolution, vert_resolution, send_levels, min_level, max_level, send_delay);
  }
};

TEST_F(QuadTreeTraversalTest, test_get_node) {
  BuildDiagonalTree(tree.get(), 40);
  float xmin, xmax, ymin, ymax;
  tree->GetTreeExtrema(0, &xmin, &xmax, &ymin, &ymax);
  float xavg = 0.5 * (xmin + xmax);
  float yavg = 0.5 * (ymin + ymax);

  // Node outside the bounds should always return nullptr
  float nodex, nodey;
  auto node1 = tree->GetNode(xmin-1, yavg, 0, &nodex, &nodey);
  ASSERT_EQ(node1, nullptr);
  auto node2 = tree->GetNode(xmax+1, yavg, 0, &nodex, &nodey);
  ASSERT_EQ(node2, nullptr);
  auto node3 = tree->GetNode(xavg, ymin-1, 0, &nodex, &nodey);
  ASSERT_EQ(node3, nullptr);
  auto node4 = tree->GetNode(xavg, ymax+1, 0, &nodex, &nodey);
  ASSERT_EQ(node4, nullptr);

  // Inside the bounds and on the diagonal is NOT nullptr
  auto node5 = tree->GetNode(xmin+1, ymin+1, 0, &nodex, &nodey);
  ASSERT_NE(node5, nullptr);
  // Inside the bounds and far from the diagonal should be nullptr again
  auto node6 = tree->GetNode(xmin+1, yavg, 0, &nodex, &nodey);
  ASSERT_EQ(node6, nullptr);

  // Outside the bounds and on the diagonal is where we actually expect
  // GetNode to require bounds checking (otherwise, missing internal nodes
  // will cause it to return the correct nullptr)
  // NB: xmin/ymin are the _center_ of the cells; and hres = 1.
  auto node7 = tree->GetNode(xmin-0.6, ymin-0.6, 0, &nodex, &nodey);
  ASSERT_EQ(node7, nullptr);
}
//////////////////////////////////////////////////////////

class QuadTreePrioritizationTest : public ::testing::Test {
  /*
    Tests the interface for requesting subgrid updates.
    This class can only test a fixed min/max/send level configuration
    (It is intentionally not changeable after construction.)
   */
 protected:
  std::unique_ptr<quadtree::QuadTreeSender> tree;
  void SetUp() override {
    float horiz_resolution = 1.0;
    float vert_resolution = 1.0;
    int min_level = 0;
    int max_level = 4;
    int send_levels = 3;
    int32_t send_delay = 0;
    tree = std::make_unique<quadtree::QuadTreeSender>(
        horiz_resolution, vert_resolution, send_levels, min_level, max_level, send_delay);
  }
};

TEST_F(QuadTreePrioritizationTest, test_send_success) {
  bool small_packet = false;

  uint32_t curr_timestamp = 100;
  // First, empty tree should not return data to send
  // (This also tests that operations don't fail on an empty tree...)
  auto subgrid1 = tree->GetNextSubgrid(small_packet, curr_timestamp);
  ASSERT_TRUE(subgrid1 == nullptr);

  // Too-short tree should not return data ...
  tree->AddPoint(1, 1, 1, 1);
  ASSERT_EQ(1, tree->num_levels());
  auto subgrid2 = tree->GetNextSubgrid(small_packet, curr_timestamp);
  ASSERT_TRUE(subgrid2 == nullptr);

  tree->AddPoint(2, 2, 2, 2);
  ASSERT_EQ(2, tree->num_levels());
  auto subgrid3 = tree->GetNextSubgrid(small_packet, curr_timestamp);
  ASSERT_TRUE(subgrid3 == nullptr);

  // Adding 3rd point makes tree have 3 levels, and thus 4x4 and sendable
  tree->AddPoint(3, 2, 3, 3);
  ASSERT_EQ(3, tree->num_levels());
  auto full_grid = tree->MakeGrid(0);
  auto subgrid4 = tree->GetNextSubgrid(small_packet, curr_timestamp);
  ASSERT_TRUE(subgrid4 != nullptr);
  ASSERT_EQ(2, subgrid4->level);
  ASSERT_EQ(2, subgrid4->xx);
  ASSERT_EQ(2, subgrid4->yy);
  ASSERT_EQ(3, subgrid4->timestamp);

  // Grid should be (showing height, but also valid for timestamp):
  //   col     0 1 2 3
  //   -----   -------
  //   row 3 - 0 0 0 0
  //   row 2 - 0 0 0 0
  //   row 1 - 0 2 3 0
  //   row 0 - 1 0 0 0
  //
  //   Recall that the tree traversal order is:
  //    0  1  4  5
  //    2  3  6  7
  //    8  9 12 13
  //   10 11 14 15
  //
  //   Running PrintGrid on it yields:
  //   row 0 -      1 65535 65535
  //   row 1 -  65535     2     3

  std::vector<int> answer(16, std::numeric_limits<int>::max());
  answer[9] = 2;
  answer[10] = 1;
  answer[12] = 3;
  ASSERT_EQ(answer, subgrid4->data);
}

TEST_F(QuadTreePrioritizationTest, test_send_small) {
  /*
   Test that the "small" argument is handled correctly
  */

  // Create a grid that looks like:
  // col   -     0     1     2     3     4     5     6     7
  // -----   -----------------------------------------------
  // row 0 -     -     -     -     -     -     -     -     -
  // row 1 -     -     -     -     -     -     -     -     -
  // row 2 -     -     -     -   192     -     -     -  3072
  // row 3 -     -     -   128     -     -     -  2048     -
  // row 4 -     -    64     -     -     -  1024     -     -
  // row 5 -     0     -     -     -     0     -     -     -
  // row 6 -     -     -     -     -     -     -     -     -
  // row 7 -     -     -     -     -     -     -     -     -
  // The age order (oldest -> newest): Lower left, lower right, upper left,
  // upper right
  for (int ii = 0; ii < 4; ii++) {
    // diagnoal with uint8 will be more recent on average than uint16,
    //  with lower values older (and thus should be send first)
    tree->AddPoint(ii, ii, ii * 64, 5 * ii);
    // lower left corner will be oldest
    tree->AddPoint(ii + 4, ii, ii * 1024, 10 * ii);
  }
  ASSERT_EQ(4, tree->num_levels());
  uint32_t curr_timestamp = 100;
  // Asking for a small tree. See p. 63 (Dec 2 2019) for cell numbering.
  // Lower left wil be oldest.
  auto subgrid1 = tree->GetNextSubgrid(true, curr_timestamp);
  ASSERT_TRUE(subgrid1 != nullptr);
  ASSERT_EQ(2, subgrid1->level);
  std::vector<int> first_answer(16, std::numeric_limits<int>::max());
  first_answer[1] = 64;
  first_answer[2] = 0;
  ASSERT_EQ(first_answer, subgrid1->data);

  // asking for any subgrid tree; should get the lowest resolution subgrid.
  auto subgrid2 = tree->GetNextSubgrid(false, curr_timestamp);
  ASSERT_TRUE(subgrid2 != nullptr);
  ASSERT_EQ(3, subgrid2->level);
  std::vector<int> second_answer(16, std::numeric_limits<int>::max());
  second_answer[3] = 160;
  second_answer[7] = 2560;
  second_answer[8] = 32;
  second_answer[12] = 512;
  ASSERT_EQ(second_answer, subgrid2->data);

  // Lower right is the oldest higher-resolution subgrid that hasn't been sent.
  auto subgrid3 = tree->GetNextSubgrid(false, curr_timestamp);
  ASSERT_EQ(2, subgrid3->level);
  ASSERT_TRUE(subgrid3 != nullptr);
  std::vector<int> third_answer(16, std::numeric_limits<int>::max());
  third_answer[1] = 1024;
  third_answer[2] = 0;
  ASSERT_EQ(third_answer, subgrid3->data);

  // Upper left is next-oldest; it is small, but not requested explicitly.
  auto subgrid4 = tree->GetNextSubgrid(false, curr_timestamp);
  ASSERT_TRUE(subgrid4 != nullptr);
  std::vector<int> fourth_answer(16, std::numeric_limits<int>::max());
  fourth_answer[13] = 192;
  fourth_answer[14] = 128;
  ASSERT_EQ(fourth_answer, subgrid4->data);

  // Finally, upper right.
  auto subgrid5 = tree->GetNextSubgrid(false, curr_timestamp);
  ASSERT_TRUE(subgrid5 != nullptr);
  std::vector<int> fifth_answer(16, std::numeric_limits<int>::max());
  fifth_answer[13] = 3072;
  fifth_answer[14] = 2048;
  ASSERT_EQ(fifth_answer, subgrid5->data);

  // After sending 5 updates, everything has been sent
  auto subgrid6 = tree->GetNextSubgrid(false, curr_timestamp);
  ASSERT_TRUE(subgrid6 == nullptr);
}

TEST_F(QuadTreePrioritizationTest, test_send_small_max_range) {
  /*
    Test that requesting a small grid works when data range is exactly 8 bits,
    and fails when values range from 0 - 256.

    This also implicitly tests:
    - adding a new point will cause the next call to GetNextSubgrid to succeed.
    - Empty subgrids won't be sent.
  */
  bool small_packet = true;

  tree->AddPoint(0, 0, 0, 0);
  tree->AddPoint(1, 1, 64, 0);
  tree->AddPoint(2, 2, 128, 0);
  tree->AddPoint(3, 3, 254, 0);

  // Make sure that the points we inserted created tree with the expected
  // topology (rest of test assumes only one valid subgrid)
  ASSERT_EQ(3, tree->num_levels());

  // with a range of exactly 8 bits, this should work with small_packet
  auto subgrid1 = tree->GetNextSubgrid(small_packet, 1);
  ASSERT_TRUE(subgrid1 != nullptr);

  // Shouldn't get ANY updates (full resolution already sent)
  auto subgrid2 = tree->GetNextSubgrid(false, 2);
  ASSERT_TRUE(subgrid2 == nullptr);

  // now make the tree's range > 8 bits
  tree->AddPoint(3, 3, 257, 3);

  // Requesting a small packet should fail now.
  auto subgrid3 = tree->GetNextSubgrid(small_packet, 4);
  ASSERT_TRUE(subgrid3 == nullptr);

  // But a regular one should work
  auto subgrid4 = tree->GetNextSubgrid(false, 5);
  ASSERT_TRUE(subgrid4 != nullptr);

  // ... but only once, since there are no more updates.
  auto subgrid5 = tree->GetNextSubgrid(false, 5);
  ASSERT_TRUE(subgrid5 == nullptr);
}

TEST_F(QuadTreePrioritizationTest, test_send_small_max_range_shifted) {
  /*
    Test that requesting a small grid works when data range is exactly 8 bits,
    but not in the range of [0, 256)

    This also implicitly tests:
    - adding a new point will cause the next call to GetNextSubgrid to succeed.
    - Empty subgrids won't be sent.
  */
  tree->AddPoint(0, 0, 256, 0);
  tree->AddPoint(1, 1, 320, 0);
  tree->AddPoint(2, 2, 384, 0);
  tree->AddPoint(3, 3, 510, 0);

  // Make sure that the points we inserted created tree with the expected
  // topology (rest of test assumes only one valid subgrid)
  ASSERT_EQ(3, tree->num_levels());

  // with a range of exactly 8 bits, this should work with small_packet
  auto subgrid1 = tree->GetNextSubgrid(true, 1);
  ASSERT_TRUE(subgrid1 != nullptr);

  // Shouldn't get ANY updates (full resolution already sent)
  auto subgrid2 = tree->GetNextSubgrid(false, 2);
  ASSERT_TRUE(subgrid2 == nullptr);

  // now make the tree's range > 8 bits
  tree->AddPoint(3, 3, 513, 3);

  // Requesting a small packet should fail now.
  auto subgrid3 = tree->GetNextSubgrid(true, 4);
  ASSERT_TRUE(subgrid3 == nullptr);

  // But a regular one should work
  auto subgrid4 = tree->GetNextSubgrid(false, 5);
  ASSERT_TRUE(subgrid4 != nullptr);

  // ... but only once, since there are no more updates.
  auto subgrid5 = tree->GetNextSubgrid(false, 5);
  ASSERT_TRUE(subgrid5 == nullptr);
}

//////////////////////////////////////////////////////////
class QuadTreeSendLevelsTest : public ::testing::Test {
  /*
    Tests the parameters controlling send levels.
   */
 protected:
  // Needs to be initialized by each test, since the tree doesn't
  // support changing send level parameters.
  std::unique_ptr<quadtree::QuadTreeSender> tree;
};

TEST_F(QuadTreeSendLevelsTest, test_save_small_grids) {
  // Test that saving of grids doesn't crash.
  // (Actual test will be reading them in in python ...)
  auto tree =
        std::make_unique<quadtree::QuadTreeSender>(1.0, 1.0, 3, 0, 10, 0);
  BuildDiagonalTree(tree.get(), 20);
  tree->SaveGrids("diagonal_grids", 0, 5);
}

TEST_F(QuadTreeSendLevelsTest, test_send_grid_size) {
  // Check that the send_levels parameter is reflected in resulting
  // subgrid size and that too-big grids won't be sent.
  for (int num_levels = 1; num_levels < 10; num_levels++) {
    // set min/max level s.t. they won't influence whether a grid can be sent.
    tree =
        std::make_unique<quadtree::QuadTreeSender>(1.0, 1.0, num_levels, 0, 10, 0);
    BuildDenseTree(tree.get(), 20);
    ASSERT_EQ(6, tree->num_levels());
    auto subgrid = tree->GetNextSubgrid(false, 1);
    if (num_levels <= 6) {
      ASSERT_TRUE(subgrid != nullptr);
      int npts = pow(pow(2, num_levels - 1), 2);
      ASSERT_EQ(npts, subgrid->data.size());
    } else {
      ASSERT_TRUE(subgrid == nullptr);
    }
  }
}

TEST_F(QuadTreeSendLevelsTest, test_send_grid_levels) {
  // Checks that the correct number of updates are sent as a
  // function of the range of sendable levels.
  int num_levels = 3;  // for 8x8 grids
  std::unique_ptr<Subgrid> subgrid;

  for (int min_level = 0; min_level < 8; min_level++) {
    for (int max_level = min_level; max_level < 8; max_level += 2) {
      // Create compact tree for 32x32 grid. (64 is  prohibitively slow)
      tree = std::make_unique<quadtree::QuadTreeSender>(1.0, 1.0, num_levels,
                                                        min_level, max_level, 0);
      BuildDenseTree(tree.get(), 32);

      // How many levels can the tree be rooted at?
      // This needs a +1 because min/max are inclusive, and needs the max
      // for the case that a cell at the minimum level doesn't
      // have enough levels underneath it for a full subgrid.
      int num_root_levels = (std::min(tree->num_levels(), 1 + max_level) -
                             std::max(min_level, num_levels - 1));
      if (num_root_levels <= 0) {
        continue;
      }

      // Figure out how many subgrid root nodes are between min/max levels,
      // in a fully-populated tree:
      int num_subgrids = 0;
      for (int level = tree->num_levels() - 1; level >= 0; level--) {
        if (level <= max_level &&
            level >= std::max(min_level, num_levels - 1)) {
          num_subgrids += pow(4, tree->num_levels() - 1 - level);
        }
      }

      // Check that we have exactly the expected number of subgrids
      for (int ii = 0; ii < num_subgrids; ii++) {
        subgrid = tree->GetNextSubgrid(false, 2);
        if (subgrid == nullptr) {
          std::cout << "Whoops! the " << ii << "-th subgrid call failed"
                    << std::endl;
          std::cout << "For tree with min/max level = " << min_level << ", "
                    << max_level << ", we expect " << num_root_levels
                    << " levels of root nodes, yielding " << num_subgrids
                    << " subgrids." << std::endl;
        } else {
          // std::cout << "Got subgrid at level " << subgrid->level << ", and
          // coords = " << subgrid->xx << ", " << subgrid->yy << " with " <<
          // subgrid->data.size() << " points." << std::endl;
        }
        ASSERT_TRUE(subgrid != nullptr);
      }
      subgrid = tree->GetNextSubgrid(false, 2);
      ASSERT_TRUE(subgrid == nullptr);
    }
  }
}

//////////////////////////////////////////////////////////

class QuadTreeGridRequestTest : public ::testing::Test {
  /**
   Test user-prioritization of subgrids.
   */

 protected:
  std::unique_ptr<quadtree::QuadTreeSender> tree;
  void SetUp() override {
    float horiz_resolution = 1.0;
    float vert_resolution = 1.0;
    int min_level = 0;
    int max_level = 4;
    int send_levels = 3;  // 4x4 grids
    int32_t send_delay = 0;
    tree = std::make_unique<quadtree::QuadTreeSender>(
        horiz_resolution, vert_resolution, send_levels, min_level, max_level, send_delay);
  }

  int GetAllSubgrids(int32_t timestamp) {
    int count = 0;
    auto subgrid = tree->GetNextSubgrid(false, timestamp);
    while (subgrid != nullptr) {
      count += 1;
      subgrid = tree->GetNextSubgrid(false, timestamp);
    }
    return count;
  }
};

TEST_F(QuadTreeGridRequestTest, test_send_prioritized_first) {
  // Test that prioritized coordinate gets sent before older / bigger subgrids.

  int timestamp = BuildDiagonalTree(tree.get(), 32);
  std::list<GridRequest> requests;
  GridRequest request;
  request.cx = 30;
  request.cy = 30;
  request.level = 2;
  request.timestamp = 0;  // No data has been received yet...
  requests.push_back(request);
  tree->AddPriorityRequests(requests);

  // Request any size subgrid, at a time after the last point was added.
  // Later than any points have been added to the tree.
  auto subgrid = tree->GetNextSubgrid(false, timestamp);
  // otherwise, would have sent 2 level 4's & 4 level 3's first ...
  ASSERT_EQ(2, subgrid->level);
  ASSERT_LE(std::abs(request.cx - subgrid->xx), 2);
  ASSERT_LE(std::abs(request.cy - subgrid->yy), 2);
  // This checks that the above worked even though the exact center wasn't given
  ASSERT_NE(request.cx, subgrid->xx);

  // There was only one prioritized grid, so next call should go back to
  // streaming
  subgrid = tree->GetNextSubgrid(false, timestamp);
  ASSERT_EQ(4, subgrid->level);
}

TEST_F(QuadTreeGridRequestTest, test_dont_send_empty_prioritized) {
  // Test that a prioritized coordinate w/in the tree bounding box but in a
  // blank region of the map doesn't get sent as empty grid.
  BuildDiagonalTree(tree.get(), 32);
  ASSERT_EQ(6, tree->num_levels());

  // Make sure that all subgrids have been streamed.
  int32_t timestamp = 32 * 32 + 1;
  auto num_streamed = GetAllSubgrids(timestamp);

  // Request prioritized subgrid in empty region of map.
  {
    std::list<GridRequest> requests;
    GridRequest request;
    request.cx = 30;
    request.cy = 0;
    request.level = 2;
    request.timestamp = 0;  // No data has been received yet...
    requests.push_back(request);
    tree->AddPriorityRequests(requests);
  }

  // And, we should NOT get an empty subgrid!
  auto subgrid = tree->GetNextSubgrid(false, 32 * 32 + 1);
  ASSERT_TRUE(subgrid == nullptr);

  // And, test with one on non-empty region...
  {
    std::list<GridRequest> requests;
    GridRequest request;
    request.cx = 15;
    request.cy = 15;
    request.level = 2;
    request.timestamp = 0;  // No data has been received yet...
    requests.push_back(request);
    tree->AddPriorityRequests(requests);
  }

  // And, we should NOT get an empty subgrid!
  subgrid = tree->GetNextSubgrid(false, 32 * 32 + 1);
  ASSERT_EQ(2, subgrid->level);
}

TEST_F(QuadTreeGridRequestTest, test_dont_send_stale_prioritized) {
  // Test that prioritized coordinate doesn't get sent if its stale based on the
  // request time.

  BuildDiagonalTree(tree.get(), 32);

  // This will be after all points were added
  int32_t timestamp = 32 * 32 + 1;

  std::list<GridRequest> requests;
  GridRequest request;
  request.cx = 30;
  request.cy = 30;
  request.level = 2;
  request.timestamp = timestamp;
  requests.push_back(request);
  tree->AddPriorityRequests(requests);

  // Requet any size subgrid, at a time after the last point was added.
  auto subgrid = tree->GetNextSubgrid(false, timestamp);
  ASSERT_EQ(4, subgrid->level);
}

TEST_F(QuadTreeGridRequestTest, test_dont_send_overlapping_prioritized) {
  // Checks requesting of dropped packets AND that
  BuildDiagonalTree(tree.get(), 32);

  // First, make sure whole tree has been sent at least once.
  int32_t timestamp = 32 * 32 + 1;
  int num_streamed = GetAllSubgrids(timestamp);
  ASSERT_EQ(14, num_streamed);  // 2 16x16 + 4 8x8 + 8 4x4

  // topside requests overlapping grids.
  std::list<GridRequest> requests;
  for (int ii = 0; ii < 32; ii++) {
    GridRequest request;
    request.cx = ii;
    request.cy = ii;
    request.level = 2;
    request.timestamp = 0;
    requests.push_back(request);
  }
  tree->AddPriorityRequests(requests);

  // We should only get 8 4x4 grids
  auto subgrid = tree->GetNextSubgrid(false, timestamp);
  int priority_count = 0;
  while (subgrid != nullptr) {
    ASSERT_EQ(2, subgrid->level);
    priority_count += 1;
    timestamp += 1;
    subgrid = tree->GetNextSubgrid(false, timestamp);
  }
  ASSERT_EQ(8, priority_count);
}

TEST_F(QuadTreeGridRequestTest, test_clear_prioritized) {
  // Test clearing of prioritized coordinates

  // If just streaming, this will send 2 16x16 + 4 8x8 + 8 4x4
  BuildDiagonalTree(tree.get(), 32);

  // First, make sure whole tree has been sent at least once.
  int32_t timestamp = 32 * 32 + 1;
  auto subgrid = tree->GetNextSubgrid(false, timestamp);
  ASSERT_EQ(4, subgrid->level);

  // topside requests overlapping grids.
  std::list<GridRequest> requests;
  for (int ii = 0; ii < 32; ii++) {
    GridRequest request;
    request.cx = ii;
    request.cy = ii;
    request.level = 2;
    request.timestamp = 0;
    requests.push_back(request);
  }
  tree->AddPriorityRequests(requests);

  // Check that the priority requests actually got through ...
  subgrid = tree->GetNextSubgrid(false, timestamp);
  ASSERT_EQ(2, subgrid->level);

  // Check that clearing the priorities reverts to sending the next 16x16 grid
  tree->ClearPriorities();
  subgrid = tree->GetNextSubgrid(false, timestamp);
  ASSERT_EQ(4, subgrid->level);
}

TEST_F(QuadTreeGridRequestTest, test_prioritized_level_out_of_range) {
  // Test that requesting a level that's too big/small for the tree is ignored
  // (It's OK if the level is outside the streaming bounds, but we it has to be
  // less than the tree height and greater than the grid size)

  BuildDiagonalTree(tree.get(), 32);
  int32_t timestamp = 32 * 32 + 1;
  GetAllSubgrids(timestamp);
  ASSERT_EQ(6, tree->num_levels());

  timestamp += 1;
  auto subgrid = tree->GetNextSubgrid(false, timestamp);
  ASSERT_TRUE(subgrid == nullptr);

  // Try sending with level 1; should be too short
  GridRequest request1(5, 5, 1, 0);
  std::list<GridRequest> requests1{request1};
  tree->AddPriorityRequests(requests1);

  timestamp += 1;
  subgrid = tree->GetNextSubgrid(false, timestamp);
  ASSERT_TRUE(subgrid == nullptr);

  // Now, with level 6 (should be too big)
  GridRequest request2(5, 5, 6, 0);
  std::list<GridRequest> requests2{request2};
  tree->AddPriorityRequests(requests2);

  timestamp += 1;
  subgrid = tree->GetNextSubgrid(false, timestamp);
  ASSERT_TRUE(subgrid == nullptr);
}

/////////////////////////////////////////

class QuadTreeReceiverTest : public ::testing::Test {
 protected:
  std::unique_ptr<quadtree::QuadTreeSender> sender;
  std::unique_ptr<quadtree::QuadTreeReceiver> receiver;
  void SetUp() override {
    float horiz_resolution = 1.0;
    float vert_resolution = 1.0;
    int send_levels = 3;  // will be 4x4
    int min_level = 0;
    int max_level = 4;
    int32_t send_delay = 0;

    sender = std::make_unique<quadtree::QuadTreeSender>(
        horiz_resolution, vert_resolution, send_levels, min_level, max_level, send_delay);
    receiver = std::make_unique<quadtree::QuadTreeReceiver>(
        horiz_resolution, vert_resolution, send_levels);
  }
};

TEST_F(QuadTreeReceiverTest, test_receiver_grow_tree) {
  // Explicitly tests that the receiver quadtree's topology matches what
  // is specified in the subgrid.

  // level-3 subgrid, centered at 8, 8. Will be used as root.
  Subgrid initial_subgrid(3, 8, 8, 3, 8, 8, 1);

  // Subgrids that are incompatible with the existing subgrid's root
  // Can't have new root lower than existing subgrid's root
  Subgrid invalid_subgrid0(2, 8, 8, 2, 1, 1, 2);
  // Can't have root at same level but with different coordinates
  Subgrid invalid_subgrid1(3, 7, 7, 3, 8, 8, 2);

  // These have invalid root coordinates
  Subgrid invalid_subgrid2(5, 18, 18, 3, 8, 8, 2);  // Need 16*n + 8 away

  // These all have valid roots, but invalid subgrid coordinates
  Subgrid invalid_subgrid3(5, 20, 20, 4, 10, 10, 2);  // Need 16*n + 8 away
  Subgrid invalid_subgrid4(5, 20, 20, 3, 6, 6, 2);  // Need 8*n + 4 away
  Subgrid invalid_subgrid5(5, 20, 20, 2, 8, 8, 2);  // Need 4*n + 2 away
  Subgrid invalid_subgrid6(5, 20, 20, 1, 6, 6, 2);  // Need 2*n + 1 away


  std::vector<Subgrid> invalid_subgrids = {invalid_subgrid0, invalid_subgrid1,
  					   invalid_subgrid2, invalid_subgrid3,
  					   invalid_subgrid4, invalid_subgrid5,
					   invalid_subgrid6};

  for (int ii=0; ii < pow(2, pow(2, receiver->send_levels()-1)); ii++) {
    initial_subgrid.data.push_back(0);
  }
  for (Subgrid& subgrid : invalid_subgrids) {
    for (int ii=0; ii < pow(2, pow(2, receiver->send_levels()-1)); ii++) {
      subgrid.data.push_back(0);
    }
  }

  receiver->AddSubgrid(initial_subgrid);
  for (auto subgrid : invalid_subgrids) {
    bool success = receiver->AddSubgrid(subgrid);
    ASSERT_FALSE(success);
  }

}

TEST_F(QuadTreeReceiverTest, test_receive_single_subgrid) {
  // simplest-possible receiver test -- build a 4x4 grid that should be sendable
  // in single update.
  auto timestamp = BuildDenseTree(sender.get(), 4);
  auto subgrid = sender->GetNextSubgrid(false, timestamp);
  timestamp += 1;
  ASSERT_TRUE(subgrid != nullptr);
  std::cout << "trying to update receiver with subgrid at " << subgrid->xx
            << ", " << subgrid->yy << " and at level = " << subgrid->level
            << std::endl;
  bool success = receiver->AddSubgrid(*subgrid.get());
  ASSERT_TRUE(success);
  ASSERT_EQ(3, receiver->num_levels());

  std::cout << "Trying to make grids ..." << std::endl;
  auto send_grid = sender->MakeGrid(0);
  auto recv_grid = receiver->MakeGrid(0);
  ASSERT_EQ(*send_grid.get(), *recv_grid.get());

  // That should have been the only update ...
  subgrid = sender->GetNextSubgrid(false, timestamp);
  ASSERT_TRUE(subgrid == nullptr);
}

TEST_F(QuadTreeReceiverTest, test_receive_grows_sideways) {
  // Test that the receive subtree grows sideways as needed, using priorities
  // to mess with which updates get sent.
  // TODO: this test is less relevant now that trees always grow up to sender root.
  auto timestamp = BuildDenseTree(sender.get(), 64);
  ASSERT_EQ(7, sender->num_levels());

  // Request updates at opposite sides of the tree's span.
  std::list<GridRequest> requests;
  // TODO: I've found a bug! calling (0, 0, 2, 0) here doesn't work because
  // it is treated as outside the tree's span. Despite the fact that a node
  // has been added at (0, 0). The bounds are forced to be ints, but based
  // on how I build the tree, the cell boundaries are at x.5 units
  requests.emplace_back(3, 3, 2, 0);
  requests.emplace_back(60, 60, 2, 0);
  sender->AddPriorityRequests(requests);

  auto subgrid = sender->GetNextSubgrid(false, timestamp);
  timestamp += 1;
  ASSERT_TRUE(subgrid != nullptr);
  ASSERT_EQ(2, subgrid->level);

  receiver->AddSubgrid(*subgrid.get());
  ASSERT_EQ(sender->num_levels(), receiver->num_levels());

  subgrid = sender->GetNextSubgrid(false, timestamp);
  timestamp += 1;
  ASSERT_TRUE(subgrid != nullptr);
  ASSERT_EQ(2, subgrid->level);

  receiver->AddSubgrid(*subgrid.get());
  ASSERT_EQ(7, receiver->num_levels());
}

TEST_F(QuadTreeReceiverTest, test_receive_grows_upwards) {
  // Test that the receive subtree grows up as needed (subgrid is above existing
  // grid), using priorities to manipulate which updates get sent when.
  // TODO: Maybe ditch this one as well? So long as there are equivalent tests
  //     for growing the sender tree...
  auto timestamp = BuildDenseTree(sender.get(), 64);
  ASSERT_EQ(7, sender->num_levels());

  // Request updates at opposite sides of the tree's span.
  std::list<GridRequest> requests;
  // TODO: I've found a bug! calling (0, 0, 2, 0) here doesn't work because
  // it is treated as outside the tree's span. Despite the fact that a node
  // has been added at (0, 0). The bounds are forced to be ints, but based
  // on how I build the tree, the cell boundaries are at x.5 units
  requests.emplace_back(32, 32, 2, 0);
  requests.emplace_back(32, 32, 5, 0);
  sender->AddPriorityRequests(requests);

  auto subgrid = sender->GetNextSubgrid(false, timestamp);
  timestamp += 1;
  ASSERT_TRUE(subgrid != nullptr);
  ASSERT_EQ(2, subgrid->level);

  receiver->AddSubgrid(*subgrid.get());
  ASSERT_EQ(sender->num_levels(), receiver->num_levels());

  subgrid = sender->GetNextSubgrid(false, timestamp);
  timestamp += 1;
  ASSERT_TRUE(subgrid != nullptr);
  ASSERT_EQ(5, subgrid->level);

  receiver->AddSubgrid(*subgrid.get());
  ASSERT_EQ(sender->num_levels(), receiver->num_levels());
}

TEST_F(QuadTreeReceiverTest, test_receive_identical_reconstruction) {
  // Test that the reconstructed grid matches the original one.
  // (This works b/c the minimum send level is 0, so full resolution gets
  // streamed)

  auto timestamp = BuildDenseTree(sender.get(), 8);

  auto subgrid = sender->GetNextSubgrid(false, timestamp);
  while (subgrid) {
    receiver->AddSubgrid(*subgrid.get());
    subgrid = sender->GetNextSubgrid(false, timestamp);
  }

  auto send_grid = sender->MakeGrid(0);
  auto recv_grid = receiver->MakeGrid(0);
  ASSERT_EQ(*send_grid.get(), *recv_grid.get());
}

TEST_F(QuadTreeReceiverTest, test_receive_handling_timestamps) {
  // Manually create subgrid updates, checking that they are only incorporated
  // when the timestamp is newer.
  int32_t timestamp1 = 1;
  // sending tree's level and coordinates can be same as the overall update
  int ll = 2;
  int xx = 0;
  int yy = 0;
  Subgrid subgrid1(ll, xx, yy, ll, xx, yy, timestamp1);
  subgrid1.data = std::vector<int>(16, 1);

  int32_t timestamp2 = 0;
  Subgrid subgrid2(ll, xx, yy, ll, xx, yy, timestamp2);
  subgrid2.data = std::vector<int>(16, 2);

  int32_t timestamp3 = 2;
  Subgrid subgrid3(ll, xx, yy, ll, xx, yy, timestamp3);
  subgrid3.data = std::vector<int>(16, 3);

  // First update should always have an effect ...
  receiver->AddSubgrid(subgrid1);
  ASSERT_EQ(3, receiver->num_levels());
  auto recv_grid = receiver->MakeGrid(0);
  ASSERT_EQ(std::vector<std::vector<int>>(4, std::vector<int>(4, 1)),
            *recv_grid.get());

  // Second one should be ignored b/c it is stale
  receiver->AddSubgrid(subgrid2);
  recv_grid = receiver->MakeGrid(0);
  ASSERT_EQ(std::vector<std::vector<int>>(4, std::vector<int>(4, 1)),
            *recv_grid.get());

  // Third one should be an update, because it's newer.
  receiver->AddSubgrid(subgrid3);
  recv_grid = receiver->MakeGrid(0);
  ASSERT_EQ(std::vector<std::vector<int>>(4, std::vector<int>(4, 3)),
            *recv_grid.get());
}

// TOOD: implement utility functions for requesting updates
// * all within region
// * "fill in" for whole known span of tree at given level
// * "neighbors" for given level for all descendants of non-null nodes at
// next-higher level

// MakeGrid works even if we haven't received level0 yet (add
// argument to them?)

////////////////// MISCELLANEOUS TO-TESTs //////////////////////////////

// Test that subgrid still sends even if its range is greater than uint16_t (It
// should be shifted and then truncated....) Worst case would be overflow :-(

// Test handling of timestamps  in GetNextSubgrid: should handle time older than
// updates, anad newer than updates

// Test that a small subgrid with no-data cells serializes properly to small
// (I ran into this bug in the sender, but just fixed it)

// Test that subgrids are ordered by level when the whole map has the same
// timestamp
// Test that a higher level (but less stale) subgrid will be sent first)

}  // namespace quadtree

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
