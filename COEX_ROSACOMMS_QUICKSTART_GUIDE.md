# COEX_ROSACOMMS_LAUNCH_NOTES

**A coex "Quick Start" guide for running the various coex utilities (scalar transfer, progressive imagery, quadtree) both on the bench and in the water. If you just want to run things without making any config changes, see the commands section below. Read on for more info about launch and what actually gets called and where to make changes.**

## The Holy Grail Commands

### Simulation Mode

#### Quadtree
To run quadtree utility in simulation mode: 

Subsea:

- roslaunch src/coexploration/launch/coex.launch rosacomms_sim:=true run_subsea:=true run_quadtree:=true use_replay_bagfile:=true

- rosbag play --clock --rate=12 src/coexploration/launch/kongsberg_2019-10-13-06-30-25_6.bag src/coexploration/launch/tf_2019-10-13-06-30-26_6.bag 

Topside:

- roslaunch src/coexploration/launch/coex.launch rosacomms_sim:=true run_topside:=true run_quadtree:=true

#### Progressive Imagery
To run progressive imagery in simulation mode:

Subsea:

- roslaunch src/coexploration/launch/coex.launch rosacomms_sim:=true run_subsea:=true run_progressive_imagery:=true

Topside:

- roslaunch src/coexploration/launch/coex.launch rosacomms_sim:=true run_topside:=true run_progressive_imagery:=true

#### Scalar Data

Subsea:

- roslaunch src/coexploration/launch/coex.launch rosacomms_sim:=true run_subsea:=true run_scalar_transfer:=true use_replay_bagfile:=true

- rosbag play --clock -r 10 -s 600 src/coexploration/launch/sensors_2019-12-21-07-19-47_6.bag --topics /sentry/sensors/orp/a2d2 /sentry/sensors/paro/depth /sentry/sensors/obs/a2d2 /sentry/sensors/sbe49_ctd/ctd

Topside:

- roslaunch src/coexploration/launch/coex.launch rosacomms_sim:=true run_topside:=true run_scalar_transfer:=true

### Launch Mode

Launch mode commands are very similar to the above except:

- `rosacomms_sim` arg gets replaced by `rosacomms_launch`
- The `use_replay_bagfile` arg should not be used at all

## High Level Structure
The main coex repository serves primarily as a launch point for the different coex utilities. It also houses the comms_manager package which allows coex to operate on a computing system not connected to the uModem (i.e. the Sentry datapod). All launching, variables, and logging at the top level is handled in the `coex.launch` file in this repo. Depending on the launch args chosen, `coex.launch` will then load the corresponding node/xml file for each utility. 

The args for the current coex utilities are:

- `run_scalar_transfer`: This corresponds to a group of Scalar Receiver/Sender nodes that are defined in the main `coex.launch` file

- `run_progressive_imagery`: This will open the `progressive_imagery.xml` file in the same launch directory as the main `coex.launch` file. It also sets some argument values required by progressive_imagery.

- `run_quadtree`: This will open the `quadtree.xml` file in the same launch directory as the main `coex.launch` file. It also sets some argument values required by quadtree.

The other important args set in the `coex.launch` file are whether you wish to run the topside or subsea versions of the nodes and whether you are running ros_acomms in sim or with modem hardware. These args are:

- `run_topside` and `run_subsea`

- `rosacomms_sim` and `rosacomms_launch`

The topside and subsea args will set appropriate args for the corresponding coex utilities and/or only launch the topside or subsea specific nodes for that utility. For instance, the `run_subsea` arg will launch the scalar sender nodes while the `run_topside` arg will launch the scalar receiver nodes. The ros_acomms args determine whether to load the `modem_xm_test` xml files with a `modem_sim_node` or an `acomms_driver_node`. The `acomms_driver_node` includes Rosparams for connecting to modem hardware while the sim node spins up the ros_acomms modem sim environment and is useful for bench testing. These can be further configured in the same launch directory as `coex.launch`, the files are:

- Sim:
    - `modem_xm_test_sentry_subsea_sim.xml`
    
    - `modem_xm_test_sentry_topside_sim.xml`

- Launch:

    - `modem_xm_test_sentry_subsea.xml`

    - `modem_xm_test_sentry_topside.xml`

### ROS_ACOMMS Launch

#### Sim
Ros_acomms launches the following nodes for simulation mode:

- modem_sim_node

- tdma

- packet_dispatch

- message_queue_node

- sim_packet_performance_node

- platform_location_node

- sim_transmission_loss_node

#### Launch
Ros_acomms launches the following nodes for launch mode:

- acomms_driver_node

- tdma

- packet_dispatch

- message_queue_node

The rosparams are very similar across launch and sim with the exception of the hardware connection and config params in the acomms_driver_node. The codec config is the same for sim and launch and can be found in `comms_bridge/config/sentry_{topside|subsea}_codec_config.yaml`. Codec configs define topics, ROS message types, and fields for encoding and transmitting.

### Scalar Transfer Launch
Scalar transfer launch is handled directly in `coex.launch` and consists of Sender nodes for subsea and receiver nodes + a GUI for topside. There is also a "dorp_detector" node defined and launched in `coex.launch`. Scalar transfer nodes are located in the `scalar_data` package while the dorp detector is in the `dorp_detector` package. Message codec configs can be found in the ros_acomms codec config setup (see ros_acomms section).

### Quadtree Launch
Quadtree launch is mostly handled in `quadtree.xml` in the same top-level launch directory of the coex repo. The `run_subsea` arg will launch the "map_sender" node and the `run_topside` arg will launch the "map_receiver" and "map_gui" nodes when passed. Message codec configs can be found in the ros_acomms codec config.

### Progressive Imagery Launch
Progressive imagery launch is mostly handled in `progressive_imagery.xml` in the top-level launch directory. The `run_subsea` arg will launch the "pi_sender" node and the `run_topside` arg will launch the "pi_receiver" and "pi_gui" nodes. Message codec configs can be found in the ros_acomms codec config.

## Branches
Due to active development in repos used by other projects, there are a number of separate branches required for coex as of 2021-Resing. They are:

coexploration: dev/noetic

ros_acomms: dev/sentryacomms

progressive_imagery: dev/sentryacomms

ds_navg: dev/navg_coex_plugin

### NavG3 Integration
navg3-git: dev/navg_coex_plugin

