#! /usr/bin/env python3
"""
Simple command-line script that takes a human-readable time and publishes
it as a std_msgs/Time message.
"""

import os
os.environ['TZ'] = 'UTC'
import rospy
import time

import std_msgs.msg

time_format = "%Y/%m/%d %H:%M:%S"

def get_posix(timestring):
    try:
        timestamp = time.strptime(timestring, time_format)
        tt = time.mktime(timestamp)
        return tt
    except:
        return None

def main():
    pub = rospy.Publisher("requested_image_time", std_msgs.msg.Time, queue_size=1)

    while not rospy.is_shutdown():
        resp = input("Please enter desired timestamp (yyyy/mm/dd hh:mm:ss): ")
        print("got {}".format(resp))

        posix = get_posix(resp)
        if posix is None:
            print("Could not convert input time: {}".format(resp))
            continue

        send = input("Send image request for {} -> {}? (y/n)".format(resp, posix))
        if send.upper() == "Y":
            msg = std_msgs.msg.Time()
            msg.data.secs = int(posix)
            pub.publish(msg)
            print("published!")
        else:
            print("not publishing")


if __name__ == "__main__":
    rospy.init_node("topside_image_requester")
    main()
