/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Jan 24, 2020.
//
#pragma once

#include <quadtree/quadtree.h>
#include <quadtree/quadtree_receiver.h>
#include <quadtree/quadtree_ros.h>
#include <quadtree/quadtree_serialization.h>

// NB: starting in c++17, we have std::filesystem::create_directories
//     However, for now, we're running on 16.04 ... with max c++14.
#include <boost/filesystem.hpp>  // for create_directories

#include <queue>

#include "coexploration_msgs/QuadtreeMetadata.h"
#include "coexploration_msgs/QuadtreeRegion.h"
#include "ds_acomms_msgs/ModemData.h"
#include "ds_acomms_msgs/ModemDataRequest.h"
#include "std_msgs/Empty.h"

#include <ros/ros.h>


namespace quadtree {

class QuadTreeReceiverNode {
  friend class QuadTreeReceiverNodeTest_test_request_grids_Test;
 public:
  explicit QuadTreeReceiverNode();
  ~QuadTreeReceiverNode();

 protected:
  void setupParameters();
  void setupPublishers();
  void setupSubscriptions();
  void setupServices();
  void setupTimers();
  void setup();

 private:

  void handleMetadata(const coexploration_msgs::QuadtreeMetadata& msg);
  void handleSubgrid(const ds_acomms_msgs::ModemData& msg);
  void saveGrids(const ros::TimerEvent&);
  void handleRegionRequest(const coexploration_msgs::QuadtreeRegion& msg);

  bool dataRequestCallback(
      const ds_acomms_msgs::ModemDataRequest::Request& request,
      ds_acomms_msgs::ModemDataRequest::Response& response);

  // Whether the first metadata message has been received.
  bool initialized_;

  std::unique_ptr<quadtree::QuadTreeReceiver> tree_;

  std::queue<quadtree::GridRequest> requests_;

  // Parameters controlling the quadtree...
  // Side length for level 0 cells
  float horizontal_resolution_;
  // Vertical resolution for quadtree (it is transmitted in ints, so this scales it)
  float vertical_resolution_;
  // How many levels to send in a subgrid; subgrid will have sides of
  // length pow(2, send_levels_-1)
  int send_levels_;

  // Where to save grids to
  std::string grid_directory_;

  ros::Subscriber metadata_sub_;
  ros::Subscriber subgrid_sub_;
  ros::Subscriber save_grids_sub_;
  ros::Subscriber region_sub_;
  // Timer started when grids are updated, leading to saving them N seconds later.
  // The save operation is relatively slow, so we don't want to do it unnecessarily.
  ros::Timer save_grids_timer_;

  ros::ServiceServer data_server_;

  ros::Publisher saved_grids_pub_;
};

}  // namespace quadtree
