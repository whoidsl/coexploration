## Quickstart

In 3 terminal windows:

* `roscore`
* `roslaunch scalar_data scalar_data.launch`
* `rosbag play --clock -r 10 -s 600 sensors_2019-12-21-07-19-47_6.bag`

The provided bagfile has some actual dORP peaks ~ 10 minutes in, and can be downloaded [here](https://bitbucket.org/whoidsl/coexploration/downloads/).

To request ORP data around a reported peak:

* select the "Time Selection Active" checkbox
* left-click and drag in the time series window to select a time period
* Enter requested data rate in text edit box, and check that you're requesting a reasonable number of points.
* Click "Send"


## Overview

### Sender

The sender node runs subsea and keeps the entire history in memory.
16 bytes are required for each entry (a python float for the timestamp and the data). 
For a topic published at 10 Hz, this comes to 576 kB/hour or 13.8 MB/day.

It listens to `coexploration_msgs/ScalarDataRequest` messages, and on receipt of a request will immediately publish the corresponding message(s).
There are two cases in which more than one message will be sent:

1. The returned `ScalarData` message format assumes uniformly spaced data; if there is a gap in the raw data, scalar_sender.py will detect that gap and split the reply into continuous constant-rate segments.
2. There is an optional `max_length` argument that can be used to limit the maximum size of the generated messages. This is useful if you do not want to enable message fragmentation in ros_acomms.

### Receiver

The receiver runs topside and aggregates all data that has been sent across the acoustic link.
This simple node only exists to make it possible to restart the GUI without losing your entire history.

### GUI

This GUI included in this package is meant to serve as a proof of concept providing minimal functionality.
NavG3 will provide a more complete user interface that includes both map view and time-view of the received data.



## Unit Testing

For development, it is easiest to invoke them manualy: `nosetests -s -v tests`

Huh. I started having issues with my nosetests.
`nosetests -s -v tests` can't find numpy
However
`'which nosetests' -s -v tests` worked just fine.

I also think that the relative imports are ugly.
In order to get them to work, I have __init__.py files in the root directory and in nodes/. 
I don't think this is ideal -- the actual pattern is probably to pull library functions into a module and have a setup.py.


### Coverage testing

I did not attempt to automate testing of all the ROS interfaces, so coverage
is not 100%. Tests exist for functions where TDD was a useful way to
actually do the development.

`pip install coverage`

`coverage run --source=. -m nose -s -v tests`

Then, to see the results, use either:

* `coverage report` : only gives summary
* `coverage html` : generates website with line-by-line annotations. View it using `firefox htmlcov/index.html`


### Catkin integration

The tests will also run with: `catkin build scalar_data --make-args test`.

`catkin_test_results` will show a summary.

(At this point, trying to build & run ALL tests in a coexploration workspace will yield failures from other packages.)

