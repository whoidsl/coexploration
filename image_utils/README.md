## Overview

This package contains utilities for acoustically transferring images of interest.

There are two major subcomponents to this:

* Sunshine is used to identify which images might be interesting
* Progressive Imagery is the library used for transferring images

The progressive_imagery library's documentation can be found [in its repository](https://www.bitbucket.org/whoidsl/progressive_imagery); 
the short version is that it encodes images using the jpeg2000 codec, and sends chunks up sequentially. 
As more chunks are received topside, the image can be reconstructed at higher and higher resolution.

By default, all images placed in the to-be-transmited directory (controlled by the `common/img_dir` parameter for the sender node) will be transmitted in the order of their ID.
Topside, images are placed in the receive directory (`common/img_dir` param for receiver node), and all incremental images are saved.

The first pass gets every to-be-transmitted image to 20% complete, and the second pass to 100%.
The user can add additional goals, prioritizing interesting images or deprioritizing ones that are unimportant. 
Any user-added goals will be prepended to the current list.

In preliminary tests on Sentry with dropped packets, it typically took ~30 minutes for an image to be fully transferred.
NB: These percentages aren't of full resolution on disk -- Sentry saves ~20+ MB bayer-encoded tiffs, while the fully transferred
image is greyscale and has been lossily compressed using JPEG2000. The resulting PNG files are <1 MB.

For an example of the progression in image quality, see these images: 
[1%](design_docs/img5_01pct.png), [7%](design_docs/img5_07pct.png), [14%](design_docs/img5_14pct.png), 
[24%](design_docs/img5_24pct.png), [50%](design_docs/img5_50pct.png), [99%](design_docs/img5_99pct.png).  
([Bitbucket doesn't support resizing images in its markdown](https://community.atlassian.com/t5/Bitbucket-questions/What-HTML-does-BitBucket-support-in-their-READMEs/qaq-p/589154), 
so I couldn't cleanly embed them here.)

![Alt text](design_docs/progressive_imagery.png)

The above figure shows the communications between different components that are required to use progresive imagery within the coexploration ecosystem.


## Components (implemented in this package)

* **progressive_imagery_gui.py** -- topside GUI for interacting with progressive imagery. Provides visualization of the images that have been received and controls for prioritizing new images.

* **sentry_photoproc.py** -- single node running on sentry that will query images available on datapod and send one closest to requested timestamp. 
  It is complete and tested on Sentry during 2019-valentine,
  This node remotely calls `ls` on the relevant datapod directory, copies over the image closest to the timestamp that it receives, 
  and then debayers and color corrects it before placing it in the transmit directory. This is not ideal, since the `ls` has been observed
  to overload the old datapod and cause a camera frame to be skipped. 
  Some implementation details should probably change with the new camera driver and datapod, since all nodes will be run on the datapod.

* **topside_image_requester.py** -- command line program to request images based on timestamp. 
  Currently requires user to type formatted input into stdin. The final version of this should probably be integrated into 
  the GUI for Sunshine, since that data will be used to decide which images to send.

* **datapod_notifier.py** -- example script for using watchdog on datapod and sending all new filenames to dpa. 
  Not currently used on sentry because the datapod is running 12.04... instead, photoproc calls `ls` remotely.
  
`sentry_photoproc.py` and `topside_image_requester.py` are not included in the coexploration project's [example launch file](../launch/README.md).
For example usage, see the [relevant section from Sentry's launch files](https://bitbucket.org/whoidsl/sentry_config/src/master/config/coexploration/launch.xml).

## Usage

See the [launch README](../launch/README.md) for how to actually launch the coexploration infrastructure with progresive imagery.

![Alt text](design_docs/progressive_imagery_gui.png)

This is the GUI for controlling progressive imagery from the topside.

* The top pane shows thumbnails for all images that have been received. Click the thumbnail to get a larger pop-up, 
  at which point you can scroll through the history of images to see how much resolution was gained at each update.
* The bottom left pane shows the most recently received list of goals from the sender. Use this to confirm that 
  user-added priorities have been received.
* The bottom right pane lets the user add priorities. They are prepended to the existing list.
  (Fill in the ID and desired percentage, click "Add". Once you've finished updating the list, click "Publish")

## Progressive Imagery setup

For the most part, the progressive_imagery library just works.

I did have to change the max packet sizes that it creates, in order for it to fit in the micromodem's rate 5 messages with the comms_manager's header.
Currently, they are sized for a maximum micromodem frame size of 248 bytes.
This is a limitation due to the micromodem1 on Sentry -- presumably a firmware issue, since it should have been 256 bytes.

If this needs to be changed again (micromodem upgrade?), the sizes are determined by the .proto files; 
see [commit 0e83195](https://bitbucket.org/whoidsl/progressive_imagery/commits/0e83195070f73415c51250acb8f14e713c44bbca?w=1#chg-src/progressive_imagery/jpeg2000-image.proto) 
for an example of what to change.

## Sunshine integration

The overall vision for CoExploration involves using Sunshine's topic modeling to identify which images
are worth transmitting acoustically.

Stewart Jamieson, one of Yogi's grad students, has been helping out with modifying Sunshine to work with Sentry's data.
Their code is not publicly available -- contact them for access.

Other resources are available in the VAST drive, under PROJECTS/Co-exploration/Sunshine.

