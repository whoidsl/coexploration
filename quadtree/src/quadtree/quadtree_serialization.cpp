/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Jan 4, 2020
//

#include "quadtree/quadtree_serialization.h"

bool serialize32(int value, std::vector<uint8_t>* data) {
  if (value < std::numeric_limits<int32_t>::min() ||
      value > std::numeric_limits<int32_t>::max()) {
    return false;
  }
  data->push_back((uint8_t)((value >> 24) & 0xff));
  data->push_back((uint8_t)((value >> 16) & 0xff));
  data->push_back((uint8_t)((value >> 8) & 0xff));
  data->push_back((uint8_t)(value & 0xff));
  return true;
}

bool serialize16(int value, std::vector<uint8_t>* data) {
  if (value < std::numeric_limits<int16_t>::min() ||
      value > std::numeric_limits<int16_t>::max()) {
    return false;
  }
  data->push_back((uint8_t)((value >> 8) & 0xff));
  data->push_back((uint8_t)(value & 0xff));
  return true;
}

bool serialize16u(int value, std::vector<uint8_t>* data) {
  if (value < std::numeric_limits<uint16_t>::min() ||
      value > std::numeric_limits<uint16_t>::max()) {
    return false;
  }
  data->push_back((uint8_t)((value >> 8) & 0xff));
  data->push_back((uint8_t)(value & 0xff));
  return true;
}

bool serialize8(int value, std::vector<uint8_t>* data) {
  if (value < std::numeric_limits<int8_t>::min() ||
      value > std::numeric_limits<int8_t>::max()) {
    return false;
  }
  data->push_back((uint8_t)(value & 0xff));
  return true;
}

bool serialize8u(int value, std::vector<uint8_t>* data) {
  if (value < std::numeric_limits<uint8_t>::min() ||
      value > std::numeric_limits<uint8_t>::max()) {
    return false;
  }
  data->push_back((uint8_t)(value & 0xff));
  return true;
}

bool serializeRequest(const quadtree::GridRequest& request,
		      std::vector<uint8_t>* data) {
  // float cx, cy, int level, int32_t timestamp
  // I'm going to cheat here and round to an int ...
  if (!serialize32((int)std::round(request.cx), data)) {
    return false;
  }
  if (!serialize32((int)std::round(request.cy), data)) {
    return false;
  }
  if (!serialize8((int)request.level, data)) {
    return false;
  }
  if (!serialize32((int)request.timestamp, data)) {
    return false;
  }
  return true;
}

bool serializeSubgrid(const quadtree::Subgrid& subgrid,
                      std::vector<uint8_t>* data) {
  if (subgrid.level < 0 || subgrid.level > 127) {
    return false;
  }
  uint8_t level = subgrid.level;
  if (subgrid.IsSmall()) {
    level = level | (1 << 7);
  }
  data->push_back(level);

  if (!serialize8(subgrid.tree_level, data)) {
    return false;
  }
  if (!serialize16(subgrid.tree_xx, data)) {
    return false;
  }
  if (!serialize16(subgrid.tree_yy, data)) {
    return false;
  }

  uint32_t ts = subgrid.timestamp;
  if (!serialize32(ts, data)) {
    return false;
  }
  int offset = subgrid.GetOffset();
  if (!serialize16(offset, data)) {
    return false;
  }

  if (!serialize16(subgrid.xx, data)) {
    return false;
  }

  if (!serialize16(subgrid.yy, data)) {
    return false;
  }

  bool success;
  for (int zz : subgrid.data) {
    if (subgrid.IsSmall()) {
      if (zz == std::numeric_limits<int>::max()) {
        success = serialize8u(std::numeric_limits<uint8_t>::max(), data);
      } else {
        success = serialize8u(zz - offset, data);
      }
    } else {
      if (zz == std::numeric_limits<int>::max()) {
        success = serialize16u(std::numeric_limits<uint16_t>::max(), data);
      } else {
	success = serialize16u(zz - offset, data);
      }
    }
    if (!success) {
      break;
    }
  }
  return success;
}

int32_t deserialize32(const std::vector<uint8_t>& data, int* index) {
  int32_t value =
      (int32_t)((data.at(*index) << 24) + (data.at(*index + 1) << 16) +
                (data.at(*index + 2) << 8) + data.at(*index + 3));
  *index += 4;
  return value;
}

int16_t deserialize16(const std::vector<uint8_t>& data, int* index) {
  int16_t value = (int16_t)((data.at(*index) << 8) + (data.at(*index + 1)));
  *index += 2;
  return value;
}

uint16_t deserialize16u(const std::vector<uint8_t>& data, int* index) {
  uint16_t value = (uint16_t)((data.at(*index) << 8) + (data.at(*index + 1)));
  *index += 2;
  return value;
}

int8_t deserialize8(const std::vector<uint8_t>& data, int* index) {
  int8_t value = (int8_t)data.at(*index);
  *index += 1;
  return value;
}

uint8_t deserialize8u(const std::vector<uint8_t>& data, int* index) {
  uint8_t value = data.at(*index);
  *index += 1;
  return value;
}

bool deserializeRequests(const std::vector<uint8_t>& data,
			std::list<quadtree::GridRequest>* requests) {
  int index = 0;
  int num_requests = deserialize8(data, &index);
  for (int ii = 0; ii < num_requests; ii++) {
    int xx = deserialize32(data, &index);
    int yy = deserialize32(data, &index);
    int level = deserialize8(data, &index);
    int ts = deserialize32(data, &index);
    requests->emplace_back(xx, yy, level, ts);
  }
  return true;
}

bool deserializeSubgrid(const std::vector<uint8_t>& data,
                        quadtree::Subgrid* subgrid) {
  // TODO: assert data length is correct?
  bool small = (1 == data.at(0) >> 7);
  subgrid->level = (int)data.at(0) & 0x7f;

  int index = 1;
  subgrid->tree_level = deserialize8(data, &index);
  subgrid->tree_xx = deserialize16(data, &index);
  subgrid->tree_yy = deserialize16(data, &index);
  subgrid->timestamp = deserialize32(data, &index);
  int offset = deserialize16(data, &index);
  subgrid->xx = deserialize16(data, &index);
  subgrid->yy = deserialize16(data, &index);

  while (index < data.size()) {
    if (small) {
      int value = deserialize8u(data, &index);
      if (value == 255) {
	subgrid->data.push_back(std::numeric_limits<int>::max());
      } else {
	subgrid->data.push_back(offset + value);
      }
    } else {
      int value = deserialize16u(data, &index);
      if (value == std::numeric_limits<uint16_t>::max()) {
	subgrid->data.push_back(std::numeric_limits<int>::max());
      } else {
	subgrid->data.push_back(offset + value);
      }
    }
  }
  // TODO: Add any sort of failure handling here? (What happens if data length is wrong and we call deserialize16 with only one element left?)

  return true;
}
