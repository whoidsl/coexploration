#! /usr/bin/env python2.7

'''
For testing, I want to be able to run the unittests by hand using only unittest
and as part of `catkin_make run_tests`, which requires integration into
rostest/rosunit.

It looks like this is usually used for integration-level testing where a roscore
is running and we're testing the API. However, I *think* rosunit may be designed
for roscore-less testing ...

I can't have a command-line argument that chooses which to
run because rostest depends on using the command line to give the output path
for the results file.

So, instead, I've done a separate unittest_qm_ros.py

If I iterate through all the ros tests, adding them using rostest.rosrun,
it works on the command line, but when called through catkin_make,
only the last test's results are printed.

I tried to create a test suite to address that, but that failed because it
apparently expects my test cases to have a runTest method, when instead they
all have a handful of test_* methods. It seems weird that single test cases
work fine but suites don't.

So, I guess my question boils down to:
How do I get my unittest test results to be reported in catkin_make run_tests
without having to enumerate by hand every Test*(unittest.TestCase) class and
every one of its component tests?
=> using TestSuite.addTest(unittest.makeSuite(Test<blah>)) works for discovering
   individual tests without runTest / enumerating every test_<blah> function.
=> I still haven't figured out how to auto-discover TestCases for the test suite.





'''


import rostest
import rosunit
import unittest

import unittest_qm
if __name__=="__main__":
    PKG = "comms_manager"

    print dir(unittest_qm)

    # # This works on the command line, but when run with catkin_make it only
    # # reports results from the final test.
    # for dd in dir(unittest_qm):
    #     if dd.startswith("Test"):
    #         rostest.rosrun(PKG, 'unittest_comms_manager_'+dd, getattr(unittest_qm, dd))

    # For rostest vs rosunit, I think that rosunit is meant for unit tests that have no
    # ROS dependencies, and for which roscore pub/sub/param/etc. aren't used.
    # Example code shows using a test suite, but I haven't yet gotten it to behave :-\
    # Ugh. This seems to fail because it expects runTest() to be called. And, I rely
    # on how unittest auto-discovers all methods starting with test_*. Double ugh.
    #rosunit.unitrun(PKG, 'unittest_comms_manager', 'unittest_qm.UnittestCommsManager')
    rostest.rosrun(PKG, 'unittest_comms_manager', 'unittest_qm.UnittestCommsManager')

    #suite = unittest.TestSuite()
    #suite.addTest(unittest.makeSuite(unittest_qm.TestEncoding))
    #rostest.rosrun(PKG, 'asdf', 'suite')
